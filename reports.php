<?php
include_once('header.php');
include_once('dao/config/db.php');
require_once('dao/functions.php');
require_once('include/user_roles.php');
if (!isset($_SESSION["isuzu_user_id"])) {
	header('Location: /'.ROOT_FOLDER.'dao/login.php');
}

$userId = $_SESSION["isuzu_user_id"];
?>


<div class="container-fluid primary-color">
    <div class="row">
        <div class="col-md-12 col-sm-12" style="padding:0;">
            <div class="col-main">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-2" style="padding:0;">
                            <?php 
                                
                                if (isset($_SESSION['isuzu_dealer'])) {
                                    $dealer = dealer($_SESSION['isuzu_dealer']);
                                    echo "<p style='color:red;font-weight:800;'>Welcome <span style=''>".$dealer['name']."</span></p>";
                                }
                            ?>

                            <?php if ($agent == '1' || $dealer_r == '1'): ?>
                            <div class="row">
                                <div class="col-md-12 my-1">
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" placeholder="Client" aria-label="Recipient's username" aria-describedby="basic-addon2" id="searchPhrase">
                                        <div class="input-group-append">
                                            <span class="btn btn-secondary" id="basic-addon2" onclick="searchCustomer()">Search</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endif ?>
                            <div class="left-nav-section">
                                <?php include_once('include/side_content.php'); ?>
                            </div>
                        </div>
                        <div class="col-md-10" style="padding:0;">
                            <div id="responseSection" class="response-section">
                                <?php
                                    if (isset($_GET['response'])) {
                                        echo "<p>".$_GET['response']."</p>";
                                    }
                                ?>
                            </div>
                                <div class="dash-col-main">
                                    <div id="reportDashboard"></div>
                                </div>
                        </div>	
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include_once('footer.php');
?>


<script>
$( "#reportDashboard a" ).click(function() {
  stopFunction();
});
loadStats();
//var lodingTime = setInterval(loadTimer, 50000);

/*function loadTimer() {
    loadStats();
}*/

function stopFunction() {
    clearInterval(lodingTime);
}



function loadStats (){
    var request = $.ajax({
    url: 'reports/index.php',
    data: '',
    dataType: 'html',
    });
    request.done(function( data ) {
      $( "#reportDashboard" ).html(data);
    });
}
</script>