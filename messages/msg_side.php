
<?php
if (isset($_GET['int'])) {
  $intId = $_GET['int'];
  $qinteraction = "SELECT (SELECT name FROM contact_type WHERE id = m.contact_type) contact_type, (SELECT cat_name FROM categories WHERE id = m.cat_id) cat_name, (SELECT sub_cat_name FROM sub_categories WHERE id = m.sub_cat_id) sub_cat_name, complain FROM messages m WHERE m.id = :msgId";
  $getInteraction = $con->prepare($qinteraction);
  $getInteraction->bindParam(':msgId', $intId, PDO::PARAM_STR);
  $getInteraction->execute();
  $interaction = $getInteraction->fetch();    
}

?>
<ul class="list-group">
  <li class="list-group-item active">
   <strong>Customer Details</strong>
  </li>
<?php if (isset($_GET['customerId'])): ?>
<?php
  $customerId = $_GET['customerId'];
  $qcustomer = "SELECT u.first_name, u.last_name, u.email, i.company_name, i.telephone_no, i.business_type, i.physical_address, i.town FROM users u INNER JOIN customer_info i ON i.customer_id = u.id WHERE u.id = :customerId";
  $getCustomer = $con->prepare($qcustomer);
  $getCustomer->bindParam(':customerId', $customerId, PDO::PARAM_STR);
  $getCustomer->execute();
  $customer = $getCustomer->fetch();  
?>
  <li class="list-group-item">
   <div>
     <div style="float: left; width: 40%; font-size: 12px;"><strong>Name</strong></div>
     <div style="float: left; width: 60%; font-size: 12px;"><?php echo $customer['first_name'].' '.$customer['last_name'] ?></div>
   </div>
  </li>
  <li class="list-group-item">
   <div>
     <div style="float: left; width: 40%; font-size: 12px;"><strong>Company</strong></div>
     <div style="float: left; width: 60%; font-size: 12px;"><?php echo $customer['company_name']?></div>
   </div>
  </li>
  <li class="list-group-item">
  <div>
     <div style="float: left; width: 40%; font-size: 12px;"><strong>Address</strong></div>
     <div style="float: left; width: 60%; font-size: 12px;"><?php echo $customer['physical_address']?></div>
   </div>
  </li>
</ul>
<input type="hidden" name="" id="customerId" value="<?php echo $_GET['customerId']; ?>">
    <?php endif ?>
</ul>

<br>
   <ul class="list-group">
    <li class="list-group-item active">
     <strong>Interaction Details</strong>
    </li>
    <?php if (isset($_GET['int'])): ?>
    <li class="list-group-item">
    <div>
       <div style="float: left; width: 40%; font-size: 12px;"><strong>Contact Type</strong></div>
       <div style="float: left; width: 60%; font-size: 12px;"><?php echo $interaction['contact_type'];?></div>
    </div>
    </li>
    <li class="list-group-item">
    <div>
       <div style="float: left; width: 40%; font-size: 12px;"><strong>Category</strong></div>
       <div style="float: left; width: 60%; font-size: 12px;"><?php echo $interaction['cat_name'];?></div>
    </div>
    </li>
    <li class="list-group-item">
    <div>
       <div style="float: left; width: 40%; font-size: 12px;"><strong>Subcategory</strong></div>
       <div style="float: left; width: 60%; font-size: 12px;"><?php echo $interaction['sub_cat_name'];?></div>
    </div>
    </li>
    <li class="list-group-item">
    <div>
       <div style="float: left; width: 40%; font-size: 12px;"><strong>Complaint</strong></div>
       <div style="float: left; width: 60%; font-size: 12px;"><?php echo $interaction['complain'];?></div>
    </div>
    </li>
    <input type="hidden" name="" id="messageId" value="<?php echo $_GET['int']; ?>">
    <?php endif ?>
    </ul>