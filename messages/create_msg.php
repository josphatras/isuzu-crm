<?php
$contactTypes = allConTypes();
$categories = sel_Cat('msg');
$userName = $_SESSION["isuzu_user_name"];
// I'm India so my timezone is Asia/Calcutta
date_default_timezone_set('Africa/Nairobi');

// 24-hour format of an hour without leading zeros (0 through 23)
$Hour = date('G');

if ($Hour >= 5 && $Hour <= 11 ) {
	$greeting = "Good Morning";
}elseif($Hour >= 12 && $Hour <= 18){
	$greeting = "Good Afternoon";
}elseif( $Hour >= 19 || $Hour <= 4 ){
	$greeting = "Evening!";
}
?>
<div class="row">
	<div class="col-md-3">
        <?php include('msg_side.php');	?>	
	</div>
	<div class="col-md-9">  
        <?php
            if (isset($_GET['msg_item'])){
                $item = $_GET['msg_item'];
                if ($item == 'contact') {
                    include('forms/contact_details.php');
                }elseif ($item == 'customer') {
                    include('forms/customer_details.php');
                }elseif ($item == 'vehicle') {
                    include('forms/vehicle_details.php');
                }elseif ($item == 'message') {
                    include('forms/message.php');
                }elseif ($item == 'search') {
                    include('forms/customer_search.php');
                }elseif ($item == 'customer_profile') {
                    include('customers/profile.php');
                }elseif ($item == 'response') {
                    include('forms/response.php');
                }elseif ($item == 'closing') {
                    include('forms/contact_closing.php');
                }
            }else{

        ?>   
        <div id="formSection">
            <p>1.) <strong><?php echo $greeting; ?></strong>, thank you for calling ISUZU, my name is Jane <strong><?php echo $userName; ?></strong> how may I help you?</p>
            <p><strong>(Agent to proceed as per client query)</strong></p>

            <p>2.) Which Product or Service would you like information on?</p>
            <?php include('forms/contact_details.php'); ?>
        </div>
        <?php     
            }
        ?>
	</div>
</div>