<?php
include_once('dao/config/include.php');
session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<title>ISUZU CRM</title>
	<link rel="stylesheet" type="text/css" href="/<?php echo ROOT_FOLDER; ?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/<?php echo ROOT_FOLDER; ?>assets/css/bootstrap-grid.min.css">
	<link rel="stylesheet" type="text/css" href="/<?php echo ROOT_FOLDER; ?>assets/css/bootstrap-reboot.min.css">
	<link rel="stylesheet" type="text/css" href="/<?php echo ROOT_FOLDER; ?>assets/css/style-2.2.css">
	<link rel="stylesheet" href="/<?php echo ROOT_FOLDER; ?>assets/css/dataTables.bootstrap4.min.css" />
	<link rel="stylesheet" href="/<?php echo ROOT_FOLDER; ?>assets/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/<?php echo ROOT_FOLDER; ?>assets/css/jquery-ui.css">
</head>
<body>
<header class="main-header">
<div class="row">
        <div class="col-md-6">
                <div class="">
                        <div class="">
                            <a href="/<?php echo ROOT_FOLDER; ?>"><img src="/<?php echo IMAGES_FOLDER; ?>/logo.png"  class="img-responsive" width="150px"></a>
                        </div>
                </div>                
        </div>
        <div class="col-md-6">
                <div class="top-nav-list" style="float: right;">
                        <?php
                                if (isset($_SESSION['isuzu_user_id'])) {

                                ?>
                                        <ul>
                                                <li><span class="theme-color-3"><?php echo $_SESSION['isuzu_user_name'] ?></span></li>
                                               <li><a href="/<?php echo ROOT_FOLDER; ?>dashboard.php">Dashboard</a></li>
                                                <li><a href="/<?php echo ROOT_FOLDER; ?>dashboard.php?content=account"> Account</a></li>
                                                <li><a href="/<?php echo ROOT_FOLDER; ?>dao/logout.php"> Logout</a></li>
                                        </ul>
                                <?php
                                }else{
                                        echo "<p><a href=''><span class='theme-color-3'>Login</span></a> </p>";
                                }
                        ?>
                </div>                
        </div>
</div>
</header>	