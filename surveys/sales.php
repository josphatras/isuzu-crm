<?php
	$surveys = surveys('1', $fromDate, $toDate);
?>
<table border="1" class="table table-bordered table-striped table-sm isuzu-datatable" style="font-size: 0.8em;border-collapse: collapse;">
	<thead>
		<th> Dealer </th>
		<th> Branch </th>
		<th> Survey by </th>
		<th> Customer name </th>
		<th> Transaction type </th>
		<th> Reg No. </th>
		<th> vin no </th>
		<th> Successful survey? </th>
		<th> Reason </th>
		<th> Escalate? </th>
		<th> Q1 </th>
		<th> Q2 </th>
		<th> Q3 </th>
		<th> Q4 </th>
		<th> Q5 </th>
		<th> Q6 </th>
		<th> Other models? </th>
		<th> Which ones? </th>
		<th> Where do you service your vehicle </th>
		<th> Recommend a customer </th>
		<th> Voice of ustomer </th>
		<th> Done on </th>
	</thead>
	<tbody>
		<?php foreach ($surveys as $survey): ?>
		<?php
		$reasons = array('1' => "Busy", '2' => "Rejecting", '3' => "Wrong number", '4' => "No number", '5' => "Unavailable", '6' => "Pending");
		$reason = '';
		foreach ($reasons as $key => $value) {
			if ($key == $survey['reason']) {
				$reason = $value;
				break;
			}
		}
		
			$scores = surveyScore($survey['id']);
			$vehicle = vehicle($survey['vehicle_id']);
		?>
		<tr>
			<td><?php echo $survey['dealer_name'] ?></td>
			<td><?php echo $survey['branch_name'] ?></td>
			<td><?php echo $survey['user_name'] ?></td>
			<td><?php echo $survey['customer_name'] ?></td>
			<td><?php echo $survey['transaction'] ?></td>
			<td><?php echo $vehicle['reg_no'] ?></td>
			<td><?php echo $vehicle['vin_no'] ?></td>
			<td><?php echo $survey['successful'] ?></td>
			<td><?php echo $reason ?></td>
			<td><?php echo $survey['escalate'] ?></td>
			<?php
			for ($i=1; $i <= 10 ; $i++) { 
				$state = 0;
				foreach ($scores as $score) {
					if ($i == $score['qn_id']) {
						$state += 1;
						break;
					}else{

					}
				}

				if ($state == '1') {
					if ($i <= 6) {
						echo "<td>".$score['score']."</td>";
					}else{
						echo "<td>".$score['verbal']."</td>";
					}
					unset($state);
				}else{
					echo "<td></td>";
				}

			}
			
			?>
			<td><?php echo $survey['customer_voice'] ?></td>
			<td><?php echo $survey['created_on'] ?></td>
			<td><button class="btn btn-info" onclick="delSurveyPop('<?php echo $survey['id'] ?>')">Delete</button></td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>
<div class="modal fade" id="delete_record" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background: #dd4f43">
        <h5 class="modal-title" id="exampleModalLabel">Warning</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <p>Are you sure you want to delete the <strong><span id="contactName"></span></strong> survey?</p>
      </div>
      <div class="modal-footer">
        <div><button type="submit" class="btn btn-secondary" onclick="deleteSurvey()">Yes <span class="loading"></span></button></div>
        <button type="button" class="btn btn-info" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>
<input type="hidden" id="survey_id" value="">