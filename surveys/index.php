<?php
if (isset($_GET['from_date'])) {
	$fromDate = $_GET['from_date'];
}else{
	$fromDate = date('Y-m-d');
}

if (isset($_GET['to_date'])) {
	$toDate = $_GET['to_date'];
}else{
	$toDate = date('Y-m-d');
}

if (isset($_GET['type'])) {
	$type = $_GET['type'];
	$service = $_GET['type'];
}else{
	$type = '';
	$service = '';
}

?>

<div style="margin: 0 0 30px 0">
	<div class="row">
		<div class="col-md-4 col-sm-12 col-xm-12">
			<nav class="nav nav-pills nav-fill">
				<?php if (!isset($_SESSION['isuzu_dealer'])): ?>
			  <a class="nav-item nav-link <?php echo ($type == 'new_sales' || $type == 'new_services')? 'active' : ''; ?>" href="#">New Survey</a>
				<?php endif; ?>
			  <a class="nav-item nav-link <?php echo ($type == 'view_sales')? 'active' : ''; ?>" href="?content=surveys&type=view_sales">Sales</a>
			  <a class="nav-item nav-link <?php echo ($type == 'view_services')? 'active' : ''; ?>" href="?content=surveys&type=view_services">Service</a>
			</nav>		
		</div>
	</div>
</div>
<div style="overflow: auto; overflow-y: hidden; margin: 0 auto; white-space: nowrap">

	<?php if ($type == 'view_sales' || $type == 'view_services'): ?>
	<div style="border: 1px solid #ccc; padding: 20px; margin-bottom: 30px;">
		<div class="row">
			<div class="col-md-4">
				
			</div>
			<div class="col-md-8 col-sm-12 col-sm-12">
				<div class="row">									
					<div class="col-md-5">
					  <div class="input-group">
					    <div class="input-group-prepend">
					      <div class="input-group-text" id="btnGroupAddon">From</div>
					    </div>
					    <input type="text" class="form-control dateSelect" id="fromDate" placeholder="From Date" value="<?php echo $fromDate; ?>">
					  </div>								
					</div>
					<div class="col-md-5">
					  <div class="input-group">
					    <div class="input-group-prepend">
					      <div class="input-group-text" id="btnGroupAddon">To</div>
					    </div>
					    <input type="text" class="form-control dateSelect" id="toDate" placeholder="To Date" value="<?php echo $toDate; ?>">
						</div>								
					</div>
					<input type="hidden" id="service_id" value="<?php echo $service; ?>">
					<div class="col-md-2">
						<span class="btn btn-info" id="basic-addon2" onclick="surveySearch('view')">Search</span>
					</div>
				</div>			
			</div>
		</div>						
	</div>
	<?php endif; ?>
	<?php

	switch ($type) {
		case 'new_sales':
			include('surveys/sales/new_survey.php');
			break;
		case 'new_services':
			include('surveys/services/new_survey.php');
			break;
		case 'view_sales':
			include 'sales.php';
			break;
		case 'view_services':
			include 'service.php';
			break;
		
		default:
			# code...
			break;
	}
	?>
</div>