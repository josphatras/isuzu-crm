<?php
	$service = vehicleService($_GET['id']);
	$vehicle = vehicle($service['vehicle_id']);
	$customerDetail = sel_customer($vehicle['customer_id']);
	$int_categories = sel_Cat('msg');
	$esc_categories = sel_Cat('ticket');
?>
<div class="container" id="cnt1">
	<div class="col-md-12" id="panel1">
		<div class="card ">
			<div class="card-header">
				<h3 class="card-header">
				<span class="fa fa-question-circle"></span>   Service Survey</h3>
			</div>
			<!-- [vehicle_id]-->
			<table border="1" class="table table-bordered table-sm" style="font-size: 0.8em;border-collapse: collapse;">
				<tr>
					<td colspan="3"><strong>Dealer</strong></td>
				</tr>				
				<tr>
					<td><strong>Dealer:</strong> <?php echo $service['dealer_name']; ?></td>
					<td><strong>Branch:</strong> <?php echo $service['branch_name']; ?></td>
					<td><strong>Transaction:</strong> <?php echo $service['transaction']; ?></td>
				</tr>
				<tr>
					<td colspan="3"><strong>Customer</strong></td>
				</tr>
				<tr>
					<td><strong>Name:</strong> <?php echo $customerDetail['first_name'].' '.$customerDetail['last_name']; ?></td>
					<td><strong>Phone:</strong> <?php echo $customerDetail['telephone_no']; ?></td>
					<td><strong>Company:</strong> <?php echo $customerDetail['company_name']; ?></td>
				</tr>
				<tr>
					<td colspan="3"><strong>Driver details</strong></td>
				</tr>
				<tr>
					<td><strong>Name.:</strong> <?php echo $service['driver_name']; ?></td>
					<td><strong>Phone:</strong> <?php echo $service['driver_phone']; ?></td>
				</tr>
				<tr>
					<td colspan="3"><strong>Vehicle</strong></td>
				</tr>
				<tr>
					<td><strong>Reg No.:</strong> <?php echo $vehicle['reg_no']; ?></td>
					<td><strong>VIN:</strong> <?php echo $vehicle['vin_no']; ?></td>
				</tr>

			</table>
			<form id="survey" action="" method="POST">
			<div class="card-block two-col" style="padding: 20px;">
				<table class="table table-sm table-bordered table-hover" cellspacing="10" cellpadding="10">
					<tr>
						<td rowspan="2"><strong>No.</strong></td>
						<td rowspan="2" style="width: 70%"><h2>Survey Question</h2></td>
						<td colspan="4"><h2>Rating</h2></td>
					</tr>
					<tr style="text-align: center;">
						<td><strong>1</strong></td>
						<td><strong>8</strong></td>
						<td><strong>9</strong></td>
						<td><strong>10</strong></td>
					</tr>
					<tr>
						<td>1.</td>
						<td><h2>Having Convenient hours of service & location</h2></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q1" id="gridRadios1" value="1"></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q1" id="gridRadios1" value="8"></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q1" id="gridRadios1" value="9"></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q1" id="gridRadios1" value="10"></td>
					</tr>
					<tr>
						<td>2.</td>
						<td><h2>Cleanliness & comfortable of waiting area</h2></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q2" id="gridRadios1" value="1"></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q2" id="gridRadios1" value="8"></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q2" id="gridRadios1" value="9"></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q2" id="gridRadios1" value="10"></td>
					</tr>
					<tr>
						<td>3.</td>
						<td><h2>Notification on next Maintenance due</h2></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q3" id="gridRadios1" value="1"></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q3" id="gridRadios1" value="8"></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q3" id="gridRadios1" value="9"></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q3" id="gridRadios1" value="10"></td>
					</tr>
					<tr>
						<td>4.</td>
						<td><h2>Friendliness and helpfulness of service advisor</h2></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q4" id="gridRadios1" value="1"></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q4" id="gridRadios1" value="8"></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q4" id="gridRadios1" value="9"></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q4" id="gridRadios1" value="10"></td>
					</tr>
					<tr>
						<td>5.</td>
						<td><h2>Inspection of your vehicle and understood all your requests</h2></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q5" id="gridRadios1" value="1"></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q5" id="gridRadios1" value="8"></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q5" id="gridRadios1" value="9"></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q5" id="gridRadios1" value="10"></td>
					</tr>
					<tr>
						<td>6.</td>
						<td><h2>Explanation of work to be performed and estimation provided before start</h2></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q6" id="gridRadios1" value="1"></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q6" id="gridRadios1" value="8"></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q6" id="gridRadios1" value="9"></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q6" id="gridRadios1" value="10"></td>
					</tr>
					<tr>
						<td>7.</td>
						<td><h2>Explanation of work performed and breakdown of the charges</h2></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q7" id="gridRadios1" value="1"></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q7" id="gridRadios1" value="8"></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q7" id="gridRadios1" value="9"></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q7" id="gridRadios1" value="10"></td>
					</tr>
					<tr>
						<td>8.</td>
						<td><h2>Quality of work performed</h2></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q8" id="gridRadios1" value="1"></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q8" id="gridRadios1" value="8"></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q8" id="gridRadios1" value="9"></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q8" id="gridRadios1" value="10"></td>
					</tr>
					<tr>
						<td>9.</td>
						<td><h2>Cleanliness of the vehicle during delivery</h2></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q9" id="gridRadios1" value="1"></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q9" id="gridRadios1" value="8"></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q9" id="gridRadios1" value="9"></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q9" id="gridRadios1" value="10"></td>
					</tr>
					<tr>
						<td>10.</td>
						<td><h2>Kept you informed on the status of the vehicle</h2></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q10" id="gridRadios1" value="1"></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q10" id="gridRadios1" value="8"></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q10" id="gridRadios1" value="9"></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q10" id="gridRadios1" value="10"></td>
					</tr>
					<tr>
						<td>11.</td>
						<td><h2>On time delivery</h2></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q11" id="gridRadios1" value="1"></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q11" id="gridRadios1" value="8"></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q11" id="gridRadios1" value="9"></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q11" id="gridRadios1" value="10"></td>
					</tr>
					<tr>
						<td>12.</td>
						<td><h2>Were you shown the used parts removed from your vehicle after the repairs </h2></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q12" id="gridRadios1" value="1"></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q12" id="gridRadios1" value="8"></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q12" id="gridRadios1" value="9"></td>
						<td class="rating"><input class="form-check-input" type="radio" name="q12" id="gridRadios1" value="10"></td>
					</tr>
				</table>
			</div>
			<input type="hidden" name="service_id" value="<?php echo $_GET['id']; ?>">
			<input type="hidden" name="form_type" value="survey">
	<div style="margin: 20px;">
			<div class="custom-control custom-radio custom-control-inline">
			  <input type="radio" id="success" name="success" value="success" class="custom-control-input" onclick="successSurvey()">
			  <label class="custom-control-label" for="success">Successful survey</label>
			</div>
			<div class="custom-control custom-radio custom-control-inline">
			  <input type="radio" id="unsuccess" name="success" value="unsuccess" class="custom-control-input" onclick="successSurvey()">
			  <label class="custom-control-label" for="unsuccess">Unsuccessful survey</label>
			</div>
			<div id="dispositionSec" style="display: none;">
				<br>
    		<label>Unsuccessful reason</label>
        <select name="survey_disposition" id="survey_disposition" class="form-control" size="0">
			    <option value="">Choose...</option>
			    <option value="1">Busy</option>
			    <option value="2">Rejecting</option>
			    <option value="3">Wrong number</option>
			    <option value="4">No number</option>
			    <option value="5">Unavailable</option>
			    <option value="6">Pending</option>
        </select>               		
			</div>
			<div id="voice_sec" style="display: none;">
				<div class="form-group">
				    <label class="col-form-label form-control-label">Voice of ustomer</label>
	          <textarea class="form-control" name="customer_voice" rows="3" columns="8"></textarea>
				</div>			
				<label>Escalate?</label>
				<br>
				<div class="custom-control custom-radio custom-control-inline">
				  <input type="radio" id="escSurvey1" name="escalate_survey" value="yes" class="custom-control-input" onclick="escalateSection()">
				  <label class="custom-control-label" for="escSurvey1">Yes</label>
				</div>
				<div class="custom-control custom-radio custom-control-inline">
				  <input type="radio" id="escSurvey2" name="escalate_survey" value="no" class="custom-control-input" onclick="escalateSection()">
				  <label class="custom-control-label" for="escSurvey2">No</label>
				</div>
			</div>
			<div id="survey_esc_sec" style="display: none;">
				<table id="" border="1" class="table table-bordered table-striped table-sm" style="font-size: 0.8em;border-collapse: collapse;">
			    <thead>
			        <tr>
			            <th>Category</th>
			            <th>Sub-category</th>
			            <th>SLA</th>
			            <th>Escalated to</th>
			        </tr>
			    </thead>
			    <tbody>
			        <tr>
			            <td>
					        <select name="category" id="category" class="form-control" size="0" onchange="dir_esc_sub_cat()">
						    <option value="">Choose...</option>
						    <?php
						    	foreach ($esc_categories as $category) {
						    		echo '<option value="'.$category['id'].'">'.$category['cat_name'].'</option>';
						    	}
						    ?>
					        </select>            	
			            </td>
			            <td>
			            	<div id="esc_sub_cat">
							        <select name="category" id="category" class="form-control" size="0">
										    <option value="">Choose...</option>
							        </select>               		
			            	</div>
			            </td>
			            <td>
			            	<input type="text" class="form-control" id="sla" name="sla" placeholder="SLA" value="">
			            </td>
			            <td>
			            	<div id="contacts">
					        <select name="category" id="category" class="form-control" size="0">
								    <option value="">Choose...</option>
					        </select>               		
			            	</div>            	
			            </td>
			        </tr>
			    </tbody>
			</table>
		</div>
	</div>
	<div class="card-footer">
		<div id="saveWithEscalate" style="display: none;">
			<div class="row">
				<div class="col-md-3">
					<button id="submit_btn" type="Submit" class="btn btn-success btn-sm btn-block">
						<span class="fa fa-send"></span> Escate survey<span class="loading"></span></button>
				</div>
			</div>
		</div>
		<div id="saveWithoutEscalate" style="display: none;">
			<div class="row">
				<div class="col-md-3">
					<button id="submit_btn" type="Submit" class="btn btn-success btn-sm btn-block">
						<span class="fa fa-send"></span> Save survey   <span class="loading"></span></button>
				</div>
			</div>
		</div>
		
		<div id="response"></div>
	</div>
			<!-- <span class="fa fa-send"></span> -->
			<!-- <span class="loading"></span> -->
			</form>
		</div>
	</div>
</div>