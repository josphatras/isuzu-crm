<?php
if (isset($_GET['from_date'])) {
	$fromDate = $_GET['from_date'];
}else{
	$fromDate = date('Y-m-d');
}

if (isset($_GET['to_date'])) {
	$toDate = $_GET['to_date'];
}else{
	$toDate = date('Y-m-d');
}

$bookings = bookings($fromDate, $toDate);
?>
<div style="border: 1px solid #ccc; padding: 20px; margin-bottom: 30px;">
	<div class="row">
		<div class="col-md-4">
			
		</div>
		<div class="col-md-8 col-sm-12 col-sm-12">
			<div class="row">									
				<div class="col-md-5">
				  <div class="input-group">
				    <div class="input-group-prepend">
				      <div class="input-group-text" id="btnGroupAddon">From</div>
				    </div>
				    <input type="text" class="form-control dateSelect" id="fromDate" placeholder="From Date" value="<?php echo $fromDate; ?>">
				  </div>								
				</div>
				<div class="col-md-5">
				  <div class="input-group">
				    <div class="input-group-prepend">
				      <div class="input-group-text" id="btnGroupAddon">To</div>
				    </div>
				    <input type="text" class="form-control dateSelect" id="toDate" placeholder="To Date" value="<?php echo $toDate; ?>">
					</div>								
				</div>
				<div class="col-md-2">
					<span class="btn btn-info" id="basic-addon2" onclick="bookingsSearch('view')">Search</span>
				</div>
			</div>			
		</div>
	</div>						
</div>

<table class="isuzu-datatable table table-bordered table-striped table-sm " style="font-size: 0.8em;border-collapse: collapse;">
	<thead>
		<tr>
			<th>Branch</th>
			<th>Booked By</th>
			<th>Customer</th>
			<th>Contact Person</th>
			<th>Reg No.</th>
			<th>Model</th>
			<th>Section</th>
			<th>Service Type</th>
			<th>Appointment Date</th>
			<th>Appointment Time</th>
			<th>Repair Description</th>
			<th>Booked on</th>
		</tr>
	</thead>
	<tbody>
		<?php
			foreach ($bookings as $booking) {
				$custmer = sel_customer($booking['customer_id']);
				if ($booking['vehicle_id'] !== '') {
					$vehicle = vehicle($booking['vehicle_id']);
				}else{
					$vehicle = array('reg_no' => '', 'v_model' => '');
				}
		?>
		<tr>
			<td><?php echo $booking['branch']; ?></td>
			<td><?php echo $booking['agent']; ?></td>
			<td><a href="?content=customer&customer=<?php echo $booking['customer_id']; ?>"><?php echo $booking['customer_name']; ?></a></td>
			<td><?php echo $booking['contact_person']; ?></td>
			<td><?php echo $vehicle['reg_no']; ?></td>
			<td><?php echo $vehicle['v_model']; ?></td>
			<td><?php echo $booking['section']; ?></td>
			<td><?php echo $booking['service_type']; ?></td>
			<td><?php echo date('Y-m-d', strtotime($booking['appointment_date'])); ?></td>
			<td><?php echo date('G:i', strtotime($booking['appointment_date'])); ?></td>
			<td><?php echo $booking['repair_description']; ?></td>
			<td><?php echo $booking['created_on']; ?></td>
		</tr>
		<?php
			}
		?>		
	</tbody>
</table>