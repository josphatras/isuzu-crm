<?php
	$customerId = $_GET['customerId'];
	$customer = sel_customer($customerId);
	$vehicles = vehicles($customerId);
	// $model = model($customer['v_model']);
	$ints = bookingTime();
	$sections = bookingSections();

	$dealers = dealers();
	$branches = branches();

	if (isset($_SESSION['isuzu_dealer'])) {
	    $branches = dealerBranches($_SESSION['isuzu_dealer']);
	}else{
	    $branches = branches();
	}

?>
<div class="row">
	<div class="col-md-6 col-sm-12 col-xm-12">
		<h2>Customer Details</h2>
		<hr>
		<div style="margin: 10px 0 10px 0";">
			<div class="row ticketDetails">
				<div class="col-md-12 col-sm-12 col-xm-12">
					<table border="1" cellpadding="5" bordercolor="#7fb1b2" style="width: 100%;">
						<tr>
							<td style="width: 40%;"><strong>Customer Name:</strong></td>
							<td><?php echo $customer['first_name'].' '.$customer['last_name'] ?></td>
						</tr>
						<tr>
							<td><strong>Telephone (Cell):</strong></td>
							<td><?php echo $customer['telephone_no']?></td>
						</tr>
						<tr>
							<td><strong>Email Address:</strong></td>
							<td><?php echo $customer['email']?></td>
						</tr>
						<!-- <tr>
							<td><strong>REG NO.:</strong></td>
							<td><?php echo $customer['reg_no']?></td>
						</tr>
						<tr>
							<td><strong>Model:</strong></td>
							<td><?php echo $model['model_name']?></td>
						</tr> -->
					</table>
					<br>
					<h2>Vehicle</h2>
					<select class="form-control" size="0" id="vehicle" name="vehicle">
					    <option value="">Choose...</option>
			    		<?php
					    	foreach ($vehicles as $vehicle) {
					    		echo '<option value="'.$vehicle['id'].'">'.$vehicle['reg_no'].'</option>';
					    	}
					    ?>
	        </select>
	        <br>
					<a href="dashboard.php?content=msg&msg_item=vehicle&customerId=796&next=no"><button class="btn btn-secondary">Add vehicle</button></a>
				</div>
			</div>				
		</div>
	</div>
	<div class="col-md-6 col-sm-12 col-xm-12">
		<h2>Booking Details</h2>
		<hr>
		<form id="new_booking" style="background: #f5f5f5; padding: 20px;">
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label class="col-form-label form-control-label">Dealer name</label>
						<?php if (isset($_SESSION['isuzu_dealer'])): ?>
					  <select class="form-control" size="0" id="branch" name="branch" required>
							<?php foreach ($dealers as $dealer){
								if ($dealer['id'] == $_SESSION['isuzu_dealer']) {
									echo '<option value="'.$dealer['id'].'" selected>'.$dealer['name'].'</option>';
								}
							}
							?>
						</select>
						<?php endif; ?>

						<?php if (!isset($_SESSION['isuzu_dealer'])): ?>
					  <select class="form-control" size="0" id="dealer" name="dealer" onchange="dealerBranches()" required>
							<option value="">Choose...</option>
							<?php foreach ($dealers as $dealer): ?>
							<option value="<?php echo $dealer['id'] ?>"><?php echo $dealer['name'] ?></option>
							<?php endforeach; ?>
						</select>
						<?php endif; ?>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label class="col-form-label form-control-label">Branch</label>
						<?php if (isset($_SESSION['isuzu_dealer'])): ?>
					  <select class="form-control" size="0" id="branch" name="branch" required>
							<option value="">Choose...</option>
							<?php foreach ($branches as $branch): ?>
								<option value="<?php echo $branch['id'] ?>"><?php echo $branch['name'] ?></option>
							<?php endforeach; ?>
						</select>
						<?php endif; ?>

						<?php if (!isset($_SESSION['isuzu_dealer'])): ?>
					  <select class="form-control" size="0" id="branch" name="branch" required>
							<option value="">Choose...</option>
						</select>
						<?php endif; ?>

					</div>
				</div>
			</div>
			<div class="form-group">
		    	<label class="col-form-label form-control-label">Contact Person</label>
				<input type="text" class="form-control" id="contact_person" name="contact_person" required>
			</div>
			<div class="form-group">
				    <label class="col-form-label form-control-label">Section</label>
				         <select class="form-control" size="0" id="section" name="section" onchange="appointment()" required>
						    <option value="">Choose...</option>
						    <?php
						    	foreach ($sections as $section) {
						    		echo '<option value="'.$section['id'].'">'.$section['name'].'</option>';
						    	}
						    ?>
				        </select>
				</div>			
			<div class="form-row">
				<div class="col-md-8 col-sm-12 col-xm-12">
					<div class="form-group">
					    <label class="col-form-label form-control-label">Appointment date</label>
				        <input type="text" class="form-control dateSelect" id="app_date" name="app_date" onchange="appointment()" required>
					</div>								
				</div>
				<div class="col-md-4 col-sm-12 col-xm-12">
					<div class="form-group">
					    <label class="col-form-label form-control-label">Appointment time</label>
				        <select class="form-control" size="0" id="app_time" name="app_time" disabled required>
						    <option value="">Choose...</option>
			        	</select>
			        	<div id="check_response"></div>
					</div>								
				</div>		
			</div>											
			<div class="form-group">
			    <label class="col-form-label form-control-label">Service type</label>
		          <select class="form-control" size="0" name="service_type" required>
					    <option value="">Choose...</option>
					    <option value="Minor">Minor</option>
					    <option value="Medium">Medium</option>
					    <option value="Major">Major</option>
					    <option value="Comprehensive">Comprehensive</option>
					    <option value="Repairs">Repairs</option>
			        </select>
			</div>
			<div class="form-group">
			    <label class="col-form-label form-control-label">Repair Description(If any)</label>
			          <textarea class="form-control" name="repair_desc" rows="3" columns="8"></textarea>
			</div>
			<input type="hidden" name="customerId" value="<?php echo $customerId;?>">
			<input type="hidden" name="formType" value="new_booking">
			<div class="row">
				<div class="col-md-3 col-sm-12 col-xm-12">
					<div class="form-group">
						<button type="submit" id="btn_new_book" class="btn btn-secondary">Submit</button>
					</div>								
				</div>
				<div class="col-md-9 col-sm-12 col-xm-12">
					<div id="loading" style="">

					</div>
					<div id="response"></div>								
					
				</div>
			</div>
		</form>
	</div>
</div>