<?php
$timeIntervals = bookingTime();

$dealers = dealers();
$branches = branches();

if (isset($_SESSION['isuzu_dealer'])) {
    $branches = dealerBranches($_SESSION['isuzu_dealer']);
}else{
    $branches = branches();
}

if (isset($_GET['date'])) {
	$c_date = $_GET['date'];
	$bnc = $_GET['branch'];
}else{
	$c_date = date('Y-m-d');
	$bnc = '1';
}

$items = b_calendar($c_date, $bnc);
?>
<div style="margin: 20px 0 0 0">
	<div class="row">
		<div class="col-md-6 col-sm-12 col-xm-12">
			<div style="margin: 0 0 20px 0;">
				<div class="row">				
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label class="col-form-label form-control-label">Dealer name</label>
									<?php if (isset($_SESSION['isuzu_dealer'])): ?>
								  <select class="form-control" size="0" id="branch" name="branch" required>
										<?php foreach ($dealers as $dealer){
											if ($dealer['id'] == $_SESSION['isuzu_dealer']) {
												echo '<option value="'.$dealer['id'].'" selected>'.$dealer['name'].'</option>';
											}
										}
										?>
									</select>
									<?php endif; ?>

									<?php if (!isset($_SESSION['isuzu_dealer'])): ?>
								  <select class="form-control" size="0" id="dealer" name="dealer" onchange="dealerBranches()" required>
										<option value="">Choose...</option>
										<?php foreach ($dealers as $dealer): ?>
										<option value="<?php echo $dealer['id'] ?>"><?php echo $dealer['name'] ?></option>
										<?php endforeach; ?>
									</select>
									<?php endif; ?>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label class="col-form-label form-control-label">Branch</label>
									<?php if (isset($_SESSION['isuzu_dealer'])): ?>
								  <select class="form-control" size="0" id="branch" name="branch" required>
										<option value="">Choose...</option>
										<?php foreach ($branches as $branch): ?>
											<option value="<?php echo $branch['id'] ?>"><?php echo $branch['name'] ?></option>
										<?php endforeach; ?>
									</select>
									<?php endif; ?>

									<?php if (!isset($_SESSION['isuzu_dealer'])): ?>
								  <select class="form-control" size="0" id="branch" name="branch" required>
										<option value="">Choose...</option>
									</select>
									<?php endif; ?>

								</div>
							</div>
							<div class="col-md-4">
							  <div class="form-group">
							  	<label class="col-form-label form-control-label">Date</label>
							    <input type="text" class="form-control dateSelect" id="date" value="<?php echo $c_date; ?>" onchange="bookingCl()">
								</div>
							</div>
						</div>

<!-- 					  <div class="input-group">
					    <div class="input-group-prepend">
					      <div class="input-group-text" id="btnGroupAddon">Branch</div>
					    </div>
						<select class="form-control" size="0" id="branch" name="branch" required>
						    <option value="">Choose...</option>
				    		<?php
						    	foreach ($branches as $branch) {
						    		$select = ($branch['id'] == '1')? "selected": "";
						    		echo '<option value="'.$branch['id'].'" '.$select.'>'.$branch['name'].'</option>';
						    	}
						    ?>
				        </select>
					  </div>	 -->							
					</div>
				</div>
			</div>			
			<table class="table table-bordered table-striped table-sm table-hover">
				<tr>
					<th width="100">Time</th>
					<th>Details</th>
				</tr>
				<?php
					foreach ($timeIntervals as $key => $value) {
						echo '<tr>
								<td>'.$value.'</td>
								<td>';
						foreach ($items as $item) {
							if ($item['period'] == $value && $item['booking_id'] !== '') {
								$booking = booking($item['booking_id']);
								echo '<div style="margin=10px;"><a href="?content=customer&customer='.$booking['customer_id'].'">'.$booking['customer_name'].' <span class="badge badge-pill badge-info">'.$booking['section'].'</span></a></div>';
							}
						}
						echo '</td>
							</tr>';
						
					}
				?>				
			</table>		
		</div>
		<div class="col-md-6 col-sm-12 col-xm-12">
			
		</div>
	</div>			
</div>