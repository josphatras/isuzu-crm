<?php
include 'dao/reports/customers.php';
?>

<div class="container-fluid primary-color">
    <div class="row p-1" >
        <div class="col-md-12">
            <div class="col-main">
                <h3>Customers</h3>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-condensed table-striped isuzu-datatable" style="font-size: 0.8em;">
                            <thead>
                                <tr>
                                    <th>S. No.</th>
                                    <th>Customer ID</th>
                                    <th>Customer Name</th>
                                    <th>Customer Email</th>
                                    <th>Company Name</th>
                                    <th>Telephone</th>
                                    <th>Town</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>S. No.</th>
                                    <th>Customer ID</th>
                                    <th>Customer Name</th>
                                    <th>Customer Email</th>
                                    <th>Company Name</th>
                                    <th>Telephone</th>
                                    <th>Town</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php
                                    GetCustomers($con);
                                ?>
                            </tbody>
                            

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="delete_record" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header" style="background: #dd4f43">
      <h5 class="modal-title" id="exampleModalLabel">Warning</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
    <p>Are you sure you want to delete <strong><span id="customer_name"></span></strong> ?</p>
    </div>
    <div class="modal-footer">
      <div><button type="submit" class="btn btn-secondary" onclick="deleteCustomer()">Yes <span class="loading"></span></button></div>
      <button type="button" class="btn btn-info" data-dismiss="modal">No</button>
    </div>
  </div>
</div>
</div>
<input type="hidden" id="customer_id" value="">