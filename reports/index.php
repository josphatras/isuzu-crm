<?php
session_start();
include_once '../dao/reports/home.php';
include_once '../dao/functions.php';
require_once('../include/user_roles.php');
$summary = getSummary($con);
$top_msg = getTopMsg($con);
$trend_msg = getTrendMsg($con);
$top_esc = getTopEsc($con);
$q_sum = getQuickSum($con);
$q_sum1 = getQuickSum1($con);
?>

<?php if (!isset($_SESSION["isuzu_dealer"])): ?>
<div class="container-fluid primary-color">
    <div class="row p-1" >
        <div class="col-md-12">
            <div class="col-main">
                <h3>Summary</h3>
                <div class="card">
                    <div class="card-header">
                        <h5>Interactions</h5>
                    </div>
                    <div class="card-body" >
                        <div class="row">
                            <div class="col-md-3">
                                <div class="custom-card" data-toggle="modal" data-target="#vehicleInfo" onclick="totalInteractions();">
                                    <div class="custom-card-header">
                                        <h4>Total Interactions</h4>
                                    </div>
                                    <div class="custom-card-body text-center">
                                        <h1>
                                            <?php
                                                $interactions = $summary['interactions'];
                                                echo $interactions;
                                            ?>
                                        </h1>
                                        <p class="text-default">total interactions</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="custom-card">
                                    <div class="custom-card-header card-green">
                                        <h4>Top Categories</h4>
                                    </div>
                                    <div class="custom-card-body">
                                        <ol class="text-default" style="font-size: 0.8em;">
                                            <?php
                                                foreach ($top_msg as $tc=>$tm){
                                                    foreach($tm as $ky=>$val){
                                                        echo "<li><a class='text-default' href='#' data-toggle='modal' data-target='#vehicleInfo' onclick='categoryInteractions($tc);'>$ky - $val%</a></li>";
                                                    }
                                                }
                                            ?>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="custom-card">
                                    <div class="custom-card-header card-blue">
                                        <h4>Quick Summary</h4>
                                    </div>
                                    <div class="custom-card-body">
                                        <ol class="text-default" style="font-size: 0.8em;">
                                            <?php
                                                foreach ($q_sum as $ky=>$val){
                                                    echo "<li><a class='text-default' href='#' data-toggle='modal' data-target='#vehicleInfo' onclick=quickMessages('".explode(' ',$ky)[0]."') >$ky [$val interactions]</a></li>";
                                                }
                                            ?>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="custom-card">
                                    <div class="custom-card-header card-red">
                                        <h4>Trending(By Week)</h4>
                                    </div>
                                    <div class="custom-card-body">
                                        <ol class="text-default" style="font-size: 0.8em;">
                                            <?php
                                                foreach ($trend_msg as $tc=>$tm){
                                                    foreach($tm as $ky=>$val){
                                                        echo "<li><a class='text-default' href='#' data-toggle='modal' data-target='#vehicleInfo' onclick='categoryInteractions($tc);'>$ky - $val% </a></li>";
                                                    }
                                                }
                                            ?>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="card">
                    <div class="card-header">
                        <h5>Escalations</h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="custom-card" data-toggle="modal" data-target="#vehicleInfo" onclick="totalEscalations();">
                                    <div class="custom-card-header">
                                        <h4>Total Escalations</h4>
                                    </div>
                                    <div class="custom-card-body text-center">
                                        <h1>
                                            <?php
                                                $escalations = $summary['escalations'];
                                                echo $escalations;
                                            ?>
                                        </h1>
                                        <p class="text-default">total escalations</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="custom-card">
                                    <div class="custom-card-header card-green">
                                        <h4>Top Categories</h4>
                                    </div>
                                    <div class="custom-card-body">
                                        <ol class="text-default" style="font-size: 0.8em;">
                                            <?php
                                                foreach ($top_esc as $tc=>$te){
                                                    foreach($te as $ky=>$val){
                                                        echo "<li><a class='text-default' href='#' data-toggle='modal' data-target='#vehicleInfo' onclick='categoryEscalations($tc)'>$ky - $val%</a></li>";
                                                    }
                                                }
                                            ?>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="custom-card">
                                    <div class="custom-card-header card-blue">
                                        <h4>Quick Summary</h4>
                                    </div>
                                    <div class="custom-card-body">
                                        <ol class="text-default" style="font-size: 0.8em;">
                                            <?php
                                                foreach ($q_sum1 as $ky=>$val){
                                                    echo "<li><a class='text-default' href='#' data-toggle='modal' data-target='#vehicleInfo' onclick=quickEscalations('".explode(' ',$ky)[0]."') >$ky [$val escalations]</a></li>";
                                                }
                                            ?>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="custom-card">
                                    <div class="custom-card-header card-red">
                                        <h4>Trending</h4>
                                    </div>
                                    <div class="custom-card-body">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="vehicleInfo" tabindex="-1" role="dialog" aria-labelledby="vehicleInfoLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" style="max-width: 90% !important">
      <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="vehicleInfoLabel"><ttl></ttl></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" id="total-interactions">
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>
<?php endif; ?>
<?php if (isset($_SESSION["isuzu_dealer"])): ?>
    <h1>Dealer dashbord</h1>
<?php endif; ?>
