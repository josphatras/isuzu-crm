<?php
include 'dao/reports/escalations.php';
?>
<div class="container-fluid primary-color">
    <div class="row p-1" >
        <div class="col-md-12">
            <div class="col-main">
                <h3>Escalations</h3>
                <table id="isuzu-datatable" border="1" class="isuzu-datatable table table-bordered table-striped table-sm " style="font-size: 0.8em;border-collapse: collapse;">
                    <thead>
                        <tr>
                            <th>S/No</th>
                            <th>Ticket ID</th>
                            <th>Category</th>
                            <th>Sub-category</th>
                            <th>Issue</th>
                            <th>Resolution</th>
                            <th>SLA</th>
                            <th>Escalated to</th>
                            <th>Status</th>
                            <th>Escalated On</th>
                            <th>Age</th>
                            <th>Resolution Date</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>S/No</th>
                            <th>Ticket ID</th>
                            <th>Category</th>
                            <th>Sub-category</th>
                            <th>Issue</th>
                            <th>Resolution</th>
                            <th>SLA</th>
                            <th>Escalated to</th>
                            <th>Status</th>
                            <th>Escalated On</th>
                            <th>Age</th>
                            <th>Resolution Date</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php
                            if(isset($category)){
                                $filter = " WHERE a.cat_id='$category'";
                                GetInteractions($con,$filter);
                            }
                            else if(isset($range)){
                                $filter = '';
                                if(strtolower($range)=='today'){
                                    $filter = " WHERE DATE(a.created_on)=CURRENT_DATE()";
                                }
                                else if(strtolower($range)=='past'){
                                    $date1 = date('Y-m-d', strtotime('-7 days'));
                                    $filter = " WHERE DATE(a.created_on) BETWEEN DATE('$date1') AND CURRENT_DATE()";
                                }
                                else if(strtolower($range)=='this'){
                                    $date1 = date('Y-m').'-01';
                                    $filter = " WHERE DATE(a.created_on) BETWEEN DATE('$date1') AND CURRENT_DATE()";
                                }
                                GetInteractions($con,$filter);
                            }
                            else{
                                GetInteractions($con); 
                            }
                            
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="getescEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-lg" role="document">
    <div class="modal-content" style="padding: 20px;">
        <ul class="nav nav-tabs">
          <li class="nav-item">
            <div id="editsec">
                <a class="nav-link" href="#" onclick="showEditForm()" >Escalation Edit</a>
            </div>
          </li>
          <li class="nav-item">
            <div id="commentSec">
                <a class="nav-link" href="#" onclick="showCommentSec()">Comments</a>
            </div>
          </li>
        </ul>
      <div class="modal-body">
        <div id="showEscEditForm"></div>
        <div id="showCommentForm" style="display: none;">
            <form id="commentForm">
                <p><strong>Previous Comments</strong></p>
                <div id="messageView" style="margin-bottom: 10px;"></div>
                <label class="col-lg-3 col-form-label form-control-label">Add Message</label>
                <div class="col-lg-9">
                    <textarea class="form-control" id="escComment" name="escComment" rows="3" columns="8"></textarea>
                </div>
                <input type="hidden" name="formType" value="escComment">
                <div class="escId"></div>
                <div class="col-md-8" style="margin-top: 30px;">        
                    <button type="button" class="btn btn-secondary" onclick="saveComment()">Save</button>
                </div>               
            </form>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
    function escEdit(){
        $("#getescEdit").modal();
    }

    function escEditform(escId) {
    var reqlink = 'dao/admin/manage/esc_edit.php?escId=' + escId;
    var request = $.ajax({
      url: reqlink,
      data: '',
      dataType: 'html'
    });
     
    request.done(function( data ) {
    messageView(escId);
        $( "#showEscEditForm" ).html( data );
        $( "#editsec a" ).addClass( "active-pop" );
        $('.escId').html('<input type="hidden" name="escId" value="' + escId + '">');
    });

    }  
    function messageView(escId) {
    var reqlink = 'dao/admin/manage/message_view.php?escId=' + escId;
    var request = $.ajax({
      url: reqlink,
      data: '',
      dataType: 'html'
    });
     
    request.done(function( data ) {
        $( "#messageView" ).html( data );
    });

    } 


    function showEditForm(){
        $( "#editsec a" ).addClass( "active-pop" );
        $( "#commentSec a" ).removeClass( "active-pop" );
        $( "#showEscEditForm" ).show();       
        $( "#showCommentForm" ).hide();         
    }
    function showCommentSec(){
        $( "#editsec a" ).removeClass( "active-pop" );
        $( "#commentSec a" ).addClass( "active-pop" );
        $( "#showEscEditForm" ).hide();
        $( "#showCommentForm" ).show(); 
    } 
</script>
