<?php
include 'dao/reports/customer_account.php';
if (isset($_SESSION['isuzu_dealer'])) {
	header('Location: /'.ROOT_FOLDER.'dashboard.php?content=msg&msg_item=customer_profile&customerId='.$customerId);
}
$customerInfo = GetCustomer($con,$customerId);
?>
<div class="container-fluid primary-color">
	<div class="row p-1" >
		<div class="col-md-12">
			<div class="col-main">
				<h3>Customer Account</h3>
				<div style="float: right; margin-bottom: 10px;">
					<button type="button" class="btn btn-info" onclick="showSendSMS();"><span class="fa fa-envelope-o"></span> SMS</button> 
				</div>
				<div class="clearfix"></div>
				<div id="smsSection" style="margin-top: 20px; display: none">
					<div style="background: #f5f5f5;margin-bottom: 20px;float: right; padding: 20px;">
						<div id="smsResponse"></div>
						<div class="form-group row">
							<label class="col-lg-4 col-form-label form-control-label">Phone No</label>
							<div class="col-lg-8">
								<input type="text" class="form-control" id="smsNumber" name="smsNumber" placeholder="" value="<?php echo $customerInfo['phone'] ?>" required>
							</div>
						</div>
						<textarea class="form-control" id="smsText" name="smsText" rows="1" columns="8"></textarea>
						<div style="float: right; margin-top: 10px;">
							<button id="submit_btn" type="button" class="btn btn-info" onclick="sendSMS();" ><span class="fa fa-envelope-o"></span> Send SMS</button>           
						</div>
						<div style="float: right; padding: 10px;">
							<div id="loader"></div>     
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<div id="customerInfoSec">
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div style="float: left;"><h5>Customer Information</h5></div>
									<div style="float: right;">
										<button type="button" class="btn btn-secondary" onclick="showCustomerEdit();" >Edit</button>
										<a href="?content=msg&msg_item=vehicle&customerId=<?php echo $customerInfo['customer_id'];?>&next=no"><button type="button" class="btn btn-secondary">Add Vehicle</button></a>
										<a href="?content=services&type=new&customerId=<?php echo $customerInfo['customer_id'];?>"><button type="button" class="btn btn-secondary">New transaction</button></a>
										<a href="?content=bookings&item=new_booking&customerId=<?php echo $customerInfo['customer_id'];?>"><button type="button" class="btn btn-secondary">Service Book</button></a>
									</div>
								</div>
								<div class="card-body">
									<div class="row">
										<div class="col-md-6">
											<div class="row border">
												<div class="col-md-4 bg-danger text-light">Customer Name:</div>
												<div class="col-md-8"><label style="font-weight: bold;"><?php echo $customerInfo['name'] ?></label></div>
											</div>
											<div class="row border">
												<div class="col-md-4 bg-danger text-light">Email</div>
												<div class="col-md-8"><label style="font-weight: bold;"><?php echo $customerInfo['email'] ?></label></div>
											</div>
											<div class="row border">
												<div class="col-md-4 bg-danger text-light">Company Name</div>
												<div class="col-md-8"><label style="font-weight: bold;"><?php echo $customerInfo['company'] ?></label></div>
											</div>
											<div class="row border">
												<div class="col-md-4 bg-danger text-light">Phone</div>
												<div class="col-md-8"><label style="font-weight: bold;"><?php echo $customerInfo['phone'] ?></label></div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="row border">
												<div class="col-md-4 bg-danger text-light">Business Type</div>
												<div class="col-md-8"><label style="font-weight: bold;"><?php echo $customerInfo['business_type'] ?></label></div>
											</div>
											<div class="row border">
												<div class="col-md-4 bg-danger text-light">Physical Address</div>
												<div class="col-md-8"><label style="font-weight: bold;"><?php echo $customerInfo['p_address'] ?></label></div>
											</div>
											<div class="row border">
												<div class="col-md-4 bg-danger text-light">Town</div>
												<div class="col-md-8"><label style="font-weight: bold;"><?php echo $customerInfo['town'] ?></label></div>
											</div>
											<div class="row border">
												<div class="col-md-4 bg-danger text-light">Vehicles</div>
												<div class="col-md-8" style="padding-right: 0;"><label style="font-weight: bold;"><?php echo $customerInfo['vehicles'] ?></label><button class="btn btn-info pull-right" title="View Vehicle Info" data-toggle="modal" data-target="#vehicleInfo">View Vehicles  <span class="fa fa-list"></span></button></div>
											</div>
										</div>
										
										<div class="modal fade" id="vehicleInfo" tabindex="-1" role="dialog" aria-labelledby="vehicleInfoLabel" aria-hidden="true">
											<div class="modal-dialog modal-lg" role="document">
											  <div class="modal-content">
												<div class="modal-header">
												  <h5 class="modal-title" id="vehicleInfoLabel">Vehicle Information</h5>
												  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												  </button>
												</div>
												<div class="modal-body">
													<table class="table table-condensed table-striped" style="font-size: 0.7em;">
														<tr>
															<th>I.No</th>
															<th>Make</th>
															<th>Model</th>
															<th>Reg. No</th>
															<th>VIN No.</th>
															<th>Body Type</th>
															<th>Vehicle use</th>
															<th>Fleet Size</th>
														</tr>
														<?php CustomerVehicles($con,$customerId); ?>
													</table>
												</div>
												<div class="modal-footer">
												  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
												</div>
											  </div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="editCustomerSec" style="display: none;">
					<form id="customer_edit">
						<div class="row">
							<div class="col-md-6">
								<div class="form-row" style="">
									<div class="col-md-4" style="background: #dc3545; color: #fff;border: 1px solid #ccc;">Name</div>
									<div class="col-md-6"><input type="text" class="form-control" id="customerName" name="customerName" value="<?php echo $customerInfo['name'] ?>"></div>
								</div>
								<div class="form-row" style="">
									<div class="col-md-4" style="background: #dc3545; color: #fff;border: 1px solid #ccc;">Company Name</div>
									<div class="col-md-6"><input type="text" class="form-control" id="companyName" name="companyName" value="<?php echo $customerInfo['company'] ?>"></div>
								</div>
								<div class="form-row" style="">
									<div class="col-md-4" style="background: #dc3545; color: #fff;border: 1px solid #ccc;">Email</div>
									<div class="col-md-6"><input type="text" class="form-control" id="email" name="email" value="<?php echo $customerInfo['email'] ?>"></div>
								</div>      
								<div class="form-row" style="">
									<div class="col-md-4" style="background: #dc3545; color: #fff;border: 1px solid #ccc;">Phone</div>
									<div class="col-md-6"><input type="text" class="form-control" id="telNo" name="telNo" value="<?php echo $customerInfo['phone'] ?>"></div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-row" style="">
									<div class="col-md-4" style="background: #dc3545; color: #fff;border: 1px solid #ccc;">Business Type</div>
									<div class="col-md-6"><input type="text" class="form-control" id="busType" name="busType" value="<?php echo $customerInfo['business_type'] ?>"></div>
								</div>
								<div class="form-row" style="">
									<div class="col-md-4" style="background: #dc3545; color: #fff;border: 1px solid #ccc;">Physical Address</div>
									<div class="col-md-6"><input type="text" class="form-control" id="phyAddress" name="phyAddress" value="<?php echo $customerInfo['p_address'] ?>"></div>
								</div>
								<div class="form-row" style="">
									<div class="col-md-4" style="background: #dc3545; color: #fff;border: 1px solid #ccc;">Town</div>
									<div class="col-md-6"><input type="text" class="form-control" id="town" name="town" value="<?php echo $customerInfo['town'] ?>"></div>
								</div>    
							</div>
						</div> 
						<input type="hidden" name="formType" value="customer_edit">
						<input type="hidden" name="customerId" value="<?php echo $customerId ?>">
						<div style="padding-top: 20px; margin-bottom: 20px;">
							<button type="submit" class="btn btn-info" data-dismiss="modal">Save</button>  
						</div> 
					</form>
				</div>


				<div class="row">
					
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h5>Customer Interactions</h5>
							</div>
							<div class="card-body">
								<table class="table table-condensed table-striped isuzu-datatable" style="font-size: 0.8em;">
									<thead>
										<tr>
											<th>S. No.</th>
											<th>Interaction ID</th>
											<th>Date</th>
											<th>Contact Type</th>
											<th>Category</th>
											<th>Sub-category</th>
											<th>Comments</th>
											<th>Agent</th>
										</tr>
									</thead>
									<tfoot>
										<tr>
											<th>S. No.</th>
											<th>Interaction ID</th>
											<th>Date</th>
											<th>Contact Type</th>
											<th>Category</th>
											<th>Sub-category</th>
											<th>Comments</th>
											<th>Agent</th>
										</tr>
									</tfoot>
									<tbody>
										<?php
											CustomerInteractions($con,$customerId);
										?>
									</tbody>


								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h5>Customer Escalations</h5>
							</div>
							<div class="card-body">
								<table class="table table-condensed table-striped isuzu-datatable" style="font-size: 0.8em;">
									<thead>
										<tr>
											<th>S. No.</th>
											<th>Ticket ID</th>
											<th>Category</th>
											<th>Sub-category</th>
											<th>Issue</th>
											<th>Resolution</th>
											<th>SLA</th>
											<th>Escalated to</th>
											<th>Status</th>
											<th>Age</th>
											<th>Resolution Date</th>
										</tr>
									</thead>
									<tfoot>
										<tr>
											<th>S. No.</th>
											<th>Ticket ID</th>
											<th>Category</th>
											<th>Sub-category</th>
											<th>Issue</th>
											<th>Resolution</th>
											<th>SLA</th>
											<th>Escalated to</th>
											<th>Status</th>
											<th>Age</th>
											<th>Resolution Date</th>
										</tr>
									</tfoot>
									<tbody>
										<?php
											CustomerEscalations($con,$customerId);
										?>
									</tbody>


								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>