<?php
include 'dao/reports/messages.php';
?>
<div class="container-fluid primary-color">
    <div class="row p-1" >
        <div class="col-md-12">
            <div class="col-main">
                <h3>Interactions</h3>
                <table class="isuzu-datatable table table-bordered table-striped table-condensed table-sm" style="font-size: 0.8em;">
                    <thead>
                        <tr>
                            <th>S/No</th>
                            <th>Interaction ID</th>
                            <th>Date</th>
                            <th>Customer</th>
                            <th>Company</th>
                            <th>Contact Type</th>
                            <th>Category</th>
                            <th>Sub-category</th>
                            <th>Comments</th>
                            <th>Agent</th>
                            <th>Escalated</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>S/No</th>
                            <th>Interaction ID</th>
                            <th>Date</th>
                            <th>Customer</th>
                            <th>Company</th>
                            <th>Contact Type</th>
                            <th>Category</th>
                            <th>Sub-category</th>
                            <th>Comments</th>
                            <th>Agent</th>
                            <th>Escalated</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        
                        <?php
                            if(isset($category)){
                                $filter = " WHERE a.cat_id='$category'";
                                GetInteractions($con,$filter);
                            }
                            else if(isset($range)){
                                $filter = '';
                                if(strtolower($range)=='today'){
                                    $filter = " WHERE DATE(a.created_on)=CURRENT_DATE()";
                                }
                                else if(strtolower($range)=='past'){
                                    $date1 = date('Y-m-d', strtotime('-7 days'));
                                    $filter = " WHERE DATE(a.created_on) BETWEEN DATE('$date1') AND CURRENT_DATE()";
                                }
                                else if(strtolower($range)=='this'){
                                    $date1 = date('Y-m').'-01';
                                    $filter = " WHERE DATE(a.created_on) BETWEEN DATE('$date1') AND CURRENT_DATE()";
                                }
                                GetInteractions($con,$filter);
                            }
                            else{
                                GetInteractions($con); 
                            }
                            
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>