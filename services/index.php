<?php

if (isset($_GET['from_date'])) {
	$fromDate = $_GET['from_date'];
}else{
	$fromDate = date('Y-m-d');
}

if (isset($_GET['to_date'])) {
	$toDate = $_GET['to_date'];
}else{
	$toDate = date('Y-m-d');
}

$contentType = $_GET['type'];
if (isset($_GET['service'])) {
	$services = services($_GET['service'], 'not_surveyed', $fromDate, $toDate);
}else{
	$services = '';
}

?>

<div style="margin: 0 0 30px 0">
	<div class="row">
		<div class="col-md-4 col-sm-12 col-xm-12">
			<nav class="nav nav-pills nav-fill">
			  <a class="nav-item nav-link <?php echo (!isset($_GET['service']))? 'active' : ''; ?>" href="#">New Transaction</a>
			  <a class="nav-item nav-link <?php echo ($_GET['service'] == '1')? 'active' : ''; ?>" href="?content=services&type=history&service=1">Sales</a>
			  <a class="nav-item nav-link <?php echo ($_GET['service'] == '2')? 'active' : ''; ?>" href="?content=services&type=history&service=2">Service</a>
			</nav>		
		</div>
	</div>
</div>
<div style="border: 1px solid #ccc; padding: 20px; margin-bottom: 30px;">
	<div class="row">
		<div class="col-md-4">
			
		</div>
		<div class="col-md-8 col-sm-12 col-sm-12">
			<div class="row">									
				<div class="col-md-5">
				  <div class="input-group">
				    <div class="input-group-prepend">
				      <div class="input-group-text" id="btnGroupAddon">From</div>
				    </div>
				    <input type="text" class="form-control dateSelect" id="fromDate" placeholder="From Date" value="<?php echo $fromDate; ?>">
				  </div>								
				</div>
				<div class="col-md-5">
				  <div class="input-group">
				    <div class="input-group-prepend">
				      <div class="input-group-text" id="btnGroupAddon">To</div>
				    </div>
				    <input type="text" class="form-control dateSelect" id="toDate" placeholder="To Date" value="<?php echo $toDate; ?>">
					</div>								
				</div>
				<input type="hidden" id="service_id" value="<?php echo $_GET['service']; ?>">
				<div class="col-md-2">
					<span class="btn btn-info" id="basic-addon2" onclick="serviceSearch('view')">Search</span>
				</div>
			</div>			
		</div>
	</div>						
</div>
<?php if($contentType == 'history' && $_GET['service'] == '1'): ?>
<div style="overflow: auto; overflow-y: hidden; margin: 0 auto; white-space: nowrap">
	<table border="1" class="table table-bordered table-striped table-sm isuzu-datatable" style="font-size: 0.8em;border-collapse: collapse;">
		<thead>
			<th> Dealer </th>
			<th> Branch </th>
			<th> Entry by </th>
			<th> Customer name </th>
			<th> Transaction type </th>
			<th> Reg No. </th>
			<th> vin no </th>
			<th> Date of sale </th>
			<th> Delivery date </th>
			<th> Date of transaction </th>
			<th> Survey status </th>
			<th></th>
			<th></th>
		</thead>
		<tbody>
			<?php foreach ($services as $service): ?>
			<?php
				$vehicle = vehicle($service['vehicle_id']);
				$customer = sel_customer($vehicle['customer_id']);
				$status = serviceSurveyStatus($service['id']);
				$reasons = array('1' => "Busy", '2' => "Rejecting", '3' => "Wrong number", '4' => "No number", '5' => "Unavailable", '6' => "Pending");
				$reason = '';
				foreach ($reasons as $key => $value) {
					if ($key == $status['reason']) {
						$reason = $value;
						break;
					}
				}

				if ($reason !== '') {
					$res = $status['successful']."( ".$reason." )";
				}else{
					$res = $status['successful'];
				}
			?>
			<tr>
				<td><?php echo $service['dealer_name'] ?></td>
				<td><?php echo $service['branch_name'] ?></td>
				<td><?php echo $service['user_name'] ?></td>
				<td><?php echo $customer['first_name'].' '.$customer['last_name'] ?></td>
				<td><?php echo $service['transaction'] ?></td>
				<td><?php echo $vehicle['reg_no'] ?></td>
				<td><?php echo $vehicle['vin_no'] ?></td>
				<td><?php echo $service['sale_date'] ?></td>
				<td><?php echo $service['delivery_date'] ?></td>
				<td><?php echo $service['created_on'] ?></td>
				<td><?php echo $res ?></td>
				<td>
					<?php if (!isset($_SESSION['isuzu_dealer'])): ?>
						<a href="?content=surveys&type=new_sales&id=<?php echo $service['id'] ?>">Start survey</a>
					<?php endif ?>
				</td>
					<td><button class="btn btn-primary" onclick="delServicePop('<?php echo $service['id'] ?>')">Delete</button></td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
</div>
<?php endif; ?>


<?php if($contentType == 'history' && $_GET['service'] == '2'): ?>
<div style="overflow: auto; overflow-y: hidden; margin: 0 auto; white-space: nowrap">
	<table border="1" class="table table-bordered table-striped table-sm isuzu-datatable" style="font-size: 0.8em;border-collapse: collapse;">
		<thead>
			<th> Dealer </th>
			<th> Branch </th>
			<th> Entry by </th>
			<th> Transaction type </th>
			<th> Customer name </th>
			<th> Service advisor </th>
			<th> Received date </th>
			<th> Released date </th>
			<th> WRKORDNO </th>
			<th> Reg No. </th>
			<th> vin no </th>
			<th> Driver name </th>
			<th> Driver phone </th>
			<th> Work scope </th>
			<th> Survey status </th>
			<th></th>
			<th></th>
		</thead>
		<tbody>
			<?php foreach ($services as $service): ?>
			<?php
				$vehicle = vehicle($service['vehicle_id']);
				$customer = sel_customer($vehicle['customer_id']);
				$status = serviceSurveyStatus($service['id']);
				$reasons = array('1' => "Busy", '2' => "Rejecting", '3' => "Wrong number", '4' => "No number", '5' => "Unavailable", '6' => "Pending");
				$reason = '';
				foreach ($reasons as $key => $value) {
					if ($key == $status['reason']) {
						$reason = $value;
						break;
					}
				}
				if ($reason !== '') {
					$res = $status['successful']."( ".$reason." )";
				}else{
					$res = $status['successful'];
				}
			?>
			<tr>
				<td><?php echo $service['dealer_name'] ?></td>
				<td><?php echo $service['branch_name'] ?></td>
				<td><?php echo $service['user_name'] ?></td>
				<td><?php echo $service['transaction'] ?></td>
				<td><?php echo $customer['first_name'].' '.$customer['last_name'] ?></td>
				<td><?php echo $service['service_advisor'] ?></td>
				<td><?php echo $service['receieve_date'] ?></td>
				<td><?php echo $service['release_date'] ?></td>
				<td><?php echo $service['wrkord_no'] ?></td>
				<td><?php echo $vehicle['reg_no'] ?></td>
				<td><?php echo $vehicle['vin_no'] ?></td>
				<td><?php echo $service['driver_name'] ?></td>
				<td><?php echo $service['driver_phone'] ?></td>
				<td><?php echo $service['work_scope'] ?></td>
				<td><?php echo $res ?></td>
				<td>
				<?php if (!isset($_SESSION['isuzu_dealer'])): ?>
					<a href="?content=surveys&type=new_services&id=<?php echo $service['id'] ?>">Start survey</a>
				<?php endif ?>
				</td>
				<td><button class="btn btn-primary" onclick="delServicePop('<?php echo $service['id'] ?>')">Delete</button></td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
</div>
<?php endif; ?>
  <div class="modal fade" id="delete_record" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header" style="background: #dd4f43">
          <h5 class="modal-title" id="exampleModalLabel">Warning</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        <p>Are you sure you want to delete the <strong><span id="contactName"></span></strong> transaction?</p>
        </div>
        <div class="modal-footer">
          <div><button type="submit" class="btn btn-secondary" onclick="deleteService()">Yes <span class="loading"></span></button></div>
          <button type="button" class="btn btn-info" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>
  <input type="hidden" id="transaction_id" value="">

<?php if($contentType == 'new'): ?>

<?php
 include 'create.php';
?>

<?php endif; ?>