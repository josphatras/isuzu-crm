<div class="form-group">
	<label class="col-form-label form-control-label">Service advisor</label>
	<input type="text" class="form-control" id="service_advisor" name="service_advisor">
</div>
<div class="form-group">
	<label class="col-form-label form-control-label">Date received</label>
	<input type="text" class="form-control dateSelect" id="date_received" name="date_received">
</div>
<div class="form-group">
	<label class="col-form-label form-control-label">Date released</label>
	<input type="text" class="form-control dateSelect" id="date_released" name="date_released">
</div>
<div class="form-group">
	<label class="col-form-label form-control-label">WRKORDNO</label>
	<input type="text" class="form-control" id="wrkordno" name="wrkordno">
</div>
<div class="form-group">
	<label class="col-form-label form-control-label">Driver name</label>
	<input type="text" class="form-control" id="driver" name="driver">
</div>
<div class="form-group">
	<label class="col-form-label form-control-label">Driver Phone</label>
	<input type="text" class="form-control" id="driver" name="driver_phone">
</div>
<div class="form-group">
	<label class="col-form-label form-control-label">Work scope (work done)</label>
  <textarea class="form-control" name="work_scope" rows="3" columns="8"></textarea>
</div>