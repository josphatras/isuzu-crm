<div class="form-group">
	<label class="col-form-label form-control-label">Sales person</label>
	<input type="text" class="form-control" name="sales_person">
</div>
<div class="form-group">
	<label class="col-form-label form-control-label">Date of sale</label>
	<input type="text" class="form-control dateSelect" name="date_of_sale">
</div>
<div class="form-group">
	<label class="col-form-label form-control-label">Vehicle dellivery date</label>
	<input type="text" class="form-control dateSelect" name="delivery_date">
</div>