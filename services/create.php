<?php
	$customerId = $_GET['customerId'];
	$customer = sel_customer($customerId);
	$vehicles = vehicles($customerId);
	// $model = model($customer['v_model']);
	$ints = bookingTime();
	$sections = bookingSections();
	$dealers = dealers();
	if (isset($_SESSION['isuzu_dealer'])) {
	    $branches = dealerBranches($_SESSION['isuzu_dealer']);
	}else{
	    $branches = branches();
	}
?>
<div class="row">
	<div class="col-md-6 col-sm-12 col-xm-12">
		<h2>Customer Details</h2>
		<hr>
		<div style="margin: 10px 0 10px 0";">
			<div class="row ticketDetails">
				<div class="col-md-12 col-sm-12 col-xm-12">
					<table border="1" cellpadding="5" bordercolor="#7fb1b2" style="width: 100%;">
						<tr>
							<td style="width: 40%;"><strong>Customer Name:</strong></td>
							<td><?php echo $customer['first_name'].' '.$customer['last_name'] ?></td>
						</tr>
						<tr>
							<td><strong>Telephone (Cell):</strong></td>
							<td><?php echo $customer['telephone_no']?></td>
						</tr>
						<tr>
							<td><strong>Email Address:</strong></td>
							<td><?php echo $customer['email']?></td>
						</tr>
					</table>
					<br>
					<h2>Vehicle</h2>
					<select class="form-control" size="0" id="vehicle" name="vehicle">
						<option value="">Choose...</option>
						<?php
							foreach ($vehicles as $vehicle) {
								echo '<option value="'.$vehicle['id'].'">'.$vehicle['reg_no'].'</option>';
							}
						?>
					</select>
					<br>
					<a href="dashboard.php?content=msg&msg_item=vehicle&customerId=<?php echo $customerId ?>&next=no"><button class="btn btn-secondary">Add vehicle</button></a>
				</div>
			</div>              
		</div>
	</div>
	<div class="col-md-6 col-sm-12 col-xm-12">
		<h2>Transaction Details</h2>
		<hr>
		<form id="service" style="background: #f5f5f5; padding: 20px;">
<!-- 			<div class="form-group">
				<label class="col-form-label form-control-label">Dealer name</label>
			  <select class="form-control" size="0" id="dealer" name="dealer" onchange="dealerBranches()" required>
					<option value="">Choose...</option>
					<?php foreach ($dealers as $dealer): ?>
					<option value="<?php echo $dealer['id'] ?>"><?php echo $dealer['name'] ?></option>
					<?php endforeach; ?>
				</select>
			</div>
			<div class="form-group">
				<label class="col-form-label form-control-label">Branch</label>
			  <select class="form-control" size="0" id="branch" name="branch">
					<option value="">Choose...</option>
				</select>
			</div> -->
		<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label class="col-form-label form-control-label">Dealer name</label>
						<?php if (isset($_SESSION['isuzu_dealer'])): ?>
					  <select class="form-control" size="0" id="branch" name="branch" required>
							<?php foreach ($dealers as $dealer){
								if ($dealer['id'] == $_SESSION['isuzu_dealer']) {
									echo '<option value="'.$dealer['id'].'" selected>'.$dealer['name'].'</option>';
								}
							}
							?>
						</select>
						<?php endif; ?>

						<?php if (!isset($_SESSION['isuzu_dealer'])): ?>
					  <select class="form-control" size="0" id="dealer" name="dealer" onchange="dealerBranches()" required>
							<option value="">Choose...</option>
							<?php foreach ($dealers as $dealer): ?>
							<option value="<?php echo $dealer['id'] ?>"><?php echo $dealer['name'] ?></option>
							<?php endforeach; ?>
						</select>
						<?php endif; ?>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label class="col-form-label form-control-label">Branch</label>
						<?php if (isset($_SESSION['isuzu_dealer'])): ?>
					  <select class="form-control" size="0" id="branch" name="branch" required>
							<option value="">Choose...</option>
							<?php foreach ($branches as $branch): ?>
								<option value="<?php echo $branch['id'] ?>"><?php echo $branch['name'] ?></option>
							<?php endforeach; ?>
						</select>
						<?php endif; ?>

						<?php if (!isset($_SESSION['isuzu_dealer'])): ?>
					  <select class="form-control" size="0" id="branch" name="branch" required>
							<option value="">Choose...</option>
						</select>
						<?php endif; ?>

					</div>
				</div>
			</div>



										
			<div class="form-group">
				<label class="col-form-label form-control-label">Transaction type</label>
			  <select class="form-control" size="0" id="transaction_type" name="transaction_type" onchange="serviceSection()">
					<option value="">Choose...</option>
					<option value="1">Vehicle Sales</option>
					<option value="2">Vehicle Service</option>
				</select>
			</div>
			<div id="saleSec"> 
				
			</div>
			<div id="service_loading"></div>
			<div id="serviceSec" style="display: none;">
				<?php include 'new_service.php'; ?>
			</div>
			<div id="salesSec" style="display: none;">
				<?php include 'new_sales.php'; ?>
			</div>

			<input type="hidden" name="customerId" value="<?php echo $customerId;?>">
			<input type="hidden" name="form_type" value="new_service">
			<div class="row">
				<div class="col-md-3 col-sm-12 col-xm-12">
					<div class="form-group">
						<button type="submit" id="btn_new_book" class="btn btn-secondary">Submit</button>
					</div>                              
				</div>
				<div class="col-md-9 col-sm-12 col-xm-12">
					<div id="loading" style="">

					</div>
					<div id="response"></div>                               
					
				</div>
			</div>
		</form>
	</div>
</div>