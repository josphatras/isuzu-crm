<?php
include_once('header.php');
include_once('dao/config/db.php');
if (!isset($_SESSION["esc_user_id"])) {
	header('Location: /'.ROOT_FOLDER.'dao/login.php');
}
$userId = $_SESSION["esc_user_id"];
$userRole = $_SESSION["esc_role"];

$qallCount = "SELECT COUNT(id) count_all FROM escalations";
$getAllCount = $con->prepare($qallCount);
$getAllCount->execute();
$allCount = $getAllCount->fetch();

$qclosedCount = "SELECT COUNT(id) closed FROM escalations WHERE status = '4'";
$getClosedCount = $con->prepare($qclosedCount);
$getClosedCount->execute();
$closedCount = $getClosedCount->fetch();

$qrepliedCount = "SELECT COUNT(id) replied FROM escalations WHERE status = '2'";
$getRepliedCount = $con->prepare($qrepliedCount);
$getRepliedCount->execute();
$repliedCount = $getRepliedCount->fetch();

$qProgressCount = "SELECT COUNT(id) progress FROM escalations WHERE status = '1'";
$getProgressCount = $con->prepare($qProgressCount);
$getProgressCount->execute();
$progressCount = $getProgressCount->fetch();

$qPendingCount = "SELECT COUNT(id) pending FROM escalations WHERE status = '0'";
$getPendingCount = $con->prepare($qPendingCount);
$getPendingCount->execute();
$pendingCount = $getPendingCount->fetch();
 
if (isset($_GET['status'])) {
	$status = $_GET['status'];
	if ($status == 'all') {
		if (isset($_GET['from_date'])) {
			$fromDate = $_GET['from_date'].' '.'00:00:18';
			$toDate = $_GET['to_date'].' '.'23:59:18';
			$qesc = "SELECT e.id esc_id, e.cl_account, e.cl_phone, e.cl_name, e.esc_message,(SELECT id FROM messages  WHERE created_on=( SELECT MAX(created_on) FROM messages WHERE esc_id = e.id)) message_id, (SELECT status_name FROM esc_status WHERE esc_status.status_id = e.status) esc_status, e.created_on esc_on, u.first_name, u.last_name FROM escalations e INNER JOIN users u ON  u.id = e.from_user WHERE e.created_on >= :fromDate AND e.created_on <= :toDate ORDER BY e.created_on DESC";
			$getesc = $con->prepare($qesc);
			$getesc->bindParam(':fromDate', $fromDate, PDO::PARAM_STR);
			$getesc->bindParam(':toDate', $toDate, PDO::PARAM_STR);
			$getesc->execute();
		}else{
			$qesc = "SELECT e.id esc_id, e.cl_account, e.cl_phone, e.cl_name, e.esc_message,(SELECT id FROM messages  WHERE created_on=( SELECT MAX(created_on) FROM messages WHERE esc_id = e.id)) message_id, (SELECT status_name FROM esc_status WHERE esc_status.status_id = e.status) esc_status, e.created_on esc_on, u.first_name, u.last_name FROM escalations e INNER JOIN users u ON  u.id = e.from_user WHERE e.status = 'no result'";
			$getesc = $con->prepare($qesc);
			$getesc->bindParam(':status', $status, PDO::PARAM_STR);
			$getesc->execute();
		}
	}else{
		if (isset($_GET['from_date'])) {
			$fromDate = $_GET['from_date'].' '.'00:00:18';
			$toDate = $_GET['to_date'].' '.'23:59:18';
			$qesc = "SELECT e.id esc_id, e.cl_account, e.cl_phone, e.cl_name, e.esc_message,(SELECT id FROM messages  WHERE created_on=( SELECT MAX(created_on) FROM messages WHERE esc_id = e.id)) message_id, (SELECT status_name FROM esc_status WHERE esc_status.status_id = e.status) esc_status, e.created_on esc_on, u.first_name, u.last_name FROM escalations e INNER JOIN users u ON  u.id = e.from_user WHERE e.status = :status AND e.created_on >= :fromDate AND e.created_on <= :toDate";
			$getesc = $con->prepare($qesc);
			$getesc->bindParam(':status', $status, PDO::PARAM_STR);
			$getesc->bindParam(':fromDate', $fromDate, PDO::PARAM_STR);
			$getesc->bindParam(':toDate', $toDate, PDO::PARAM_STR);
			$getesc->execute();
		}else {
			$qesc = "SELECT e.id esc_id, e.cl_account, e.cl_phone, e.cl_name, e.esc_message,(SELECT id FROM messages  WHERE created_on=( SELECT MAX(created_on) FROM messages WHERE esc_id = e.id)) message_id, (SELECT MAX(created_on) FROM messages WHERE esc_id = e.id) , (SELECT status_name FROM esc_status WHERE esc_status.status_id = e.status) esc_status, e.created_on esc_on, u.first_name, u.last_name FROM escalations e INNER JOIN users u ON  u.id = e.from_user WHERE e.status = :status ORDER BY e.created_on DESC";
			$getesc = $con->prepare($qesc);
			$getesc->bindParam(':status', $status, PDO::PARAM_STR);
			$getesc->execute();
		}
	}
}
?>


<div class="container-fluid primary-color">
	<div class="row">
		<div class="col-md-12 col-sm-12">
			<div class="col-main">
				<div class="row">
					<div class="col-md-12">
						<div id="responseSection" class="response-section">
							<?php
								if (isset($_GET['response'])) {
									echo "<p>".$_GET['response']."</p>";
								}
							?>
						</div>
						<div class="dash-col-main">
							<div class="row">
								<div class="col-md-2 report-navigation">
									<ul class="list-group">
									  <li class="list-group-item list-group-item-info d-flex justify-content-between align-items-center <?php if($status == "4"){echo 'active';} ?>">
									    <a href="admin.php?status=4">Closed <span class="badge badge-info badge-pill"><?php echo $closedCount['closed']; ?></span></a>
									  </li>
									  <li class="list-group-item list-group-item-info d-flex justify-content-between align-items-center <?php if($status == "2"){echo 'active';} ?>">
									  	<a href="admin.php?status=2">Replied <span class="badge badge-info badge-pill"><?php echo $repliedCount['replied']; ?></span></a>
									  </li>
									  <li class="list-group-item list-group-item-info d-flex justify-content-between align-items-center <?php if($status == "1"){echo 'active';} ?>">
									  	<a href="admin.php?status=1">In Progress <span class="badge badge-info badge-pill"><?php echo $progressCount['progress']; ?></span></a>
									  </li>
									  <li class="list-group-item list-group-item-info d-flex justify-content-between align-items-center <?php if($status == "0"){echo 'active';} ?>">
									    
									    <a href="admin.php?status=0">Pending <span class="badge badge-info badge-pill"><?php echo $pendingCount['pending']; ?></span></a>
									  </li>
									  <li class="list-group-item list-group-item-info d-flex justify-content-between align-items-center <?php if($status == "all"){echo 'active';} ?>">
									  	<a href="admin.php?status=all">Main Report </a>
									  </li>
									</ul>			
								</div>
								<div class="col-md-10">
									<div style="border: 1px solid #ccc; padding: 20px;">
										<div class="row">
											<?php
											if (isset($_GET['status'])) {
												echo "<input type='hidden' name='' id='esc_status' value='".$_GET['status']."'>";
											}
											?>
											
											<div class="col-md-3">
											  <div class="input-group">
											    <div class="input-group-prepend">
											      <div class="input-group-text" id="btnGroupAddon">From</div>
											    </div>
											    <?php
											    	if (isset($_GET['from_date'])) {
											    		echo '<input type="text" class="form-control" id="fromDate" value="'.$_GET['from_date'].'" aria-label="Input group example" aria-describedby="btnGroupAddon">';
											    	}else{
											    		echo '<input type="text" class="form-control" id="fromDate" placeholder="From Date" aria-label="Input group example" aria-describedby="btnGroupAddon">';
											    	}
											    ?>
											  </div>								
											</div>
											<div class="col-md-3">
											  <div class="input-group">
											    <div class="input-group-prepend">
											      <div class="input-group-text" id="btnGroupAddon">To</div>
											    </div>
											    <?php
											    	if (isset($_GET['to_date'])) {
											    		echo '<input type="text" class="form-control" id="toDate" value="'.$_GET['to_date'].'" aria-label="Input group example" aria-describedby="btnGroupAddon">';
											    	}else{
											    		echo '<input type="text" class="form-control" id="toDate" placeholder="To Date" aria-label="Input group example" aria-describedby="btnGroupAddon">';
											    	}
											    ?>
											  </div>								
											</div>
											<div class="col-md-1">
												<span class="btn btn-info" id="basic-addon2" onclick="searchReport()">Search</span>
											</div>
											<div class="col-md-5">
												<div class="download" style="float: right;">
													<div class="btn-group" role="group">
													  <button type="button" class="btn btn-secondary" onclick="reportDownload()"><span class="fa fa-download"></span></button>
													  <button type="button" class="btn btn-info" onclick="reportDownload()">Download</button>
													</div>
												</div>
											</div>
										</div>						
									</div>
									<div class="">
										<table style="width: 100%;" border="1" cellspacing="0" bordercolor = "#ccc" cellpadding="2">
										<tr style="background: #4d436a; color: #fff; font-size: 11px; text-align: center;">
											<th>Message Id</th>
											<th>Customer Name</th>
											<th>Message</th>
											<th>Escalation Time</th>
											<th>Reply Date</th>
											<th>Status</th>
											<th>Escalated By</th>
											<th>Replied By</th>
										</tr>
										<?php
											$i = 0;
											while($esc = $getesc->fetch(PDO::FETCH_ASSOC)){
											$i++;
											$messageId = $esc['message_id'];
											$qreply = "SELECT m.created_on replied_on, u.first_name, u.last_name FROM messages m INNER JOIN users u ON m.user_id = u.id WHERE m.id = :messageId ";
											$getReply = $con->prepare($qreply);
											$getReply->bindParam(':messageId', $messageId, PDO::PARAM_INT);
											$getReply->execute();
											$reply = $getReply->fetch();
											if ($i % 2 == 0)
												{
													$style = 'style="background:  #d6dbdf; color:  #283747 ; font-size: 11px; text-align: left; overflow: hidden;"';
												}
											else
											{
												$style = 'style="background: #fff; color: #283747; font-size: 11px; text-align: left; overflow: hidden;"';
											}
										?>
										<tr <?php echo $style; ?>>
											<td><?php echo $esc['esc_id']; ?></td>
											<td><?php echo $esc['cl_name']; ?></td>
											<td><?php echo $esc['esc_message']; ?></td>
											<td><?php echo $esc['esc_on']; ?></td>
											<td><?php echo $reply['replied_on']; ?></td>
											<td><?php echo $esc['esc_status']; ?></td>
											<td><?php echo '  '.$esc['first_name'].' '. $esc['last_name']; ?></td>
											<td><?php echo '  '.$reply['first_name'].' '. $reply['last_name']; ?></td>						
										</tr>
										<?php
											}
											//echo "<p><span class='theme-color-3'>No records found</span></p>";
										?>
									</div>
								</div>
							</div>

							<?php
/*							if ($content == 'new_user') {
								include_once('users/new_user.php');
							}elseif ($content == 'edit_user') {
								include_once('users/edit_user.php');
							}*/
							?>
						</div>
					</div>	
				</div>
			</div>
		</div>
	</div>
</div>

<?php
include_once('footer.php');
?>