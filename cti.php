<?php
session_start();
include_once('dao/config/db.php');
include_once('dao/config/include.php');
if (!isset($_SESSION["isuzu_user_id"])) {
	header('Location: /'.ROOT_FOLDER.'dao/login.php');
}

if (isset($_GET['phone'])) {
	$customer = checkCustomer($_GET['phone']);
	if (isset($customer['id'])) {
		header('Location:/'.ROOT_FOLDER.'dashboard.php?content=msg&msg_item=customer_profile&customerId='.$customer['id']);
	}else{
		header('Location:/'.ROOT_FOLDER.'dashboard.php?content=msg&msg_item=customer');
	}
}else{
	die('<h5 style="margin:20px;">Pass phone number</h2>');
}

function checkCustomer($phoneNo){
	global $con;
	$qry = "SELECT c.telephone_no, u.id FROM customer_info c LEFT JOIN users u  ON u.id = c.customer_id WHERE c.telephone_no = :phoneNo";
	$stmt = $con->prepare($qry);
	$stmt->bindParam(':phoneNo',$phoneNo, PDO::PARAM_STR);
	$stmt->execute();	
	$customer = $stmt->fetch();
	return $customer;	
}
?>