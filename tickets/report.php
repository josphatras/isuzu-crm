<?php
include_once('dao/config/db.php');
$userRole = $_SESSION["esc_role"];

	$qclosedCount = "SELECT COUNT(id) closed FROM escalations WHERE status = '4'";
	$getClosedCount = $con->prepare($qclosedCount);
	$getClosedCount->execute();
	$closedCount = $getClosedCount->fetch();

	$qrepliedCount = "SELECT COUNT(id) replied FROM escalations WHERE status = '2'";
	$getRepliedCount = $con->prepare($qrepliedCount);
	$getRepliedCount->execute();
	$repliedCount = $getRepliedCount->fetch();

	$qProgressCount = "SELECT COUNT(id) progress FROM escalations WHERE status = '1'";
	$getProgressCount = $con->prepare($qProgressCount);
	$getProgressCount->execute();
	$progressCount = $getProgressCount->fetch();

	$qPendingCount = "SELECT COUNT(id) pending FROM escalations WHERE status = '0'";
	$getPendingCount = $con->prepare($qPendingCount);
	$getPendingCount->execute();
	$pendingCount = $getPendingCount->fetch();

if (isset($_GET['from_date'])) {
	$fromDate = $_GET['from_date'].' '.'00:00:18';
	$toDate = $_GET['to_date'].' '.'23:59:18';
	$qesc = "SELECT e.id esc_id, e.cl_account, e.cl_phone, e.cl_name, e.esc_message, (SELECT status_name FROM esc_status WHERE esc_status.status_id = e.status) esc_status, e.created_on esc_on, u.first_name, u.last_name FROM escalations e INNER JOIN users u ON  u.id = e.from_user WHERE e.created_on >= :fromDate AND e.created_on <= :toDate";
	$getesc = $con->prepare($qesc);
	$getesc->bindParam(':fromDate', $fromDate, PDO::PARAM_STR);
	$getesc->bindParam(':toDate', $toDate, PDO::PARAM_STR);
	$getesc->execute();	
}else{
	$qesc = "SELECT e.id esc_id, e.cl_account, e.cl_phone, e.cl_name, e.esc_message, (SELECT status_name FROM esc_status WHERE esc_status.status_id = e.status) esc_status, e.created_on esc_on, u.first_name, u.last_name FROM escalations e INNER JOIN users u ON  u.id = e.from_user WHERE e.status != '4' ORDER BY e.created_on DESC";
	$getesc = $con->prepare($qesc);
	$getesc->execute();
}



function limit_text($text, $limit) {
      if (str_word_count($text, 0) > $limit) {
          $words = str_word_count($text, 2);
          $pos = array_keys($words);
          $text = substr($text, 0, $pos[$limit]) . '...';
      }
      return $text;
    }
?>

<div class="">
	<div class="row">
		<div class="col-md-2">
			<ul class="list-group">
			  <li class="list-group-item d-flex justify-content-between align-items-center">
			    
			    Closed
			    <span class="badge badge-info badge-pill"><?php echo $closedCount['closed']; ?></span>
			  </li>
			  <li class="list-group-item d-flex justify-content-between align-items-center">
			    Replied
			    <span class="badge badge-info badge-pill"><?php echo $repliedCount['replied']; ?></span>
			  </li>
			  <li class="list-group-item d-flex justify-content-between align-items-center">
			    On Progress
			    <span class="badge badge-info badge-pill"><?php echo $progressCount['progress']; ?></span>
			  </li>
			  <li class="list-group-item d-flex justify-content-between align-items-center">
			    Pending
			    <span class="badge badge-info badge-pill"><?php echo $pendingCount['pending']; ?></span>
			  </li>
			</ul>			
		</div>
		<div class="col-md-10">
			<div style="border: 1px solid #ccc; padding: 20px;">
				<div class="row">
					<div class="col-md-3">
					  <div class="input-group">
					    <div class="input-group-prepend">
					      <div class="input-group-text" id="btnGroupAddon">From</div>
					    </div>
					    <?php
					    	if (isset($_GET['from_date'])) {
					    		echo '<input type="text" class="form-control" id="fromDate" value="'.$_GET['from_date'].'" aria-label="Input group example" aria-describedby="btnGroupAddon">';
					    	}else{
					    		echo '<input type="text" class="form-control" id="fromDate" placeholder="From Date" aria-label="Input group example" aria-describedby="btnGroupAddon">';
					    	}
					    ?>
					  </div>								
					</div>
					<div class="col-md-3">
					  <div class="input-group">
					    <div class="input-group-prepend">
					      <div class="input-group-text" id="btnGroupAddon">To</div>
					    </div>
					    <?php
					    	if (isset($_GET['to_date'])) {
					    		echo '<input type="text" class="form-control" id="toDate" value="'.$_GET['to_date'].'" aria-label="Input group example" aria-describedby="btnGroupAddon">';
					    	}else{
					    		echo '<input type="text" class="form-control" id="toDate" placeholder="To Date" aria-label="Input group example" aria-describedby="btnGroupAddon">';
					    	}
					    ?>
					  </div>								
					</div>
					<div class="col-md-1">
						<span class="btn btn-info" id="basic-addon2" onclick="searchReport()">Search</span>
					</div>
					<div class="col-md-5">
						<div class="download" style="float: right;">
							<div class="btn-group" role="group">
							  <button type="button" class="btn btn-secondary"><span class="fa fa-download"></span></button>
							  <button type="button" class="btn btn-info">Download</button>
							</div>
						</div>
					</div>
				</div>						
			</div>
			<div class="">
				<table border="1" bordercolor="#ccc" id="agentDailyReport" style="width: 100%;">
				<tr style="font-size: 11px; background: #504c6a; color: #fff; text-align: center; padding: 7px;">
					<th>Message Id</th>
					<th>Customer Name</th>
					<th>Message</th>
					<th>Escalation Time</th>
					<th>Reply Date</th>
					<th>Status</th>
					<th>Escalated By</th>
					<th>Replied By</th>
				</tr>
				<?php
					while($esc = $getesc->fetch(PDO::FETCH_ASSOC)){
				?>
				<tr>
					<td><?php echo $esc['esc_id']; ?></td>
					<td><?php echo $esc['cl_name']; ?></td>
					<td><?php echo $esc['esc_message']; ?></td>
					<td><?php echo $esc['esc_on']; ?></td>
					<td></td>
					<td><?php echo $esc['esc_status']; ?></td>
					<td><?php echo '  '.$esc['first_name'].' '. $esc['last_name']; ?></td>
					<td></td>						
				</tr>
				<?php
					}
					//echo "<p><span class='theme-color-3'>No records found</span></p>";
				?>
			</div>
		</div>
	</div>
</div>	
