<?php
include '../dao/config/db.php';
include '../dao/functions.php';
include '../dao/sms.php';
$todayDate = strtotime(date('Y-m-d G:i:s'));
$nextDay = date('Y-m-d', strtotime('+1 day',$todayDate));

$branches = branches();
foreach ($branches as $branch) {
	$items = booked($nextDay,$branch['id']);
	foreach ($items as $item) {
		reminderSMS($item['id']);
	}
}

function reminderSMS($bk){
	$booking = booking($bk);
	$custmer = sel_customer($booking['customer_id']);
	$text = 'Dear '.$booking['customer_name'].' this is a reminder that you have a service appointment on '.date('Y-m-d h:i A', strtotime($booking['appointment_date'])).' for your Isuzu '.$custmer['reg_no'].'. For any enquiries call 0800724724. Thank you';
	echo $text;
}

?>
