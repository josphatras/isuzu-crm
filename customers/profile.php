<?php
include 'dao/reports/messages.php';
$customerId = $_GET['customerId'];
$customerDetail = sel_customer($customerId);
$int_categories = sel_Cat('msg');
$esc_categories = sel_Cat('ticket');
?>

<div class="row">
	<div class="col-md-6">
		<h4>Customer Details</h4>
		<div class="form-row" style="border: 1px solid #ccc;">
			<div class="col-md-6" style="background: #dc3545; color: #fff">Name</div>
			<div class="col-md-6"><?php echo $customerDetail['first_name'].' '.$customerDetail['last_name'] ?></div>
		</div>
		<div class="form-row" style="border: 1px solid #ccc;">
			<div class="col-md-6" style="background: #dc3545; color: #fff">Email</div>
			<div class="col-md-6"><?php echo $customerDetail['email'] ?></div>
		</div>
		<div class="form-row" style="border: 1px solid #ccc;">
			<div class="col-md-6" style="background: #dc3545; color: #fff">Company Name</div>
			<div class="col-md-6"><?php echo $customerDetail['company_name'] ?></div>
		</div>
		<div class="form-row" style="border: 1px solid #ccc;">
			<div class="col-md-6" style="background: #dc3545; color: #fff">Names</div>
			<div class="col-md-6"><?php echo $customerDetail['telephone_no'] ?></div>
		</div>
		<div class="form-row" style="border: 1px solid #ccc;">
			<div class="col-md-6" style="background: #dc3545; color: #fff">Business Type</div>
			<div class="col-md-6"><?php echo $customerDetail['business_type'] ?></div>
		</div>
		<div class="form-row" style="border: 1px solid #ccc;">
			<div class="col-md-6" style="background: #dc3545; color: #fff">Physical Address</div>
			<div class="col-md-6"><?php echo $customerDetail['physical_address'] ?></div>
		</div>
		<div class="form-row" style="border: 1px solid #ccc;">
			<div class="col-md-6" style="background: #dc3545; color: #fff">Town</div>
			<div class="col-md-6"><?php echo $customerDetail['town'] ?></div>
		</div>
	</div>
	<div class="col-md-6">
		<h4>Vehicle Details</h4>

		<div class="form-row" style="border: 1px solid #ccc;">
			<div class="col-md-6" style="background: #dc3545; color: #fff">Make</div>
			<div class="col-md-6"><?php echo $customerDetail['v_make'] ?></div>
		</div>
		<div class="form-row" style="border: 1px solid #ccc;">
			<div class="col-md-6" style="background: #dc3545; color: #fff">Model</div>
			<div class="col-md-6"><?php echo $customerDetail['v_model'] ?></div>
		</div>
		<div class="form-row" style="border: 1px solid #ccc;">
			<div class="col-md-6" style="background: #dc3545; color: #fff">Reg No.</div>
			<div class="col-md-6"><?php echo $customerDetail['reg_no'] ?></div>
		</div>
		<div class="form-row" style="border: 1px solid #ccc;">
			<div class="col-md-6" style="background: #dc3545; color: #fff">vin no</div>
			<div class="col-md-6"><?php echo $customerDetail['vin_no'] ?></div>
		</div>
		<div class="form-row" style="border: 1px solid #ccc;">
			<div class="col-md-6" style="background: #dc3545; color: #fff">Body Type</div>
			<div class="col-md-6"><?php echo $customerDetail['body_type'] ?></div>
		</div>
		<div class="form-row" style="border: 1px solid #ccc;">
			<div class="col-md-6" style="background: #dc3545; color: #fff">Application</div>
			<div class="col-md-6"><?php echo $customerDetail['application'] ?></div>
		</div>
		<div class="form-row" style="border: 1px solid #ccc;">
			<div class="col-md-6" style="background: #dc3545; color: #fff">Fleet Size</div>
			<div class="col-md-6"><?php echo $customerDetail['fleet_size'] ?></div>
		</div>		
	</div>
</div>
<br><br>
<?php if (!isset($_SESSION['isuzu_dealer'])): ?>
<div>
	<div style="float: right; margin-left: 10px;">
	<button type="button" class="btn btn-info" onclick="showSendSMS();"><span class="fa fa-envelope-o"></span> SMS</button>	
	</div>
	<div style="float: right; margin-left: 10px;">
		<a href="?content=bookings&item=new_booking&customerId=<?php echo $_GET['customerId'];?>"><button type="button" class="btn btn-info">Service Book</button></a>
	</div>
	<div style="float: right; margin-left: 10px;">
		<a href="?content=services&type=new&customerId=<?php echo $_GET['customerId'];?>"><button type="button" class="btn btn-info">New transaction</button></a>
	</div>
	<div style="float: right; margin-left: 10px;">
	<a href="dashboard.php?content=msg&msg_item=contact&customerId=<?php echo $_GET['customerId']; ?>"><button type="link" class="btn btn-info" >Start Interaction</button></a>	
	</div>
</div>
<?php endif; ?>
<?php if (isset($_SESSION['isuzu_dealer'])): ?>
	<div style="float: right; margin-left: 10px;">
		<a href="?content=services&type=new&customerId=<?php echo $_GET['customerId'];?>"><button type="button" class="btn btn-info">New transaction</button></a>
		<a href="dashboard.php?content=bookings&item=new_booking&customerId=<?php echo $_GET['customerId'];?>"><button type="button" class="btn btn-info">Service Book</button></a>
		<a href="?content=msg&msg_item=vehicle&customerId=<?php echo $_GET['customerId'];?>&next=no"><button type="button" class="btn btn-info">Add Vehicle</button></a>

	</div>
<?php endif; ?>

<div class="clearfix"></div>
<div id="smsSection" style="margin-top: 20px; display: none">
	<div style="background: #f5f5f5;float: right; padding: 20px;">
		<div id="smsResponse"></div>
		<div class="form-group row">
		    <label class="col-lg-4 col-form-label form-control-label">Phone No</label>
		    <div class="col-lg-8">
		        <input type="text" class="form-control" id="smsNumber" name="smsNumber" placeholder="" value="<?php echo $customerDetail['telephone_no'] ?>" required>
		    </div>
		</div>
		<textarea class="form-control" id="smsText" name="smsText" rows="1" columns="8"></textarea>
		<div style="float: right; margin-top: 10px;">
			<button id="submit_btn" type="button" class="btn btn-info" onclick="sendSMS();" ><span class="fa fa-envelope-o"></span> Send SMS</button>			
		</div>
		<div style="float: right; padding: 10px;">
			<div id="loader"></div>		
		</div>
	</div>
</div>
<div class="clearfix"></div>
<br><br>
<!-- :::::::::::::::::::::::::::::::::::::::::::::: not for dealer -->
<?php if (!isset($_SESSION['isuzu_dealer'])): ?>
<div>
	<div style="float: left"><h4 style="text-decoration: underline;">Interactions</h4></div>
	<div style="float: left;padding-left: 20px;"><button type="button" class="btn btn-secondary btn-sm" onclick="showDirInt()">Add Interaction</button></div>
</div>
<div class="clearfix"></div>
<div id="add-int" style="display: none">
	<form id="dir_int">
		<table id="" border="1" class="table table-bordered table-striped table-sm " style="font-size: 0.8em;border-collapse: collapse;">
		    <thead>
		        <tr>
		        	<th>Contact type</th>
		            <th>Category</th>
		            <th>Sub-category</th>
		            <th>Message</th>
		            <th></th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr>
		        	<td>
						<select id="contactType" name="contactType" class="form-control" size="0" required>
						    <option value="">Choose...</option>
						    <?php
						    	foreach ($contactTypes as $contactType) {
						    		echo '<option value="'.$contactType['id'].'">'.$contactType['name'].'</option>';
						    	}
						    ?>
				        </select>
		        	</td>
		            <td>
				        <select name="intCategory" id="intCategory" class="form-control" size="0" onchange="dir_int_sub_cat()" required>
					    <option value="">Choose...</option>
					    <?php
					    	foreach ($int_categories as $category) {
					    		echo '<option value="'.$category['id'].'">'.$category['cat_name'].'</option>';
					    	}
					    ?>
				        </select>            	
		            </td>
		            <td>
		            	<div id="int_sub_cat">
				        <select name="intSubCat" id="intSubCat" class="form-control" size="0" required>
					    <option value="">Choose...</option>
				        </select>               		
		            	</div>
		            </td>
		            <td>
		            	<textarea class="form-control" id="msg" name="msg" rows="1" columns="8"></textarea>
		            </td>
		            <td>
		            	<input type="hidden" name="customerId" value="<?php echo $_GET['customerId']; ?>">
		            	<input type="hidden" name="formType" value="dir_int">
		            	<button type="submit" class="btn btn-info btn-sm">Save</button>
		            </td>
		        </tr>
		    </tbody>
		</table>	
	</form>
</div>
<table id="isuzu-datatable" class="table table-bordered table-striped table-condensed table-sm" style="font-size: 0.8em;">
    <thead>
        <tr>
            <th>S/No</th>
            <th>Interaction ID</th>
            <th>Date</th>
            <th>Contact Type</th>
            <th>Category</th>
            <th>Sub-category</th>
            <th>Comments</th>
            <th>Agent</th>
        </tr>
    </thead>
    <tbody>
        <?php customer_int($con, $customerId); ?>
    </tbody>
</table>
<br><br>
<div>
	<div style="float: left"><h4 style="text-decoration: underline;">Escalations</h4></div>
	<div style="float: left;padding-left: 20px;"><button type="button" class="btn btn-secondary btn-sm" onclick="showDirEsc()">Add Escalations</button></div>
</div>
<div id="add_esc" style="display: none">
	<form id="dir_esc">
		<table id="" border="1" class="table table-bordered table-striped table-sm " style="font-size: 0.8em;border-collapse: collapse;">
		    <thead>
		        <tr>
		            <th>Category</th>
		            <th>Sub-category</th>
		            <th>Issue</th>
		            <th>SLA</th>
		            <th>Escalated to</th>
		            <th></th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr>
		            <td>
				        <select name="category" id="category" class="form-control" size="0" onchange="dir_esc_sub_cat()" required>
					    <option value="">Choose...</option>
					    <?php
					    	foreach ($esc_categories as $category) {
					    		echo '<option value="'.$category['id'].'">'.$category['cat_name'].'</option>';
					    	}
					    ?>
				        </select>            	
		            </td>
		            <td>
		            	<div id="esc_sub_cat">
				        <select name="category" id="category" class="form-control" size="0" required>
					    <option value="">Choose...</option>
				        </select>               		
		            	</div>
		            </td>
		            <td>
		            	<textarea class="form-control" id="msg" name="msg" rows="1" columns="8"></textarea>
		            </td>
		            <td>
		            	<input type="text" class="form-control" id="sla" name="sla" placeholder="SLA" value="">
		            </td>
		            <td>
		            	<div id="contacts">
				        <select name="category" id="category" class="form-control" size="0" required>
					    <option value="">Choose...</option>
				        </select>               		
		            	</div>            	
		            </td>
		            <td>
		            	<input type="hidden" name="customerId" value="<?php echo $_GET['customerId']; ?>">
		            	<input type="hidden" name="formType" value="dir_esc">
		            	<button type="submit" class="btn btn-info btn-sm">Save</button>
		            </td>
		        </tr>
		    </tbody>
		</table>	
	</form>
</div>
<table id="isuzu-datatable" border="1" class="table table-bordered table-striped table-sm " style="font-size: 0.8em;border-collapse: collapse;">
    <thead>
        <tr>
            <th>S/No</th>
            <th>Ticket ID</th>
            <th>Category</th>
            <th>Sub-category</th>
            <th>Issue</th>
            <th>Resolution</th>
            <th>SLA</th>
            <th>Escalated to</th>
            <th>Status</th>
            <th>Age</th>
            <th>Resolution Date</th>
        </tr>
    </thead>
    <tbody>
        <?php customer_esc($con, $customerId); ?>
    </tbody>
</table>
<?php endif; ?>