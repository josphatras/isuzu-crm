<?php
if (isset($_SESSION["isuzu_dealer"])) {
	$customers = dealerCustomers($_SESSION["isuzu_dealer"]);
}else{
	
}
?>


<div class="container-fluid primary-color">
    <div class="row p-1" >
        <div class="col-md-12">
            <div class="col-main">
                <h3>Customers</h3>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-condensed table-striped isuzu-datatable" style="font-size: 0.8em;">
                            <thead>
                                <tr>
                                    <th>S. No.</th>
                                    <th>Customer ID</th>
                                    <th>Customer Name</th>
                                    <th>Customer Email</th>
                                    <th>Company Name</th>
                                    <th>Telephone</th>
                                    <th>Town</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>S. No.</th>
                                    <th>Customer ID</th>
                                    <th>Customer Name</th>
                                    <th>Customer Email</th>
                                    <th>Company Name</th>
                                    <th>Telephone</th>
                                    <th>Town</th>
                                    <th></th>
                                </tr>
                            </tfoot>
                            <tbody>
                            		<?php $sno = 1 ?>
                                <?php foreach ($customers as $customer): ?>
													        <tr>
													            <td><?php echo $sno++ ?></td>
													            <td><?php echo $customer['customer_id'] ?></td>
													            <td><?php echo $customer['customer_name'] ?></td>
													            <td><?php echo $customer['email'] ?></td>
													            <td><?php echo $customer['company_name'] ?></td>
													            <td><?php echo $customer['telephone_no'] ?></td>
													            <td><?php echo $customer['town'] ?></td>
													            <td><a href="dashboard.php?content=customer&customer=<?php echo $customer['customer_id'] ?>"><span class='fa fa-list'></span></a></td>
													        </tr>
                                <?php endforeach; ?>
                            </tbody>
                            

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>