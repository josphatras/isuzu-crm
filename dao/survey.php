<?php
include_once('../dao/config/include.php');
include_once('../dao/config/db.php');

session_start();
$form = $_POST['form_type'];
$userId = $_SESSION['isuzu_user_id'];
switch ($form) {
	case 'survey':
		salesSurvey();
		break;

	case 'delete_survey':
		deleteService();
		break;

	default:
		# code...
		break;
}
// echo count($_POST);
// print_r($_POST);
function serviceSurvey(){
	global $con;
	if (count($_POST) > 1 ) {
		saveSurvey('1');
	}else{
		die(json_encode(array('status' => 'fail', 'msg' => 'No survey data' )));
	}
}

function salesSurvey(){
	global $con;
	if (count($_POST) > 1 ) {
		saveSurvey('2');
	}else{
		die(json_encode(array('status' => 'fail', 'msg' => 'No survey data' )));
	}
}

function saveSurvey($survey){
	global $con;
	global $userId;
	$qry = "INSERT INTO s_form(service_id, user_id, survey_id, successful, escalate, reason, customer_voice ) VALUES(:service, :userId, :survey, :successful, :escalate, :reason, :customerVoice)";
	$stmt = $con->prepare($qry);
	$stmt->bindParam(':service',$_POST['service_id'], PDO::PARAM_INT);
	$stmt->bindParam(':survey',$survey, PDO::PARAM_INT);
	$stmt->bindParam(':userId',$userId, PDO::PARAM_INT);
	$stmt->bindParam(':successful',$_POST['success'], PDO::PARAM_STR);
	$stmt->bindParam(':escalate',$_POST['escalate_survey'], PDO::PARAM_STR);
	$stmt->bindParam(':reason',$_POST['survey_disposition'], PDO::PARAM_STR);
	$stmt->bindParam(':customerVoice',$_POST['customer_voice'], PDO::PARAM_STR);
	$stmt->execute();
	$form = $con->lastInsertId();

	$qns = $_POST;
	foreach ($qns as $key => $value) {
		$sprt =  str_split($key);
		if (Is_Numeric($value) && $sprt['0'] == 'q') {
			if (isset($sprt['2'])) {
				$qn = $sprt['1'].$sprt['2'];
			}else{
				$qn = $sprt['1'];
			}

			$qry = "INSERT INTO s_ans(form_id, qn_id, score) VALUES(:form, :qn, :score)";
			$stmt = $con->prepare($qry);
			$stmt->bindParam(':form',$form, PDO::PARAM_STR);
			$stmt->bindParam(':qn',$qn, PDO::PARAM_STR);
			$stmt->bindParam(':score',$value, PDO::PARAM_STR);
			$stmt->execute();			
		}
	}

	if (isset($_POST['other_model'])) {

			$qry = "INSERT INTO s_ans(form_id, qn_id, verbal) VALUES(:form, '7', :verbal)";
			$stmt = $con->prepare($qry);
			$stmt->bindParam(':form',$form, PDO::PARAM_STR);
			$stmt->bindParam(':verbal',$_POST['other_model'], PDO::PARAM_STR);
			$stmt->execute();

			$qry = "INSERT INTO s_ans(form_id, qn_id, verbal) VALUES(:form, '8', :verbal)";
			$stmt = $con->prepare($qry);
			$stmt->bindParam(':form',$form, PDO::PARAM_STR);
			$stmt->bindParam(':verbal',$_POST['other_model_names'], PDO::PARAM_STR);
			$stmt->execute();

			$qry = "INSERT INTO s_ans(form_id, qn_id, verbal) VALUES(:form, '9', :verbal)";
			$stmt = $con->prepare($qry);
			$stmt->bindParam(':form',$form, PDO::PARAM_STR);
			$stmt->bindParam(':verbal',$_POST['where_service'], PDO::PARAM_STR);
			$stmt->execute();

			$qry = "INSERT INTO s_ans(form_id, qn_id, verbal) VALUES(:form, '10', :verbal)";
			$stmt = $con->prepare($qry);
			$stmt->bindParam(':form',$form, PDO::PARAM_STR);
			$stmt->bindParam(':verbal',$_POST['recommend'], PDO::PARAM_STR);
			$stmt->execute();	
		
	}

	echo json_encode(array('status' => 'success', 'msg' => 'Survey successfuly saved' ));
}

function escalateSurvey(){
/*service_id	1
form_type	survey
success	success
survey_disposition	
customer_voice	Zxzcxv+cvxczxXZCXc+
escalate_survey	yes
category	52
escSubCategory	443
sla	sdasdasd
escTo	559*/

}


function deleteService(){
	global $con;
	$surveyId = $_POST['survey_id'];

	$qry = "DELETE FROM s_form WHERE id = :surveyId";
	$delCat = $con->prepare($qry);
	$delCat->bindParam(':surveyId',$surveyId, PDO::PARAM_STR);
	$delCat->execute();

	$qry = "DELETE FROM s_ans WHERE form_id = :surveyId";
	$delCat = $con->prepare($qry);
	$delCat->bindParam(':surveyId',$surveyId, PDO::PARAM_STR);
	$delCat->execute();

	echo json_encode(array('status' => 'success', 'msg' => 'Successfuly deleted' ));
}


?>