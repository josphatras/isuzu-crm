<?php
include_once('../dao/config/include.php');
include_once('../dao/config/db.php');
include_once('esc_contact.php');
include_once('../dao/functions.php');


if (isset($_SESSION["isuzu_user_id"])) {
    $userId = $_SESSION["isuzu_user_id"];
}else{
    die();
}

    $filename=$_FILES["file"]["tmp_name"];
    if($_FILES["file"]["size"] > 0)
    {
        $file = fopen($filename, "r");
        $record = 0;
        while (($data = fgetcsv($file,0,',')) !== FALSE)
        {
            $record++;
            if ($record !== 1) {
            	// echo $record;
            	// echo "<hr>";
                $qnewCustomer = " INSERT INTO users(created_by, first_name, last_name, email, role) VALUES (:createdBy, :cusFirstName,:cusLastName, :email,'3')";
                $newCustomer = $con->prepare($qnewCustomer);
                $newCustomer->bindParam(':createdBy', $userId, PDO::PARAM_STR);
                $newCustomer->bindParam(':cusFirstName',$data['0'], PDO::PARAM_STR);
                $newCustomer->bindParam(':cusLastName',$data['1'], PDO::PARAM_STR);
                $newCustomer->bindParam(':email',$data['2'], PDO::PARAM_STR);
                $newCustomer->execute();        
                $customerId = $con->lastInsertId();

                $qcustomerInfo = " INSERT INTO customer_info(customer_id, company_name, telephone_no, business_type, physical_address, town) VALUES (:customerId, :companyName, :telNo, :busType, :phyAddress,:town)";
                $customerInfo = $con->prepare($qcustomerInfo);
                $customerInfo->bindParam(':customerId',$customerId, PDO::PARAM_STR);
                $customerInfo->bindParam(':companyName',$data['3'], PDO::PARAM_STR);
                $customerInfo->bindParam(':telNo',$data['4'], PDO::PARAM_STR);
                $customerInfo->bindParam(':busType',$data['5'], PDO::PARAM_STR);
                $customerInfo->bindParam(':phyAddress',$data['6'], PDO::PARAM_STR);
                $customerInfo->bindParam(':town',$data['7'], PDO::PARAM_STR);
                $customerInfo->execute();

                $makes = veh_make();
                $types = veh_body_types();
                foreach ($makes as $make) {
                    if ($make['type'] == $data['8']) {
                        $v_make = $make['id'];
                        $models = veh_model($make['id']);
                        foreach ($models as $model) {
                            if ($model['model_name'] == $data['9']) {
                                $v_model = $model['id'];
                            }else{
                                $v_model = 0;
                            }
                        }
                        break;
                    }else{
                        $v_make = 0;
                        $v_model = 0;
                    }
                }

                foreach ($types as $type) {
                    if ($type['body_type'] == $data['12']) {
                        $v_body_type = $type['id'];
                        break;
                    }else{
                        $v_body_type = 0;
                    }
                }

                $qnewmsg = " INSERT INTO vehicle_info(customer_id, make, model, reg_no, vin_no, body_type, application, fleet_size) VALUES (:customerId,:make, :model, :regNo, :vinNo, :bodyType, :application, :fleetSize)";
                $newmsg = $con->prepare($qnewmsg);
                $newmsg->bindParam(':customerId',$customerId, PDO::PARAM_STR);
                $newmsg->bindParam(':make',$v_make, PDO::PARAM_STR);
                $newmsg->bindParam(':model',$v_model, PDO::PARAM_STR);
                $newmsg->bindParam(':regNo',$data['10'], PDO::PARAM_STR);
                $newmsg->bindParam(':vinNo',$data['11'], PDO::PARAM_STR);
                $newmsg->bindParam(':bodyType',$v_body_type, PDO::PARAM_STR);
                $newmsg->bindParam(':application',$data['13'], PDO::PARAM_STR);
                $newmsg->bindParam(':fleetSize',$data['14'], PDO::PARAM_STR);
                $newmsg->execute();            
            }
        }
        fclose($file);
        echo 'CSV File has been successfully Imported';
    } else{
        echo 'Invalid File:Please Upload CSV File';
    }
?>
<br>
<p><strong>Please note the last uploaded row is (<?php echo $record ?>)</strong></p>
<br>
<a href="../">Back to Dashboard</a>
