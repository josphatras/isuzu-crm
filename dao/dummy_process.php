<?php
include_once('../dao/config/include.php');
include_once('../dao/config/db.php');
include_once('esc_contact.php');
include_once('sms.php');



function customerInsert() {
	global $con;
	$customerName = explode(" ", $_POST['customerName']);
	$cusFirstName = $customerName['0'];
	if (isset($customerName['1'])) {
		$cusLastName = $customerName['1'];
	}else{
		$cusLastName = "";
	}

	$companyName = $_POST['companyName'];
	$telNo = $_POST['telNo'];
	$email = $_POST['email'];
	$busType = $_POST['busType'];
	$phyAddress = $_POST['phyAddress'];
	$town = $_POST['town'];

	if ($customerName == '' || $telNo == '') {
		$array_status = array('status' => 'fail' );
		$json_status = json_encode($array_status);
		die($json_status);
	}

	$qnewCustomer = " INSERT INTO users(first_name, last_name, email, role) VALUES (:cusFirstName,:cusLastName, :email,'3')";
	$newCustomer = $con->prepare($qnewCustomer);
	$newCustomer->bindParam(':cusFirstName',$cusFirstName, PDO::PARAM_STR);
	$newCustomer->bindParam(':cusLastName',$cusLastName, PDO::PARAM_STR);
	$newCustomer->bindParam(':email',$email, PDO::PARAM_STR);
	$newCustomer->execute();		
	$customerId = $con->lastInsertId();

	$qcustomerInfo = " INSERT INTO customer_info(customer_id, company_name, telephone_no, business_type, physical_address, town) VALUES (:customerId, :companyName, :telNo, :busType, :phyAddress,:town)";
	$customerInfo = $con->prepare($qcustomerInfo);
	$customerInfo->bindParam(':customerId',$customerId, PDO::PARAM_STR);
	$customerInfo->bindParam(':companyName',$companyName, PDO::PARAM_STR);
	$customerInfo->bindParam(':telNo',$telNo, PDO::PARAM_STR);
	$customerInfo->bindParam(':busType',$busType, PDO::PARAM_STR);
	$customerInfo->bindParam(':phyAddress',$phyAddress, PDO::PARAM_STR);
	$customerInfo->bindParam(':town',$town, PDO::PARAM_STR);
	$customerInfo->execute();
	return $customerId;
}

if ( $_POST['formType'] == 'advisor_create_customer') {
	$customerId = customerInsert();

	if (is_numeric($customerId)) {
		$array_status = array('status' => 'success', 'user' => $customerId);
	}else{
		$array_status = array('status' => 'fail');
	}

	$json_status = json_encode($array_status);
	print_r($json_status);
}


if ($_POST['formType'] == 'dummy_int') {

	$contactType = $_POST['contactType'];
	$category = $_POST['intCategory'];
	$subCategory = $_POST['intSubCategory'];
	$msg = $_POST['msg'];
	if (isset($_POST['complain'])) {
		$complain = $_POST['complain'];
	}else{
		$complain = '';
	}

	if ($contactType == '' || $category == '' || $subCategory == '' || $msg == '') {
		$array_status = array('status' => 'fail' );
		$json_status = json_encode($array_status);
		die($json_status);
	}	

	$customerId = customerInsert();

	$qnewmsg = "INSERT INTO messages(customer_id, complain, agent_id, contact_type, cat_id, sub_cat_id, text) VALUES (:customerId, :complain, :agentId, :contactType,:category, :subCategory, :msg)";
	$newmsg = $con->prepare($qnewmsg);
	$newmsg->bindParam(':customerId',$customerId, PDO::PARAM_STR);
	$newmsg->bindParam(':agentId',$userId, PDO::PARAM_STR);
	$newmsg->bindParam(':contactType',$contactType, PDO::PARAM_STR);
	$newmsg->bindParam(':category',$category, PDO::PARAM_STR);
	$newmsg->bindParam(':subCategory',$subCategory, PDO::PARAM_STR);
	$newmsg->bindParam(':complain',$complain, PDO::PARAM_STR);
	$newmsg->bindParam(':msg',$msg, PDO::PARAM_STR);
	$newmsg->execute();
	$array_status = array('status' => 'success');
	$json_status = json_encode($array_status);
	print_r($json_status);
}


if ($_POST['formType'] == 'dummy_esc') {
	$contactType = '2';
	$category = $_POST['category'];
	$subCategory = $_POST['escSubCategory'];
	$sla = $_POST['sla'];
	$msg = $_POST['msg'];
	$escTo = $_POST['escTo'];
	$smsCustomerNumber = $_POST['telNo'];
	if (isset($_POST['complain'])) {
		$complain = $_POST['complain'];
	}else{
		$complain = '';
	}



	if ($contactType == '' || $category == '' || $subCategory == '' || $msg == '') {
		$array_status = array('status' => 'fail' );
		$json_status = json_encode($array_status);
		die($json_status);
	}

	$customerId = customerInsert();

	$qnewmsg = "INSERT INTO messages(customer_id, complain, agent_id, contact_type, cat_id, sub_cat_id, text) VALUES (:customerId, :complain, :agentId, :contactType,:category, :subCategory, :msg)";
	$newmsg = $con->prepare($qnewmsg);
	$newmsg->bindParam(':customerId',$customerId, PDO::PARAM_STR);
	$newmsg->bindParam(':agentId',$userId, PDO::PARAM_STR);
	$newmsg->bindParam(':contactType',$contactType, PDO::PARAM_STR);
	$newmsg->bindParam(':category',$category, PDO::PARAM_STR);
	$newmsg->bindParam(':subCategory',$subCategory, PDO::PARAM_STR);
	$newmsg->bindParam(':complain',$complain, PDO::PARAM_STR);
	$newmsg->bindParam(':msg',$msg, PDO::PARAM_STR);
	$newmsg->execute();
	$msgId = $con->lastInsertId();

	$qnewEsc = "INSERT INTO tickets(msg_id,cat_id, sub_cat_id, escalated_to, status) VALUES (:msgId,:cat, :subCat, :escTo, '0')";
	$insertEsc = $con->prepare($qnewEsc);
	$insertEsc->bindParam(':cat',$category, PDO::PARAM_STR);
	$insertEsc->bindParam(':subCat',$subCategory, PDO::PARAM_STR);
	$insertEsc->bindParam(':escTo',$escTo, PDO::PARAM_STR);
	$insertEsc->bindParam(':msgId',$msgId, PDO::PARAM_STR);
	$insertEsc->execute();
	$ticketNo = $con->lastInsertId();

	$qry = "SELECT c.telephone_no, c.company_name, u.email, u.first_name, u.last_name FROM users u LEFT JOIN customer_info c ON c.customer_id = u.id WHERE u.id = :customerId";
	$getCustomer = $con->prepare($qry);
	$getCustomer->bindParam(':customerId',$customerId, PDO::PARAM_STR);
	$getCustomer->execute();
	$customer = $getCustomer->fetch();

	$qry = "SELECT * FROM esc_contact WHERE id = :escTo";
	$escContact = $con->prepare($qry);
	$escContact->bindParam(':escTo',$escTo, PDO::PARAM_STR);
	$escContact->execute();	
	$contact = $escContact->fetch();

	$qry = "SELECT c.cat_name, sc.sub_cat_name FROM  sub_categories sc INNER JOIN categories c ON sc.cat_id = c.id WHERE sc.id = :subCategory";
	$escCategory = $con->prepare($qry);
	$escCategory->bindParam(':subCategory',$subCategory, PDO::PARAM_STR);
	$escCategory->execute();	
	$e_category = $escCategory->fetch();


		$to = $contact['email'];
		$subj = 'CONTACT CENTRE CRM ESCALATION';
		$mail_msg = 'Dear '.$contact['name'].' ,<br><br>';
		$mail_msg .= 'The below issue has been escalated to you. Kindly action and respond within 12hours. Details:<br><br><br>';
		$mail_msg .= 'Customer Id	: '.$customerId.'<br><br>';
		$mail_msg .= 'Customer Name	: '.$customer['first_name'].' '.$customer['last_name'].'<br><br>';
		$mail_msg .= 'Phone No.	: '.$customer['telephone_no'].'<br><br>';
		$mail_msg .= 'Company	: '.$customer['company_name'].'<br><br>';
		$mail_msg .= 'Email	: '.$customer['email'].'<br><br>';
		$mail_msg .= 'Category	: '.$e_category['cat_name'].'<br><br>';
		$mail_msg .= 'Sub Category	: '.$e_category['sub_cat_name'].'<br><br>';
		$mail_msg .= 'Message	: '.$msg.'<br><br>';
		$mail_msg .= 'Sincerely,<br>';
		$mail_msg .= 'Isuzu Contact Center Team <br>';
		$mail_msg .= 'Contact Center Phone no: 0800724724 <br><br><br>';
		$mail_msg .= '* Please respond within 12hours. Thank you';
		escalationMail($to, $subj, $mail_msg);	

		if ($smsCustomerNumber !== '' && is_numeric($smsCustomerNumber)) {
			$msgText = 'Dear Valued Customer, thank you for contacting us. Your query has been escalated. Your ticket id is '.$ticketNo.'. You will receive feedback within 24hours. Thank you';
			sendSMS($smsCustomerNumber, $msgText);
			
		}

		if ($contact['phone'] !== '' && is_numeric($contact['phone'])) {
			$msgText = 'Dear '.$contact['name'].', you have received a customer query on your email for your prompt action. Please respond within 12hours. Thank you';
			sendSMS($contact['phone'], $msgText);
		}
	
	$array_status = array('status' => 'success');
	$json_status = json_encode($array_status);
	print_r($json_status);
}
?>