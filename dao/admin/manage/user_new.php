<div class="col-md-6 col-sm-12">
	<div class="container-signup">
		<p><strong>New user</strong></p>
		<hr>
		<form action="dao/account.php" method="POST">
		  <div class="form-group">
		    <label for="loginuser">Full Names</label>
		    <input type="text" class="form-control" id="loginuser" name="fullNames" aria-describedby="emailHelp" placeholder="Full Names" required>
		  </div>
		  <div class="form-group">
		    <label for="loginuser">Employee No.</label>
		    <input type="text" class="form-control" id="loginuser" name="employeeNo" aria-describedby="emailHelp" placeholder="Employee No." required>
		  </div>
		  <div class="form-group">
		    <label for="loginuser">Email</label>
		    <input type="mail" class="form-control" id="loginuser" name="email" aria-describedby="emailHelp" placeholder="Email" required>
		  </div>
		  <div class="form-group">
		    <label for="loinpassword">Password</label>
		    <input type="password" class="form-control" id="loinpassword" name="password" placeholder="Password" required>
		  </div>
		<div class="form-group">
	      <label class="mr-sm-2" for="inlineFormCustomSelect">Role</label>
	      <select class="custom-select mr-sm-1" id="inlineFormCustomSelect" name="userRole">
	        <option selected>Choose...</option>
	        <option value="1">Admin</option>
	        <option value="2">Agent</option>
	      </select>
	    </div>
			<br>
		  <input type="hidden" name="form_type" value="newUser">
		  <button type="submit" class="btn btn-primary">Submit</button>
		</form>	
	</div>
</div>