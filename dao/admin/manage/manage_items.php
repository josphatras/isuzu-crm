<?php
include_once "dao/functions.php";
$contactTypes = allConTypes();
$categories = sel_Cat('msg');
if (isset($_GET['item'])) {
   $item = $_GET['item'];
}else{
    $item = 'categories';
}
?>
<div class="row" style="margin-top: 20px;">
    <div class="col-md-2">
        <div class="report-navigation">
            <ul class="list-group">
              <li class="list-group-item theme-background d-flex justify-content-between align-items-center <?php if($item == "source"){echo 'active';} ?>">
                <a href="dashboard.php?content=settings&item=source">Contact Type <span class="badge badge-info badge-pill"></span></a>
              </li>
              <li class="list-group-item theme-background d-flex justify-content-between align-items-center <?php if($item == "categories"){echo 'active';} ?>">
                <a href="dashboard.php?content=settings&item=categories">Categories <span class="badge badge-info badge-pill"></span></a>
              </li>
              <li class="list-group-item theme-background d-flex justify-content-between align-items-center <?php if($item == "sub_categories"){echo 'active';} ?>">
                <a href="dashboard.php?content=settings&item=sub_categories">Sub Categories <span class="badge badge-info badge-pill"></span></a>
              </li>
              <li class="list-group-item theme-background d-flex justify-content-between align-items-center <?php if($item == "script"){echo 'active';} ?>">
                <a href="dashboard.php?content=settings&item=script">Script <span class="badge badge-info badge-pill"></span></a>
              </li>
              <li class="list-group-item theme-background d-flex justify-content-between align-items-center <?php if($item == "esc_contacts"){echo 'active';} ?>">
                <a href="dashboard.php?content=settings&item=esc_contacts">Contacts <span class="badge badge-info badge-pill"></span></a>
              </li>
            </ul>           
        </div> 
    </div>
    <div class="col-md-10">
        <div id="manItemResponse">
            <?php
                if (isset($_GET['man_item_response'])) {
                   echo '<div class="alert alert-success">Successfully deleted!</div>';
                }
            ?>
        </div>
        <?php
            if ($item == 'source') {
            ?>
                <h4>Contact Type</h4>
                <table border="1" class="table table-bordered table-striped table-sm isuzu-datatable" style="font-size: 0.8em;border-collapse: collapse;">
                    <thead>
                    <tr style="background: #e6e6e4;">
                        <th>Contact ID</th>
                        <th>Contact Name</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php getContact() ?>
                    </tbody>
                </table> 
                <br>
                <div id="scoreButton">
                </div>  
                <div class="clearfix"></div>
                <div id="addSource" style="display: none;">
                    <form>
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                <label for="exampleInputEmail1">Source Name</label>
                                <input type="text" class="form-control" id="source" aria-describedby="" placeholder="Source Name">
                                </div>
                                <div class="clearfix"></div>
                                <button type="button" class="btn btn-primary" onclick="addSource()">Add</button>  
                            </div>
                        </div>                
                    </form>
                </div>

            <?php
            }elseif ($item == 'categories') {
            ?>
                <h4>Categories</h4>
                <table border="1" class="table table-bordered table-striped table-sm" id="catTable" style="font-size: 0.8em;border-collapse: collapse;">
                    <thead>
                    <tr style="background: #e6e6e4;">
                        <th>Category ID</th>
                        <th>Type</th>
                        <th>Category Name</th>
                        <th>Visibility</th>
                        <th></th>
                    </tr>
                    <thead>
                    <tbody>
                    <?php getCat() ?>
                    </tbody>
                </table> 
                <br>
                <div id="catButton">
                    <button type="button" class="btn btn-primary user-add-new" onclick="showAddCat()"><i class="fa fa-plus fa-fw"></i>Add New</button>
                </div>  
                <div class="clearfix"></div>
                <div id="addCat" style="display: none;">
                    <form>
                        <div class="row">
                            <div class="col-md-6 col-sm-12">

                                <div class="form-group">
                                <label for="exampleInputEmail1">Type</label>
                                <select id="categoryType" class="form-control" name="categoryType"> 
                                   <option value="">Select</option>
                                   <option value="msg">Interaction</option>
                                   <option value="ticket">Escalation</option>
                                </select>                                
                                </div>

                                <div class="form-group">
                                <label for="exampleInputEmail1">Category Name</label>
                                <input type="" class="form-control" id="category" aria-describedby="emailHelp" placeholder="Category Name">
                                </div>
                                <div class="clearfix"></div>
                                <button type="button" class="btn btn-primary"  onclick="addCat()">Add</button>  
                            </div>
                        </div>                
                    </form>
                </div>
            <?php
            }elseif ($item == 'sub_categories') {
            ?>
                <h4>Sub Categories</h4>
                <table border="1" class="table table-bordered table-striped table-sm " id="subCatTable" style="font-size: 0.8em;border-collapse: collapse;">
                    <thead>
                    <tr style="background: #e6e6e4;">
                        <th>Sub Category ID</th>
                        <th>Type</th>
                        <th>Category</th>
                        <th>Sub Category Name</th>
                        <th>Visibility</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                       <?php getSubCat() ?>
                    </tbody>
                </table>
                <br>
                <div id="subCatButton">
                    <button type="button" class="btn btn-primary user-add-new" onclick="showAddSubCat()"><i class="fa fa-plus fa-fw"></i>Add New</button>
                </div>  
                <div class="clearfix"></div>
                <div id="addSubCat" style="display: none;">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                            <label for="exampleInputEmail1">Type</label>
                            <select id="typeSelect" class="form-control" name="" onchange="getCategories()"> 
                               <option value="">Select</option>
                               <option value="msg">Interaction</option>
                               <option value="ticket">Escalation</option>
                            </select>                                
                            </div>
                            <div id="subCat_catSec">
                                <div class="form-group"> 
                                    <label>Category</label> 
                                    <select id="catId" class="form-control" name="categories"> 
                                    <option value="">Select</option>
                                    <?php
                                        foreach ($categories as $category) {
                                            echo '<option value="'.$category['id'].'">'.$category['cat_name'].'</option>';
                                        }
                                    ?>
                                    </select>
                                    <span id="m-p-error" class="f-error"></span>
                                </div>
                            </div>
                            <div class="form-group">
                            <label for="exampleInputEmail1">Sub-Category Name</label>
                            <input type="" class="form-control" id="subCategory" aria-describedby="emailHelp" placeholder="Sub-Category Name">
                            </div>
                            <div class="clearfix"></div>
                            <button type="button" class="btn btn-primary" onclick="addSubCat()">Add</button>  
                        </div>
                    </div>                
                </div>
            <?php
            }elseif ($item == 'script') {
            ?>
            <div id="viewScriptSec">
                <h4>Script</h4>
                <table border="1" class="table table-bordered table-striped table-sm scrip-guide" style="font-size: 0.8em;border-collapse: collapse;">
                    <thead>
                    <tr style="background: #e6e6e4;">
                        <th>Script ID</th>
                        <th>Guide Text</th>
                        <th>Sub category Applicable</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php getScriptGuide() ?>
                    </tbody>
                </table>
                <br>
                <div id="dispButton">
                    <button type="button" class="btn btn-primary user-add-new" onclick="showAddScript()"><i class="fa fa-plus fa-fw"></i>Add New</button>
                </div>                  
            </div>
            <div id="addScriptSec" style="display: none">
                <form id="scriptContent">
                    <div class="row">
                        <div class="col-md-8 col-sm-12">
                            <div class="form-group">
                            <label for="exampleInputEmail1">Type</label>
                            <select id="typeSelect" class="form-control" name="" onchange="getCategories()"> 
                               <option value="">Select</option>
                               <option value="ticket">Escalation</option>
                            </select>                                
                            </div>
                            <div id="subCat_catSec">
                                <div class="form-group"> 
                                    <label>Category</label> 
                                    <select id="category" class="form-control" name="categories" onchange="selectSubCat();"> 
                                    <option value="">Select</option>
                                    <?php
                                        foreach ($categories as $category) {
                                            echo '<option value="'.$category['id'].'">'.$category['cat_name'].'</option>';
                                        }
                                    ?>
                                    </select>
                                    <span id="m-p-error" class="f-error"></span>
                                </div>
                            </div>

                            <div id="showSubCategory">
                                <div class="form-group">
                                    <label class="col-form-label form-control-label">Subcategory</label>
                                    <select class="form-control" size="0" id="subCategory" name="subCategory">
                                    <option selected>Choose...</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-form-label form-control-label">Contact Script</label>
                                    <textarea class="form-control" id="scriptContent" name="scriptContent" rows="3" columns="8"></textarea>
                            </div>
                            <div class="clearfix"></div>
                            <button type="button" class="btn btn-primary" onclick="saveScriptContent()">Add</button>  
                        </div>
                    </div> 
                </form>
            </div>
            <div class="modal fade" id="delScript" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header" style="background: #dd4f43">
                    <h5 class="modal-title" id="exampleModalLabel">Warning</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                  <p>Are you sure you want to delete Script identified by id <strong><span id="scriptTitle"></span></strong>?</p>
                  </div>
                  <div class="modal-footer">
                    <div id="delBtn"></div>
                    <button type="button" class="btn btn-info" data-dismiss="modal">No</button>
                  </div>
                </div>
              </div>
            </div>
            <?php
            }elseif ($item == 'esc_contacts') {
            ?>
            <div id="contactView">
                <h4>Escalation Contacts</h4>
                <table border="1" class="table table-bordered table-striped table-sm scrip-guide" style="font-size: 0.8em;border-collapse: collapse;">
                    <thead>
                    <tr style="background: #e6e6e4;">
                        <th>Contact Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Applicable</th>
                        <th>Contact Level</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php getAllContacts() ?>
                    </tbody>
                </table>
                <br>
                <div id="dispButton">
                    <button type="button" class="btn btn-primary user-add-new" onclick="showAddContact()"><i class="fa fa-plus fa-fw"></i>Add New</button>
                </div>
                </div>     
                <div id="addContactSec" style="display: none">
                    <div class="container py-3">
                        <div class="row">
                            <div class="mx-auto col-sm-8">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="mb-0">Create Contact</h4>
                                    </div>
                                    <div class="card-body">
                                        <form id="esc_contact_form">
                                            <div class="form-group">
                                            <label for="exampleInputEmail1">Type</label>
                                            <select id="typeSelect" class="form-control" name="" onchange="getCategories()"> 
                                               <option value="">Select</option>
                                               <option value="ticket">Escalation</option>
                                            </select>                                
                                            </div>
                                            <div id="subCat_catSec">
                                                <div class="form-group"> 
                                                    <label>Category</label> 
                                                    <select id="category" class="form-control" name="categories" onchange="selectSubCat();"> 
                                                    <option value="">Select</option>
                                                    <?php
                                                        foreach ($categories as $category) {
                                                            echo '<option value="'.$category['id'].'">'.$category['cat_name'].'</option>';
                                                        }
                                                    ?>
                                                    </select>
                                                    <span id="m-p-error" class="f-error"></span>
                                                </div>
                                            </div>

                                            <div id="showSubCategory">
                                                <div class="form-group">
                                                    <label class="col-form-label form-control-label">Subcategory</label>
                                                    <select class="form-control" size="0" id="subCategory" name="subCategory">
                                                    <option selected>Choose...</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-form-label form-control-label">Name</label>
                                                    <input type="text" class="form-control" id="contactName" name="contactName" placeholder="" value="" required>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-form-label form-control-label">Email</label>
                                                   <input type="text" class="form-control" id="contactEmail" name="contactEmail" placeholder="" value="">
                                            </div>
                                            <div class="form-group">
                                                <label class="col-form-label form-control-label">Phone</label>
                                                    <input type="text" class="form-control" id="telNo" name="telNo" placeholder="" value="" required>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-form-label form-control-label">Contact Level</label>
                                                <select class="form-control" size="0" id="contactLevel" name="contactLevel">
                                                <option value="">Choose...</option>
                                                <option value="1">Level 1</option>
                                                <option value="2">Level 2</option>
                                                <option value="3">Level 3</option>
                                                </select>
                                            </div>
                                            <div class="col-md-8 mb-2">
                                                <button type="submit" class="btn btn-secondary">Save</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                      
                </div>            
            </div>

            <div class="modal fade" id="delContact" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header" style="background: #dd4f43">
                    <h5 class="modal-title" id="exampleModalLabel">Warning</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                  <p>Are you sure you want to delete the <strong><span id="contactName"></span></strong> contact?</p>
                  </div>
                  <div class="modal-footer">
                    <div id="delBtn"></div>
                    <button type="button" class="btn btn-info" data-dismiss="modal">No</button>
                  </div>
                </div>
              </div>
            </div>
            <?php
            }
        ?>      
    </div>
</div>
