
<?php
include_once('dao/config/db.php');
	$qUser = "SELECT * FROM users";
	$getUsers = $con->prepare($qUser);
	$getUsers->execute();
?>

<table border="1" bordercolor="#ccc" id="agentDailyReport" style="width: 70%;" cellpadding="2">
	<tr style="font-size: 14px; background: #504c6a; color: #fff; text-align: center; padding: 7px;">
		<td>Name</td>
		<td>Employee No.</td>
		<td>Role</td>
		<td>Action</td>
	</tr>
<?php
while ( $user = $getUsers->fetch(PDO::FETCH_ASSOC)) {
if ($user['role'] == '1') {
	$role = 'Admin';
}elseif ($user['role'] == '2') {
	$role = 'Agent';
}
?>
	<tr>
		<td><?php echo $user['full_name']; ?></td>
		<td style="text-align: center;"><?php echo $user['empl_no']; ?></td>
		<td><?php echo $role; ?></td>
		<td style="text-align: center;"><a href="dashboard.php?section=user_edit&userId=<?php echo $user['empl_no'] ?>">Edit</a> | <button type="link" onclick="resetPassword('<?php echo $user['empl_no']; ?>')">Reset</button></td>
	</tr>
<?php
}
?>
</table>
<div id="resetResponse"></div>
