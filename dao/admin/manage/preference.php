<?php
include_once "../data/agent.php";

$type = $_GET['type'];
$selectId = $_GET['selectId'];

if ($type == 'subCat') {
?>
<div class="form-group"> 
    <label>Sub-Category</label> 
    <select id="subCat" name="subCat" class="form-control" onchange ="disDisposition()"> 
        <?php //echo subCategory($con,$selectId) ?>;
    </select>
    <span id="m-p-error" class="f-error"></span>
</div>
<?php
}elseif ($type == 'disp') {
?>
<div class="form-group"> 
    <label>Disposition</label> 
    <select id="disposition" name="disposition" class="form-control" onchange="disDispResponse();inputResponse();"> 
        <?php //echo disposition($con,$selectId) ?>;
    </select>
    <span id="m-p-error" class="f-error"></span>
</div>
<?php
}elseif ($type == 'dispResponse') {
?>
        <?php //echo dispResponse($con,$selectId) ?>

<?php
}elseif ($type == 'faqSubCat') {

?>
<div class="form-group"> 
    <label>Sub-Category</label> 
    <select id="faqSubCat" name="faqSubCat" class="form-control" onchange ="getFAQS()"> 
        <?php //echo subCategory($con,$selectId) ?>;
    </select>
    <span id="m-p-error" class="f-error"></span>
</div>
<?php
}
?>