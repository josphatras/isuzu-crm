<?php
include_once('../../config/db.php');
require_once('../../functions.php');

function escComments($escId){
    global $con;
    $sql = "SELECT ec.esc_id ,(SELECT first_name FROM users WHERE id = ec.user_id) first_name, (SELECT last_name FROM users WHERE id = ec.user_id) last_name, ec.comment FROM esc_comments ec WHERE esc_id = :escId";
    $getComments = $con->prepare($sql);
    $getComments->bindParam(':escId', $escId, PDO::PARAM_STR);
    $getComments->execute();
    $comments = $getComments->fetchAll(PDO::FETCH_ASSOC);  
    return($comments);
}
?>

<?php
$comments = escComments($_GET['escId']);
foreach ($comments as $comment) {

?>

<div style="margin-bottom: 5px; border-bottom: 1px solid #ccc">
<p><?php echo $comment['comment'] ?></p>
<p style="font-style: italic;"><strong>Comment from:</strong> <?php echo $comment['first_name'].' '.$comment['last_name']; ?></p>	
</div>

<?php

}

?>