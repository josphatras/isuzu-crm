<?php
//session_start();
$escId = $_GET['escId'];
include_once('../../config/db.php');
require_once('../../functions.php');
$contactTypes = allConTypes();
$categories = sel_Cat('msg');
$escCategories = sel_Cat('ticket');
$escStaus = escStatus();

if (isset($_SESSION["customerId"])) {
$customerId = $_SESSION["customerId"];
}elseif(isset($_GET['customerId'])) {
 $customerId = $_GET['customerId'];
}

$sqlesc = "SELECT a.id,a.msg_id,a.resolution,a.sla,a.escalated_to,a.`status`,
    a.resolution_date,a.created_on,a.status status_id,
    b.text,e.id cat_id,f.id sub_cat_id, (SELECT name FROM esc_contact WHERE id = a.escalated_to) escalated_usr,
    d.status_name,e.cat_name,f.sub_cat_name
    FROM tickets a
    LEFT JOIN messages b ON a.msg_id = b.id
    LEFT JOIN users c ON a.escalated_to = c.id
    LEFT JOIN ticket_status d ON a.`status` = d.status_id
    LEFT JOIN categories e ON a.cat_id = e.id
    LEFT JOIN sub_categories f ON a.sub_cat_id = f.id WHERE a.id = :escId";
$getesc=$con->prepare($sqlesc);
$getesc->bindParam(':escId', $escId, PDO::PARAM_STR);
$getesc->execute();
$esc = $getesc->fetch();
?>


<div class="container py-3">
    <div class="row">
        <div class="mx-auto col-sm-12">
	        <div class="card">
	            <div class="card-body">
					<form role="form" id="editesc">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group row">
								    <label class="col-lg-3 col-form-label form-control-label">Message</label>
								    <div class="col-lg-9">
								        <textarea class="form-control" id="msg" name="msg" rows="3" columns="8" disabled><?php echo $esc['text']; ?></textarea>
								    </div>
								</div>
								<div class="form-group row">
								    <label class="col-lg-3 col-form-label form-control-label">Escalate?</label>
								    <div class="col-lg-9">
								        <select class="form-control" size="0" name="escSelect" id="escSelect" onchange="escOption();" disabled>
											    <option value="yes" selected>Yes</option>
											    <option value="no">No</option>
								        </select>
								    </div>
								</div>
								<div id="escYes" style="">
									<div class="form-group row">
									    <label class="col-lg-3 col-form-label form-control-label">Category</label>
									    <div class="col-lg-9">
									        <select class="form-control" size="0" name="escCategory" id="escCategory" onchange="getSubCat('esc');" disabled>
										        <option selected value="<?php echo $esc['cat_id'] ?>"><?php echo $esc['cat_name'] ?></option>
										    <?php
										    	foreach ($escCategories as $esc_category) {
										    		echo '<option value="'.$esc_category['id'].'">'.$esc_category['cat_name'].'</option>';
										    	}
										    ?>
									        </select>
									    </div>
									</div>
									<div id="escSubCat">
										<div class="form-group row">
										    <label class="col-lg-3 col-form-label form-control-label">Subcategory</label>
										    <div class="col-lg-9">
										        <select class="form-control" size="0" name="escSubCategory" id="escSubCategory" disabled>
										        <option selected value="<?php echo $esc['sub_cat_id'] ?>"><?php echo $esc['sub_cat_name'] ?></option>
										        </select>
										    </div>
										</div>										
									</div>			
									<div class="form-group row">
									    <label class="col-lg-3 col-form-label form-control-label">Escalate to</label>
									    <div class="col-lg-9">
									        <select name="escTo" id="escTo" class="form-control" size="0" <?php if($esc['status_id'] == '3'){echo 'disabled';}  ?>>
										        <option selected value="<?php echo $esc['escalated_to'] ?>"><?php echo $esc['escalated_usr'] ?></option>
											    <?php
											    	$contacts = getContacts($esc['sub_cat_id']);
											    	foreach ($contacts as $contact) {
										            echo '<option value="'.$contact['id'].'">'.$contact['name']. ' - Level '.$contact['contact_level'].'</option>';
											    	}
											    ?>
									        </select>
									    </div>
									</div>
									<div class="form-group row">
									    <label class="col-lg-3 col-form-label form-control-label">Escalation Status</label>
									    <div class="col-lg-9">
									        <select class="form-control" size="0" id="escStatus" name="escStatus" <?php if($esc['status_id'] == '3'){echo 'disabled';}  ?> onchange="showResolution()">
										        <option selected value="<?php echo $esc['status_id'] ?>"><?php echo $esc['status_name'] ?></option>
									        <?php
									          foreach ($escStaus as $es) {
									            echo '<option value="'.$es['status_id'].'">'.$es['status_name'].'</option>';
									          }
									        ?>
									        </select>
									    </div>
									</div>
									<div id="resolutionSec" style="display: none;">
										<div class="form-group row">
										    <label class="col-lg-3 col-form-label form-control-label">Resolution</label>
										    <div class="col-lg-9">
										        <textarea class="form-control" id="resolution" name="resolution" rows="3" columns="8"></textarea>
										    </div>
										</div>										
									</div>
									<input type="hidden" name="escId" id="escId" value="<?php echo $esc['id']; ?>">
									<input type="hidden" name="msgId" id="msgId" value="<?php echo $esc['msg_id']; ?>">
									<input type="hidden" name="formType" value="escEdit">
									<?php if($esc['status_id'] !== '3'){
									?>
									<div class="col-md-8 mb-2" style="margin-top: 30px;" style="">
										<button type="button" class="btn btn-secondary" onclick="submitEscEdit();">Save</button>			
									</div>	
									<?php
									}
									?>
								</div>
							</div>
						</div>
					</form>
	            </div>
	        </div>  
        </div>
    </div>
</div>	