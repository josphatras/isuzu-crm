<?php 
include_once('dao/config/db.php');
if (isset($_GET['userId'])) {
	$userId = $_GET['userId'];
	$qUser = "SELECT * FROM users WHERE empl_no = :userId";
	$getUser = $con->prepare($qUser);
	$getUser->bindParam(':userId', $userId, PDO::PARAM_INT);
	$getUser->execute();
	$user = $getUser->fetch(PDO::FETCH_ASSOC);
	if ($user['role'] == '1') {
		$role = 'Admin';
	}elseif ($user['role'] == '2') {
		$role = 'Agent';
	}	
}

?>
<div class="col-md-6 col-sm-12">
	<div class="container-signup">
		<p><strong>Edit user</strong></p>
		<hr>
		<form action="dao/account.php" method="POST">
		  <div class="form-group">
		    <label for="loginuser">Full Names</label>
		    <input type="text" class="form-control" id="loginuser" name="fullNames" aria-describedby="emailHelp" value="<?php echo $user['full_name']; ?>" Names" required>
		  </div>
		  <div class="form-group">
		    <label for="loginuser">Employee No.</label>
		    <input type="text" class="form-control" id="loginuser" name="noupdate" aria-describedby="emailHelp" value="<?php echo $user['empl_no']; ?>">
		  </div>
		  <input type="hidden" name="employeeNo" value="<?php echo $user['empl_no']; ?>">
		  <div class="form-group">
		    <label for="loginuser">Email</label>
		    <input type="mail" class="form-control" id="loginuser" name="email" aria-describedby="emailHelp" value="<?php echo $user['email']; ?>" required>
		  </div>
		<div class="form-group">
	      <label class="mr-sm-2" for="inlineFormCustomSelect">Role</label>
	      <select class="custom-select mr-sm-1" id="inlineFormCustomSelect" name="userRole">
			<?php
				if ($user['role'] == '1') {
					echo "<option value='1' selected>Admin</option>";
					echo "<option value='2'>Agent</option>";
				}elseif ($user['role'] == '2') {
					echo "<option value='1'>Admin</option>";
					echo "<option value='2' selected>Agent</option>";
				}	
	        ?>
	      </select>
	    </div>
			<br>
		  <input type="hidden" name="form_type" value="update_user">
		  <button type="submit" class="btn btn-primary">Update</button>
		</form>	
	</div>
</div>