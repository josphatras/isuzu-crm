<?php
include_once('../config/db.php');
session_start();
require '../mail/class.phpmailer.php';
function Send_Mail1($to, $subj, $msg){
	$from = "no-reply@technobrainbpo.com";
	$mail = new PHPMailer();
	$mail->IsSMTP(true); // SMTP
	$mail->SMTPAuth   = true;  // SMTP authentication
	$mail->Mailer = "smtp";
	$mail->Host       = "mail.technobrainbpo.com"; // Techno Brain server,
	$mail->Port       = 26;                    // set the SMTP port
	$mail->Username   = "no-reply@technobrainbpo.com";  // SMTP  username
	$mail->Password   = "portal123";  //SMTP password
	$mail->SetFrom($from, 'ISUZU');
	$mail->AddReplyTo($from,'ISUZU');
	$mail->Subject = $subj;
	$mail->MsgHTML($msg);
	$address =explode(',',$to);
	foreach($address as $many)
	{
		$mail->AddAddress($many);
	}

	if(!$mail->Send())
		return false;
	else
		return true;
	}


$userId = $_SESSION["isuzu_user_id"];

if (isset($_GET['reset_password'])) {
$resetUserId = $_GET['reset_password'];
$qry = "SELECT * FROM users WHERE id = :userId";
$getUser = $con->prepare($qry);
$getUser->bindParam(':userId', $resetUserId, PDO::PARAM_INT);
$getUser->execute();
$user = $getUser->fetch();

$resetPassword = time();
$password = md5($resetPassword);
	$postQuery = "UPDATE users SET password = :password WHERE id = :resetUserId";
		$postValue = $con->prepare($postQuery);
		$postValue->bindParam(':resetUserId', $resetUserId, PDO::PARAM_INT);
		$postValue->bindParam(':password', $password, PDO::PARAM_STR);
		$postValue->execute();

		$to = $user['email'];
		$subj = 'Password successfully reset!';
		$msg = 'Account Details:<br><br>';
		$msg .= 'Account Name	:'.$user['first_name'].' '.$user['last_name'].'<br><br>';
		$msg .= 'Username	:'.$user['email'].'<br><br>';
		$msg .= 'Password	:'.$resetPassword.'<br><br>';
		$msg .= 'Sincerely,<br>';
		$msg .= 'ISUZU Account Manager.<br>';
		$msg .= '*Login and change your password<br>';

		Send_Mail1($user['email'], $subj, $msg);
		echo "<div class='resetResponse' style='color:#008000; font-size:18px;'>Password successfuly reset</div>";
}

if (isset($_POST['reset_password'])) {
	$old = $_POST['oldPassword'];
	$new = $_POST['newPassword'];

	$oldPassword = md5($old);
	$newPassword = md5($new);

	$qgetUser = "SELECT * FROM users WHERE id = :userId";
	$getUser = $con->prepare($qgetUser);
	$getUser->bindParam(':userId', $userId, PDO::PARAM_STR);
	$getUser->execute();
	$user = $getUser->fetch();

	if ($user['password'] == $oldPassword) {
		$postQuery = "UPDATE users SET password = :password WHERE id = :userId";
		$postValue = $con->prepare($postQuery);
		$postValue->bindParam(':userId', $userId, PDO::PARAM_INT);
		$postValue->bindParam(':password', $newPassword, PDO::PARAM_STR);
		$postValue->execute();
			header("location: ../../dashboard.php?content=account&reset_response=success");
	}else{
		header("location: ../../dashboard.php?content=account&reset_response=fail");
	}

}
?>