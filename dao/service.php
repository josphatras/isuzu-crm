<?php
include_once('../dao/config/include.php');
include_once('../dao/config/db.php');
session_start();
$form = $_POST['form_type'];
$userId = $_SESSION['isuzu_user_id'];

switch ($form) {
	case 'new_service':
		newService();
		break;
	case 'delete_service':
		deleteService();
		break;

	default:
		# code...
		break;
}

function newService(){
	global $con;
	global $userId;
	if ($_POST['vehicle'] == '') {
		die(json_encode(array('status' => 'fail', 'msg' => 'Vehicle not selected')));
	}
	$qry = "INSERT INTO services(user_id, bk_branch_id, dealer_name, transaction_type, vehicle_id) VALUES(:userId,:branch, :dealer, :transaction, :vehicle)";
	$stmt = $con->prepare($qry);
	$stmt->bindParam(':userId',$userId, PDO::PARAM_INT);
	$stmt->bindParam(':branch',$_POST['branch'], PDO::PARAM_INT);
	$stmt->bindParam(':dealer',$_POST['dealer'], PDO::PARAM_INT);
	$stmt->bindParam(':transaction',$_POST['transaction_type'], PDO::PARAM_INT);
	$stmt->bindParam(':vehicle',$_POST['vehicle'], PDO::PARAM_INT);
	$stmt->execute();
	$service = $con->lastInsertId();

	if ($_POST['transaction_type'] == '1') {
		$qry = "INSERT INTO service_sales(service_id, sale_date, delivery_date) VALUES(:service, :saleDate, :deliveryDate)";
		$stmt = $con->prepare($qry);
		$stmt->bindParam(':service',$service, PDO::PARAM_STR);
		$stmt->bindParam(':saleDate',$_POST['date_of_sale'], PDO::PARAM_STR);
		$stmt->bindParam(':deliveryDate',$_POST['delivery_date'], PDO::PARAM_STR);
		$stmt->execute();
	}else if ($_POST['transaction_type'] == '2') {
		$qry = "INSERT INTO service_v_services(service_id, service_advisor, receieve_date, release_date, wrkord_no, work_scope, driver_name, driver_phone) VALUES(:service,:advisor,:dateReceived,:dateReleased,:wrkordno,:workScope,:driver, :driverPhone)";
		$stmt = $con->prepare($qry);
		$stmt->bindParam(':service',$service, PDO::PARAM_STR);
		$stmt->bindParam(':advisor',$_POST['service_advisor'], PDO::PARAM_STR);
		$stmt->bindParam(':dateReceived',$_POST['date_received'], PDO::PARAM_STR);
		$stmt->bindParam(':dateReleased',$_POST['date_received'], PDO::PARAM_STR);
		$stmt->bindParam(':wrkordno',$_POST['wrkordno'], PDO::PARAM_STR);
		$stmt->bindParam(':workScope',$_POST['work_scope'], PDO::PARAM_STR);
		$stmt->bindParam(':driver',$_POST['driver'], PDO::PARAM_STR);
		$stmt->bindParam(':driverPhone',$_POST['driver_phone'], PDO::PARAM_STR);
		$stmt->execute(); 
	}

	// id 	 	created_on

		// id 	service_id 	receieve_date 	release_date 	wrkord_no 	work_scope 	vehicle_id 	driver_name 	created_on  


		
	echo json_encode(array('status' => 'success', 'msg' => 'Survey successfuly saved' ));
}

function deleteService(){
	global $con;
	$serviceId = $_POST['service_id'];

	$qry = "DELETE FROM services WHERE id = :serviceId";
	$delCat = $con->prepare($qry);
	$delCat->bindParam(':serviceId',$serviceId, PDO::PARAM_STR);
	$delCat->execute();

	$qry = "DELETE FROM service_sales WHERE service_id = :serviceId";
	$delCat = $con->prepare($qry);
	$delCat->bindParam(':serviceId',$serviceId, PDO::PARAM_STR);
	$delCat->execute();

	$qry = "DELETE FROM service_v_services WHERE service_id = :serviceId";
	$delCat = $con->prepare($qry);
	$delCat->bindParam(':serviceId',$serviceId, PDO::PARAM_STR);
	$delCat->execute();

	echo json_encode(array('status' => 'success', 'msg' => 'Successfuly deleted' ));
}

?>