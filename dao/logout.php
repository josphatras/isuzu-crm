<?php
session_start();
unset($_SESSION["isuzu_user_id"]);
unset($_SESSION["isuzu_user_name"]);
unset($_SESSION["isuzu_role"]);
unset($_SESSION["isuzu_dealer"]);
header('Location:login.php');
?>