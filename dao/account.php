<?php
include_once('../dao/config/include.php');
include_once('../dao/config/db.php');
session_start();
if (isset($_SESSION["isuzu_user_id"])) {
	$userId = $_SESSION["isuzu_user_id"];
}
require 'mail/class.phpmailer.php';
function Send_Mail1($to, $subj, $msg){
	$from = "no-reply@technobrainbpo.com";
	$mail = new PHPMailer();
	$mail->IsSMTP(true); // SMTP
	$mail->SMTPAuth   = true;  // SMTP authentication
	$mail->Mailer = "smtp";
	$mail->Host       = "mail.technobrainbpo.com"; // Techno Brain server,
	$mail->Port       = 26;                    // set the SMTP port
	$mail->Username   = "no-reply@technobrainbpo.com";  // SMTP  username
	$mail->Password   = "portal123";  //SMTP password
	$mail->SetFrom($from, 'ISUZU CONTACT CENTER');
	$mail->AddReplyTo('contactcenter@isuzu.co.ke','ISUZU CONTACT CENTER');
	$mail->AddCC('contactcenter@isuzu.co.ke');
	$mail->Subject = $subj;
	$mail->MsgHTML($msg);
	$address =explode(',',$to);
	foreach($address as $many)
	{
		$mail->AddAddress($many);
	}

	if(!$mail->Send())
		return false;
	else
		return true;
	}


if (isset($_POST['form_type'])) {
$formType = $_POST['form_type'];
if ($formType == 'login') {
	$username = $_POST['username'];
	$password = md5($_POST['password']);
	$userQuery = "SELECT * FROM users WHERE (email = :username) AND status = '1' AND password = :password";
	$userlog = $con->prepare($userQuery);
	$userlog->bindParam(':username', $username, PDO::PARAM_STR);
	$userlog->bindParam(':password', $password, PDO::PARAM_STR);
	$userlog->execute();
	$userDetail = $userlog->fetch();

	$userId = $userDetail['id'];
	$rolesqry = "SELECT role_id FROM user_roles WHERE user_id = :userId";
	$getRoles = $con->prepare($rolesqry);
	$getRoles->bindParam(':userId', $userId, PDO::PARAM_STR);
	$getRoles->execute();
	$roles = $getRoles->fetchAll(PDO::FETCH_ASSOC);

	if ($userlog->rowCount() > 0) {
		$_SESSION["isuzu_user_id"]=$userDetail['id'];
		$_SESSION["isuzu_user_name"]=$userDetail['first_name'];
		$_SESSION["isuzu_role"]=$roles;
		header('Location: /'.ROOT_FOLDER.'dashboard.php?content=ticket');
	} else {
		header('Location: /'.ROOT_FOLDER.'dao/login.php?res_message=Wrong username or password');
	}
}elseif ($formType == 'newUser') {
	$firstName = $_POST['firstName'];
	$lastName = $_POST['lastName'];
	$email = $_POST['email'];
	$password = md5($_POST['password']);
	$postQuery = "INSERT INTO users(created_by, first_name, last_name, email, password, status) VALUES (:createdBy, :firstName, :lastName, :email, :password, '1')";
		$postValue = $con->prepare($postQuery);
		$postValue->bindParam(':createdBy', $userId, PDO::PARAM_STR);
		$postValue->bindParam(':firstName', $firstName, PDO::PARAM_STR);
		$postValue->bindParam(':lastName', $lastName, PDO::PARAM_STR);
		$postValue->bindParam(':email', $email, PDO::PARAM_STR);
		$postValue->bindParam(':password', $password, PDO::PARAM_STR);
		$postValue->execute();
		$newUserId = $con->lastInsertId();

		if (isset($_POST['agent'])) {
			if ($_POST['agent'] !== '') {
				$roles[] = $_POST['agent'];
			}
		}
		if (isset($_POST['admin'])) {
			if ($_POST['admin'] !== '') {
				$roles[] = $_POST['admin'];
			}
		}
		if (isset($_POST['manager'])) {
			if ($_POST['manager'] !== '') {
				$roles[] = $_POST['manager'];
			}
		}
		if (isset($_POST['advisor'])) {
			if ($_POST['advisor'] !== '') {
				$roles[] = $_POST['advisor'];
			}
		}
		if (isset($_POST['dealer'])) {
			if ($_POST['dealer'] !== '') {
				$roles[] = $_POST['dealer'];
				$qry = "INSERT INTO dealer_users(bk_dealer_id, user_id) VALUES (:dealer, :userId)";
				$stmt = $con->prepare($qry);
				$stmt->bindParam(':userId', $newUserId, PDO::PARAM_STR);
				$stmt->bindParam(':dealer', $_POST['dealer_id'], PDO::PARAM_STR);
				$stmt->execute();
			}
		}

		foreach ($roles as $role) {
			$roleqry = "INSERT INTO user_roles(user_id, role_id) VALUES (:userId, :roleId)";
			$assignRoles = $con->prepare($roleqry);
			$assignRoles->bindParam(':userId', $newUserId, PDO::PARAM_STR);
			$assignRoles->bindParam(':roleId', $role, PDO::PARAM_STR);
			$assignRoles->execute();
		}
		$to = $email;
		$subj = 'Isuzu Account Created';
		$msg = 'Dear '.$firstName.' ,<br><br>';
		$msg .= 'Your account has been successfully created. Kindly use below information to access.<br>';
		$msg .= 'Account Details:<br><br>';
		$msg .= 'Account Name	:'.$firstName.' '.$lastName.'<br><br>';
		$msg .= 'Username	:'.$email.'<br><br>';
		$msg .= 'Password	:'.$_POST['password'].'<br><br>';
		$msg .= 'Sincerely,<br>';
		$msg .= 'Isuzu Contact Center Team <br>';
		$msg .= 'Contact Center Phone no: 0800724724 <br><br><br>';

		Send_Mail1($to, $subj, $msg);
		header('Location: /'.ROOT_FOLDER.'dashboard.php?content=users&message=User successfuly created');
		
}elseif ($formType == 'update_user') {
	$userId = $_POST['userId'];
	$firstName = $_POST['firstName'];
	$lastName = $_POST['lastName'];
	$email = $_POST['email'];
	$postQuery = "UPDATE users SET first_name = :firstName, last_name = :lastName, email = :email WHERE id = :userId";
		$postValue = $con->prepare($postQuery);
		$postValue->bindParam(':userId', $userId, PDO::PARAM_STR);
		$postValue->bindParam(':firstName', $firstName, PDO::PARAM_STR);
		$postValue->bindParam(':lastName', $lastName, PDO::PARAM_STR);
		$postValue->bindParam(':email', $email, PDO::PARAM_STR);
		$postValue->execute();

		if (isset($_POST['Agent'])) {
			if ($_POST['Agent'] !== '') {
				$roles[] = $_POST['Agent'];
			}
		}
		if (isset($_POST['Admin'])) {
			if ($_POST['Admin'] !== '') {
				$roles[] = $_POST['Admin'];
			}
		}
		if (isset($_POST['Manager'])) {
			if ($_POST['Manager'] !== '') {
				$roles[] = $_POST['Manager'];
			}
		}
		if (isset($_POST['Service Advisor'])) {
			if ($_POST['Service Advisor'] !== '') {
				$roles[] = $_POST['Service Advisor'];
			}
		}
		if (isset($_POST['Advisor'])) {
			if ($_POST['Advisor'] !== '') {
				$roles[] = $_POST['Advisor'];
			}
		}
		if (isset($_POST['Dealer'])) {
			if ($_POST['Dealer'] !== '') {
				$roles[] = $_POST['Dealer'];
				$qry = "INSERT INTO dealer_users(bk_dealer_id, user_id) VALUES (:dealer, :userId)";
				$stmt = $con->prepare($qry);
				$stmt->bindParam(':userId', $userId, PDO::PARAM_STR);
				$stmt->bindParam(':dealer', $_POST['Dealer'], PDO::PARAM_STR);
				$stmt->execute();
			}
		}

		if (!isset($roles)) {
			header('Location: /'.ROOT_FOLDER.'dashboard.php?content=users&user_section=edit&userId='.$userId.'&message=Please%20select%20atleast%20one%20role');
			die();
		}

		$roleqry = "DELETE FROM user_roles WHERE user_id = :userId";
		$assignRoles = $con->prepare($roleqry);
		$assignRoles->bindParam(':userId', $userId, PDO::PARAM_STR);
		$assignRoles->execute();

		foreach ($roles as $role) {
			$roleqry = "INSERT INTO user_roles(user_id, role_id) VALUES (:userId, :roleId)";
			$assignRoles = $con->prepare($roleqry);
			$assignRoles->bindParam(':userId', $userId, PDO::PARAM_STR);
			$assignRoles->bindParam(':roleId', $role, PDO::PARAM_STR);
			$assignRoles->execute();
		}
		header('Location: /'.ROOT_FOLDER.'dashboard.php?content=users&message=User successfuly update');
}else if ($formType == 'delete_customer') {
	$customerId = $_POST['customer_id'];

	$qry = "DELETE FROM users WHERE id = :customerId";
	$stmt = $con->prepare($qry);
	$stmt->bindParam(':customerId',$customerId, PDO::PARAM_STR);
	$stmt->execute();

	$qry = "DELETE FROM vehicle_info WHERE customer_id = :customerId";
	$stmt = $con->prepare($qry);
	$stmt->bindParam(':customerId',$customerId, PDO::PARAM_STR);
	$stmt->execute();

	$qry = "DELETE FROM customer_info WHERE customer_id = :customerId";
	$stmt = $con->prepare($qry);
	$stmt->bindParam(':customerId',$customerId, PDO::PARAM_STR);
	$stmt->execute();

	echo json_encode(array('status' => 'success', 'msg' => 'Successfuly deleted' ));
}

}


?>