<?php
include_once('../dao/config/include.php');
include_once('../dao/config/db.php');
include_once('esc_contact.php');
include_once('sms.php');

if ($_POST['formType'] == 'dir_int') {
	$contactType = $_POST['contactType'];
	$category = $_POST['intCategory'];
	$subCategory = $_POST['intSubCategory'];
	$customerId = $_POST['customerId'];
	$msg = $_POST['msg'];
	if (isset($_POST['complain'])) {
		$complain = $_POST['complain'];
	}else{
		$complain = '';
	}

	$qnewmsg = "INSERT INTO messages(customer_id, complain, agent_id, contact_type, cat_id, sub_cat_id, text) VALUES (:customerId, :complain, :agentId, :contactType,:category, :subCategory, :msg)";
	$newmsg = $con->prepare($qnewmsg);
	$newmsg->bindParam(':customerId',$customerId, PDO::PARAM_STR);
	$newmsg->bindParam(':agentId',$userId, PDO::PARAM_STR);
	$newmsg->bindParam(':contactType',$contactType, PDO::PARAM_STR);
	$newmsg->bindParam(':category',$category, PDO::PARAM_STR);
	$newmsg->bindParam(':subCategory',$subCategory, PDO::PARAM_STR);
	$newmsg->bindParam(':complain',$complain, PDO::PARAM_STR);
	$newmsg->bindParam(':msg',$msg, PDO::PARAM_STR);
	$newmsg->execute();
}


if ($_POST['formType'] == 'dir_esc') {
	$contactType = '2';
	$category = $_POST['category'];
	$subCategory = $_POST['escSubCategory'];
	$customerId = $_POST['customerId'];
	$sla = $_POST['sla'];
	$msg = $_POST['msg'];
	$escTo = $_POST['escTo'];
	$msg = $_POST['msg'];
	if (isset($_POST['complain'])) {
		$complain = $_POST['complain'];
	}else{
		$complain = '';
	}

	$qnewmsg = "INSERT INTO messages(customer_id, complain, agent_id, contact_type, cat_id, sub_cat_id, text) VALUES (:customerId, :complain, :agentId, :contactType,:category, :subCategory, :msg)";
	$newmsg = $con->prepare($qnewmsg);
	$newmsg->bindParam(':customerId',$customerId, PDO::PARAM_STR);
	$newmsg->bindParam(':agentId',$userId, PDO::PARAM_STR);
	$newmsg->bindParam(':contactType',$contactType, PDO::PARAM_STR);
	$newmsg->bindParam(':category',$category, PDO::PARAM_STR);
	$newmsg->bindParam(':subCategory',$subCategory, PDO::PARAM_STR);
	$newmsg->bindParam(':complain',$complain, PDO::PARAM_STR);
	$newmsg->bindParam(':msg',$msg, PDO::PARAM_STR);
	$newmsg->execute();
	$msgId = $con->lastInsertId();

	$qnewEsc = "INSERT INTO tickets(msg_id,cat_id, sub_cat_id, escalated_to, status) VALUES (:msgId,:cat, :subCat, :escTo, '0')";
	$insertEsc = $con->prepare($qnewEsc);
	$insertEsc->bindParam(':cat',$category, PDO::PARAM_STR);
	$insertEsc->bindParam(':subCat',$subCategory, PDO::PARAM_STR);
	$insertEsc->bindParam(':escTo',$escTo, PDO::PARAM_STR);
	$insertEsc->bindParam(':msgId',$msgId, PDO::PARAM_STR);
	$insertEsc->execute();
	$ticketNo = $con->lastInsertId();

	$qry = "SELECT c.telephone_no, c.company_name, u.email, u.first_name, u.last_name FROM users u LEFT JOIN customer_info c ON c.customer_id = u.id WHERE u.id = :customerId";
	$getCustomer = $con->prepare($qry);
	$getCustomer->bindParam(':customerId',$customerId, PDO::PARAM_STR);
	$getCustomer->execute();
	$customer = $getCustomer->fetch();

	$qry = "SELECT * FROM esc_contact WHERE id = :escTo";
	$escContact = $con->prepare($qry);
	$escContact->bindParam(':escTo',$escTo, PDO::PARAM_STR);
	$escContact->execute();	
	$contact = $escContact->fetch();

	$qry = "SELECT c.cat_name, sc.sub_cat_name FROM  sub_categories sc INNER JOIN categories c ON sc.cat_id = c.id WHERE sc.id = :subCategory";
	$escCategory = $con->prepare($qry);
	$escCategory->bindParam(':subCategory',$subCategory, PDO::PARAM_STR);
	$escCategory->execute();	
	$e_category = $escCategory->fetch();
		$to = $contact['email'];
		$subj = 'CONTACT CENTRE CRM ESCALATION';
		$mail_msg = 'Dear '.$contact['name'].' ,<br><br>';
		$mail_msg .= 'The below issue has been escalated to you. Kindly action and respond within 12hours. Details:<br><br><br>';
		$mail_msg .= 'Customer Id	: '.$customerId.'<br><br>';
		$mail_msg .= 'Customer Name	: '.$customer['first_name'].' '.$customer['last_name'].'<br><br>';
		$mail_msg .= 'Phone No.	: '.$customer['telephone_no'].'<br><br>';
		$mail_msg .= 'Company	: '.$customer['company_name'].'<br><br>';
		$mail_msg .= 'Email	: '.$customer['email'].'<br><br>';
		$mail_msg .= 'Category	: '.$e_category['cat_name'].'<br><br>';
		$mail_msg .= 'Sub Category	: '.$e_category['sub_cat_name'].'<br><br>';
		$mail_msg .= 'Message	: '.$msg.'<br><br>';
		$mail_msg .= 'Sincerely,<br>';
		$mail_msg .= 'Isuzu Contact Center Team <br>';
		$mail_msg .= 'Contact Center Phone no: 0800724724 <br><br><br>';
		$mail_msg .= '* Please respond within 12hours. Thank you';
		escalationMail($to, $subj, $mail_msg);

		$smsCustomerNumber = $customer['telephone_no'];

		if ($smsCustomerNumber !== '' && is_numeric($smsCustomerNumber)) {
			$msgText = 'Dear Valued Customer, thank you for contacting us. Your query has been escalated. Your ticket id is '.$ticketNo.'. You will receive feedback within 24hours. Thank you';
			sendSMS($smsCustomerNumber, $msgText);
		}

		if ($contact['phone'] !== '' && is_numeric($contact['phone'])) {
			$msgText = 'Dear '.$contact['name'].', you have received a customer query on your email for your prompt action. Please respond within 12hours. Thank you';
			sendSMS($contact['phone'], $msgText);
		}
}
?>