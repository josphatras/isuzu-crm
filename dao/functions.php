<?php

function allConTypes() {
	global $con;
	$qallContactTypes = "SELECT * FROM contact_type";
	$getcontactTypes = $con->prepare($qallContactTypes);
	$getcontactTypes->execute();
	$contactTypes = $getcontactTypes->fetchAll(PDO::FETCH_ASSOC);	
	return($contactTypes);
}
function sel_Cat($item){
	global $con;
	$qselCat = "SELECT id,item,cat_name FROM categories WHERE item = :item AND visible = '1'";
	$getSelCat = $con->prepare($qselCat );
	$getSelCat->bindParam(':item', $item, PDO::PARAM_STR);
	$getSelCat->execute();
	$categories = $getSelCat->fetchAll(PDO::FETCH_ASSOC);
	return($categories);
}

function sel_Sub_Cat($item){
    global $con;
    $qselCat = "SELECT id,item,cat_name FROM categories WHERE item = :item AND visible = '1'";
    $getSelCat = $con->prepare($qselCat );
    $getSelCat->bindParam(':item', $item, PDO::PARAM_STR);
    $getSelCat->execute();
    $categories = $getSelCat->fetchAll(PDO::FETCH_ASSOC);
    return($categories);
}

function isuzu_users(){
	global $con;
	$qisuzuUsers = "SELECT id,first_name,last_name FROM users WHERE role = '1' OR role = '2'";
	$getIsuzuUsers = $con->prepare($qisuzuUsers);
	$getIsuzuUsers->execute();
	$isuzuUsers = $getIsuzuUsers->fetchAll(PDO::FETCH_ASSOC);
	return($isuzuUsers);	
}

function esc_contact_subcat($catId){

}
function isuzu_esc_contacts($subCat){
    global $con;
    $qisuzuUsers = "SELECT id,first_name,last_name FROM users WHERE role = '1' OR role = '2'";
    $getIsuzuUsers = $con->prepare($qisuzuUsers);
    $getIsuzuUsers->execute();
    $isuzuUsers = $getIsuzuUsers->fetchAll(PDO::FETCH_ASSOC);
    return($isuzuUsers);    
}


function vehicles($customerId){
    global $con;
    $qry = "SELECT v.id, (SELECT type FROM v_make WHERE v_make.id = v.make) make, (SELECT model_name FROM v_model WHERE v_model.id = v.model) model, v.reg_no, v.vin_no FROM vehicle_info v WHERE customer_id = :customerId";
    $stmt = $con->prepare($qry);
    $stmt->bindParam(':customerId', $customerId, PDO::PARAM_STR);
    $stmt->execute();
    $vehicles = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return($vehicles); 
}

function vehicle($id){
    global $con;
    $qry = "SELECT v.id, v.customer_id, (SELECT type FROM v_make WHERE v_make.id = v.make) v_make, (SELECT model_name FROM v_model WHERE v_model.id = v.model) v_model, v.reg_no, v.vin_no FROM vehicle_info v WHERE v.id = :id";
    $stmt = $con->prepare($qry);
    $stmt->bindParam(':id', $id, PDO::PARAM_STR);
    $stmt->execute();
    $vehicle = $stmt->fetch();
    return($vehicle);
}

function veh_make(){
	global $con;
	$qVehMake = "SELECT id,type FROM v_make";
	$getvehMake = $con->prepare($qVehMake );
	$getvehMake->execute();
	$vehMake = $getvehMake->fetchAll(PDO::FETCH_ASSOC);
	return($vehMake);
}

function veh_model($makeId){
	global $con;
	$qVehModel = "SELECT id,model_name FROM v_model WHERE make_id = :makeId";
	$getVehModel = $con->prepare($qVehModel);
	$getVehModel->bindParam(':makeId', $makeId, PDO::PARAM_STR);
	$getVehModel->execute();
	$vehModels = $getVehModel->fetchAll(PDO::FETCH_ASSOC);
	return($vehModels);
}

function model($modelId){
    global $con;
    $qry = "SELECT id,model_name FROM v_model WHERE id = :modelId";
    $stmt = $con->prepare($qry);
    $stmt->bindParam(':modelId', $modelId, PDO::PARAM_STR);
    $stmt->execute();
    $vehModel = $stmt->fetch();
    return($vehModel);
}

function veh_body_types(){
	global $con;
	$qVehBodyType = "SELECT id,body_type FROM v_body_type";
	$getvehBodyType = $con->prepare($qVehBodyType);
	$getvehBodyType->execute();
	$vehBodyType = $getvehBodyType->fetchAll(PDO::FETCH_ASSOC);
	return($vehBodyType);	
}

function allClients() {
	global $con;
	$qcustomers = "SELECT u.id user_id, u.first_name, u.last_name, u.email, c.company_name, c.telephone_no, c.business_type, c.physical_address, c.town FROM users u INNER JOIN customer_info c ON u.id = c.customer_id WHERE u.role = '3'";
	$getCustomers = $con->prepare($qcustomers);
	$getCustomers->execute();
	$customers = $getCustomers->fetchAll(PDO::FETCH_ASSOC);
	return($customers);
}

function sel_customer($customerId){
	global $con;
	$qcustomer = "SELECT u.id user_id, u.first_name, u.last_name, u.email, c.company_name, c.telephone_no, c.business_type, c.physical_address, c.town, (SELECT type FROM v_make WHERE v_make.id = v.make) v_make, (SELECT model_name FROM v_model WHERE v_model.id = v.model) v_model,v.reg_no, v.reg_no, v.vin_no, v.body_type, v.application, v.fleet_size FROM users u INNER JOIN customer_info c ON u.id = c.customer_id LEFT JOIN vehicle_info v ON v.customer_id = u.id WHERE u.id = :customerId ";
	$getCustomer = $con->prepare($qcustomer);
	$getCustomer->bindParam(':customerId', $customerId, PDO::PARAM_STR);
	$getCustomer->execute();
	$customer = $getCustomer->fetch();
	return($customer);
}

function dealerCustomers($dealerId){
    global $con;
    $sql = "SELECT u.id AS customer_id,CONCAT(u.first_name,' ',u.last_name) AS customer_name, u.email, c.company_name, c.telephone_no, c.business_type, c.physical_address, c.town
        FROM users u
        INNER JOIN customer_info c ON u.id = c.customer_id
        LEFT JOIN dealer_users du ON du.user_id = u.created_by 
        WHERE role = 3 AND du.bk_dealer_id = '$dealerId'";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $customers = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return($customers);  
}

function escStatus(){
	global $con;
	$qticketStatus = "SELECT ts.status_id, ts.status_name FROM ticket_status ts";
	$getTicketStatus = $con->prepare($qticketStatus);
	$getTicketStatus->execute();
	$ticketStatus = $getTicketStatus->fetchAll(PDO::FETCH_ASSOC);
	return($ticketStatus);	
}



//::::::::::::::::::::::::::::::::::::: SETTINGS :::::::::::::::::::::::::::::::::::::::::
function getContact() {
	global $con;
    $sql = "SELECT id, name FROM contact_type";
    $getContacts = $con->prepare($sql);
    $getContacts->execute();
    $contactCount = $getContacts->rowCount();
    if ($contactCount > 0) {
        while($rs = $getContacts->fetch(PDO::FETCH_ASSOC)){
        	echo "<tr>";
            echo " <td>" . $rs['id'] . "</td>\n";
            echo " <td>" . $rs['name'] . "</td>\n";
        	echo "</tr>";
        }
    }else{
        //echo "no records";
    }
}


function getCat(){
	global $con;
    $sql = "SELECT id, item, cat_name, visible FROM categories";
    $getCategories = $con->prepare($sql);
    $getCategories->execute();
    $categoriesCount = $getCategories->rowCount();
    if ($categoriesCount > 0) {
        while($rs = $getCategories->fetch(PDO::FETCH_ASSOC)){
            if ($rs['item'] == 'msg') {
                $catType = 'Interaction';
            }elseif ($rs['item'] == 'ticket') {
                $catType = 'Escalation';
            }
            if ($rs['visible'] == '1') {
                $visibility = '<span class="fa fa-eye" style="font-size:20px"></span>';
            }else if($rs['visible'] == '2'){
                $visibility = '<span class="fa fa-eye-slash" style="font-size:20px; color: #6c757d"></span>';
            }
        	echo "<tr>";
            echo " <td>" . $rs['id'] . "</td>\n";
            echo " <td>" . $catType . "</td>\n";
            echo " <td>" . $rs['cat_name'] . "</td>\n";
            echo " <td><div id='cat".$rs['id']."' style='cursor: pointer;' onclick='catVisibility(".$rs['visible'].','.$rs['id'].")'>".$visibility."</div></td>\n";
            echo " <td style='text-align:center;'>
                <a class='btn btn-secondary faq-btn btn-sm' href='dao/process.php?delete_category=". $rs['id']."' title='Edit'><i class='    fa fa-trash-o'></i></a>  
                </td>";
        	echo "</tr>";
        }
    }else{
        //echo "no records";
    }    
}
function getSubCat(){
    global $con;
    $sql = "SELECT sc.id, sc.cat_id, sc.sub_cat_name, c.item, c.cat_name, sc.visible FROM sub_categories sc INNER JOIN categories c ON c.id = sc.cat_id";
    $getSubCategories = $con->prepare($sql);
    $getSubCategories->execute();
    $subCategoriesCount = $getSubCategories->rowCount();
    if ($subCategoriesCount > 0) {
        while($rs = $getSubCategories->fetch(PDO::FETCH_ASSOC)){
            if ($rs['item'] == 'msg') {
                $catType = 'Interaction';
            }elseif ($rs['item'] == 'ticket') {
                $catType = 'Escalation';
            }
            if ($rs['visible'] == '1') {
                $visibility = '<span class="fa fa-eye" style="font-size:20px"></span>';
            }else if($rs['visible'] == '2'){
                $visibility = '<span class="fa fa-eye-slash" style="font-size:20px; color: #6c757d"></span>';
            }else{
                $visibility = 'hello';
            }
            echo "<tr>";
            echo " <td>" . $rs['id'] . "</td>\n";
            echo " <td>" . $catType . "</td>\n";
            echo " <td>" . $rs['cat_name'] . "</td>\n";
            echo " <td>" . $rs['sub_cat_name'] . "</td>\n";
            echo " <td><div id='subCat".$rs['id']."' style='cursor: pointer;' onclick='subCatVisibility(".$rs['visible'].','.$rs['id'].")'>".$visibility."</div></td>\n";
            echo " <td style='text-align:center;'>
                <a class='btn btn-secondary faq-btn btn-sm' href='dao/process.php?delete_sub_category=". $rs['id']."' title='Edit'><i class='    fa fa-trash-o'></i></a>  
                </td>";
            echo "</tr>";
        }
    }else{
        //echo "no records";
    }
}

function getScriptGuide() {
    global $con;
    $sql = "SELECT g.id, g.guide_text FROM guide g";
    $getGuideScript = $con->prepare($sql);
    $getGuideScript->execute();
    $guideScript = $getGuideScript->fetchAll(PDO::FETCH_ASSOC);
	foreach ($guideScript as $guide) {
        $guideId = $guide['id'];
        $sql = "SELECT  sc.sub_cat_name,sc.visible sub_cat_visible, c.cat_name, c.visible cat_visible FROM script_guide sg INNER JOIN sub_categories sc ON sg.sub_cat_id = sc.id INNER JOIN categories c ON sc.cat_id = c.id WHERE sg.guide_id = :guideId";
        $getGuideCat = $con->prepare($sql);
        $getGuideCat->bindParam(':guideId', $guideId, PDO::PARAM_STR);
        $getGuideCat->execute();
        $guideCats = $getGuideCat->fetchAll(PDO::FETCH_ASSOC);
        $guideCatViews = '';
        foreach ($guideCats as $guideCat) {
            if ($guideCat['cat_visible'] == '1' && $guideCat['sub_cat_visible'] == '1') {
                $scriptVisible = '<span class="badge badge-success">Visible</span>';
            }else{
                $scriptVisible = '<span class="badge badge-danger">Not visible</span>';
            }
            $guideCatViews .= $guideCat['cat_name']." >> ".$guideCat['sub_cat_name']."<br>".$scriptVisible."<br><hr>";
        }
            echo "<tr>";
            echo " <td>" . $guide['id'] . "</td>\n";
            echo " <td>" . $guide['guide_text'] . "</td>\n";
            echo " <td>".$guideCatViews."</td>\n";
            echo " <td style='text-align:center;'>
                <button class='btn btn-secondary faq-btn btn-sm' onclick='delScriptModel(".$guide['id'].','.$guide['id'].")' ><i class='fa fa-trash-o'></i> Delete</button>  
                </td>";
            echo "</tr>";
	}
}
//:::::::::::::::::::::::::::::::::::::::::::: CONTACT SUB CATEGORIES ::::::::::::::::::::::::::

function getContactSubCat ($contactCatId){
    global $con;
    $sql = "SELECT contact_name FROM esc_contact WHERE cat_id = :catId GROUP BY contact_name";
    $getContactSubCat = $con->prepare($sql);
    $getContactSubCat->bindParam(':catId', $contactCatId, PDO::PARAM_STR);
    $getContactSubCat->execute();
    $contactSubCat = $getContactSubCat->fetchAll(PDO::FETCH_ASSOC); 
    return($contactSubCat);       
}
function getSelectedContact ($contactCatId){
    global $con;
    $sql = "SELECT id, name, email, phone, contact_level FROM esc_contact WHERE status = '1' AND cat_id = :catId";
    $getContactSubCat = $con->prepare($sql);
    $getContactSubCat->bindParam(':catId', $contactCatId, PDO::PARAM_STR);
    $getContactSubCat->execute();
    $contactSubCat = $getContactSubCat->fetchAll(PDO::FETCH_ASSOC); 
    return($contactSubCat);       
}

function getAllContacts(){
    global $con;
    $sql = "SELECT ec.id, ec.name, ec.email, ec.phone, ec.sub_cat_id, ec.contact_level, sc.sub_cat_name, c.cat_name, sc.visible sub_cat_visible, c.visible cat_visible FROM esc_contact ec INNER JOIN sub_categories sc ON ec.sub_cat_id = sc.id INNER JOIN categories c ON sc.cat_id = c.id WHERE ec.status = '1' ORDER BY name ASC";
    $getContacts = $con->prepare($sql);
    $getContacts->execute();
    $contacts = $getContacts->fetchAll(PDO::FETCH_ASSOC); 
    foreach ($contacts as $contact) {
            $contactCatViews = '';
            if ($contact['cat_visible'] == '1' && $contact['sub_cat_visible'] == '1') {
                $scriptVisible = '<span class="badge badge-success">Visible</span>';
            }else{
                $scriptVisible = '<span class="badge badge-danger">Not visible</span>';
            }
            $contactCatViews .= $contact['cat_name']." >> ".$contact['sub_cat_name']."<br>".$scriptVisible."<br><hr>";       
            echo "<tr>";
            echo " <td>" . $contact['name'] . "</td>\n";
            echo " <td>" . $contact['email'] . "</td>\n";
            echo " <td>". $contact['phone']."</td>\n";
            echo " <td>".$contactCatViews."</td>\n";
            echo " <td>". $contact['contact_level']."</td>\n";
            echo " <td style='text-align:center;'>
                <button class='btn btn-secondary faq-btn btn-sm' onclick='delContactModel(".$contact['id'].','.$contact['id'].")' ><i class='fa fa-trash-o'></i> Delete</button>  
                </td>";
            echo "</tr>";
    }
}

function getContacts($contactSubCategory) {
    global $con;
    $sql = "SELECT id, name, email, phone, contact_level FROM esc_contact WHERE status = '1' AND sub_cat_id = :contactName";
    $getContact = $con->prepare($sql);
    $getContact->bindParam(':contactName', $contactSubCategory, PDO::PARAM_STR);
    $getContact->execute();
    $contacts = $getContact->fetchAll(PDO::FETCH_ASSOC); 
    return($contacts);       
}

function bookingTime(){
    $c_date = date('Y-m-d').' '.'00:00:00';
    $fromTime = strtotime($c_date) + 27000;
    $toTime = strtotime($c_date) + 54000;

    $difference = $toTime - $fromTime;
    $intervals = $difference/(60*15);

    for ($intv=0; $intv <= $intervals; $intv++) { 
        $schedle = $fromTime + ($intv*60*15);
        $time[] = date('G:i', $schedle);
    }
    return $time;
}

function checkAvailability($date, $branchId, $section){
    $bookings = booked($date,$branchId);
    $bkIntervals = bookingTime();

    foreach ($bkIntervals as $key => $value) {
        $interval = strtotime($date.' '.$value.':00');
        $booked = 0;
        foreach ($bookings as $booking) {
            $bookedTime = strtotime($booking['appointment_date']);
            if ($interval == $bookedTime) {
                if ($booking['section_id'] == '1') {
                    $booked += 3;
                }elseif($booking['section_id'] == '2') {
                    $booked += 2;
                }
            }
        }

        if ($booked == 0) {
            $available[] = $value;
        }elseif (($booked == 2) || ($booked == 3 && $section == '2')) {
                $available[] = $value;
        }

    }
    return $available;
}

function b_calendar($date, $branchId){
    $bookings = booked($date,$branchId);
    $bkIntervals = bookingTime();

    foreach ($bkIntervals as $key => $value) {
        $interval = strtotime($date.' '.$value.':00');
        $booked = 0;
        foreach ($bookings as $booking) {
            $bookedTime = strtotime($booking['appointment_date']);
            if ($interval == $bookedTime) {
                $items[] = array('period' => $value, 'booking_id' => $booking['id']);
            }else{
                $items[] = array('period' => $value, 'booking_id' => '');
            }
        }

    }
    if (isset($items)) {
        return $items;
    }else{
        return array();
    }
}

function booked($date,$branchId){
    global $con;
    $fromTime = $date.' 00:01:00';
    $toTime = $date.' 23:59:00';

    $qry = "SELECT id, branch_id, appointment_date, section_id FROM bk_reservations WHERE branch_id = :branch AND (appointment_date >= :fromTime AND appointment_date <= :toTime )";
    $stmt = $con->prepare($qry);
    $stmt->bindParam(':branch', $branchId, PDO::PARAM_STR);
    $stmt->bindParam(':fromTime', $fromTime, PDO::PARAM_STR);
    $stmt->bindParam(':toTime', $toTime, PDO::PARAM_STR);
    $stmt->execute();
    $booked = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $booked;
}

function bookingSections(){
    global $con;
    $qry = "SELECT id, name FROM bk_sections";
    $stmt = $con->prepare($qry);
    $stmt->execute();
    $sections = $stmt->fetchAll(PDO::FETCH_ASSOC); 
    return($sections);        
}

function branches(){
    global $con;
    $qry = "SELECT id, name, location, phone, email FROM bk_branches";
    $stmt = $con->prepare($qry);
    $stmt->execute();
    $branches = $stmt->fetchAll(PDO::FETCH_ASSOC); 
    return($branches);
}

//:::::::::::::::::::::::::::::::::::  Jan 2019 Services & Survey  ::::::::::::::::::::::::::::::::
function dealers(){
    global $con;
    $qry = "SELECT id, name, location, phone, email FROM bk_dealers";
    $stmt = $con->prepare($qry);
    $stmt->execute();
    $dealers = $stmt->fetchAll(PDO::FETCH_ASSOC); 
    return($dealers);
}
function dealer($id){
    global $con;
    $qry = "SELECT id, name, location, phone, email FROM bk_dealers WHERE id = '$id'";
    $stmt = $con->prepare($qry);
    $stmt->execute();
    $dealer = $stmt->fetch(); 
    return($dealer);
}
function dealerBranches($dealer){
    global $con;
    $qry = "SELECT id, name, location, phone, email FROM bk_branches WHERE dealer_id = '$dealer'";
    $stmt = $con->prepare($qry);
    $stmt->execute();
    $branches = $stmt->fetchAll(PDO::FETCH_ASSOC); 
    return($branches);
}
function dealerUser($userId){
    global $con;
    $qry = "SELECT d.id, d.name FROM dealer_users du LEFT JOIN bk_dealers d ON d.id = du.bk_dealer_id  WHERE user_id = '$userId'";
    $stmt = $con->prepare($qry);
    $stmt->execute();
    $dealer = $stmt->fetch(); 
    return($dealer);
}

function services($service, $type, $fd, $td){
    global $con;

    if (isset($_SESSION['isuzu_dealer'])) {
        $dealer = $_SESSION['isuzu_dealer'];
    }else{
        $dealer = '';
    }

    if ($dealer == '') {
        $filter = "WHERE transaction_type = '$service'";
    }else{
        $filter = "WHERE transaction_type = '$service' AND d.id = '$dealer'";
    }

    if ($service == '1') {
    $qry = "SELECT s.id, CONCAT(u.first_name,' ',u.last_name) user_name, b.name branch_name, d.name dealer_name, if(s.transaction_type = '1', 'Sales', 'Services') transaction, s.created_on, ss.sale_date, s.vehicle_id, ss.delivery_date FROM services s LEFT JOIN bk_branches b ON b.id = s.bk_branch_id LEFT JOIN bk_dealers d ON d.id = b.dealer_id LEFT JOIN users u ON u.id = s.user_id LEFT JOIN service_sales ss ON ss.service_id = s.id $filter AND (ss.delivery_date >= '$fd' AND ss.delivery_date <= '$td') ORDER BY ss.delivery_date ASC";
    }elseif ($service == '2') {
    $qry = "SELECT s.id, CONCAT(u.first_name,' ',u.last_name) user_name, b.name branch_name, d.name dealer_name, if(s.transaction_type = '1', 'Sales', 'Services') transaction, s.created_on, ss.service_advisor, ss.receieve_date, ss.release_date, ss.wrkord_no, ss.work_scope, s.vehicle_id, ss.driver_name, ss.driver_phone FROM services s LEFT JOIN bk_branches b ON b.id = s.bk_branch_id LEFT JOIN bk_dealers d ON d.id = b.dealer_id LEFT JOIN users u ON u.id = s.user_id LEFT JOIN service_v_services ss ON ss.service_id = s.id  $filter AND (ss.release_date >= '$fd' AND ss.release_date <= '$td') ORDER BY ss.release_date ASC";
    }

    $stmt = $con->prepare($qry);
    $stmt->execute();
    $services = $stmt->fetchAll(PDO::FETCH_ASSOC); 
    return($services);
}

function salesService($id){
    global $con;
    $qry = "SELECT s.id, CONCAT(u.first_name,' ',u.last_name) user_name, b.name branch_name, d.name dealer_name, if(s.transaction_type = '1', 'Sales', 'Services') transaction, s.created_on, ss.sale_date, s.vehicle_id, ss.delivery_date FROM services s LEFT JOIN bk_branches b ON b.id = s.bk_branch_id LEFT JOIN bk_dealers d ON d.id = b.dealer_id LEFT JOIN users u ON u.id = s.user_id LEFT JOIN service_sales ss ON ss.service_id = s.id WHERE s.id = '$id'";
    $stmt = $con->prepare($qry);
    $stmt->execute();
    $services = $stmt->fetch(); 
    return($services);
}

function vehicleService($id){
    global $con;
    $qry = "SELECT s.id, CONCAT(u.first_name,' ',u.last_name) user_name, b.name branch_name, d.name dealer_name, if(s.transaction_type = '1', 'Sales', 'Services') transaction, s.created_on, ss.service_advisor, ss.receieve_date, ss.release_date, ss.wrkord_no, ss.work_scope, s.vehicle_id, ss.driver_name, ss.driver_phone FROM services s LEFT JOIN bk_branches b ON b.id = s.bk_branch_id LEFT JOIN bk_dealers d ON d.id = b.dealer_id LEFT JOIN users u ON u.id = s.user_id LEFT JOIN service_v_services ss ON ss.service_id = s.id WHERE s.id = '$id'";
    $stmt = $con->prepare($qry);
    $stmt->execute();
    $services = $stmt->fetch(); 
    return($services);
}

function surveys($type, $fd, $td){
    global $con;
    if (isset($_SESSION['isuzu_dealer'])) {
        $dealer = $_SESSION['isuzu_dealer'];
        $filter = "AND d.id = '$dealer'";
    }else{
        $filter = '';
    }

    $fromDate = $fd.' 00:01:00';
    $toDate = $td.' 23:59:00';

    $qry = "SELECT f.id, f.successful, f.escalate, f.reason, f.customer_voice, CONCAT(u.first_name,' ',u.last_name) user_name, (SELECT CONCAT(first_name,' ',last_name) user_name FROM users WHERE id = v.customer_id ) customer_name, b.name branch_name, d.name dealer_name, if(s.transaction_type = '1', 'Sales', 'Services') transaction, s.vehicle_id, f.created_on FROM s_form f  LEFT JOIN services s ON s.id = f.service_id LEFT JOIN bk_branches b ON b.id = s.bk_branch_id LEFT JOIN bk_dealers d ON d.id = b.dealer_id LEFT JOIN users u ON u.id = f.user_id LEFT JOIN vehicle_info v ON v.id = s.vehicle_id WHERE s.transaction_type = '$type' AND (f.created_on >= '$fromDate' AND f.created_on <= '$toDate') $filter ORDER BY f.created_on ASC";
    
    $stmt = $con->prepare($qry);
    $stmt->execute();
    $services = $stmt->fetchAll(PDO::FETCH_ASSOC); 
    return($services);
}

function surveyScore($surveyId){
    global $con;
    $qry = "SELECT qn_id, score, verbal FROM s_ans WHERE form_id = $surveyId";
    $stmt = $con->prepare($qry);
    $stmt->execute();
    $services = $stmt->fetchAll(PDO::FETCH_ASSOC); 
    return($services);
}

function serviceSurveyStatus($serviceId){
    global $con;
    $qry = "SELECT successful, reason FROM s_form WHERE service_id = $serviceId ORDER BY created_on DESC LIMIT 1";
    $stmt = $con->prepare($qry);
    $stmt->execute();
    $status = $stmt->fetch(); 
    return($status);
}

//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
function bookings($fd, $td){
    global $con;
    if (isset($_SESSION['isuzu_dealer'])) {
        $dealer = $_SESSION['isuzu_dealer'];
        $filter = "AND d.id = '$dealer'";
    }else{
        $filter = "";
    }

    $fromDate = $fd.' 00:01:00';
    $toDate = $td.' 23:59:00';
    $qry = "SELECT (SELECT b.name FROM bk_branches b WHERE b.id = r.branch_id) branch,contact_person, (SELECT CONCAT(u.first_name,' ',u.last_name) FROM users u WHERE u.id = r.customer_id ) customer_name, r.customer_id, r.vehicle_id, (SELECT CONCAT(u.first_name,' ',u.last_name) FROM users u WHERE u.id = r.user_id ) agent, (SELECT s.name FROM bk_sections s WHERE s.id = r.section_id) section, r.appointment_date, r.service_type, r.repair_description, r.created_on FROM bk_reservations r LEFT JOIN  bk_branches b ON b.id = r.branch_id  LEFT JOIN bk_dealers d ON d.id = b.dealer_id  WHERE (created_on >= :fromDate AND created_on <= :toDate) $filter ORDER BY created_on DESC";
    $stmt = $con->prepare($qry);
    $stmt->bindParam(':fromDate', $fromDate, PDO::PARAM_STR);
    $stmt->bindParam(':toDate', $toDate, PDO::PARAM_STR);
    $stmt->execute();
    $bookings = $stmt->fetchAll(PDO::FETCH_ASSOC); 
    return($bookings);
}

function booking($bookingId){
    global $con;
    $qry = "SELECT (SELECT b.name FROM bk_branches b WHERE b.id = r.branch_id) branch, (SELECT CONCAT(u.first_name,' ',u.last_name) FROM users u WHERE u.id = r.customer_id ) customer_name, r.customer_id, r.vehicle_id, (SELECT CONCAT(u.first_name,' ',u.last_name) FROM users u WHERE u.id = r.user_id ) agent, (SELECT s.name FROM bk_sections s WHERE s.id = r.section_id) section, r.appointment_date, r.service_type, r.repair_description, r.created_on  FROM bk_reservations r WHERE r.id = :bookingId";
    $stmt = $con->prepare($qry);
    $stmt->bindParam(':bookingId', $bookingId, PDO::PARAM_STR);
    $stmt->execute();
    $booking = $stmt->fetch(); 
    return($booking);
}

?>