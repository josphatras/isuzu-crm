<?php
include_once('../dao/config/db.php');
if (isset($_SESSION["isuzu_user_id"])) {
	$userId = $_SESSION["isuzu_user_id"];
}else{
	die();
}

$unixTimestamp = time();
$timeStamp = date("YmdHis", $unixTimestamp);
function objectToArray($d) {
    if (is_object($d)) {
        // Gets the properties of the given object
        // with get_object_vars function
        $d = get_object_vars($d);
    }
	
    if (is_array($d)) {
        /*
        * Return array converted to object
        * Using __FUNCTION__ (Magic constant)
        * for recursive call
        */
        return array_map(__FUNCTION__, $d);
    }
    else {
        // Return array
        return $d;
    }
}

function sendSMS($phoneNo, $msgText) {
	global $con;
	global $userId;
	global $timeStamp;
	$msgDeatails = '
	{
		"AuthDetails":[{
			"UserID":"1427",
			"Token":"68eacb97d86f0c4621fa2b0e17cabd8c",
			"Timestamp":"'.$timeStamp.'"
		}
	],
			"SubAccountID":["0"],
			"MessageType":["3"],
			"BatchType":["0"],
			"SourceAddr":["Isuzu"],

			"MessagePayload":[{
				"Text":"'.$msgText.'"
			}],

			"DestinationAddr":[{
				"MSISDN":"'.$phoneNo.'",
				"LinkID":"",
				"SourceID":"2"
			}
		],

			"DeliveryRequest":[{
				"EndPoint":"",
				"Correlator":""
			}
		]
	}
	';

	$url = 'http://197.248.4.47/smsapi/submit2.php';
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $msgDeatails);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 

	$getResult = curl_exec($ch);
	$response = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	curl_close($ch);
		$xyz = json_decode($getResult);
		$array = objectToArray($xyz);
		$qnewmsg = "INSERT INTO crm_sms(phone_no, msg_text, from_user_id) VALUES (:phoneNo, :msgText, :userId)";
		$newmsg = $con->prepare($qnewmsg);
		$newmsg->bindParam(':phoneNo',$phoneNo, PDO::PARAM_STR);
		$newmsg->bindParam(':msgText',$msgText, PDO::PARAM_STR);
		$newmsg->bindParam(':userId',$userId, PDO::PARAM_STR);
		$newmsg->execute();
		$msgId = $con->lastInsertId();

	if (isset($array['0']['ResponseCode'])) {
		$qry = "UPDATE crm_sms SET status= :status WHERE id = :msgId";
		$stmt = $con->prepare($qry);
		$stmt->bindParam(':status',$array['0']['ResponseCode'], PDO::PARAM_STR);
		$stmt->bindParam(':msgId',$msgId, PDO::PARAM_STR);
		$stmt->execute();

		if ($array['0']['ResponseCode'] == '1001') {
			return('success');
		}else{
			return('fail');
		}
	}else{
		return('fail');
	}

}
?>