<?php
include_once('../header.php');
?>
<div class="form-container">
	<div class="log-form center-small-form">
		<div class="col-md-12 col-sm-12">
			<div class="container-login">
				<form action="/<?php echo ROOT_FOLDER; ?>dao/account.php" method="POST">
				  <div class="form-group">
				    <label for="loginuser">Username</label>
				    <input type="text" class="form-control" id="loginuser" name="username" aria-describedby="emailHelp" placeholder="Enter email">
				    <small id="emailHelp" class="form-text text-muted">Enter Email</small>
				  </div>
				  <div class="form-group">
				    <label for="loinpassword">Password</label>
				    <input type="password" class="form-control" id="loinpassword" name="password" placeholder="Password">
				  </div>
				  <input type="hidden" name="form_type" value="login">
				  <button type="submit" class="btn btn-info">Submit</button>
				</form>	
				<?php
					if (isset($_GET['res_message'])) {
						echo "<p style='color: #FF0000;'>".$_GET['res_message']."</p>";
					}
				?>
				<p></p>
				<p style="margin-top: 20px; text-decoration: underline #007bff;"><a href="#">Create account</a></p>
			</div>
		</div>
	</div>
</div>
<?php
include_once('../footer.php');

?>