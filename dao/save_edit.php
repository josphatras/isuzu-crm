<?php
include_once('../dao/config/include.php');
include_once('../dao/config/db.php');
include_once('esc_contact.php');

if (isset($_POST['formType'])) {
	$formType = $_POST['formType'];
	if ($formType == 'customer_edit') {
		$customerId = $_POST['customerId'];
		customerEdit($customerId);
	}
}

function customerEdit($customerId) {
	global $con;
	$customerName = explode(" ", $_POST['customerName']);
	$cusFirstName = $customerName['0'];
	if (isset($customerName['1'])) {
		$cusLastName = $customerName['1'];
	}else{
		$cusLastName = "";
	}	
	$email = $_POST['email'];

	$busType = $_POST['busType'];
	$companyName = $_POST['companyName'];	
	$formType = $_POST['formType'];
	$phyAddress = $_POST['phyAddress'];
	$telNo = $_POST['telNo'];
	$town = $_POST['town'];	

	$qry = "UPDATE users SET first_name = :cusFirstName, last_name = :cusLastName, email = :email WHERE id = :customerId";
	$message = $con->prepare($qry);
	$message->bindParam(':cusFirstName',$cusFirstName, PDO::PARAM_STR);
	$message->bindParam(':cusLastName',$cusLastName, PDO::PARAM_STR);
	$message->bindParam(':email',$email, PDO::PARAM_STR);
	$message->bindParam(':customerId',$customerId, PDO::PARAM_STR);
	$message->execute();

	$qry = "UPDATE customer_info SET company_name = :companyName, telephone_no = :telNo, business_type = :busType,physical_address = :phyAddress, town = :town WHERE customer_id = :customerId";
	$message = $con->prepare($qry);
	$message->bindParam(':busType',$busType, PDO::PARAM_STR);
	$message->bindParam(':companyName',$companyName, PDO::PARAM_STR);
	$message->bindParam(':phyAddress',$phyAddress, PDO::PARAM_STR);
	$message->bindParam(':telNo',$telNo, PDO::PARAM_STR);
	$message->bindParam(':town',$town, PDO::PARAM_STR);
	$message->bindParam(':customerId',$customerId, PDO::PARAM_STR);
	$message->execute();	

}
?>