<?php
include_once('dao/config/include.php');
include_once('dao/config/db.php');

function GetInteractions($link,$filter=""){
    $sql = "SELECT a.id,a.msg_id,a.resolution,a.sla,a.escalated_to,a.`status`,
        a.resolution_date,a.created_on,
        b.text, (SELECT name FROM esc_contact WHERE id = a.escalated_to) escalated_usr,
        d.status_name,e.cat_name,f.sub_cat_name
        FROM tickets a
        LEFT JOIN messages b ON a.msg_id = b.id
        LEFT JOIN users c ON a.escalated_to = c.id
        LEFT JOIN ticket_status d ON a.`status` = d.status_id
        LEFT JOIN categories e ON a.cat_id = e.id
        LEFT JOIN sub_categories f ON a.sub_cat_id = f.id $filter";
    $stmt=$link->prepare($sql);
    $stmt->execute();
    $query = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $sno = 0;
    
    foreach($query as $rs){
        $sno++;
        $age = 0;
        if(isset($rs['resolution_date'])){
            if($rs['resolution_date']==''){
                $age = age_in_days($rs['created_on'],date('Y-m-d'));
            }
            else{
                $age = age_in_days($rs['created_on'],$rs['resolution_date']);
            }
        }
        else{
            $age = age_in_days($rs['created_on'],date('Y-m-d'));
        }
        echo "<tr onclick='escEdit(); escEditform(\"".$rs['id']."\")' class='row-select'>
            <td>$sno</td>
            <td>$rs[id]</td>
            <td>$rs[cat_name]</td>
            <td>$rs[sub_cat_name]</td>            
            <td>$rs[text]</td>
            <td>$rs[resolution]</td>
            <td>$rs[sla]</td>
            <td>$rs[escalated_usr]</td>
            <td>$rs[status_name]</td>
            <td>$rs[created_on]</td>
            <td>$age</td>
            <td>".(isset($rs['resolution_date'])?date('Y-m-d G:i:s',strtotime($rs['resolution_date'])):"")."</td>
        </tr>";
    }
}

function age_in_days($day_a,$day_b){
    if(date('Y-m-d',strtotime($day_a))<=date('Y-m-d',strtotime($day_b))){
        $datetime1 = new DateTime($day_a);
        $datetime2 = new DateTime($day_b);
        $interval = $datetime1->diff($datetime2);
        return $interval->format('%a days');
    }
    else{
        $datetime1 = new DateTime($day_a);
        $datetime2 = new DateTime($day_b);
        $interval = $datetime1->diff($datetime2);
        return $interval->format('%R%a days');
    }
}


