<?php

include_once('dao/config/include.php');
include_once('dao/config/db.php');

$customerId = filter_input(1,'customer');
function age_in_days($day_a,$day_b){
    if(date('Y-m-d',strtotime($day_a))<=date('Y-m-d',strtotime($day_b))){
        $datetime1 = new DateTime($day_a);
        $datetime2 = new DateTime($day_b);
        $interval = $datetime1->diff($datetime2);
        return $interval->format('%a days');
    }
    else{
        $datetime1 = new DateTime($day_a);
        $datetime2 = new DateTime($day_b);
        $interval = $datetime1->diff($datetime2);
        return $interval->format('%R%a days');
    }
}


function CustomerVehicles($link,$customerId){
    $sql = "SELECT v.id AS vehicle_id,v.customer_id,(SELECT type FROM v_make WHERE id = v.make) make,(SELECT model_name FROM v_model WHERE id = v.model) model,v.reg_no,v.vin_no,(SELECT body_type FROM v_body_type WHERE id = v.body_type) body_type,
            v.application,v.fleet_size,v.created_on FROM vehicle_info v WHERE v.customer_id = '$customerId'";
    $stmt=$link->prepare($sql);
    //$stmt->bindParam(':customerId', $customerId, PDO::PARAM_STR);
    $stmt->execute();
    $query = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $sno = 0;
    foreach($query as $rs){
        $sno++;
        echo "<tr>
            <td>$sno</td>
            <td>$rs[make]</td>
            <td>$rs[model]</td>
            <td>$rs[reg_no]</td>
            <td>$rs[vin_no]</td>
            <td>$rs[body_type]</td>
            <td>$rs[application]</td>
            <td>$rs[fleet_size]</td>
        </tr>";
    }
}

function GetCustomer($link,$customerId){
    $sql = "SELECT a.id AS customer_id,CONCAT(first_name,' ',last_name) AS customer_name,
        email,company_name,telephone_no,business_type,physical_address,town,
        (SELECT COUNT(id) FROM vehicle_info WHERE customer_id=a.id) AS vehicles
        FROM users a
        LEFT JOIN customer_info b ON a.id = b.customer_id
        WHERE a.id = '$customerId'";
    $stmt=$link->prepare($sql);
    $stmt->execute();
    $customer = array(
            "customer_id"=>"",
            "name"=>"",
            "company"=>"",
            "phone"=>"",
            "business_type"=>"",
            "p_address"=>"",
            "town"=>"",
            "vehicles"=>""
        );
    if($stmt->rowCount()>0){
        $query = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $customer = array(
            "customer_id"=>$query[0]['customer_id'],
            "email"=>$query[0]['email'],
            "name"=>$query[0]['customer_name'],
            "company"=>$query[0]['company_name'],
            "phone"=>$query[0]['telephone_no'],
            "business_type"=>$query[0]['business_type'],
            "p_address"=>$query[0]['physical_address'],
            "town"=>$query[0]['town'],
            "vehicles"=>$query[0]['vehicles']
        );
    }
    return $customer;
}

function CustomerInteractions($link, $customerId){
    $sql = "SELECT 
        a.id,a.customer_id,a.agent_id,a.contact_type,a.cat_id,a.sub_cat_id,
        a.text,a.created_on,a.updated_on,b.company_name,CONCAT(g.first_name,' ',g.last_name) AS customer_name,
        CONCAT(c.first_name,' ',c.last_name) AS agent_name,
        d.name AS c_type,e.cat_name,
        f.sub_cat_name,
        (SELECT COUNT(id) FROM tickets h WHERE h.msg_id = a.id) AS in_escalations
        FROM messages a
        LEFT JOIN customer_info b ON a.customer_id = b.customer_id
        LEFT JOIN users c ON a.agent_id = c.id
        LEFT JOIN contact_type d ON a.contact_type = d.id
        LEFT JOIN categories e ON a.cat_id = e.id
        LEFT JOIN sub_categories f ON a.sub_cat_id = f.id
    LEFT JOIN users g ON a.customer_id = g.id WHERE a.customer_id = '$customerId'";
    $stmt=$link->prepare($sql);
    //$stmt->bindParam(':customerId', $customerId, PDO::PARAM_STR);
    $stmt->execute();
    $query = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $sno = 0;
    foreach($query as $rs){
        $sno++;
        $escalated = 'No';
        if($rs['in_escalations']>0){
            $escalated = 'Yes';
        }
        echo "<tr>
            <td>$sno</td>
            <td>$rs[id]</td>
            <td>".date('Y-M-d',strtotime($rs['created_on']))."</td>
            <td>$rs[c_type]</td>
            <td>$rs[cat_name]</td>
            <td>$rs[sub_cat_name]</td>
            <td>$rs[text]</td>
            <td>$rs[agent_name]</td>
        </tr>";
    }
}

function CustomerEscalations($link, $customerId){
    $sql = "SELECT a.id,a.msg_id,a.resolution,a.sla,a.escalated_to,a.`status`,
        a.resolution_date,a.created_on,
        b.text, (SELECT name FROM esc_contact WHERE id = a.escalated_to) escalated_usr,
        d.status_name,e.cat_name,f.sub_cat_name
        FROM tickets a
        LEFT JOIN messages b ON a.msg_id = b.id
        LEFT JOIN users c ON a.escalated_to = c.id
        LEFT JOIN ticket_status d ON a.`status` = d.status_id
        LEFT JOIN categories e ON a.cat_id = e.id
        LEFT JOIN sub_categories f ON a.sub_cat_id = f.id WHERE b.customer_id = :customerId";
    $stmt=$link->prepare($sql);
    $stmt->bindParam(':customerId', $customerId, PDO::PARAM_STR);
    $stmt->execute();
    $query = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $sno = 0;
    
    foreach($query as $rs){
        $sno++;
        $age = 0;
        if(isset($rs['resolution_date'])){
            $age = age_in_days($rs['created_on'],$rs['resolution_date']);
        }
        else{
            $age = age_in_days($rs['created_on'],date('Y-m-d h:i:s'));
        }
        echo "<tr>
            <td>$sno</td>
            <td>$rs[id]</td>
            <td>$rs[cat_name]</td>
            <td>$rs[sub_cat_name]</td>            
            <td>$rs[text]</td>
            <td>$rs[resolution]</td>
            <td>$rs[sla]</td>
            <td>$rs[escalated_usr]</td>
            <td>$rs[status_name]</td>
            <td>$age</td>
            <td>".(isset($rs['resolution_date'])?date('Y-m-d',strtotime($rs['resolution_date'])):"")."</td>
        </tr>";
    }
}