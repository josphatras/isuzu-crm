<?php
include_once('dao/config/include.php');
include_once('dao/config/db.php');
require_once('dao/functions.php');
require_once('include/user_roles.php');

function GetCustomers($link){
    $sql = "SELECT a.id AS customer_id,CONCAT(first_name,' ',last_name) AS customer_name,email,company_name,telephone_no,business_type,physical_address,town
        FROM users a
        INNER JOIN customer_info b ON a.id = b.customer_id
        WHERE role = 3";
    $stmt=$link->prepare($sql);
    $stmt->execute();
    $query = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $sno = 0;
    foreach ($query as $qry){
        $sno++;
        echo "<tr>
            <td>$sno</td>
            <td>$qry[customer_id]</td>
            <td>$qry[customer_name]</td>
            <td>$qry[email]</td>
            <td>$qry[company_name]</td>
            <td>$qry[telephone_no]</td>
            <td>$qry[town]</td>";
        //if ($admin == '1' || $manager == '1') {
            echo "<td><span class='fa fa-trash' style='font-size:18px;cursor:pointer' onclick='delCustomerPop(\"$qry[customer_name]\",\"$qry[customer_id]\")'></span></td>";
        //}
        echo  "<td><a href='dashboard.php?content=customer&customer=$qry[customer_id]'><span class='fa fa-list'></span></a></td>
        </tr>";
    }
}