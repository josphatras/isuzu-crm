<?php
include_once('../dao/config/include.php');
include_once('../dao/config/db.php');

function getSummary($link){
    $sql = "SELECT 
        (SELECT COUNT(id) FROM messages) interactions,
        (SELECT COUNT(id) FROM tickets) escalations";
    $stmt=$link->prepare($sql);
    $stmt->execute();
    $query = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach ($query as $qry){
        return array(
            "top_interactions"=>array(
                "top_1"=>"10",
                "top_2"=>"10",
                "top_3"=>"9",
                "top_4"=>"8",
                "top_5"=>"4"
            ),
            "interactions"=>"$qry[interactions]",
            "q_summary"=>array(
                "Today"=>"11",
                "This Week"=>"13",
                "This Month"=>"25"
            ),
            "escalations"=>"$qry[escalations]"
        );
    }
    
}

function getTopMsg($link){
    $sql = "SELECT a.cat_id,COUNT(a.id) AS countt ,b.cat_name, (SELECT COUNT(id) FROM messages) msg_count
        FROM messages a
        LEFT JOIN categories b ON a.cat_id = b.id
        GROUP BY cat_id ORDER BY countt DESC LIMIT 0,5";
    $stmt=$link->prepare($sql);
    $stmt->execute();
    $query = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $res[] = array("No categories"=>"0");
    $sn=0;
    if(count($query)>0){
        unset($res);
        foreach ($query as $qry){
            $catPercent = round(($qry['countt']/$qry['msg_count'])*100,0);
            $res[$qry['cat_id']] = array($qry['cat_name']=>$catPercent);
            $sn++;
        }
    }
    
    return $res;
}

function getTrendMsg($link){
    $date1 = date('Y-m-d',strtotime('-7 days'));
    $date2 = date('Y-m-d');
    $sql = "SELECT a.cat_id,COUNT(a.id) AS countt ,b.cat_name,  (SELECT COUNT(id) FROM messages WHERE DATE(a.created_on) BETWEEN '$date1' AND '$date2') msg_count
        FROM messages a
        LEFT JOIN categories b ON a.cat_id = b.id
        WHERE DATE(a.created_on) BETWEEN '$date1' AND '$date2'
        GROUP BY cat_id
        ORDER BY countt DESC LIMIT 0,5";
    $stmt=$link->prepare($sql);
    $stmt->execute();
    $query = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $res[] = array("No categories"=>"0");
    $sn=0;
    if(count($query)>0){
        unset($res);
        foreach ($query as $qry){
            $catPercent = round(($qry['countt']/$qry['msg_count'])*100,0);
            $res[$qry['cat_id']] = array($qry['cat_name']=>$catPercent);
            $sn++;
        }
    }
    
    return $res;
}

function getTopEsc($link){
    $sql = "SELECT a.cat_id,COUNT(a.id) AS countt,b.cat_name, (SELECT COUNT(id) FROM tickets) esc_count
        FROM tickets a
        LEFT JOIN categories b ON a.cat_id = b.id
        GROUP BY a.cat_id ORDER BY countt DESC LIMIT 0,5";
    $stmt=$link->prepare($sql);
    $stmt->execute();
    $query = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $res[] = array("No categories"=>"0");
    $sn=0;
    if(count($query)>0){
        unset($res);
        foreach ($query as $qry){
            $catPercent =round(($qry['countt']/$qry['esc_count'])*100,0);            
            $res[$qry['cat_id']] = array($qry['cat_name']=>$catPercent);
            $sn++;
        }
    }
    
    return $res;
}

function getQuickSum($link){
    $mo = date('m');
    $yr = date('Y');
    
    $today = date('Y-m-d');
    $this_week = date('Y-m-d',strtotime('-7 days'));
    $this_mo = date('Y-m-d',strtotime($yr.'-'.$mo.'-01'));
    
    $sql = "SELECT
        (SELECT COUNT(id) FROM messages WHERE created_on>='$today') AS c_today,
        (SELECT COUNT(id) FROM messages WHERE created_on>='$this_mo') AS c_month,
        (SELECT COUNT(id) FROM messages WHERE created_on>='$this_week') AS c_week";
    
    $stmt=$link->prepare($sql);
    $stmt->execute();
    $query = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach ($query as $qry){
        return array(
            "Today"=>$qry['c_today'],
            "Past Week"=>$qry['c_week'],
            "This Month"=>$qry['c_month']
        );
    }
}

function getQuickSum1($link){
    $mo = date('m');
    $yr = date('Y');
    
    $today = date('Y-m-d');
    $this_week = date('Y-m-d',strtotime('-7 days'));
    $this_mo = date('Y-m-d',strtotime($yr.'-'.$mo.'-01'));
    
    $sql = "SELECT
        (SELECT COUNT(id) FROM tickets WHERE created_on>='$today') AS c_today,
        (SELECT COUNT(id) FROM tickets WHERE created_on>='$this_mo') AS c_month,
        (SELECT COUNT(id) FROM tickets WHERE created_on>='$this_week') AS c_week";
    
    $stmt=$link->prepare($sql);
    $stmt->execute();
    $query = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach ($query as $qry){
        return array(
            "Today"=>$qry['c_today'],
            "Past Week"=>$qry['c_week'],
            "This Month"=>$qry['c_month']
        );
    }
}