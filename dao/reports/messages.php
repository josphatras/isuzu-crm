<?php
include_once('dao/config/include.php');
include_once('dao/config/db.php');

function GetInteractions($link,$filter=""){
    
    $sql = "SELECT 
        a.id,a.customer_id,a.agent_id,a.contact_type,a.cat_id,a.sub_cat_id,
        a.text,a.created_on,a.updated_on,b.company_name,CONCAT(g.first_name,' ',g.last_name) AS customer_name,
        CONCAT(c.first_name,' ',c.last_name) AS agent_name,
        d.name AS c_type,e.cat_name,
        f.sub_cat_name,
        (SELECT COUNT(id) FROM tickets h WHERE h.msg_id = a.id) AS in_escalations
        FROM messages a
        LEFT JOIN customer_info b ON a.customer_id = b.customer_id
        LEFT JOIN users c ON a.agent_id = c.id
        LEFT JOIN contact_type d ON a.contact_type = d.id
        LEFT JOIN categories e ON a.cat_id = e.id
        LEFT JOIN sub_categories f ON a.sub_cat_id = f.id
	LEFT JOIN users g ON a.customer_id = g.id $filter";
    
    $stmt=$link->prepare($sql);
    $stmt->execute();
    $query = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $sno = 0;
    foreach($query as $rs){
        $sno++;
        $escalated = 'No';
        if($rs['in_escalations']>0){
            $escalated = 'Yes';
        }
        echo "<tr>
            <td>$sno</td>
            <td>$rs[id]</td>
            <td>".date('Y-M-d G:i:s',strtotime($rs['created_on']))."</td>
            <td>$rs[customer_name]</td>            
            <td>$rs[company_name]</td>
            <td>$rs[c_type]</td>
            <td>$rs[cat_name]</td>
            <td>$rs[sub_cat_name]</td>
            <td>$rs[text]</td>
            <td>$rs[agent_name]</td>
            <td>$escalated</td>
        </tr>";
    }
}

function customer_int($link, $customerId){
    $sql = "SELECT 
        a.id,a.customer_id,a.agent_id,a.contact_type,a.cat_id,a.sub_cat_id,
        a.text,a.created_on,a.updated_on,b.company_name,CONCAT(g.first_name,' ',g.last_name) AS customer_name,
        CONCAT(c.first_name,' ',c.last_name) AS agent_name,
        d.name AS c_type,e.cat_name,
        f.sub_cat_name,
        (SELECT COUNT(id) FROM tickets h WHERE h.msg_id = a.id) AS in_escalations
        FROM messages a
        LEFT JOIN customer_info b ON a.customer_id = b.customer_id
        LEFT JOIN users c ON a.agent_id = c.id
        LEFT JOIN contact_type d ON a.contact_type = d.id
        LEFT JOIN categories e ON a.cat_id = e.id
        LEFT JOIN sub_categories f ON a.sub_cat_id = f.id
    LEFT JOIN users g ON a.customer_id = g.id WHERE a.customer_id = :customerId";
    $stmt=$link->prepare($sql);
    $stmt->bindParam(':customerId', $customerId, PDO::PARAM_STR);
    $stmt->execute();
    $query = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $sno = 0;
    foreach($query as $rs){
        $sno++;
        $escalated = 'No';
        if($rs['in_escalations']>0){
            $escalated = 'Yes';
        }
        echo "<tr>
            <td>$sno</td>
            <td>$rs[id]</td>
            <td>".date('Y-M-d',strtotime($rs['created_on']))."</td>
            <td>$rs[c_type]</td>
            <td>$rs[cat_name]</td>
            <td>$rs[sub_cat_name]</td>
            <td>$rs[text]</td>
            <td>$rs[agent_name]</td>
        </tr>";
    }
}


function customer_esc($link, $customerId){
    $sql = "SELECT a.id,a.msg_id,a.resolution,a.sla,a.escalated_to,a.`status`,
        a.resolution_date,a.created_on,
        b.text, (SELECT name FROM esc_contact WHERE id = a.escalated_to) escalated_usr,
        d.status_name,e.cat_name,f.sub_cat_name
        FROM tickets a
        LEFT JOIN messages b ON a.msg_id = b.id
        LEFT JOIN users c ON a.escalated_to = c.id
        LEFT JOIN ticket_status d ON a.`status` = d.status_id
        LEFT JOIN categories e ON b.cat_id = e.id
        LEFT JOIN sub_categories f ON b.sub_cat_id = f.id WHERE b.customer_id = :customerId";
    $stmt=$link->prepare($sql);
    $stmt->bindParam(':customerId', $customerId, PDO::PARAM_STR);
    $stmt->execute();
    $query = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $sno = 0;
    
    foreach($query as $rs){
        $sno++;
        $age = 0;
        if(isset($rs['resolution_date'])){
            $age = age_in_days($rs['created_on'],$rs['resolution_date']);
        }
        else{
            $age = age_in_days($rs['created_on'],date('Y-m-d'));
        }
        echo "<tr>
            <td>$sno</td>
            <td>$rs[id]</td>
            <td>$rs[cat_name]</td>
            <td>$rs[sub_cat_name]</td>            
            <td>$rs[text]</td>
            <td>$rs[resolution]</td>
            <td>$rs[sla]</td>
            <td>$rs[escalated_usr]</td>
            <td>$rs[status_name]</td>
            <td>$age</td>
            <td>".(isset($rs['resolution_date'])?date('Y-m-d',strtotime($rs['resolution_date'])):"")."</td>
        </tr>";
    }
}


function age_in_days($day_a,$day_b){
    if(date('Y-m-d',strtotime($day_a))==date('Y-m-d',strtotime($day_b))){
        return '0 days';
    }
    else{
        $datetime1 = new DateTime($day_a);
        $datetime2 = new DateTime($day_b);
        $interval = $datetime1->diff($datetime2);
        return $interval->format('%R%a days');
    }
}