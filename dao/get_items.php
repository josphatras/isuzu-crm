<?php
include_once('../dao/config/include.php');
include_once('../dao/config/db.php');
include_once('../dao/functions.php');

if (isset($_GET['catId'])) {
  $catId = $_GET['catId'];
  $qsubCat = "SELECT id,sub_cat_name FROM sub_categories WHERE cat_id = :catId AND visible = '1'";
  $getSubCat = $con->prepare($qsubCat);
  $getSubCat->bindParam(':catId', $catId, PDO::PARAM_STR);
  $getSubCat->execute();
  $subCategories = $getSubCat->fetchAll(PDO::FETCH_ASSOC);
?>

<div class="form-group">
    <label class="col-form-label form-control-label">Subcategory</label>
        <select class="form-control" size="0" id="subCategory" name="subCategory">
        <option value="">Choose...</option>
        <?php
          foreach ($subCategories as $SubCat) {
            echo '<option value="'.$SubCat['id'].'">'.$SubCat['sub_cat_name'].'</option>';
          }
        ?>
        </select>
</div>

<?php
}
if (isset($_GET['subCategory'])) {
	$catId = $_GET['subCategory'];
  $getType = $_GET['type'];
  if ($getType == 'msg') {
   $itemId = 'id="subCategory"'.' '.'name="subCategory"'.' '.'onchange="subCatChange();"';
  }elseif ($getType == 'esc') {
     $itemId = 'id="escSubCategory"'.' '.'name="escSubCategory"'.' '.'onchange="subCatChange(); getContacts();"';
  }
	$qsubCat = "SELECT id,sub_cat_name FROM sub_categories WHERE cat_id = :catId AND visible = '1'";
	$getSubCat = $con->prepare($qsubCat);
	$getSubCat->bindParam(':catId', $catId, PDO::PARAM_STR);
	$getSubCat->execute();
	$subCategories = $getSubCat->fetchAll(PDO::FETCH_ASSOC);
?>

<div class="form-group row">
    <label class="col-lg-3 col-form-label form-control-label">Subcategory</label>
    <div class="col-lg-9">
        <select class="form-control" size="0" <?php echo $itemId; ?>>
        <option value="">Choose...</option>
        <?php
          foreach ($subCategories as $SubCat) {
            echo '<option value="'.$SubCat['id'].'">'.$SubCat['sub_cat_name'].'</option>';
          }
        ?>
        </select>
    </div>
</div>

<?php
}

if (isset($_GET['customerDetail'])) {
  $customerId = $_GET['customerDetail'];
  $qsubCat = "SELECT id,sub_cat_name FROM users WHERE cat_id = :catId";
  $getSubCat = $con->prepare($qsubCat);
  $getSubCat->bindParam(':catId', $catId, PDO::PARAM_STR);
  $getSubCat->execute();
}

if (isset($_GET['veh_make'])) {
  $make = $_GET['veh_make'];
  $models = veh_model($make);
?>
      <select class="custom-select mr-sm-2" id="model" name="model">
        <option selected>Choose...</option>
        <?php
          foreach ($models as $model) {
            echo '<option value="'.$model['id'].'">'.$model['model_name'].'</option>';
          }
        ?>
      </select>
<?php
}

if (isset($_GET['catType'])) {
  $item = $_GET['catType'];
  $categories = sel_Cat($item);
?>
  <div class="form-group"> 
      <label>Category</label> 
      <select id="category" class="form-control" name="category" onchange="selectSubCat()"> 
      <option value="">Select</option>
      <?php
          foreach ($categories as $category) {
              echo '<option value="'.$category['id'].'">'.$category['cat_name'].'</option>';
          }
      ?>
      </select>
      <span id="m-p-error" class="f-error"></span>
  </div> 
<?php 
}

if (isset($_GET['escCatId'])) {
  $contactSubCats = getContactSubCat($_GET['escCatId']);
?>
<!-- <div class="form-group row">
    <label class="col-lg-3 col-form-label form-control-label">Escalate to</label>
    <div class="col-lg-9">
        <select class="form-control" size="0" name="contactSubCat" id="contactSubCat" onchange="getContacts();">
        <option value="">Choose...</option>
        <?php
          foreach ($contactSubCats as $contactSubCat) {
            echo '<option value="'.$contactSubCat['contact_name'].'">'.$contactSubCat['contact_name'].'</option>';
          }
        ?>
        </select>
    </div>
</div> -->
<?php
}

if (isset($_GET['contactSubCat'])) {
  $contacts = getContacts($_GET['contactSubCat']);
?>
  <select class="form-control" size="0" name="escTo" id="escTo">
  <option value="">Choose...</option>
  <?php
    foreach ($contacts as $contact) {
      echo '<option value="'.$contact['id'].'">'.$contact['name']. ' - Level '.$contact['contact_level'].'</option>';
    }
  ?>
  </select>
<?php
}
?>


<?php
if (isset($_GET['dir_int_sub_cats'])) {
  $catId = $_GET['dir_int_sub_cats'];
  $qsubCat = "SELECT id,sub_cat_name FROM sub_categories WHERE cat_id = :catId AND visible = '1'";
  $getSubCat = $con->prepare($qsubCat);
  $getSubCat->bindParam(':catId', $catId, PDO::PARAM_STR);
  $getSubCat->execute();
  $subCategories = $getSubCat->fetchAll(PDO::FETCH_ASSOC);
?>
        <select class="form-control" size="0" id="intSubCategory" name="intSubCategory" onchange="getContacts();">
        <option value="">Choose...</option>
        <?php
          foreach ($subCategories as $SubCat) {
            echo '<option value="'.$SubCat['id'].'">'.$SubCat['sub_cat_name'].'</option>';
          }
        ?>
        </select>
<?php
}
?>

<?php
if (isset($_GET['dir_esc_sub_cats'])) {
  $catId = $_GET['dir_esc_sub_cats'];
  $qsubCat = "SELECT id,sub_cat_name FROM sub_categories WHERE cat_id = :catId AND visible = '1'";
  $getSubCat = $con->prepare($qsubCat);
  $getSubCat->bindParam(':catId', $catId, PDO::PARAM_STR);
  $getSubCat->execute();
  $subCategories = $getSubCat->fetchAll(PDO::FETCH_ASSOC);
?>
        <select class="form-control" size="0" id="escSubCategory" name="escSubCategory" onchange="getContacts();">
        <option value="">Choose...</option>
        <?php
          foreach ($subCategories as $SubCat) {
            echo '<option value="'.$SubCat['id'].'">'.$SubCat['sub_cat_name'].'</option>';
          }
        ?>
        </select>
<?php
}
?>

<?php
  if (isset($_GET['av_d'])) {
    $intervals = checkAvailability($_GET['av_d'], $_GET['av_b'], $_GET['av_s']);
    echo '<option value="">Choose...</option>';
    foreach ($intervals as $key => $value) {
      echo '<option value="'.$value.'">'.$value.'</option>';
    }
  }
?>

<!-- Jan 2019 service and survey  -->
<?php
  if (isset($_GET['dealer_braches'])) {
    $branches = dealerBranches($_GET['dealer_braches']);
    print_r($branches);
    echo '<option value="">Choose...</option>';
    foreach ($branches as $branch) {
      echo '<option value="'.$branch['id'].'">'.$branch['name'].'</option>';
    }
  }
?>