<?php
require 'mail/class.phpmailer.php';
include_once('../dao/config/include.php');
include_once('../dao/config/db.php');

session_start();
if (isset($_SESSION["esc_user_id"])) {
	$userId = $_SESSION["esc_user_id"];
}

$qclContact = "SELECT * FROM cl_contacts WHERE id = '1'";
$getclContact = $con->prepare($qclContact);
$getclContact->execute();
$clContact = $getclContact->fetch();
$clEmail = $clContact['email'];

function Send_Mail1($to, $subj, $msg/*, $attachment, $att_name*/)
{
	$from = "no-reply@technobrainbpo.com";
	$mail = new PHPMailer();
	$mail->IsSMTP(true); // SMTP
	$mail->SMTPAuth   = true;  // SMTP authentication
	$mail->Mailer = "smtp";
	$mail->Host       = "mail.technobrainbpo.com"; // Techno Brain server,
	$mail->Port       = 26;                    // set the SMTP port
	$mail->Username   = "no-reply@technobrainbpo.com";  // SMTP  username
	$mail->Password   = "portal123";  //SMTP password
	$mail->SetFrom($from, 'TBBL Biometrics');
	$mail->AddReplyTo($from,'TBBL Biometrics');
	$mail->Subject = $subj;
	$mail->MsgHTML($msg);
	$address =explode(',',$to);
	foreach($address as $many)
	{
		$mail->AddAddress($many);
	}

	//$mail->addStringAttachment($attachment,$att_name);
	if(!$mail->Send())
		return false;
	else
		return true;
	}

if (isset($_POST['clForwardMessage'])) {
	$message = $_POST['clForwardMessage'];
	$escId = $_POST['escId'];

	$postQuery = "INSERT INTO forwards(user_id, esc_id, message ) VALUES (:userId, :escId, :message)";
		$postValue = $con->prepare($postQuery);
		$postValue->bindParam(':message', $message, PDO::PARAM_STR);
		$postValue->bindParam(':escId', $escId, PDO::PARAM_STR);
		$postValue->bindParam(':userId', $userId, PDO::PARAM_STR);
		$postValue->execute();

	$mailResponse = Send_Mail1($clEmail,"Escalation", nl2br($message));
	echo $mailResponse;
	if ($mailResponse == '1') {
		header('Location: ../dashboard.php?content=escalations&mail_response=Success');
	}else{
		header('Location: ../dashboard.php?content=escalations&mail_response=Fail');
	}
}
?>