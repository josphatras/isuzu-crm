<?php
//include_once '../header.php';
include_once('../dao/config/include.php');
include_once('../dao/config/db.php');
include '../dao/functions.php';
$int_categories = sel_Cat('msg');
$esc_categories = sel_Cat('ticket');
$contactTypes = allConTypes();
session_start();

if (isset($_GET['searchPhrase'])) {
	$searchPhrase = $_GET['searchPhrase'];
	if ($searchPhrase == '') {
		die('Please enter search text');
	}

if (!isset($_SESSION['isuzu_dealer'])) {
	$qcustomers = "SELECT u.id, u.first_name, u.last_name, u.email, i.company_name, i.telephone_no, i.business_type, i.physical_address, i.town FROM users u INNER JOIN customer_info i ON i.customer_id = u.id WHERE u.role = '3' AND ";
}else{
	$dealer = $_SESSION['isuzu_dealer'];
	$qcustomers = "SELECT u.id, u.first_name, u.last_name, u.email, i.company_name, i.telephone_no, i.business_type, i.physical_address, i.town FROM users u INNER JOIN customer_info i ON i.customer_id = u.id LEFT JOIN dealer_users du ON du.user_id = u.created_by WHERE u.role = '3' AND du.bk_dealer_id = '$dealer' AND ";
}

$words = explode(" ", $_GET['searchPhrase']);
$conds = array();

foreach ($words as $word) {
    $conds[] = "(first_name Like '%".$word."%' OR last_name LIKE '%".$word."%' OR company_name LIKE '%".$word."%' OR telephone_no LIKE '%".$word."%' OR u.id LIKE '%".$word."%' )";
}
$qcustomers .= implode(' OR ', $conds);
$getCustomers = $con->prepare($qcustomers);
$getCustomers->execute();
$customerCount = $getCustomers->rowCount();
$customers = $getCustomers->fetchAll(PDO::FETCH_ASSOC);		
}

?>
<div style="background: #fff">

	<table border="1" cellspacing="10" bordercolor = "#ccc" cellpadding="2" style="width: 100%;">
		<tr style="background: #D3D3D3; font-size: 11px;">
			<th>First Name</th>
			<th>Last Name</th>
			<th>Email</th>
			<th>Company</th>
			<th>Telephone</th>
			<th>Business Type</th>
			<th>Physical Address</th>
			<th>Town</th>
			<th></th>
		</tr>
<?php
if ($customerCount >0) {
	foreach ($customers as $customer) {

	?>
			<tr style="font-size: 11px;"> 
				<td><?php echo $customer['first_name']; ?></td>
				<td><?php echo $customer['last_name']; ?></td>
				<td><?php echo $customer['email']; ?></td>
				<td><?php echo $customer['company_name']; ?></td>
				<td><?php echo $customer['telephone_no']; ?></td>
				<td><?php echo $customer['business_type']; ?></td>
				<td><?php echo $customer['physical_address']; ?></td>
				<td><?php echo $customer['town']; ?></td>		
				<td><button type="link" onclick="searchSelect('<?php echo $customer['id']; ?>')">Select</button></td>
			</tr>
	<?php
	}
}else{
?>
	<tr style="font-size: 11px;"> 
		<td colspan="9">No rows found</td>
	</tr>
<?php
}
?>
</table>
<?php
if ($customerCount == 0) {
?>
<br>
<div style="float: right">
	<button type="link" class="btn btn-info" onclick="createCustomer()">New Customer</button>
</div>
<br><br>
<div id="dummyResponse">
	
</div>
<?php if (!isset($_SESSION['isuzu_dealer'])): ?>
<h5>Customer Details</h5>
<form id="dummy_customer">
	<div class="row">
		<div class="col-md-6">
			<div class="form-row" style="">
				<div class="col-md-4" style="background: #dc3545; color: #fff;border: 1px solid #ccc;">Name</div>
				<div class="col-md-6"><input type="text" class="form-control" id="customerName" name="customerName" placeholder="Dummy" required></div>
			</div>
			<div class="form-row" style="">
				<div class="col-md-4" style="background: #dc3545; color: #fff;border: 1px solid #ccc;">Company Name</div>
				<div class="col-md-6"><input type="text" class="form-control" id="companyName" name="companyName" placeholder="Dummy"></div>
			</div>
			<div class="form-row" style="">
				<div class="col-md-4" style="background: #dc3545; color: #fff;border: 1px solid #ccc;">Email</div>
				<div class="col-md-6"><input type="text" class="form-control" id="email" name="email" placeholder="Dummy"></div>
			</div>		
			<div class="form-row" style="">
				<div class="col-md-4" style="background: #dc3545; color: #fff;border: 1px solid #ccc;">Phone</div>
				<div class="col-md-6"><input type="text" class="form-control" id="telNo" name="telNo" placeholder="Dummy" required></div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-row" style="">
				<div class="col-md-4" style="background: #dc3545; color: #fff;border: 1px solid #ccc;">Business Type</div>
				<div class="col-md-6"><input type="text" class="form-control" id="busType" name="busType" placeholder="Dummy"></div>
			</div>
			<div class="form-row" style="">
				<div class="col-md-4" style="background: #dc3545; color: #fff;border: 1px solid #ccc;">Physical Address</div>
				<div class="col-md-6"><input type="text" class="form-control" id="phyAddress" name="phyAddress" placeholder="Dummy"></div>
			</div>
			<div class="form-row" style="">
				<div class="col-md-4" style="background: #dc3545; color: #fff;border: 1px solid #ccc;">Town</div>
				<div class="col-md-6"><input type="text" class="form-control" id="town" name="town" placeholder="Dummy" required></div>
			</div>		
		</div>
	</div>	
</form>
<br><br>
<h5>Interaction</h5>	
	<form id="dummy_int">
		<table id="" border="1" class="table table-bordered table-striped table-sm " style="font-size: 0.8em;border-collapse: collapse;">
		    <thead>
		        <tr>
		        	<th>Contact type</th>
		            <th>Category</th>
		            <th>Sub-category</th>
		            <th>Message</th>
		            <th></th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr>
		        	<td>
						<select id="contactType" name="contactType" class="form-control" size="0" required>
						    <option value="">Choose...</option>
						    <?php
						    	foreach ($contactTypes as $contactType) {
						    		echo '<option value="'.$contactType['id'].'">'.$contactType['name'].'</option>';
						    	}
						    ?>
				        </select>
		        	</td>
		            <td>
				        <select name="intCategory" id="intCategory" class="form-control" size="0" onchange="dir_int_sub_cat()" required>
					    <option value="">Choose...</option>
					    <?php
					    	foreach ($int_categories as $category) {
					    		echo '<option value="'.$category['id'].'">'.$category['cat_name'].'</option>';
					    	}
					    ?>
				        </select>            	
		            </td>
		            <td>
		            	<div id="int_sub_cat">
				        <select id="intSubCategory" name="intSubCategory" class="form-control" size="0" required>
					    <option value="">Choose...</option>
				        </select>               		
		            	</div>
		            </td>
		            <td>
		            	<textarea class="form-control" id="msg" name="msg" rows="1" columns="8"></textarea>
		            </td>
		            <td>
		            	<input type="hidden" name="formType" value="dummy_int">
		            	<button type="button" class="btn btn-info btn-sm" onclick="dummyInt()">Save</button>
		            </td>
		        </tr>
		    </tbody>
		</table>	
	</form>
<h5>Escalation</h5>
	<form id="dummy_esc">
		<table id="" border="1" class="table table-bordered table-striped table-sm " style="font-size: 0.8em;border-collapse: collapse;">
		    <thead>
		        <tr>
		            <th>Category</th>
		            <th>Sub-category</th>
		            <th>Issue</th>
		            <th>SLA</th>
		            <th>Escalated to</th>
		            <th></th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr>
		            <td>
				        <select name="category" id="category" class="form-control" size="0" onchange="dir_esc_sub_cat()" required>
					    <option value="">Choose...</option>
					    <?php
					    	foreach ($esc_categories as $category) {
					    		echo '<option value="'.$category['id'].'">'.$category['cat_name'].'</option>';
					    	}
					    ?>
				        </select>            	
		            </td>
		            <td>
		            	<div id="esc_sub_cat">
				        <select name="escSubCategory" id="escSubCategory" class="form-control" size="0" required>
					    <option value="">Choose...</option>
				        </select>               		
		            	</div>
		            </td>
		            <td>
		            	<textarea class="form-control" id="msg" name="msg" rows="1" columns="8"></textarea>
		            </td>
		            <td>
		            	<input type="text" class="form-control" id="sla" name="sla" placeholder="SLA" value="">
		            </td>
		            <td>
		            	<div id="contacts">
				        <select name="escTo" id="escTo" class="form-control" size="0" required>
					    <option value="">Choose...</option>
				        </select>               		
		            	</div>            	
		            </td>
		            <td>
		            	<input type="hidden" name="formType" value="dummy_esc">
		            	<button type="button" class="btn btn-info btn-sm" onclick="dummyEsc()">Escalate</button>
		            </td>
		        </tr>
		    </tbody>
		</table>	
	</form>
	<?php endif; ?>
</div>
<?php
}
include_once '../footer.php';
?>