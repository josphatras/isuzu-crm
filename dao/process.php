<?php
include_once('../dao/config/include.php');
include_once('../dao/config/db.php');
include_once('esc_contact.php');
include_once('sms.php');

function vehicle($id){
    global $con;
    $qry = "SELECT v.id, (SELECT type FROM v_make WHERE v_make.id = v.make) v_make, (SELECT model_name FROM v_model WHERE v_model.id = v.model) v_model, v.reg_no, v.vin_no FROM vehicle_info v WHERE v.id = :id";
    $stmt = $con->prepare($qry);
    $stmt->bindParam(':id', $id, PDO::PARAM_STR);
    $stmt->execute();
    $vehicle = $stmt->fetch();
    return($vehicle);
}

if (isset($_SESSION["isuzu_user_id"])) {
	$userId = $_SESSION["isuzu_user_id"];
	$userRole = $_SESSION["isuzu_role"];
}

if (isset($_POST['contactType'])) {
		$contactType = $_POST['contactType'];
		$category = $_POST['category'];
		$subCategory = $_POST['subCategory'];
		$customerId = $_POST['customerId'];
		if (isset($_POST['complain'])) {
			$complain = $_POST['complain'];
		}else{
			$complain = '';
		}
		
		$qnewmsg = "INSERT INTO messages(customer_id, complain, agent_id, contact_type, cat_id, sub_cat_id) VALUES (:customerId, :complain, :agentId, :contactType,:category, :subCategory)";
		$newmsg = $con->prepare($qnewmsg);
		$newmsg->bindParam(':customerId',$customerId, PDO::PARAM_STR);
		$newmsg->bindParam(':agentId',$userId, PDO::PARAM_STR);
		$newmsg->bindParam(':contactType',$contactType, PDO::PARAM_STR);
		$newmsg->bindParam(':category',$category, PDO::PARAM_STR);
		$newmsg->bindParam(':subCategory',$subCategory, PDO::PARAM_STR);
		$newmsg->bindParam(':complain',$complain, PDO::PARAM_STR);
		$newmsg->execute();
		$msgId = $con->lastInsertId();
		$array_msgId = array('msgId' => $msgId );
		$json_msgId = json_encode($array_msgId);
		print_r($json_msgId);
}


if (isset($_POST['customerName'])) {

		$customerName = explode(" ", $_POST['customerName']);
		$cusFirstName = $customerName['0'];
		if (isset($customerName['1'])) {
			$cusLastName = $customerName['1'];
		}else{
			$cusLastName = "";
		}

		$companyName = $_POST['companyName'];
		$telNo = $_POST['telNo'];
		$email = $_POST['email'];
		$busType = $_POST['busType'];
		$phyAddress = $_POST['phyAddress'];
		$town = $_POST['town'];

		if ($customerName == '' || $telNo == '' || $town == '') {
			$array_status = array('status' => 'fail' );
			$json_status = json_encode($array_status);
			die($json_status);
		}

		$qnewCustomer = " INSERT INTO users(created_by, first_name, last_name, email, role) VALUES (:createdBy, :cusFirstName,:cusLastName, :email,'3')";
		$newCustomer = $con->prepare($qnewCustomer);
		$newCustomer->bindParam(':createdBy', $userId, PDO::PARAM_STR);
		$newCustomer->bindParam(':cusFirstName',$cusFirstName, PDO::PARAM_STR);
		$newCustomer->bindParam(':cusLastName',$cusLastName, PDO::PARAM_STR);
		$newCustomer->bindParam(':email',$email, PDO::PARAM_STR);
		$newCustomer->execute();		
		$customerId = $con->lastInsertId();

		$qcustomerInfo = " INSERT INTO customer_info(customer_id, company_name, telephone_no, business_type, physical_address, town) VALUES (:customerId, :companyName, :telNo, :busType, :phyAddress,:town)";
		$customerInfo = $con->prepare($qcustomerInfo);
		$customerInfo->bindParam(':customerId',$customerId, PDO::PARAM_STR);
		$customerInfo->bindParam(':companyName',$companyName, PDO::PARAM_STR);
		$customerInfo->bindParam(':telNo',$telNo, PDO::PARAM_STR);
		$customerInfo->bindParam(':busType',$busType, PDO::PARAM_STR);
		$customerInfo->bindParam(':phyAddress',$phyAddress, PDO::PARAM_STR);
		$customerInfo->bindParam(':town',$town, PDO::PARAM_STR);
		$customerInfo->execute();

		$array_customerId = array('status' => 'success', 'customerId' => $customerId );
		$json_customerId = json_encode($array_customerId);
		print_r($json_customerId);		
}


if(isset($_POST['regNo'])) {
	$make = $_POST['make'];
	$model = $_POST['model'];
	$regNo = $_POST['regNo'];
	$vinNo = $_POST['vinNo'];
	$bodyType = $_POST['bodyType'];
	$application = $_POST['application'];
	$fleetSize = $_POST['fleetSize'];
	$customerId = $_POST["customerId"];

	if ($make == '' || $regNo == '' || $bodyType == '') {
		$array_status = array('status' => 'fail' );
		$json_status = json_encode($array_status);
		die($json_status);
	}

	$qnewmsg = " INSERT INTO vehicle_info(customer_id, make, model, reg_no, vin_no, body_type, application, fleet_size) VALUES (:customerId,:make, :model, :regNo, :vinNo, :bodyType, :application, :fleetSize)";
	$newmsg = $con->prepare($qnewmsg);
	$newmsg->bindParam(':customerId',$customerId, PDO::PARAM_STR);
	$newmsg->bindParam(':make',$make, PDO::PARAM_STR);
	$newmsg->bindParam(':model',$model, PDO::PARAM_STR);
	$newmsg->bindParam(':regNo',$regNo, PDO::PARAM_STR);
	$newmsg->bindParam(':vinNo',$vinNo, PDO::PARAM_STR);
	$newmsg->bindParam(':bodyType',$bodyType, PDO::PARAM_STR);
	$newmsg->bindParam(':application',$application, PDO::PARAM_STR);
	$newmsg->bindParam(':fleetSize',$fleetSize, PDO::PARAM_STR);
	$newmsg->execute();

	if (isset($_SESSION['isuzu_dealer'])){
		$dealer = $_SESSION['isuzu_dealer'];
	}else{
		$dealer = '';
	}
	$array_customerId = array('status' => 'success', 'dealer' => $dealer);
	$json_customerId = json_encode($array_customerId);
	print_r($json_customerId);
}

//:::::::::::::::::::::::::::::::::::::::::: INTERACTION ::::::::::::::::::::::::::::::::::::::::::::
if (isset($_POST['formType'])) {
	if ($_POST['formType'] == 'interaction' && isset($_POST['msg'])) {
			$msg = $_POST['msg'];
			$esc_status = $_POST['escStatus'];
			$msgId = $_POST["msgId"];
			$customerId = $_POST["customerId"];
			if ($msg == '' || $esc_status == '') {
				$array_status = array('status' => 'fail');
				$json_status = json_encode($array_status);
				die($json_status);
			}
		if ($esc_status == 'yes') {
			$escTo = $_POST['escTo'];
			$cat = $_POST['escCategory'];
			if ($cat == '' || !isset($_POST['escSubCategory'])) {
				$array_status = array('status' => 'fail' );
				$json_status = json_encode($array_status);
				die($json_status);
			}
			if ($_POST['escSubCategory'] == '') {
				$array_status = array('status' => 'fail' );
				$json_status = json_encode($array_status);
				die($json_status);
			}
			$subCat = $_POST['escSubCategory'];	

			$qMessage = "UPDATE messages SET text = :msg  WHERE id = :msgId";
			$message = $con->prepare($qMessage);
			$message->bindParam(':msg',$msg, PDO::PARAM_STR);
			$message->bindParam(':msgId',$msgId, PDO::PARAM_STR);
			$message->execute();

			$qnewEsc = "INSERT INTO tickets(msg_id,cat_id, sub_cat_id, escalated_to, status) VALUES (:msgId,:cat, :subCat, :escTo, '0')";
			$insertEsc = $con->prepare($qnewEsc);
			$insertEsc->bindParam(':cat',$cat, PDO::PARAM_STR);
			$insertEsc->bindParam(':subCat',$subCat, PDO::PARAM_STR);
			$insertEsc->bindParam(':escTo',$escTo, PDO::PARAM_STR);
			$insertEsc->bindParam(':msgId',$msgId, PDO::PARAM_STR);
			$insertEsc->execute();
			$ticketNo = $con->lastInsertId();

			$qry = "SELECT c.telephone_no, c.company_name, u.email, u.first_name, u.last_name FROM users u LEFT JOIN customer_info c ON c.customer_id = u.id WHERE u.id = :customerId";
			$getCustomer = $con->prepare($qry);
			$getCustomer->bindParam(':customerId',$customerId, PDO::PARAM_STR);
			$getCustomer->execute();	
			$customer = $getCustomer->fetch();

			$qry = "SELECT * FROM esc_contact WHERE id = :escTo";
			$escContact = $con->prepare($qry);
			$escContact->bindParam(':escTo',$escTo, PDO::PARAM_STR);
			$escContact->execute();	
			$contact = $escContact->fetch();

			$qry = "SELECT c.cat_name, sc.sub_cat_name FROM  sub_categories sc INNER JOIN categories c ON sc.cat_id = c.id WHERE sc.id = :subCat";
			$escCategory = $con->prepare($qry);
			$escCategory->bindParam(':subCat',$subCat, PDO::PARAM_STR);
			$escCategory->execute();	
			$e_category = $escCategory->fetch();


				$to = $contact['email'];
				$subj = 'CONTACT CENTRE CRM ESCALATION';
				$mail_msg = 'Dear '.$contact['name'].' ,<br><br>';
				$mail_msg .= 'The below issue has been escalated to you. Kindly action and respond within 12hours. Details:<br><br><br>';
				$mail_msg .= 'Customer Id	: '.$customerId.'<br><br>';
				$mail_msg .= 'Customer Name	: '.$customer['first_name'].' '.$customer['last_name'].'<br><br>';
				$mail_msg .= 'Phone No.	: '.$customer['telephone_no'].'<br><br>';
				$mail_msg .= 'Company	: '.$customer['company_name'].'<br><br>';
				$mail_msg .= 'Email	: '.$customer['email'].'<br><br>';
				$mail_msg .= 'Category	: '.$e_category['cat_name'].'<br><br>';
				$mail_msg .= 'Sub Category	: '.$e_category['sub_cat_name'].'<br><br>';
				$mail_msg .= 'Message	: '.$msg.'<br>';
				$mail_msg .= 'Sincerely,<br>';
				$mail_msg .= 'Isuzu Contact Center Team <br>';
				$mail_msg .= 'Contact Center Phone no: 0800724724 <br><br><br>';
				$mail_msg .= '* Please respond within 12hours. Thank you';
				escalationMail($to, $subj, $mail_msg);	

				$smsCustomerNumber = $customer['telephone_no'];

				if ($smsCustomerNumber !== '' && is_numeric($smsCustomerNumber)) {
					$msgText = 'Dear Valued Customer, thank you for contacting us. Your query has been escalated. Your ticket id is '.$ticketNo.'. You will receive feedback within 24hours. Thank you';
					sendSMS($smsCustomerNumber, $msgText);
				}

				if ($contact['phone'] !== '' && is_numeric($contact['phone'])) {
					$msgText = 'Dear '.$contact['name'].', you have received a customer query on your email for your prompt action. Please respond within 12hours. Thank you';
					sendSMS($contact['phone'], $msgText);
				}

			$array_status = array('status' => 'success' );
			$json_status = json_encode($array_status);
			print_r($json_status);
		}elseif ($esc_status == 'no') {
			$qMessage = "UPDATE messages SET text = :msg  WHERE id = :msgId";
			$message = $con->prepare($qMessage);
			$message->bindParam(':msg',$msg, PDO::PARAM_STR);
			$message->bindParam(':msgId',$msgId, PDO::PARAM_STR);
			$message->execute();
			$array_status = array('status' => 'success' );
			$json_status = json_encode($array_status);
			print_r($json_status);
		}
		
	}

	if ( $_POST['formType'] == 'escEdit') {
			$escTo = $_POST['escTo'];
			$escStatus = $_POST['escStatus'];
			$escId = $_POST['escId'];
			$msgId = $_POST['msgId'];
			$resolution = $_POST['resolution'];

			if ($escStatus == '3') {
				date_default_timezone_set('Africa/Nairobi');
				$resDate = date('Y-m-d h:i:s', time());	
				$qnewEsc = "UPDATE tickets SET status=:escStatus, resolution_date = :resDate, resolution = :resolution WHERE id = :escId";
				$insertEsc = $con->prepare($qnewEsc);
				$insertEsc->bindParam(':escId',$escId, PDO::PARAM_INT);
				$insertEsc->bindParam(':resDate',$resDate, PDO::PARAM_INT);
				$insertEsc->bindParam(':escStatus',$escStatus, PDO::PARAM_INT);
				$insertEsc->bindParam(':resolution',$resolution, PDO::PARAM_INT);
				$insertEsc->execute();

				$qry = "SELECT c.telephone_no FROM messages m LEFT JOIN customer_info c ON m.customer_id = c.customer_id WHERE m.id = :msgId ";
				$getCustomer = $con->prepare($qry);
				$getCustomer->bindParam(':msgId',$msgId, PDO::PARAM_INT);
				$getCustomer->execute();
				$customer = $getCustomer->fetch();
				$customerPhone = $customer['telephone_no'];
				if ($customerPhone !== '' && is_numeric($customerPhone)) {
					$msgText = 'Dear Valued Customer, your query ticket id '.$escId.' has been closed. Thank you for choosing Isuzu. Tuko pamoja safarini!';
					sendSMS($customerPhone, $msgText);
				}

			}else{

				$qry = "SELECT m.customer_id, t.sub_cat_id, m.text, t.escalated_to FROM tickets t LEFT JOIN messages m ON m.id = t.msg_id  WHERE t.id = :escId ";
				$getTicket = $con->prepare($qry);
				$getTicket->bindParam(':escId',$escId, PDO::PARAM_INT);
				$getTicket->execute();
				$ticket = $getTicket->fetch();

				$customerId = $ticket['customer_id'];
				$subCat = $ticket['sub_cat_id'];
				$msg = $ticket['text'];

				$qnewEsc = "UPDATE tickets SET escalated_to=:escTo, status=:escStatus WHERE id = :escId";
				$insertEsc = $con->prepare($qnewEsc);
				$insertEsc->bindParam(':escId',$escId, PDO::PARAM_INT);
				$insertEsc->bindParam(':escTo',$escTo, PDO::PARAM_INT);
				$insertEsc->bindParam(':escStatus',$escStatus, PDO::PARAM_INT);
				$insertEsc->execute();
				if ($ticket['escalated_to'] !== $escTo) {
					$qry = "SELECT c.telephone_no, c.company_name, u.email, u.first_name, u.last_name FROM users u LEFT JOIN customer_info c ON c.customer_id = u.id WHERE u.id = :customerId";
					$getCustomer = $con->prepare($qry);
					$getCustomer->bindParam(':customerId',$customerId, PDO::PARAM_STR);
					$getCustomer->execute();	
					$customer = $getCustomer->fetch();

					$qry = "SELECT * FROM esc_contact WHERE id = :escTo";
					$escContact = $con->prepare($qry);
					$escContact->bindParam(':escTo',$escTo, PDO::PARAM_STR);
					$escContact->execute();	
					$contact = $escContact->fetch();

					$qry = "SELECT c.cat_name, sc.sub_cat_name FROM  sub_categories sc INNER JOIN categories c ON sc.cat_id = c.id WHERE sc.id = :subCat";
					$escCategory = $con->prepare($qry);
					$escCategory->bindParam(':subCat',$subCat, PDO::PARAM_STR);
					$escCategory->execute();	
					$e_category = $escCategory->fetch();


						$to = $contact['email'];
						$subj = 'CONTACT CENTRE CRM ESCALATION';
						$mail_msg = 'Dear '.$contact['name'].' ,<br><br>';
						$mail_msg .= 'The below issue has been escalated to you. Kindly action and respond within 12hours. Details:<br><br><br>';
						$mail_msg .= 'Customer Id	:'.$customerId.'<br><br>';
						$mail_msg .= 'Customer Name	: '.$customer['first_name'].' '.$customer['last_name'].'<br><br>';
						$mail_msg .= 'Phone No.	: '.$customer['telephone_no'].'<br><br>';
						$mail_msg .= 'Company	: '.$customer['company_name'].'<br><br>';
						$mail_msg .= 'Email	: '.$customer['email'].'<br><br>';
						$mail_msg .= 'Category	:'.$e_category['cat_name'].'<br><br>';
						$mail_msg .= 'Sub Category	:'.$e_category['sub_cat_name'].'<br><br>';
						$mail_msg .= 'Message	:'.$msg.'<br>';
						$mail_msg .= 'Sincerely,<br>';
						$mail_msg .= 'Isuzu Contact Center Team <br>';
						$mail_msg .= 'Contact Center Phone no: 0800724724 <br><br><br>';
						$mail_msg .= '* Please respond within 12hours. Thank you';
						escalationMail($to, $subj, $mail_msg);	
						
						$smsCustomerNumber = $customer['telephone_no'];

						if ($smsCustomerNumber !== '' && is_numeric($smsCustomerNumber)) {
							$msgText = 'Dear Valued Customer, thank you for contacting us. Your query has been escalated. Your ticket id is '.$ticketNo.'. You will receive feedback within 24hours. Thank you';
							sendSMS($smsCustomerNumber, $msgText);
						}

						if ($contact['phone'] !== '' && is_numeric($contact['phone'])) {
							$msgText = 'Dear '.$contact['name'].', you have received a customer query on your email for your prompt action. Please respond within 12hours. Thank you';
							sendSMS($contact['phone'], $msgText);
						}

				}			
			}
	}

	if ($_POST['formType'] == 'othermsg') {
		$othermsg = $_POST['othermsg'];
		$msgId = $_POST['intId'];
		$qMessage = "UPDATE messages SET other_issue = :othermsg  WHERE id = :msgId";
		$message = $con->prepare($qMessage);
		$message->bindParam(':othermsg',$othermsg, PDO::PARAM_STR);
		$message->bindParam(':msgId',$msgId, PDO::PARAM_STR);
		$message->execute();		
	}

	if ($_POST['formType'] == 'escComment') {
		$escComment = $_POST['escComment'];
		$escId = $_POST['escId'];
		$qnewEsc = "INSERT INTO esc_comments(esc_id,user_id, comment) VALUES (:escId, :userId, :escComment)";
		$insertEsc = $con->prepare($qnewEsc);
		$insertEsc->bindParam(':escComment',$escComment, PDO::PARAM_STR);
		$insertEsc->bindParam(':escId',$escId, PDO::PARAM_STR);
		$insertEsc->bindParam(':userId',$userId, PDO::PARAM_STR);
		$insertEsc->execute();		
	}
	// ::::::::::::::::::::::::::::::: SERVICE BOOKING ::::::::::::::::::::::::::::::::::::::::
	if ($_POST['formType'] == 'new_booking') {
		$customerId = $_POST['customerId'];
		$vehicle = $_POST['vehicle'];
		$appDate = $_POST['app_date'].' '.$_POST['app_time'].':00';	
		$branch = $_POST['branch'];
		$repairDesc = $_POST['repair_desc'];	
		$section = $_POST['section'];	
		$serviceType = $_POST['service_type'];
		$contactPerson = $_POST['contact_person'];

		$qry = "INSERT INTO bk_reservations(branch_id, customer_id, vehicle_id, contact_person, user_id, appointment_date, section_id, service_type, repair_description) VALUES (:branch, :customerId, :vehicleId, :contactPerson, :userId,:appDate, :section, :serviceType, :repairDesc)";
		$stmt = $con->prepare($qry);
		$stmt->bindParam(':customerId',$customerId, PDO::PARAM_STR);
		$stmt->bindParam(':vehicleId',$vehicle, PDO::PARAM_STR);
		$stmt->bindParam(':contactPerson',$contactPerson, PDO::PARAM_STR);
		$stmt->bindParam(':userId',$userId, PDO::PARAM_STR);
		$stmt->bindParam(':appDate',$appDate, PDO::PARAM_STR);
		$stmt->bindParam(':branch',$branch, PDO::PARAM_STR);
		$stmt->bindParam(':repairDesc',$repairDesc, PDO::PARAM_STR);
		$stmt->bindParam(':section',$section, PDO::PARAM_STR);
		$stmt->bindParam(':serviceType',$serviceType, PDO::PARAM_STR);
		$stmt->execute();

		$bookingId = $con->lastInsertId();
		$branch = branch($branch);
		$customer = customer($customerId);
		$sec = section($section);
		$vehicleInfo = vehicle($vehicle);
//::::::::::::::::::::::::::::::::::::::::::

		$to = $branch['email'].',fridah.kohogo@isuzu.co.ke';
		$subj = 'CONTACT CENTRE SERVICE BOOKING';
		$mail_msg = 'Dear ISUZU Team ,<br><br>';
		$mail_msg .= 'You have a new service booking:<br><br><br>';
		$mail_msg .= '<strong>Customer Id</strong>	: '.$customerId.'<br><br>';
		$mail_msg .= '<strong>Customer Name</strong>	: '.$customer['first_name'].' '.$customer['last_name'].'<br><br>';
		$mail_msg .= '<strong>Contact person.</strong>	: '.$contactPerson.'<br><br>';
		$mail_msg .= '<strong>Phone No.</strong>	: '.$customer['telephone_no'].'<br><br>';
		$mail_msg .= '<strong>Company</strong>	: '.$customer['company_name'].'<br><br>';
		$mail_msg .= '<strong>Email</strong>	: '.$customer['email'].'<br><br>';
		
		$mail_msg .= '<strong>Booked Date</strong>	: '.date('M-d-Y',strtotime($appDate)).'<br><br>';
		$mail_msg .= '<strong>Booked Time</strong>	: '.date('h:i A',strtotime($appDate)).'<br><br>';
		$mail_msg .= '<strong>Section</strong>		: '.$sec['name'].'<br><br>';
		$mail_msg .= '<strong>Service Type</strong>	: '.$serviceType.'<br><br>';
		$mail_msg .= '<strong>REG NO.</strong>	: '.$vehicleInfo['reg_no'].'<br><br>';
		$mail_msg .= '<strong>Model</strong>	: '.$vehicleInfo['v_model'].'<br><br>';
		$mail_msg .= '<strong>Repair Description</strong>	: '.$repairDesc.'<br><br>';
		$mail_msg .= '<strong>Sincerely,<br>';
		$mail_msg .= '<strong>Isuzu Contact Center Team <br>';
		$mail_msg .= '<strong>Contact Center Phone no: 0800724724 <br><br>';
		escalationMail($to, $subj, $mail_msg);
		$smsCustomerNumber = $customer['telephone_no'];
		if ($smsCustomerNumber !== '' && is_numeric($smsCustomerNumber)) {

			$msgText = 'Dear '.$customer['first_name'].' you have booked a service appointment on '.date('M-d-Y',strtotime($appDate)).' '.date('h:i A',strtotime($appDate)).' for your Isuzu '.$vehicleInfo['reg_no'].'. For any enquiries call 0800724724. Thank you';

			sendSMS($smsCustomerNumber, $msgText);
		}
//::::::::::::::::::::::::::::::::::::::::

	    $array_response = array('status' => 'success', 'bookingId' => $bookingId );
	    $json_response = json_encode($array_response);
	    die($json_response);
	}

}

function branch($branchId){
    global $con;
    $qry = "SELECT id, name, phone, email FROM bk_branches WHERE id = :branchId";
    $stmt = $con->prepare($qry);
    $stmt->bindParam(':branchId', $branchId, PDO::PARAM_STR);
    $stmt->execute();
    $branch = $stmt->fetch(); 
    return($branch);
}

function customer($customerId){
	global $con;
	$qry = "SELECT u.id user_id, u.first_name, u.last_name, u.email, c.company_name, c.telephone_no, c.business_type, c.physical_address, c.town, (SELECT type FROM v_make WHERE v_make.id = v.make) v_make, (SELECT model_name FROM v_model WHERE v_model.id = v.model) v_model, v.reg_no, v.vin_no, v.body_type, v.application, v.fleet_size FROM users u INNER JOIN customer_info c ON u.id = c.customer_id LEFT JOIN vehicle_info v ON v.customer_id = u.id WHERE u.id = :customerId";
	$stmt = $con->prepare($qry);
	$stmt->bindParam(':customerId',$customerId, PDO::PARAM_STR);
	$stmt->execute();	
	$customer = $stmt->fetch();
	return $customer;
}

function section($sectionId){
    global $con;
    $qry = "SELECT id, name FROM bk_sections WHERE id = :sectionId";
    $stmt = $con->prepare($qry);
    $stmt->bindParam(':sectionId',$sectionId, PDO::PARAM_STR);
    $stmt->execute();
    $section = $stmt->fetch(); 
    return($section);        
}

if (isset($_GET['source'])) {
	$source = $_GET['source'];
	$sql = "INSERT INTO sma_platforms(p_text) VALUES('$source')";
	$query = mysqli_query($con, $sql);
}

if (isset($_GET['category'])) {
	$categoryType = $_GET['categoryType'];
	$category = $_GET['category'];
	$sqlCat = "INSERT INTO categories(item,cat_name) VALUES(:categoryType,:category)";
	$insertCat = $con->prepare($sqlCat);
	$insertCat->bindParam(':categoryType',$categoryType, PDO::PARAM_STR);
	$insertCat->bindParam(':category',$category, PDO::PARAM_STR);
	$insertCat->execute();	
}

if (isset($_GET['sub_category'])) {
	$catId = $_GET['catId'];
	$subCat = $_GET['sub_category'];
	$sqlSubCat = "INSERT INTO sub_categories(cat_id,sub_cat_name, visible) VALUES(:catId, :subCat, '1')";
	$insertSubCat = $con->prepare($sqlSubCat);
	$insertSubCat->bindParam(':catId',$catId, PDO::PARAM_STR);
	$insertSubCat->bindParam(':subCat',$subCat, PDO::PARAM_STR);
	$insertSubCat->execute();
}


if (isset($_POST['scriptContent'])) {
	$content = nl2br($_POST['scriptContent']);
	$subCat = $_POST['subCategory'];

	$sqlScriptContent = "INSERT INTO guide(guide_text) VALUES(:content)";
	$scriptContent = $con->prepare($sqlScriptContent);
	$scriptContent->bindParam(':content',$content, PDO::PARAM_STR);
	$scriptContent->execute();
	$scriptId = $con->lastInsertId();

	$sqlScriptGuide = "INSERT INTO script_guide(sub_cat_id, guide_id) VALUES(:subCat, :scriptId)";
	$scriptGuide = $con->prepare($sqlScriptGuide);
	$scriptGuide->bindParam(':scriptId',$scriptId, PDO::PARAM_STR);
	$scriptGuide->bindParam(':subCat',$subCat, PDO::PARAM_STR);
	$scriptGuide->execute();
}

if (isset($_GET['visible'])) {

	if (isset($_GET['catId'])) {
		$visible = $_GET['visible'];
		$catId = $_GET['catId'];
		if ($visible == '2') {
			$visibility = '1';
			$response = '<div id="cat'.$catId.'" style="cursor: pointer;" onclick="catVisibility('.$visibility.','.$catId.')"><span class="fa fa-eye" style="font-size:20px;"></span></div>';
		}elseif ($visible == '1') {
			$visibility = '2';
			$response = '<div id="cat'.$catId.'" style="cursor: pointer;" onclick="catVisibility('.$visibility.','.$catId.')"><span class="fa fa-eye-slash" style="font-size:20px; color: #6c757d"></span></div>';
		}

		$qry = "UPDATE categories SET visible=:visibility WHERE id = :catId";
		$updateCat = $con->prepare($qry);
		$updateCat->bindParam(':visibility',$visibility, PDO::PARAM_INT);
		$updateCat->bindParam(':catId',$catId, PDO::PARAM_INT);
		$updateCat->execute();
		echo $response;
	}

	if (isset($_GET['subCatId'])) {
		$visible = $_GET['visible'];
		$subCatId = $_GET['subCatId'];
		if ($visible == '2') {
			$visibility = '1';
			$response = '<div id="subCat'.$subCatId.'" style="cursor: pointer;" onclick="subCatVisibility('.$visibility.','.$subCatId.')"><span class="fa fa-eye" style="font-size:20px;"></span></div>';
		}elseif ($visible == '1') {
			$visibility = '2';
			$response = '<div id="subCat'.$subCatId.'" style="cursor: pointer;" onclick="subCatVisibility('.$visibility.','.$subCatId.')"><span class="fa fa-eye-slash" style="font-size:20px; color: #6c757d"></span></div>';
		}

		$qry = "UPDATE sub_categories SET visible=:visibility WHERE id = :subCatId";
		$updateCat = $con->prepare($qry);
		$updateCat->bindParam(':visibility',$visibility, PDO::PARAM_INT);
		$updateCat->bindParam(':subCatId',$subCatId, PDO::PARAM_INT);
		$updateCat->execute();
		echo $response;
	}
}



if (isset($_POST['contactEmail'])) {
$category = $_POST['category'];
$contactEmail = $_POST['contactEmail'];
$contactLevel = $_POST['contactLevel'];
$contactName = $_POST['contactName'];
$subCategory = $_POST['subCategory'];
$telNo = $_POST['telNo'];

$sql = "INSERT INTO esc_contact(name, email, phone, sub_cat_id, contact_level) VALUES(:contactName, :contactEmail, :telNo, :subCategory, :contactLevel)";
$newEscContact = $con->prepare($sql);
$newEscContact->bindParam(':contactName',$contactName, PDO::PARAM_STR);
$newEscContact->bindParam(':contactEmail',$contactEmail, PDO::PARAM_STR);
$newEscContact->bindParam(':telNo',$telNo, PDO::PARAM_STR);
$newEscContact->bindParam(':subCategory',$subCategory, PDO::PARAM_STR);
$newEscContact->bindParam(':contactLevel',$contactLevel, PDO::PARAM_STR);
$newEscContact->execute();	
}

if (isset($_GET['delete_category'])) {
	$qry = "DELETE FROM categories WHERE id = :catId";
	$delCat = $con->prepare($qry);
	$delCat->bindParam(':catId',$_GET['delete_category'], PDO::PARAM_STR);
	$delCat->execute();
	header('Location:../dashboard.php?content=settings&item=categories&man_item_response=1');
}

if (isset($_GET['delete_sub_category'])) {
	$qry = "DELETE FROM sub_categories WHERE id = :subCatId";
	$delSubCat = $con->prepare($qry);
	$delSubCat->bindParam(':subCatId',$_GET['delete_sub_category'], PDO::PARAM_STR);
	$delSubCat->execute();
	header('Location:../dashboard.php?content=settings&item=sub_categories&man_item_response=1');	
}

if (isset($_GET['delete_script'])) {
	$qry = "DELETE FROM guide WHERE id = :scriptId";
	$stmt = $con->prepare($qry);
	$stmt->bindParam(':scriptId',$_GET['delete_script'], PDO::PARAM_STR);
	$stmt->execute();

	$qry = "DELETE FROM script_guide WHERE guide_id = :scriptId";
	$stmt = $con->prepare($qry);
	$stmt->bindParam(':scriptId',$_GET['delete_script'], PDO::PARAM_STR);
	$stmt->execute();
	//header('Location:../dashboard.php?content=settings&item=script&man_item_response=1');
}

if (isset($_GET['delete_contact'])) {
	echo $_GET['delete_contact'];
	$qry = "UPDATE esc_contact SET status= '0' WHERE id = :contactId";
	$stmt = $con->prepare($qry);
	$stmt->bindParam(':contactId',$_GET['delete_contact'], PDO::PARAM_STR);
	$stmt->execute();
	//header('Location:../dashboard.php?content=settings&item=esc_contacts&man_item_response=1');
}

if (isset($_GET['user_delete'])) {
	$qry = "DELETE FROM users WHERE id = :userId";
	$stmt = $con->prepare($qry);
	$stmt->bindParam(':userId',$_GET['user_delete'], PDO::PARAM_STR);
	$stmt->execute();
}

if (isset($_GET['deactivate_user'])) {
	$qry = "UPDATE users SET status= '0' WHERE id = :userId";
	$stmt = $con->prepare($qry);
	$stmt->bindParam(':userId',$_GET['deactivate_user'], PDO::PARAM_STR);
	$stmt->execute();
	header('Location: ../dashboard.php?content=users');
}
if (isset($_GET['activate_user'])) {
	$qry = "UPDATE users SET status= '1' WHERE id = :userId";
	$stmt = $con->prepare($qry);
	$stmt->bindParam(':userId',$_GET['activate_user'], PDO::PARAM_STR);
	$stmt->execute();
	header('Location: ../dashboard.php?content=users');
}

?>