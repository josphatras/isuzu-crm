<?php
include_once('../dao/config/include.php');
include_once('../dao/config/db.php');

$report = fopen('../reports/dlight Report - '.date('Y-m-d').'.csv', 'w');
$agentsReport = array(
	"Message Id",
	"Customer Name",
	"Message",
	"Escalation Time",
	"Reply Date",
	"Status",
	"Escalated By",	
	"Replied By",
);
fputcsv($report, $agentsReport);

$fromDate = $_GET['from_date'].' '.'00:00:18';
$toDate = $_GET['to_date'].' '.'23:59:18';

if (isset($_GET['status'])) {
	$status = $_GET['status'];
	if ($status == 'all') {
		if ($_GET['from_date'] !== '') {
			$fromDate = $_GET['from_date'].' '.'00:00:18';
			$toDate = $_GET['to_date'].' '.'23:59:18';
			$qesc = "SELECT e.id esc_id, e.cl_account, e.cl_phone, e.cl_name, e.esc_message,(SELECT id FROM messages  WHERE created_on=( SELECT MAX(created_on) FROM messages WHERE esc_id = e.id)) message_id, (SELECT status_name FROM esc_status WHERE esc_status.status_id = e.status) esc_status, e.created_on esc_on, u.first_name, u.last_name FROM escalations e INNER JOIN users u ON  u.id = e.from_user WHERE e.created_on >= :fromDate AND e.created_on <= :toDate ORDER BY e.created_on DESC";
			$getesc = $con->prepare($qesc);
			$getesc->bindParam(':fromDate', $fromDate, PDO::PARAM_STR);
			$getesc->bindParam(':toDate', $toDate, PDO::PARAM_STR);
			$getesc->execute();
		}else{
			$qesc = "SELECT e.id esc_id, e.cl_account, e.cl_phone, e.cl_name, e.esc_message,(SELECT id FROM messages  WHERE created_on=( SELECT MAX(created_on) FROM messages WHERE esc_id = e.id)) message_id, (SELECT status_name FROM esc_status WHERE esc_status.status_id = e.status) esc_status, e.created_on esc_on, u.first_name, u.last_name FROM escalations e INNER JOIN users u ON  u.id = e.from_user WHERE e.status = 'no result'";
			$getesc = $con->prepare($qesc);
			$getesc->bindParam(':status', $status, PDO::PARAM_STR);
			$getesc->execute();
		}
	}else{
		if ($_GET['from_date'] !== '') {
			$fromDate = $_GET['from_date'].' '.'00:00:18';
			$toDate = $_GET['to_date'].' '.'23:59:18';
			$qesc = "SELECT e.id esc_id, e.cl_account, e.cl_phone, e.cl_name, e.esc_message,(SELECT id FROM messages  WHERE created_on=( SELECT MAX(created_on) FROM messages WHERE esc_id = e.id)) message_id, (SELECT status_name FROM esc_status WHERE esc_status.status_id = e.status) esc_status, e.created_on esc_on, u.first_name, u.last_name FROM escalations e INNER JOIN users u ON  u.id = e.from_user WHERE e.status = :status AND e.created_on >= :fromDate AND e.created_on <= :toDate";
			$getesc = $con->prepare($qesc);
			$getesc->bindParam(':status', $status, PDO::PARAM_STR);
			$getesc->bindParam(':fromDate', $fromDate, PDO::PARAM_STR);
			$getesc->bindParam(':toDate', $toDate, PDO::PARAM_STR);
			$getesc->execute();
		}else {
			$qesc = "SELECT e.id esc_id, e.cl_account, e.cl_phone, e.cl_name, e.esc_message,(SELECT id FROM messages  WHERE created_on=( SELECT MAX(created_on) FROM messages WHERE esc_id = e.id)) message_id, (SELECT MAX(created_on) FROM messages WHERE esc_id = e.id) , (SELECT status_name FROM esc_status WHERE esc_status.status_id = e.status) esc_status, e.created_on esc_on, u.first_name, u.last_name FROM escalations e INNER JOIN users u ON  u.id = e.from_user WHERE e.status = :status ORDER BY e.created_on DESC";
			$getesc = $con->prepare($qesc);
			$getesc->bindParam(':status', $status, PDO::PARAM_STR);
			$getesc->execute();
		}
	}
}

		while ( $esc = $getesc->fetch(PDO::FETCH_ASSOC)) {
			$messageId = $esc['message_id'];
			$qreply = "SELECT m.created_on replied_on, u.first_name, u.last_name FROM messages m INNER JOIN users u ON m.user_id = u.id WHERE m.id = :messageId ";
			$getReply = $con->prepare($qreply);
			$getReply->bindParam(':messageId', $messageId, PDO::PARAM_INT);
			$getReply->execute();
			$reply = $getReply->fetch();

			$row = array(
				$esc['esc_id'],
				$esc['cl_name'],
				$esc['esc_message'],
				$esc['esc_on'],
				$reply['replied_on'],
				$esc['esc_status'],
				$esc['first_name'].' '. $esc['last_name'],
				$reply['first_name'].' '. $reply['last_name']
			);
			fputcsv($report, $row);

		}

 
fclose($report);


header('location: '.'../reports/dlight Report - '.date('Y-m-d').'.csv')

?>