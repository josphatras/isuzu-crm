<?php
include_once('header.php');
include_once('dao/config/db.php');
require_once('dao/functions.php');
require_once('include/user_roles.php');
if (!isset($_SESSION["isuzu_user_id"])) {
	header('Location: /'.ROOT_FOLDER.'dao/login.php');
}

$userId = $_SESSION["isuzu_user_id"];
?>


<div class="container-fluid primary-color">
	<div class="row">
		<div class="col-md-12 col-sm-12" style="padding:0;">
			<div class="col-main">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-2" style="padding:0;">
							<?php 
								
								if (isset($_SESSION['isuzu_dealer'])) {
									$dealer = dealer($_SESSION['isuzu_dealer']);
									echo "<p style='color:red;font-weight:800;'>Welcome <span style=''>".$dealer['name']."</span></p>";
								}
							?>

							<?php if ($agent == '1' || $dealer_r == '1'): ?>
		            <div class="row">
		                <div class="col-md-12 my-1">
		                    <div class="input-group mb-3">
		                        <input type="text" class="form-control" placeholder="Client" aria-label="Recipient's username" aria-describedby="basic-addon2" id="searchPhrase">
		                        <div class="input-group-append">
		                            <span class="btn btn-secondary" id="basic-addon2" onclick="searchCustomer()">Search</span>
		                        </div>
		                    </div>
		                </div>
		            </div>
			        <?php endif ?>
							<div class="left-nav-section">
								<?php include_once('include/side_content.php'); ?>
							</div>
						</div>
						<div class="col-md-10" style="padding:0;">
							<div id="responseSection" class="response-section">
								<?php
									if (isset($_GET['response'])) {
										echo "<p>".$_GET['response']."</p>";
									}
								?>
							</div>
							<div class="dash-col-main">
								<?php
									if (isset($_GET['content'])) {
										$content = $_GET['content'];

										switch ($content) {
											case 'new_user':
												include ('users/new_user.php');
												break;
											case 'edit_user':
												include ('users/edit_user.php');
												break;
											case 'view_user':
												include ('users/view_user.php');
												break;
											case 'users':
												include ('users/home.php');
												break;
											case 'account':
												include ('users/account.php');
												break;
											case 'tickets':
												include ('tickets/create_ticket.php');
												break;
											case 'msg':
												include ('messages/create_msg.php');
												break;
											case 'escalations':
												include ('reports/escalations.php');
												break;
											case 'interactions':
												include ('reports/messages.php');
												break;
											case 'bookings':
												include ('booking.php');
												break;
											case 'reports':
												include ('reports/index.php');
												break;
											case 'advisor_new_customer':
												include ('forms/advisor_create_customer.php');
												break;
											case 'customers_list':
												include ('reports/customers.php');
												break;
											case 'dealer_customer_list':
												include ('customers/dealer_customers.php');
												break;
											case 'customer':
												include ('reports/customer_account.php');
												break;
											case 'settings':
												include ('dao/admin/manage/manage_items.php');
												break;
											case 'customers_upload':
												include ('customers/upload.php');
												break;
											case 'surveys':
												include 'surveys/index.php';
												break;
											case 'services':
												include 'services/index.php';
												break;
											default:
												# code...
												break;
										}
									}else{
										include_once('main.php');
									}
								?>
								<?php if ($_SESSION['isuzu_role'] == '4'): ?>
								<?php endif ?>
							</div>
						</div>	
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
include_once('footer.php');
?>


