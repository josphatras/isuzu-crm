<script src="/<?php echo ROOT_FOLDER; ?>assets/js/jquery-3.3.1.min.js"></script>
<script src="/<?php echo ROOT_FOLDER; ?>assets/js/jquery-ui.js"></script>
<script src="/<?php echo ROOT_FOLDER; ?>assets/js/bootstrap.bundle.min.js"></script>
<script src="/<?php echo ROOT_FOLDER; ?>assets/js/bootstrap.min.js"></script>
<script src="/<?php echo ROOT_FOLDER; ?>assets/js/main-2.2.js"></script>
<script src="/<?php echo ROOT_FOLDER; ?>assets/js/jquery.dataTables.min.js"></script>
<script src="/<?php echo ROOT_FOLDER; ?>assets/js/dataTables.bootstrap4.min.js"></script>
<script>
    function totalInteractions(){
        $('ttl').html('Total Interactions');
        var request = $.ajax({
            url: 'report_dash.php?type=messages',
            data: '',
            dataType: 'html',
        });
        request.done(function( data ) {
            $('#total-interactions').html(data);
            $('.isuzu-datatable').dataTable();
        }); 
    }
    
    function totalEscalations(){
        $('ttl').html('Total Escalations');
        var request = $.ajax({
            url: 'report_dash.php?type=escalations',
            data: '',
            dataType: 'html',
        });
        request.done(function( data ) {
            $('#total-interactions').html(data);
            $('.isuzu-datatable').dataTable();
        });
    }
    function categoryInteractions(category){
        //$('ttl').html('Total Escalations');
        var request = $.ajax({
            url: 'report_dash.php?type=messages&category='+category,
            data: '',
            dataType: 'html',
        });
        request.done(function( data ) {
            $('#total-interactions').html(data);
            $('.isuzu-datatable').dataTable();
        });
    }
    function categoryEscalations(category){
        //$('ttl').html('Total Escalations');
        var request = $.ajax({
            url: 'report_dash.php?type=escalations&category='+category,
            data: '',
            dataType: 'html',
        });
        request.done(function( data ) {
            $('#total-interactions').html(data);
            $('.isuzu-datatable').dataTable();
        });
    }
    function quickMessages(range){
        var request = $.ajax({
            url: 'report_dash.php?type=messages&range='+range,
            data: '',
            dataType: 'html',
        });
        request.done(function( data ) {
            $('#total-interactions').html(data);
            $('.isuzu-datatable').dataTable();
        });
    }
    function quickEscalations(range){
        var request = $.ajax({
            url: 'report_dash.php?type=escalations&range='+range,
            data: '',
            dataType: 'html',
        });
        request.done(function( data ) {
            $('#total-interactions').html(data);
            $('.isuzu-datatable').dataTable();
        });
    }
</script>
</body>
</html>