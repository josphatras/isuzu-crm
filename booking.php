<?php


?>
<div style="margin: 0 0 30px 0">
	<div class="row">
		<div class="col-md-6 col-sm-12 col-xm-12">
			<nav class="nav nav-pills nav-fill">
				<?php if (!isset($_SESSION['isuzu_dealer'])): ?>
			  <a class="nav-item nav-link <?php echo ($_GET['item'] == 'new_booking')? 'active' : ''; ?>" href="?content=customers_list">New Booking</a>
				<?php endif ?>
				<?php if (isset($_SESSION['isuzu_dealer'])): ?>
			  <a class="nav-item nav-link <?php echo ($_GET['item'] == 'new_booking')? 'active' : ''; ?>" href="#">New Booking</a>
				<?php endif ?>
			  <a class="nav-item nav-link <?php echo ($_GET['item'] == 'view_bookings')? 'active' : ''; ?>" href="?content=bookings&item=view_bookings">View Bookings</a>
			  <a class="nav-item nav-link <?php echo ($_GET['item'] == 'view_calendar')? 'active' : ''; ?>" href="?content=bookings&item=view_calendar">View Calendar</a>
			</nav>		
		</div>
	</div>
</div>

<?php
	if (isset($_GET['item'])) {
		switch ($_GET['item']) {
			case 'new_booking':
				include 'booking/new_booking.php';
				break;
			case 'view_calendar':
				include 'booking/view_calender.php';
				break;
			case 'view_bookings':
				include 'booking/view_bookings.php';
				break;
			
			default:
				# code...
				break;
		}
		# code...
	}else{
		include 'booking/view_calender.php';
	}
?>