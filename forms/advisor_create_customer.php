<div class="container py-3">
    <div class="row">
        <div class="mx-auto col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="mb-0">Customer Details</h4>
                </div>
                <div class="card-body">
					<form id="advisor_create_customer">
						<div class="form-group row">
						    <label class="col-lg-3 col-form-label form-control-label">Customer Name</label>
						    <div class="col-lg-9">
						        <input type="text" class="form-control" id="customerName" name="customerName" placeholder="Customer Name" value="" required>
						    </div>
						</div>
						<div class="form-group row">
						    <label class="col-lg-3 col-form-label form-control-label">Company name</label>
						    <div class="col-lg-9">
						       <input type="text" class="form-control" id="companyName" name="companyName" placeholder="Company name" value="">
						    </div>
						</div>
						<div class="form-group row">
						    <label class="col-lg-3 col-form-label form-control-label">Telephone number</label>
						    <div class="col-lg-9">
						        <input type="text" class="form-control" id="telNo" name="telNo" placeholder="Telephone number" value="" required>
						    </div>
						</div>
						<div class="form-group row">
						    <label class="col-lg-3 col-form-label form-control-label">Email address</label>
						    <div class="col-lg-9">
						        <input type="text" class="form-control" id="email" name="email" placeholder="Email address" value="">
						    </div>
						</div>
						<div class="form-group row">
						    <label class="col-lg-3 col-form-label form-control-label">Type of business (Industry)</label>
						    <div class="col-lg-9">
						        	<input type="text" class="form-control" id="busType" name="busType" placeholder="Type of business" value="">
						    </div>
						</div>
						<div class="form-group row">
						    <label class="col-lg-3 col-form-label form-control-label">Physical address</label>
						    <div class="col-lg-9">
						        <input type="text" class="form-control" id="phyAddress" name="phyAddress" placeholder="Physical address" value="">
						    </div>
						</div>
						<div class="form-group row">
						    <label class="col-lg-3 col-form-label form-control-label">Town</label>
						    <div class="col-lg-9">
						        <input type="text" class="form-control" id="town" name="town" placeholder="Town" value="" required>
						    </div>
						</div>
						<input type="hidden" name="formType" value="advisor_create_customer">
						<div class="col-md-8 mb-2">
							<button id="submit_btn" type="submit" class="btn btn-secondary">Create</button>
						</div>
						<br>
						<div id="response"></div>
					</form>
                </div>
            </div>
        </div>
    </div>
</div>	