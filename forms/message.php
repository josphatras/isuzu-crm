<?php
//session_start();
include_once('dao/config/db.php');
require_once('dao/functions.php');
$contactTypes = allConTypes();
$categories = sel_Cat('msg');
$escCategories = sel_Cat('ticket');
$isuzuUsers = isuzu_users();

if (isset($_SESSION["customerId"])) {
$customerId = $_SESSION["customerId"];
}elseif(isset($_GET['customerId'])) {
 $customerId = $_GET['customerId'];
}
?>


<div class="container py-3">
    <div class="row">
        <div class="mx-auto col-sm-12">
	        <div class="card">
	            <div class="card-header">
	                <h4 class="mb-0">Interaction</h4>
	            </div>
	            <div class="card-body">
	            <div class="alert alert-danger create-danger" id="msgResponse" style="display: none">One or more empty input field</div>
					<form>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group row">
								    <label class="col-lg-3 col-form-label form-control-label">Message</label>
								    <div class="col-lg-9">
								        <textarea class="form-control" id="msg" name="msg" rows="3" columns="8"></textarea>
								    </div>
								</div>
								<div class="form-group row">
								    <label class="col-lg-3 col-form-label form-control-label">Escalate?</label>
								    <div class="col-lg-9">
								        <select class="form-control" size="0" name="escSelect" id="escSelect" onchange="escOption();">
											    <option value="">Choose...</option>
											    <option value="yes">Yes</option>
											    <option value="no">No</option>
								        </select>
								    </div>
								</div>
								<div id="escNo">
									<div class="col-md-8 mb-2" style="margin-top: 30px;">		
										<button type="button" class="btn btn-secondary" onclick="escalateMessage('no');">Save</button>
									</div>	
								</div>
								<div id="escYes" style="display: none">
									<div class="form-group row">
									    <label class="col-lg-3 col-form-label form-control-label">Category</label>
									    <div class="col-lg-9">
									        <select class="form-control" size="0" name="escCategory" id="escCategory" onchange="getSubCat('esc');">
										    <option selected>Choose...</option>
										    <?php
										    	foreach ($escCategories as $esc_category) {
										    		echo '<option value="'.$esc_category['id'].'">'.$esc_category['cat_name'].'</option>';
										    	}
										    ?>
									        </select>
									    </div>
									</div>
									<div id="escSubCat"></div>	
									<!-- <div id="contactSubCatSec"></div> -->	
									
									<div class="form-group row">
									    <label class="col-lg-3 col-form-label form-control-label">Contacts</label>
									    <div class="col-lg-9">
									        <div id="contacts">
									        	<select class="form-control" size="0" name="escTo" id="escTo">
									        		<option value="">Choose...</option>
												</select>
    
									        </div>
									    </div>
									</div>																	
										
									<input type="hidden" name="formType" value="interaction">
									<div class="col-md-8 mb-2" style="margin-top: 30px;" style="">
										<button type="button" class="btn btn-secondary" onclick="escalateMessage('yes');">Escalate</button>			
									</div>	
								</div>
							</div>
						</div>
					</form>
	            </div>
	        </div>  
        </div>
    </div>
</div>	