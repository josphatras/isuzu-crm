<p>Fantastic <strong><?php echo $customer['first_name'] ?></strong>, is there anything else you would like me to assist you with or clarify as per our conversation?</p>
<div class="form-group row">
    <div class="col-lg-3">
        <select id="elseInfo" name="elseInfo" class="form-control" size="0" onchange="anthingElse()">
	    <option>Choose...</option>
	    <option value="Yes">Yes</option>
	    <option value="No">No</option>
        </select>
    </div>
</div>

<div id="otherSection" style="display: none">
	<form id="othermsgForm">
	    <label class="col-lg-3 col-form-label form-control-label">Message</label>
	    <div class="col-lg-9">
	        <textarea class="form-control" id="othermsg" name="othermsg" rows="3" columns="8"></textarea>
	    </div>
	    <input type="hidden" name="intId" value="<?php echo $_GET['int'] ?>">
	    <input type="hidden" name="formType" value="othermsg">
		<div class="col-md-8" style="margin-top: 30px;">		
			<button type="button" class="btn btn-secondary" onclick="endContact('msgYes')">Save</button>
		</div>		
	</form>	
</div>

<div id="thankYou" style="display: none">
<p>Thank you for your time and for choosing ISUZU East Africa, in case of any queries, feel free to reach us on this address info.kenya@isuzu.co.ke. or give us a call on this number.</p>	
<div class="col-md-8" style="margin-top: 30px;">		
	<button type="button" class="btn btn-secondary" onclick="endContact('msgNo')">End Contact</button>
</div>
</div>


