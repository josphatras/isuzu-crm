<?php
//session_start();
$makes = veh_make();
$bodyTypes = veh_body_types();
?>
<div class="container py-3">
    <div class="row">
        <div class="mx-auto col-sm-12">
	        <div class="card">
	            <div class="card-header">
	                <h4 class="mb-0">Vehicle Details</h4>
	            </div>
	            <div class="card-body">
					<form id="vehicleDetails">
						<div class="form-group row">
						    <label class="col-lg-3 col-form-label form-control-label">Make</label>
						    <div class="col-lg-9">
						        <select class="form-control" size="0" id="make" name="make" onchange="getVehModels();" required>
								    <option value="" selected>Choose...</option>
								    <?php
								    	foreach ($makes as $make) {
								    		echo '<option value="'.$make['id'].'">'.$make['type'].'</option>';
								    	}
								    ?>
								    <option value="0">Other</option>
						        </select>
						    </div>
						</div>
						<div id="makeModelSection" style="display: none;">
							<div class="form-group row">
							    <label class="col-lg-3 col-form-label form-control-label">Other Make</label>
							    <div class="col-lg-9">
							        <input type="text" class="form-control" id="otherMake" name="otherMake" placeholder="Other Make" value="">
							    </div>
							</div>							

							<div class="form-group row">
							    <label class="col-lg-3 col-form-label form-control-label">Other Model</label>
							    <div class="col-lg-9">
							        <input type="text" class="form-control" id="otherModel" name="otherModel" placeholder="Other Model" value="">
							    </div>
							</div>
						</div>
						<div id="modelSection">
							<div class="form-group row">
							    <label class="col-lg-3 col-form-label form-control-label">Model</label>
							    <div class="col-lg-9">
									<div id="vehModelDis">
										<select class="custom-select mr-sm-2" id="model" name="model">
										<option value="" >Choose...</option>
										</select>	
							    	</div>
								</div>
							</div>
						</div>
						<div class="form-group row">
						    <label class="col-lg-3 col-form-label form-control-label">Registration Number</label>
						    <div class="col-lg-9">
						        <input type="text" class="form-control" id="regNo" name="regNo" placeholder="Registration Number" value="" required>
						    </div>
						</div>
						<div class="form-group row">
						    <label class="col-lg-3 col-form-label form-control-label">VIN number</label>
						    <div class="col-lg-9">
						        <input type="text" class="form-control" id="vinNo" name="vinNo" placeholder="VIN number" value="" onkeyup="vinValidation()">
						        <small><strong>Enter VIN without blank spaces</strong></small>
								    <div id="vinVerResponse"></div>
						    </div>
						</div>
						<div id="bodyTypeSec">
							<div class="form-group row">
							    <label class="col-lg-3 col-form-label form-control-label">Body Type</label>
							    <div class="col-lg-9">
								  <select class="form-control" size="0" id="bodyType" name="bodyType" onchange="bodyTypes();" required>
								    <option value="">Choose...</option>
								    <?php
								    	foreach ($bodyTypes as $bodyType) {
								    		echo '<option value="'.$bodyType['id'].'">'.$bodyType['body_type'].'</option>';
								    	}
								    ?>
								    <option value="0">Other</option>
								  </select>
							    </div>
							</div>
						</div>
						<div id="otherBodyTypeSec" style="display: none;">
							<div class="form-group row">
							    <label class="col-lg-3 col-form-label form-control-label">Other Body Type</label>
							    <div class="col-lg-9">
							        <input type="text" class="form-control" id="otherBodyType" name="otherBodyType" placeholder="Other Body Type" value="">
							    </div>
							</div>
						</div>
						<div class="form-group row">
						    <label class="col-lg-3 col-form-label form-control-label">Vehicle use</label>
						    <div class="col-lg-9">
						        <input type="text" class="form-control" id="application" name="application" placeholder="Vehicle use" value="">
						    </div>
						</div>
						<div class="form-group row">
						    <label class="col-lg-3 col-form-label form-control-label">Fleet size information etc.</label>
						    <div class="col-lg-9">
						        <input type="text" class="form-control" id="fleetSize" name="fleetSize" placeholder="Fleet size information" value="">
						    </div>
						</div>
						<?php
						if (isset($_GET['items'])) {
							if ($_GET['items'] == 'emergency') {
								
						?>
						<div style="" id="emergenciesItems">
							<div class="form-group row">
							    <label class="col-lg-3 col-form-label form-control-label">Town</label>
							    <div class="col-lg-9">
							        <input type="text" class="form-control" id="validationCustom01" placeholder="Town" value="" required>
							    </div>
							</div>
							<div class="form-group row">
							    <label class="col-lg-3 col-form-label form-control-label">Town</label>
							    <div class="col-lg-9">
							        <input type="text" class="form-control" id="validationCustom01" placeholder="Town" value="" required>
							    </div>
							</div>
						</div>
						<?php
							}
						}
						?>
						<input type="hidden" name="form_type" value="vehicle_info">
						<input type="hidden" name="customerId" value="<?php echo $_GET['customerId'] ?>">
						<input type="hidden" id="next" value="<?php echo $_GET['next']; ?>">
						<div class="col-md-8">
							<?php 
								if ($_GET['next'] == 'no') {
									echo '<button type="submit" class="btn btn-secondary">Save</button>';
								}else{
									echo '<button type="submit" class="btn btn-secondary">Next</button>';
								}
							?>
							
						</div>
					</form>
	            </div>
	        </div>
        </div>
    </div>
</div>
