<?php
if (isset($_GET['int'])) {
  $intId = $_GET['int'];
  $qsubCatId = "SELECT sub_cat_id FROM messages m WHERE m.id = :msgId";
  $getsubCatId = $con->prepare($qsubCatId);
  $getsubCatId->bindParam(':msgId', $intId, PDO::PARAM_STR);
  $getsubCatId->execute();
  $sub_Cat_Id = $getsubCatId->fetch(); 
  $subCatId = $sub_Cat_Id['sub_cat_id'];  

$SQLIntScript = "SELECT * FROM script_guide sg INNER JOIN guide g ON sg.guide_id = g.id WHERE sg.sub_cat_id = :subCatId";
$getIntScript = $con->prepare($SQLIntScript);
$getIntScript->bindParam(':subCatId', $subCatId, PDO::PARAM_STR);
$getIntScript->execute();
$intScript = $getIntScript->fetch();
}
?>
<?php echo $intScript['guide_text']; ?>
<br>
<hr>
<button type="button" class="btn btn-secondary" onclick="responseForm();">Next</button>