<p>2.1 ) Search the customer on the CRM system</p>
<div class="row">
    <div class="col-md-4 my-1">
        <div class="input-group mb-3">
            <input type="text" class="form-control" placeholder="Client" aria-label="Recipient's username" aria-describedby="basic-addon2" id="searchPhrase">
            <div class="input-group-append">
                <span class="btn btn-info" id="basic-addon2" onclick="searchCustomer()">Search</span>
            </div>
        </div>
    </div>
</div>

<div id="searchResults"></div>