<?php
include_once('dao/config/db.php');
require_once('dao/functions.php');
$contactTypes = allConTypes();
$categories = sel_Cat('msg');
?>
<div class="container py-3">
    <div class="row">
        <div class="mx-auto col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="mb-0">Contact Details</h4>
                </div>
                <div class="card-body">
					<form id="contactInfo">
						<div class="form-group row">
						    <label class="col-lg-3 col-form-label form-control-label">Type of contact</label>
						    <div class="col-lg-9">
						        <select id="contactType" name="contactType" class="form-control" size="0" required>
							    <option value="">Choose...</option>
							    <?php
							    	foreach ($contactTypes as $contactType) {
							    		echo '<option value="'.$contactType['id'].'">'.$contactType['name'].'</option>';
							    	}
							    ?>
						        </select>
						    </div>
						</div>

						<div class="form-group row">
						    <label class="col-lg-3 col-form-label form-control-label">Category</label>
						    <div class="col-lg-9">
						        <select name="category" id="category" class="form-control" size="0" onchange="getSubCat('msg');" required>
							    <option value="">Choose...</option>
							    <?php
							    	foreach ($categories as $category) {
							    		echo '<option value="'.$category['id'].'">'.$category['cat_name'].'</option>';
							    	}
							    ?>
						        </select>
						    </div>
						</div>
						<div id="showSubCategory"></div>	
						<div style="display: none" id="disComplain">
							<div class="form-group row">
							    <label class="col-lg-3 col-form-label form-control-label">Capture the complaint</label>
							    <div class="col-lg-9">
							        <select id="complain" name="complain" class="form-control" size="0">
									    <option value="">Choose...</option>
									    <option value="Sales related">Sales related</option>
									    <option value="Service Workshop related">Service Workshop related</option>
									    <option value="Parts and accessories related">Parts and accessories related</option>
							        </select>
							    </div>
							</div>
						</div>
						<input type="hidden" name="customerId" value="<?php echo $_GET['customerId'] ?>">
						<div class="col-md-8 mb-2">
							<button type="submit" class="btn btn-secondary">Next</button>
						</div>
					</form>
                </div>
            </div>
        </div>
    </div>
</div>	