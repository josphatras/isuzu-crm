<div class="left-col-nav">
	<ul>
<?php if ($admin == '1' || $manager == '1'): ?>
	<li><a href="reports.php">Dashboard</a></li>
	<li><a href="dashboard.php?content=escalations">Escalations</a></li>	
<?php endif ?>

<?php if ($admin == '1' || $manager == '1' ||  $agent == '1'): ?>
	<li><a href="dashboard.php?content=interactions">Interactions</a></li>
	<li><a href="dashboard.php?content=customers_list">Customers</a></li>		
<?php endif ?>
<?php if ($dealer_r == '1'): ?>
	<li><a href="reports.php">Dashboard</a></li>
<?php endif ?>
	<li><a href="dashboard.php?content=bookings&item=view_bookings">Bookings</a></li>
	<li><a href="dashboard.php?content=services&type=history&service=2">Transactions</a></li>
	<li><a href="dashboard.php?content=surveys&type=view_services">Surveys</a></li>
<?php if ($dealer_r == '1'): ?>
	<li><a href="#">Escalations</a></li>
	<li><a href="dashboard.php?content=dealer_customer_list">Customers</a></li>
<?php endif ?>

<?php if ($admin == '1' || $manager == '1' || $advisor == '1'): ?>
	<li><a href="download/view/customers.php">Downloads</a></li>
<?php endif ?>

<?php if ($admin == '1' || $manager == '1'): ?>
	<li><a href="dashboard.php?content=settings">Setting</a></li>	
<?php endif ?>

<?php if ($manager == '1'): ?>
	<li><a href="dashboard.php?content=users">Manage Users</a></li>
	<li><a href="dashboard.php?content=users&user_section=new">Add user</a></li>
	<li><a href="dashboard.php?content=customers_upload">Upload customers</a></li>
<?php endif ?>

<?php if ($advisor == '1'): ?>
	<li><hr></li>
	<li><a href="dashboard.php?content=advisor_new_customer">New Customer</a></li>
	<li><a href="dashboard.php?content=customers_list">View Customers</a></li>		
<?php endif ?>

	</ul>
</div>