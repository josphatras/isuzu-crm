<?php
include_once('dao/config/db.php');
	$qUser = "SELECT u.first_name, u.last_name, u.email,u.id user_id, (SELECT role FROM user_roles WHERE user_roles.id = u.role ) user_role FROM users u WHERE u.role != '3'";
	$getUsers = $con->prepare($qUser);
	$getUsers->execute();

	$qUserRoles = "SELECT * FROM user_roles";
	$getUserRoles = $con->prepare($qUserRoles);
	$getUserRoles->execute();
	$userRoles = $getUserRoles->fetchAll(PDO::FETCH_ASSOC);
?>
<div class="esc-container">
<div class="row">
	<div class="col-md-2">
		<div class="left-col-nav">
			<ul>
				<li>Change Password</li>
			</ul>
		</div>
	</div>
	<div class="col-md-10">
		<div class="row">
			<div class="col-md-4 col-sm-12">
				<div id="confirmOldPassword">
					<form action="dao/admin/reset_password.php" method="post">
						<div class="form-group">
						    <label for="exampleInputPassword1">Old Password</label>
						    <input type="password" name="oldPassword" class="form-control" id="exampleInputPassword1" placeholder="Old Password">
						 </div>
						 <div class="form-group">
						    <label for="exampleInputPassword1">New Password</label>
						    <input type="password" name="newPassword" class="form-control" id="exampleInputPassword1" placeholder="New Password">
						 </div>
						 <input type="hidden" name="reset_password" value="myAccount">
						 <div id="responseSection1">
							<?php
								if (isset($_GET['reset_response'])) {
									if ($_GET['reset_response'] == 'success') {
										echo "<div class='resetResponse' style='color:#008000; font-size:18px;'>Password successfuly reset</div>";
									}else{
										echo "<div class='resetResponse' style='color:#FF0000; font-size:18px;'>Old password is incorrect</div>";
									}
								}
							?>
						</div>
						 <button type="submit" class="btn btn-info">Reset Password</button>
					 </form>
				</div>
			</div>
		</div>
	</div>
	
</div>
</div>
