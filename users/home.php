<?php
include_once('dao/config/db.php');
	$qUser = "SELECT u.first_name, u.last_name, u.email,u.id user_id, (SELECT role FROM roles WHERE roles.id = u.role ) role, status FROM users u WHERE u.role != '3'";
	$getUsers = $con->prepare($qUser);
	$getUsers->execute();

function userRoles($userId){
	global $con;
	$qUserRoles = "SELECT r.id, r.role FROM user_roles ur INNER JOIN roles r ON ur.role_id = r.id WHERE user_id = :userId";
	$getUserRoles = $con->prepare($qUserRoles);
	$getUserRoles->bindParam(':userId', $userId, PDO::PARAM_STR);
	$getUserRoles->execute();
	$userRoles = $getUserRoles->fetchAll(PDO::FETCH_ASSOC);	
	return $userRoles;
}

$dealers = dealers();
?>
<?php

if (isset($_GET['message'])) {
	echo $_GET['message'].'<hr>';
}

?>
<div class="esc-container">
<div class="row">
	<div class="col-md-12">
		<?php
			if (isset($_GET['user_section'])) {
				$userSection = $_GET['user_section'];
				if ($userSection == 'new') {
			?>

			<div class="col-md-6 col-sm-12">
				<div class="container-signup">
					<p><strong>New user</strong></p>
					<hr>
					<form action="dao/account.php" method="POST">
					  <div class="form-group">
					    <label for="loginuser">First Name</label>
					    <input type="text" class="form-control" id="loginuser" name="firstName" aria-describedby="" placeholder="First Name" required>
					  </div>
					  <div class="form-group">
					    <label for="loginuser">Last Name</label>
					    <input type="text" class="form-control" id="loginuser" name="lastName" aria-describedby="" placeholder="Last Name" required>
					  </div>
					  <div class="form-group">
					    <label for="loginuser">Email</label>
					    <input type="mail" class="form-control" id="loginuser" name="email" aria-describedby="" placeholder="Email" required>
					  </div>
					  <div class="form-group">
					    <label for="loinpassword">Password</label>
					    <input type="password" class="form-control" id="loinpassword" name="password" placeholder="Password" required>
					  </div>
					<label class="mr-sm-2" for="inlineFormCustomSelect">Roles</label>
					<div class="form-row">
						<div class="col-md-6">
							<fieldset>
							    <input type="checkbox" name="agent" value="4" /><label for="track">&nbsp;Agent</label><br />
							    <input type="checkbox" name="admin" value="1"  /><label for="event">&nbsp;Admin</label><br />
							    <input type="checkbox" name="manager" value="2" /><label for="message">&nbsp;Manager</label><br />
							    <input type="checkbox" name="advisor" value="5" /><label for="advisor">&nbsp;Service Advisor</label><br />
							    <input type="checkbox" name="dealer" id="dealer" value="6" onclick="dealerRole()" /><label for="advisor">&nbsp;Dealer</label><br />
							</fieldset>
						</div>
						<div class="col-md-6">
							<div id="dealerSec" style="display: none;">
								<div class="form-group">
							    <label for="loinpassword"><strong>Dealer</strong></label>
							    <select name="dealer_id">
							    	<option value="">Select--</option>
										<?php foreach ($dealers as $dealer): ?>
										<option value="<?php echo $dealer['id'] ?>"><?php echo $dealer['name'] ?></option>
										<?php endforeach; ?>
							    </select>
							  </div>
							</div>
						</div>
					</div>
						<br>
					  <input type="hidden" name="form_type" value="newUser">
					  <button type="submit" class="btn btn-primary">Submit</button>
					</form>	
				</div>
			</div>

			<?php
				}elseif ($userSection == 'edit') {
					if (isset($_GET['userId'])) {
						$userId = $_GET['userId'];
						$qEditUser = "SELECT * FROM users WHERE id = :userId";
						$getEditUser = $con->prepare($qEditUser);
						$getEditUser->bindParam(':userId', $userId, PDO::PARAM_INT);
						$getEditUser->execute();
						$editUser = $getEditUser->fetch(PDO::FETCH_ASSOC);
					}

			?>
			<div class="col-md-6 col-sm-12">
				<div class="container-signup">
					<p><strong>Edit user</strong></p>
					<hr>
					<form action="dao/account.php" method="POST">
					  <div class="form-group">
					    <label for="loginuser">First Name</label>
					    <input type="text" class="form-control" id="loginuser" name="firstName" aria-describedby="emailHelp" value="<?php echo $editUser['first_name']; ?>"  required>
					  </div>
					  <div class="form-group">
					    <label for="loginuser">Last Name</label>
					    <input type="text" class="form-control" id="loginuser" name="lastName" aria-describedby="emailHelp" value="<?php echo $editUser['last_name']; ?>" required>
					  </div>
					  <input type="hidden" name="userId" value="<?php echo $editUser['id']; ?>">
					  <div class="form-group">
					    <label for="loginuser">Email</label>
					    <input type="mail" class="form-control" id="loginuser" name="email" aria-describedby="emailHelp" value="<?php echo $editUser['email']; ?>" required>
					  </div>
						<label class="mr-sm-2" for="inlineFormCustomSelect">Roles</label>
					<div class="form-row">
						<div class="col-md-6">
						<?php 
							$qry = "SELECT * FROM roles WHERE id != '3'";
							$getRoles = $con->prepare($qry);
							$getRoles->execute();
							$roles = $getRoles->fetchAll(PDO::FETCH_ASSOC);
						?>
						<fieldset>
						<?php
							$getRoles = userRoles($userId);
							foreach ($getRoles as $getRole) {
								$userRoles[$getRole['role']] = $getRole['role'];
							}
								foreach ($roles as $role) {
									if (array_key_exists( $role['role'], $userRoles)) {
										if ($role['id'] == '6') {
											echo '<input type="checkbox" name="'.$role['role'].'" id="track" value="'.$role['id'].'" onchange="dealerRole()" checked/><label for="track">&nbsp;'.$role['role'].'</label><br />';
										}else{
											echo '<input type="checkbox" name="'.$role['role'].'" id="track" value="'.$role['id'].'" checked/><label for="track">&nbsp;'.$role['role'].'</label><br />';
										}
									  
									}else{
										if ($role['id'] == '6') {
											echo '<input type="checkbox" name="'.$role['role'].'" id="track" value="'.$role['id'].'" onchange="dealerRole()" /><label for="track">&nbsp;'.$role['role'].'</label><br />';
										}else{
											echo '<input type="checkbox" name="'.$role['role'].'" id="track" value="'.$role['id'].'" /><label for="track">&nbsp;'.$role['role'].'</label><br />';
										}
										
									}
								}
						?>
						</fieldset>
						</div>
						<div class="col-md-6">
							<div id="dealerSec" style="display: none;">
								<div class="form-group">
							    <label for="loinpassword"><strong>Dealer</strong></label>
							    <select name="dealer_id">
							    	<option value="">Select--</option>
										<?php foreach ($dealers as $dealer): ?>
										<option value="<?php echo $dealer['id'] ?>"><?php echo $dealer['name'] ?></option>
										<?php endforeach; ?>
							    </select>
							  </div>
							</div>
						</div>
					</div>

						<br>
					  <input type="hidden" name="form_type" value="update_user">
					  <button type="submit" class="btn btn-info">Update</button>
					</form>	
				</div>
			</div>

			<?php
				}
			}else{
			?>
			<table border="1" class="table table-bordered table-striped table-sm isuzu-datatable" style="font-size: 0.8em;border-collapse: collapse;">
				<thead>
				<tr>
					<td>Name</td>
					<td>Email</td>
					<td>Roles</td>
					<td>Action</td>
				</tr>
				</thead>
				<tbody>
			<?php
			while ( $user = $getUsers->fetch(PDO::FETCH_ASSOC)) {
				$userId = $user['user_id'];
				$userRoles = userRoles($userId);
				$roles = '';
				foreach ($userRoles as $userRole) {
					$roles .= '<span class="badge badge-info">'.$userRole['role'].'</span>&nbsp;';
				}
			?>
				<tr>
					<td><?php echo $user['first_name'].' '.$user['last_name']; ?></td>
					<td><?php echo $user['email']; ?></td>
					<td><?php echo $roles;?></td>
					<td style="text-align: center;"><a href="dashboard.php?content=users&user_section=edit&userId=<?php echo $user['user_id'] ?>"><button type="link" class="btn btn-secondary btn-sm">Edit</button></a> | 
					<button type="link" class="btn btn-secondary btn-sm" onclick="resetPassword('<?php echo $user['user_id']; ?>')">Reset</button> | 
					<?php
						if ($user['status'] == '1') {
							echo '<a href="dao/process.php?deactivate_user='.$user["user_id"].'"><button type="link" class="btn btn-success btn-sm" style="width: 80px;">Deactivate</button></a> | ';
						}else{
							echo '<a href="dao/process.php?activate_user='.$user["user_id"].'"><button type="link" class="btn btn-danger btn-sm" style="width: 80px;">Activate</button></a> | ';
						}
						
					?>
					
					<button type="link" class="btn btn-secondary btn-sm" onclick="delUser('<?php echo $user['user_id']; ?>','<?php echo $user['first_name'].' '.$user['last_name']; ?>')">Delete User</button>
					</td>
				</tr>
			<?php
			}
			?>
			</tbody>
			</table>
			<div id="resetResponse"></div>
			<?php
			}
		?>
	</div>
	
</div>
</div>

<div class="modal fade" id="delUserSec" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background: #dd4f43">
        <h5 class="modal-title" id="exampleModalLabel">Warning</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <p>Are you sure you want to delete <strong><span id="userName"></span></strong> user account?</p>
      </div>
      <div class="modal-footer">
        <div id="delBtn"></div>
        <button type="button" class="btn btn-info" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>
