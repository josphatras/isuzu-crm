<?php
include_once('../../header.php');
include_once('../../dao/config/include.php');
include_once('../../dao/config/db.php'); 
include_once('../../dao/functions.php'); 

if (isset($_GET['from_date'])) {
	$fromDate = $_GET['from_date'];
}else{
	$fromDate = date('Y-m-d');
}

if (isset($_GET['to_date'])) {
	$toDate = $_GET['to_date'];
}else{
	$toDate = date('Y-m-d');
}

$bookings = bookings($fromDate, $toDate);

?>
<div class="container-fluid primary-color">
    <div class="row p-1" >
        <div class="col-md-12">
            <div class="col-main">
                <div class="container-fluid bg-silver-fade">
                	<div class="row">
                        <div class="col-md-3">
                            
                        </div>
                        <div class="col-md-9">
                        	<div style="margin: 20px 0 20px 0; float: right;">
	                        	<div class="row">									
									<div class="col-md-5">
									  <div class="input-group">
									    <div class="input-group-prepend">
									      <div class="input-group-text" id="btnGroupAddon">From</div>
									    </div>
									    <input type="text" class="form-control dateSelect" id="fromDate" placeholder="From Date" value="<?php echo $fromDate; ?>">
									  </div>								
									</div>
									<div class="col-md-5">
									  <div class="input-group">
									    <div class="input-group-prepend">
									      <div class="input-group-text" id="btnGroupAddon">To</div>
									    </div>
									    <input type="text" class="form-control dateSelect" id="toDate" placeholder="To Date" value="<?php echo $toDate; ?>">
										</div>								
									</div>
									<div class="col-md-2">
										<span class="btn btn-primary" id="basic-addon2" onclick="bookingsSearch('report')">Search</span>
									</div>
								</div>
                        	</div>
                        </div>					
                </div>                
                <ul class="nav nav-tabs">
                  <li class="nav-item">
                    <a class="nav-link" href="customers.php">Customers</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link active-pop" href="bookings.php">Bookings</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="messages.php">Interactions</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="escalations.php">Escalations</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="sales_services.php">Sales Transactions</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="servicing_services.php">Service Transactions</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="sales_surveys.php">Sales Surveys</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="service_surveys.php">Service Surveys</a>
                  </li>
                </ul>
                <br>
                <div>
                    <div style="float: left;">
                        <h3>Bookings</h3>
                    </div>
                    <div style="float: right;">
                        <button type="link" class="btn btn-secondary" onclick="bookingsSearch('download')"><span class="fa fa-download"></span> Download</button>
                    </div>
                </div>
                <div class="clearfix" style="padding-bottom: 30px;"></div>
				<table class="isuzu-datatable table table-bordered table-striped table-sm " style="font-size: 0.8em;border-collapse: collapse;">
					<thead>
						<tr>
							<th>Branch</th>
							<th>Booked By</th>
							<th>Customer</th>
							<th>Contact Person</th>
							<th>Telephone (Cell)</th>
							<th>Email Address</th>
							<th>Reg No.</th>
							<th>Model</th>
							<th>Section</th>
							<th>Service Type</th>
							<th>Appointment Date</th>
							<th>Appointment Time</th>
							<th>Repair Description</th>
							<th>Booked on</th>
						</tr>
					</thead>
					<tbody>
						<?php
							foreach ($bookings as $booking) {
							$custmer = sel_customer($booking['customer_id']);
							    if ($booking['vehicle_id'] !== '') {
							        $vehicle = vehicle($booking['vehicle_id']);
							    }else{
							        $vehicle = array('reg_no' => '', 'v_model' => '');
							    }
						?>
						<tr>
							<td><?php echo $booking['branch']; ?></td>
							<td><?php echo $booking['agent']; ?></td>
							<td><a href="?content=customer&customer=<?php echo $booking['customer_id']; ?>"><?php echo $booking['customer_name']; ?></a></td>
							<td><?php echo $booking['contact_person']; ?></td>
							<td><?php echo $custmer['telephone_no']; ?></td>
							<td><?php echo $custmer['email']; ?></td>
							<td><?php echo $vehicle['reg_no']; ?></td>
							<td><?php echo $vehicle['v_model']; ?></td>
							<td><?php echo $booking['section']; ?></td>
							<td><?php echo $booking['service_type']; ?></td>
							<td><?php echo date('Y-m-d', strtotime($booking['appointment_date'])); ?></td>
							<td><?php echo date('G:i', strtotime($booking['appointment_date'])); ?></td>
							<td><?php echo $booking['repair_description']; ?></td>
							<td><?php echo $booking['created_on']; ?></td>
						</tr>
						<?php
							}
						?>		
					</tbody>
				</table>



            </div>
        </div>
    </div>
</div>
<?php include_once('../../footer.php'); ?>