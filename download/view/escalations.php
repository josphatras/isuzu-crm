<?php
include_once('../../header.php');
include_once('../../dao/config/include.php');
include_once('../../dao/config/db.php');


if (isset($_GET['searchPhrase'])) {
    $searchPhrase = $_GET['searchPhrase'];
    $fromDate = $_GET['fromDate'].' '.'00:00:18';
    $toDate = $_GET['toDate'].' '.'23:59:18';

    if ($searchPhrase == '') {
        $sql = "SELECT  c.email,c.first_name customer_f_name, c.last_name customer_l_name, y.town, y.telephone_no, y.business_type, y.physical_address, y.company_name, a.id,a.msg_id,a.resolution,a.sla,a.escalated_to,a.status,
        a.resolution_date,a.created_on,
        b.text, (SELECT name FROM esc_contact WHERE id = a.escalated_to) escalated_usr,
        d.status_name,e.cat_name,f.sub_cat_name
        FROM tickets a
        LEFT JOIN messages b ON a.msg_id = b.id
        LEFT JOIN users c ON b.customer_id = c.id
        LEFT JOIN ticket_status d ON a.status = d.status_id
        LEFT JOIN categories e ON a.cat_id = e.id
        LEFT JOIN customer_info y ON y.customer_id = b.customer_id
        LEFT JOIN sub_categories f ON a.sub_cat_id = f.id WHERE a.created_on >= :fromDate AND a.created_on <= :toDate";
    }else{
        $sql = " SELECT a.id AS customer_id,CONCAT(first_name,' ',last_name) AS customer_name,email,company_name,telephone_no,business_type,physical_address,town
        FROM users a
        LEFT JOIN customer_info b ON a.id = b.customer_id
        WHERE role = 3 AND a.created_on >= :fromDate AND a.created_on <= :toDate AND";
        $words = explode(" ", $_GET['searchPhrase']);
        $conds = array();
        foreach ($words as $word) {
            $conds[] = "(a.first_name Like '%".$word."%' OR a.last_name LIKE '%".$word."%' OR b.company_name LIKE '%".$word."%' OR b.telephone_no LIKE '%".$word."%')";
        }
        $sql .= implode(' OR ', $conds);
    }


$stmt=$con->prepare($sql);
$stmt->bindParam(':fromDate', $fromDate, PDO::PARAM_INT);
$stmt->bindParam(':toDate', $toDate, PDO::PARAM_INT);
$stmt->execute();
$query = $stmt->fetchAll(PDO::FETCH_ASSOC);
$sno = 0;
$rowcount=$stmt->rowCount();
}

function age_in_days($day_a,$day_b){
    if(date('Y-m-d',strtotime($day_a))<=date('Y-m-d',strtotime($day_b))){
        $datetime1 = new DateTime($day_a);
        $datetime2 = new DateTime($day_b);
        $interval = $datetime1->diff($datetime2);
        return $interval->format('%a days');
    }
    else{
        $datetime1 = new DateTime($day_a);
        $datetime2 = new DateTime($day_b);
        $interval = $datetime1->diff($datetime2);
        return $interval->format('%R%a days');
    }
}
?>
<div class="container-fluid primary-color">
    <div class="row p-1" >
        <div class="col-md-12">
            <div class="col-main">
                <div class="container-fluid bg-silver-fade">
                    <div class="row">
                        <div class="col-md-3">
                            
                        </div>
                        <div class="col-md-9">
                            <div style="padding: 10px 50px 10px 0;">
                                <div class="row">
                                    <div class="col-sm-3" style="padding-right: 0px;">
                                      <div class="form-group mx-sm-3 mb-2">
                                        <label for="searchPhrase" class="sr-only">Search</label>
                                        <?php
                                            if (isset($_GET['searchPhrase'])) {
                                                echo '<input type="hidden" class="form-control" id="searchPhrase" value="'.$_GET['searchPhrase'].'">';
                                            }else{
                                                echo '<input type="hidden" required="" id="searchPhrase" name="to" class="form-control" placeholder="Search" >';
                                            }
                                        ?>
                                        
                                      </div>        
                                    </div>
                                    <div class="col-md-4" style="padding-left: 0px; padding-right: 0px;">
                                        <div class="row">
                                            <div class="col-md-8" style="padding-right: 0px;">
                                              <div class="input-group">
                                                <div class="input-group-prepend">
                                                  <div class="input-group-text" id="btnGroupAddon">From</div>
                                                </div>
                                                <input type="text" id="fromDate" name="from" class="form-control dateSelect" value="<?php if(isset($_GET['fromDate'])){ echo $_GET['fromDate'];} ?>"  placeholder="Select date from" >
                                              </div>  
                                            </div>  
                                        </div>              
                                    </div>

                                    <div class="col-md-4" style="padding-left: 0px; padding-right: 0px;">
                                        <div class="row">
                                            <div class="col-md-8" style="padding-right: 0px;">
                                              <div class="input-group">
                                                <div class="input-group-prepend">
                                                  <div class="input-group-text" id="btnGroupAddon">To</div>
                                                </div>
                                                <input type="text" id="toDate" name="to" class="form-control dateSelect" value="<?php if(isset($_GET['toDate'])){ echo $_GET['toDate'];} ?>"  placeholder="Select date to" >
                                              </div>                                                
                                            </div>  
                                        </div>              
                                    </div>
                                    <div class="col-md-1" style="padding-left: 0px;">
                                        <button type="button" class="btn btn-primary" onclick="escalationsReportDownload('view')"><span class=" glyphicon glyphicon-zoom-in"></span> Search</button>
                                    </div>
                                </div>  
                            </div>      
                        </div>
                    </div>
                </div>
                <ul class="nav nav-tabs">
                  <li class="nav-item">
                    <a class="nav-link" href="customers.php">Customers</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="bookings.php">Bookings</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="messages.php">Interactions</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link active-pop" href="escalations.php">Escalations</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="sales_services.php">Sales Transactions</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="servicing_services.php">Service Transactions</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="sales_surveys.php">Sales Surveys</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="service_surveys.php">Service Surveys</a>
                  </li>
                </ul>
                <br>
                <div>
                    <div style="float: left;">
                        <h3>Escalations</h3>
                    </div>
                    <div style="float: right;">
                        <button type="link" class="btn btn-secondary" onclick="escalationsReportDownload('download')"><span class="fa fa-download"></span> Download</button>
                    </div>
                </div>
                <div class="clearfix" style="padding-bottom: 30px;"></div>
                <table border="1" class="table table-bordered table-striped table-sm table-light" style="font-size: 0.8em;border-collapse: collapse;">
                    <thead>
                        <tr>
                            <th>S/No</th>
                            <th>Ticket ID</th>
                            <th>Customer Name</th>
                            <th>Telephone No</th>
                            <th>Email</th>
                            <th>Business Type</th>
                            <th>Physical Address</th>
                            <th>Town</th>
                            <th>Company Name</th>                         
                            <th>Category</th>
                            <th>Sub-category</th>
                            <th>Issue</th>
                            <th>Resolution</th>
                            <th>SLA</th>
                            <th>Escalated to</th>
                            <th>Status</th>
                            <th>Escalated on</th>
                            <th>Age</th>
                            <th>Resolution Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if (isset($_GET['fromDate'])) {
                                foreach($query as $rs){
                                    $sno++;
                                    $age = 0;
                                    if(isset($rs['resolution_date'])){
                                        if($rs['resolution_date']==''){
                                            $age = age_in_days($rs['created_on'],date('Y-m-d'));
                                        }
                                        else{
                                            $age = age_in_days($rs['created_on'],$rs['resolution_date']);
                                        }
                                    }
                                    else{
                                        $age = age_in_days($rs['created_on'],date('Y-m-d'));
                                    }
                                    echo "<tr onclick='escEdit(); escEditform(\"".$rs['id']."\")' class='row-select'>
                                        <td>$sno</td>
                                        <td>$rs[id]</td>
                                        <td>$rs[customer_f_name] $rs[customer_l_name]</td>
                                        <td>$rs[telephone_no]</td>
                                        <td>$rs[email]</td>
                                        <td>$rs[business_type]</td>
                                        <td>$rs[physical_address]</td>
                                        <td>$rs[town]</td>
                                        <td>$rs[company_name]</td>
                                        <td>$rs[cat_name]</td>
                                        <td>$rs[sub_cat_name]</td>            
                                        <td>$rs[text]</td>
                                        <td>$rs[resolution]</td>
                                        <td>$rs[sla]</td>
                                        <td>$rs[escalated_usr]</td>
                                        <td>$rs[status_name]</td>
                                        <td>$rs[created_on]</td>
                                        <td>$age</td>
                                        <td>".(isset($rs['resolution_date'])?date('Y-m-d G:i:s',strtotime($rs['resolution_date'])):"")."</td>
                                    </tr>";
                                }

                            }

                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php include_once('../../footer.php'); ?>