<?php
include_once('../../header.php');
include_once('../../dao/config/include.php');
include_once('../../dao/config/db.php'); 
include_once('../../dao/functions.php'); 

if (isset($_GET['from_date'])) {
	$fromDate = $_GET['from_date'];
}else{
	$fromDate = date('Y-m-d');
}

if (isset($_GET['to_date'])) {
	$toDate = $_GET['to_date'];
}else{
	$toDate = date('Y-m-d');
}

/*if (isset($_GET['type'])) {
	$type = $_GET['type'];
	$service = $_GET['type'];
}else{
	$type = '';
	$service = '';
}*/
$service = '2';
$surveys = surveys('2', $fromDate, $toDate);
?>
<div class="container-fluid primary-color">
    <div class="row p-1" >
        <div class="col-md-12">
            <div class="col-main">
                <div class="container-fluid bg-silver-fade">
                	<div class="row">
                      <div class="col-md-3">
                          
                      </div>
                      <div class="col-md-9">
                      	<div style="margin: 20px 0 20px 0; float: right;">
													<div class="row">									
														<div class="col-md-5">
														  <div class="input-group">
														    <div class="input-group-prepend">
														      <div class="input-group-text" id="btnGroupAddon">From</div>
														    </div>
														    <input type="text" class="form-control dateSelect" id="fromDate" placeholder="From Date" value="<?php echo $fromDate; ?>">
														  </div>								
														</div>
														<div class="col-md-5">
														  <div class="input-group">
														    <div class="input-group-prepend">
														      <div class="input-group-text" id="btnGroupAddon">To</div>
														    </div>
														    <input type="text" class="form-control dateSelect" id="toDate" placeholder="To Date" value="<?php echo $toDate; ?>">
															</div>								
														</div>
														<input type="hidden" id="service_id" value="<?php echo $service; ?>">
														<div class="col-md-2">
															<span class="btn btn-info" id="basic-addon2" onclick="surveySearch('report')">Search</span>
														</div>
													</div>
                      	</div>
                      </div>					
		                </div>                
		                <ul class="nav nav-tabs">
		                  <li class="nav-item">
		                    <a class="nav-link" href="customers.php">Customers</a>
		                  </li>
		                  <li class="nav-item">
		                    <a class="nav-link" href="bookings.php">Bookings</a>
		                  </li>
		                  <li class="nav-item">
		                    <a class="nav-link" href="messages.php">Interactions</a>
		                  </li>
		                  <li class="nav-item">
		                    <a class="nav-link" href="escalations.php">Escalations</a>
		                  </li>
		                  <li class="nav-item">
		                    <a class="nav-link" href="sales_services.php">Sales Transactions</a>
		                  </li>
		                  <li class="nav-item">
		                    <a class="nav-link" href="servicing_services.php">Service Transactions</a>
		                  </li>
		                  <li class="nav-item">
		                    <a class="nav-link" href="sales_surveys.php">Sales Surveys</a>
		                  </li>
		                  <li class="nav-item">
		                    <a class="nav-link active-pop" href="service_surveys.php">Service Surveys</a>
		                  </li>
		                </ul>
		                <br>
		                <div>
                    <div style="float: left;">
                        <h3>Bookings</h3>
                    </div>
                    <div style="float: right;">
                        <button type="link" class="btn btn-secondary" onclick="surveySearch('download')"><span class="fa fa-download"></span> Download</button>
                    </div>
                </div>
                <div class="clearfix" style="padding-bottom: 30px;"></div>
									<div style="overflow: auto; overflow-y: hidden; margin: 0 auto; white-space: nowrap">
										<table border="1" class="table table-bordered table-striped table-sm isuzu-datatable" style="font-size: 0.8em;border-collapse: collapse;">
											<thead>
												<th> Dealer </th>
												<th> Branch </th>
												<th> Survey by </th>
												<th> Customer name </th>
												<th> Transaction type </th>
												<th> Reg No. </th>
												<th> vin no </th>
												<th> Successful survey? </th>
												<th> Reason </th>
												<th> Escalate? </th>
												<th> Q1 </th>
												<th> Q2 </th>
												<th> Q3 </th>
												<th> Q4 </th>
												<th> Q5 </th>
												<th> Q6 </th>
												<th> Q7 </th>
												<th> Q8 </th>
												<th> Q9 </th>
												<th> Q10 </th>
												<th> Q11 </th>
												<th> Voice of ustomer </th>
												<th> Done on </th>
											</thead>
											<tbody>
												<?php foreach ($surveys as $survey): ?>
												<?php
												$reasons = array('1' => "Busy", '2' => "Rejecting", '3' => "Wrong number", '4' => "No number", '5' => "Unavailable", '6' => "Pending");
												$reason = '';
												foreach ($reasons as $key => $value) {
													if ($key == $survey['reason']) {
														$reason = $value;
														break;
													}
												}

													$scores = surveyScore($survey['id']);
													$vehicle = vehicle($survey['vehicle_id']);
												?>
												<tr>
													<td><?php echo $survey['dealer_name'] ?></td>
													<td><?php echo $survey['branch_name'] ?></td>
													<td><?php echo $survey['user_name'] ?></td>
													<td><?php echo $survey['customer_name'] ?></td>
													<td><?php echo $survey['transaction'] ?></td>
													<td><?php echo $vehicle['reg_no'] ?></td>
													<td><?php echo $vehicle['vin_no'] ?></td>
													<td><?php echo $survey['successful'] ?></td>
													<td><?php echo $reason ?></td>
													<td><?php echo $survey['escalate'] ?></td>
													<?php
													for ($i=1; $i <= 11 ; $i++) { 
														$state = 0;
														foreach ($scores as $score) {
															if ($i == $score['qn_id']) {
																$state += 1;
																break;
															}else{

															}
														}

														if ($state == '1') {
															echo "<td>".$score['score']."</td>";
															unset($state);
														}else{
															echo "<td></td>";
														}

													}
													
													?>
													<td><?php echo $survey['customer_voice'] ?></td>
													<td><?php echo $survey['created_on'] ?></td>
												</tr>
											<?php endforeach; ?>
											</tbody>
										</table>
									</div>
            </div>
        </div>
    </div>
</div>
<?php include_once('../../footer.php'); ?>