<?php
include_once('../../header.php');
include_once('../../dao/config/include.php');
include_once('../../dao/config/db.php');  


if (isset($_GET['searchPhrase'])) {
    $searchPhrase = $_GET['searchPhrase'];
    $fromDate = $_GET['fromDate'].' '.'00:00:18';
    $toDate = $_GET['toDate'].' '.'23:59:18';

    if ($searchPhrase == '') {
        $sql = "SELECT 
        a.id,a.customer_id,a.agent_id,a.contact_type, b.telephone_no, b.business_type, b.physical_address, b.town,a.cat_id,a.sub_cat_id,
        a.text,a.created_on,a.updated_on,b.company_name,g.first_name customer_f_name, g.last_name customer_l_name,
        CONCAT(c.first_name,' ',c.last_name) AS agent_name,
        d.name AS c_type,e.cat_name,
        f.sub_cat_name,
        (SELECT COUNT(id) FROM tickets h WHERE h.msg_id = a.id) AS in_escalations
        FROM messages a
        LEFT JOIN customer_info b ON a.customer_id = b.customer_id
        LEFT JOIN users c ON a.agent_id = c.id
        LEFT JOIN contact_type d ON a.contact_type = d.id
        LEFT JOIN categories e ON a.cat_id = e.id
        LEFT JOIN sub_categories f ON a.sub_cat_id = f.id
        LEFT JOIN users g ON a.customer_id = g.id
        WHERE a.created_on >= :fromDate AND a.created_on <= :toDate";
    }else{
        $sql = " SELECT a.id AS customer_id,CONCAT(first_name,' ',last_name) AS customer_name,email,company_name,telephone_no,business_type,physical_address,town
        FROM users a
        LEFT JOIN customer_info b ON a.id = b.customer_id
        WHERE role = 3 AND a.created_on >= :fromDate AND a.created_on <= :toDate AND


        /*FROM sma_messages msg WHERE (m_time >= '$fromDate' AND m_time <= '$toDate') AND (msg.customer_name LIKE '%$searchPhrase%' OR msg.phone_msisdn LIKE '%$searchPhrase%' OR msg.id_number LIKE '%$searchPhrase%' OR msg.sm_id LIKE '%$searchPhrase%' OR msg.m_platform = (SELECT p_id FROM sma_platforms WHERE p_text LIKE '%$searchPhrase%') OR  msg.category = (SELECT id FROM sma_cat WHERE cat_name LIKE '%$searchPhrase%') OR  msg.sub_category = (SELECT id FROM sma_sub_cat WHERE sub_cat_name LIKE '%$searchPhrase%'))*/";
        $words = explode(" ", $_GET['searchPhrase']);
        $conds = array();
        foreach ($words as $word) {
            $conds[] = "(a.first_name Like '%".$word."%' OR a.last_name LIKE '%".$word."%' OR b.company_name LIKE '%".$word."%' OR b.telephone_no LIKE '%".$word."%')";
        }
        $sql .= implode(' OR ', $conds);
    }


$stmt=$con->prepare($sql);
$stmt->bindParam(':fromDate', $fromDate, PDO::PARAM_INT);
$stmt->bindParam(':toDate', $toDate, PDO::PARAM_INT);
$stmt->execute();
$query = $stmt->fetchAll(PDO::FETCH_ASSOC);
$sno = 0;
$rowcount=$stmt->rowCount();
}
?>
<div class="container-fluid primary-color">
    <div class="row p-1" >
        <div class="col-md-12">
            <div class="col-main">
                <div class="container-fluid bg-silver-fade">
                    <div class="row">
                        <div class="col-md-3">
                            
                        </div>
                        <div class="col-md-9">
                            <div style="padding: 10px 50px 10px 0;">
                                <div class="row">
                                    <div class="col-sm-3" style="padding-right: 0px;">
                                      <div class="form-group mx-sm-3 mb-2">
                                        <label for="searchPhrase" class="sr-only">Search</label>
                                        <?php
                                            if (isset($_GET['searchPhrase'])) {
                                                echo '<input type="hidden" class="form-control" id="searchPhrase" value="'.$_GET['searchPhrase'].'">';
                                            }else{
                                                echo '<input type="hidden" required="" id="searchPhrase" name="to" class="form-control" placeholder="Search" >';
                                            }
                                        ?>
                                        
                                      </div>        
                                    </div>
                                    <div class="col-md-4" style="padding-left: 0px; padding-right: 0px;">
                                        <div class="row">
                                            <div class="col-md-8" style="padding-right: 0px;">
                                              <div class="input-group">
                                                <div class="input-group-prepend">
                                                  <div class="input-group-text" id="btnGroupAddon">From</div>
                                                </div>
                                                <input type="text" id="fromDate" name="from" class="form-control dateSelect" value="<?php if(isset($_GET['fromDate'])){ echo $_GET['fromDate'];} ?>"  placeholder="Select date from" >
                                              </div>
                                            </div>  
                                        </div>              
                                    </div>
                                    <div class="col-md-4" style="padding-left: 0px; padding-right: 0px;">
                                        <div class="row">
                                            <div class="col-md-8" style="padding-right: 0px;">
                                              <div class="input-group">
                                                <div class="input-group-prepend">
                                                  <div class="input-group-text" id="btnGroupAddon">To</div>
                                                </div>
                                                <input type="text" id="toDate" name="to" class="form-control dateSelect" value="<?php if(isset($_GET['toDate'])){ echo $_GET['toDate'];} ?>"  placeholder="Select date to" >
                                              </div> 
                                            </div>  
                                        </div>              
                                    </div>
                                    <div class="col-md-1" style="padding-left: 0px;">
                                        <button type="button" class="btn btn-primary" onclick="interactionReportDownload('view')"><span class=" glyphicon glyphicon-zoom-in"></span> Search</button>
                                    </div>
                                </div>  
                            </div>      
                        </div>
                    </div>
                </div>                
                <ul class="nav nav-tabs">
                  <li class="nav-item">
                    <a class="nav-link" href="customers.php">Customers</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="bookings.php">Bookings</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link active-pop" href="messages.php">Interactions</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="escalations.php">Escalations</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="sales_services.php">Sales Transactions</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="servicing_services.php">Service Transactions</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="sales_surveys.php">Sales Surveys</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="service_surveys.php">Service Surveys</a>
                  </li>
                </ul>
                <br>
                <div>
                    <div style="float: left;">
                        <h3>Interactions</h3>
                    </div>
                    <div style="float: right;">
                        <button type="link" class="btn btn-secondary" onclick="interactionReportDownload('download')"><span class="fa fa-download"></span> Download</button>
                    </div>
                </div>
                <div class="clearfix" style="padding-bottom: 30px;"></div>
                <table class="table table-bordered table-striped table-condensed table-sm table-light" style="font-size: 0.8em;">
                    <thead>
                        <tr>
                            <th>S/No</th>
                            <th>Interaction ID</th>
                            <th>Date</th>
                            <th>Customer</th>
                            <th>Company</th>
                            <th>Contact Type</th>
                            <th>Telephone No</th>
                            <th>Business Type</th>
                            <th>Physical Address</th>
                            <th>Town</th>
                            <th>Category</th>
                            <th>Sub-category</th>
                            <th>Comments</th>
                            <th>Agent</th>
                            <th>Escalated</th>
                        </tr>
                    </thead>
                    <tbody>                        
                        <?php
                            if (isset($_GET['fromDate'])) {
                                if(isset($category)){
                                    $filter = " WHERE a.cat_id='$category'";
                                    GetInteractions($con,$filter);
                                }
                                else if(isset($range)){
                                    $filter = '';
                                    if(strtolower($range)=='today'){
                                        $filter = " WHERE DATE(a.created_on)=CURRENT_DATE()";
                                    }
                                    else if(strtolower($range)=='past'){
                                        $date1 = date('Y-m-d', strtotime('-7 days'));
                                        $filter = " WHERE DATE(a.created_on) BETWEEN DATE('$date1') AND CURRENT_DATE()";
                                    }
                                    else if(strtolower($range)=='this'){
                                        $date1 = date('Y-m').'-01';
                                        $filter = " WHERE DATE(a.created_on) BETWEEN DATE('$date1') AND CURRENT_DATE()";
                                    }
                                        foreach($query as $rs){
                                            $sno++;
                                            $escalated = 'No';
                                            if($rs['in_escalations']>0){
                                                $escalated = 'Yes';
                                            }
                                            echo "<tr>
                                                <td>$sno</td>
                                                <td>$rs[id]</td>
                                                <td>".date('Y-M-d G:i:s',strtotime($rs['created_on']))."</td>
                                                <td>$rs[customer_f_name] $rs[customer_l_name]</td>           
                                                <td>$rs[company_name]</td>
                                                <td>$rs[c_type]</td>
                                                <td>$rs[telephone_no]</td>
                                                <td>$rs[business_type]</td>
                                                <td>$rs[physical_address]</td>
                                                <td>$rs[town]</td>
                                                <td>$rs[cat_name]</td>
                                                <td>$rs[sub_cat_name]</td>
                                                <td>$rs[text]</td>
                                                <td>$rs[agent_name]</td>
                                                <td>$escalated</td>
                                            </tr>";
                                        }
                                }
                                else{
                                    foreach($query as $rs){
                                        $sno++;
                                        $escalated = 'No';
                                        if($rs['in_escalations']>0){
                                            $escalated = 'Yes';
                                        }
                                        echo "<tr>
                                            <td>$sno</td>
                                            <td>$rs[id]</td>
                                            <td>".date('Y-M-d G:i:s',strtotime($rs['created_on']))."</td>
                                                <td>$rs[customer_f_name] $rs[customer_l_name]</td>
                                                <td>$rs[company_name]</td>            
                                                <td>$rs[c_type]</td>
                                                <td>$rs[telephone_no]</td>
                                                <td>$rs[business_type]</td>
                                                <td>$rs[physical_address]</td>
                                                <td>$rs[town]</td>
                                                <td>$rs[cat_name]</td>
                                                <td>$rs[sub_cat_name]</td>
                                                <td>$rs[text]</td>
                                                <td>$rs[agent_name]</td>
                                            <td>$escalated</td>
                                        </tr>";
                                    }
                                }
                            }                            
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php include_once('../../footer.php'); ?>