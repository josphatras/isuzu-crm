<?php
    include_once('../../header.php');
    include_once('../../dao/config/include.php');
    include_once('../../dao/config/db.php');


if (isset($_GET['searchPhrase'])) {
    $searchPhrase = $_GET['searchPhrase'];
    $fromDate = $_GET['fromDate'].' '.'00:00:18';
    $toDate = $_GET['toDate'].' '.'23:59:18';
    if ($searchPhrase == '') {
        $sql = "SELECT a.id AS customer_id,CONCAT(first_name,' ',last_name) AS customer_name,email,company_name,telephone_no,business_type,physical_address,town
        FROM users a
        LEFT JOIN customer_info b ON a.id = b.customer_id
        WHERE role = 3 AND a.created_on >= :fromDate AND a.created_on <= :toDate";
    }else{
        $sql = " SELECT a.id AS customer_id,CONCAT(first_name,' ',last_name) AS customer_name,email,company_name,telephone_no,business_type,physical_address,town
        FROM users a
        LEFT JOIN customer_info b ON a.id = b.customer_id
        WHERE role = 3 AND a.created_on >= :fromDate AND a.created_on <= :toDate AND


        /*FROM sma_messages msg WHERE (m_time >= '$fromDate' AND m_time <= '$toDate') AND (msg.customer_name LIKE '%$searchPhrase%' OR msg.phone_msisdn LIKE '%$searchPhrase%' OR msg.id_number LIKE '%$searchPhrase%' OR msg.sm_id LIKE '%$searchPhrase%' OR msg.m_platform = (SELECT p_id FROM sma_platforms WHERE p_text LIKE '%$searchPhrase%') OR  msg.category = (SELECT id FROM sma_cat WHERE cat_name LIKE '%$searchPhrase%') OR  msg.sub_category = (SELECT id FROM sma_sub_cat WHERE sub_cat_name LIKE '%$searchPhrase%'))*/";
        $words = explode(" ", $_GET['searchPhrase']);
        $conds = array();
        foreach ($words as $word) {
            $conds[] = "(a.first_name Like '%".$word."%' OR a.last_name LIKE '%".$word."%' OR b.company_name LIKE '%".$word."%' OR b.telephone_no LIKE '%".$word."%')";
        }
        $sql .= implode(' OR ', $conds);
    }


$stmt=$con->prepare($sql);
$stmt->bindParam(':fromDate', $fromDate, PDO::PARAM_INT);
$stmt->bindParam(':toDate', $toDate, PDO::PARAM_INT);
$stmt->execute();
$query = $stmt->fetchAll(PDO::FETCH_ASSOC);
$sno = 0;
$rowcount=$stmt->rowCount();
}

?>
<div class="container-fluid primary-color">

    <div class="row p-1" >
        <div class="col-md-12">
            <div class="col-main">
                <div class="container-fluid bg-silver-fade">
                    <div class="row">
                        <div class="col-md-3">
                            
                        </div>
                        <div class="col-md-9">
                            <div style="padding: 10px 50px 10px 0;">
                                <div class="row">
                                    <div class="col-sm-3" style="padding-right: 0px;">
                                      <div class="form-group mx-sm-3 mb-2">
                                        <label for="searchPhrase" class="sr-only">Search</label>
                                        <?php
                                            if (isset($_GET['searchPhrase'])) {
                                                echo '<input type="text" class="form-control" id="searchPhrase" value="'.$_GET['searchPhrase'].'">';
                                            }else{
                                                echo '<input type="text" required="" id="searchPhrase" name="to" class="form-control" placeholder="Search" >';
                                            }
                                        ?>
                                        
                                      </div>        
                                    </div>
                                    <div class="col-md-4" style="padding-left: 0px; padding-right: 0px;">
                                        <div class="row">
                                            <div class="col-md-8" style="padding-right: 0px;">
                                              <div class="input-group">
                                                <div class="input-group-prepend">
                                                  <div class="input-group-text" id="btnGroupAddon">From</div>
                                                </div>
                                                <input type="text" id="fromDate" name="from" class="form-control dateSelect" value="<?php if(isset($_GET['fromDate'])){ echo $_GET['fromDate'];} ?>"  placeholder="Select date from" >
                                              </div>   
                                            </div>  
                                        </div>              
                                    </div>

                                  

                                    <div class="col-md-4" style="padding-left: 0px; padding-right: 0px;">
                                        <div class="row">
                                            <div class="col-md-8" style="padding-right: 0px;">
                                              <div class="input-group">
                                                <div class="input-group-prepend">
                                                  <div class="input-group-text" id="btnGroupAddon">To</div>
                                                </div>
                                                <input type="text" id="toDate" name="to" class="form-control dateSelect" value="<?php if(isset($_GET['toDate'])){ echo $_GET['toDate'];} ?>"  placeholder="Select date to" >
                                              </div>   
                                            </div>  
                                        </div>              
                                    </div>
                                 
                                    <div class="col-md-1" style="padding-left: 0px;">
                                        <button type="button" class="btn btn-primary" onclick="customerReportDownload('view')"><span class=" glyphicon glyphicon-zoom-in"></span> Search</button>
                                    </div>
                                </div>  
                            </div>      
                        </div>
                    </div>
                </div>

                <ul class="nav nav-tabs">
                  <li class="nav-item">
                    <a class="nav-link active-pop" href="customers.php">Customers</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="bookings.php">Bookings</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="messages.php">Interactions</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="escalations.php">Escalations</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="sales_services.php">Sales Transactions</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="servicing_services.php">Service Transactions</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="sales_surveys.php">Sales Surveys</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="service_surveys.php">Service Surveys</a>
                  </li>
                </ul>
                <br>
                <div>
                    <div style="float: left;">
                        <h3>Customer</h3>
                    </div>
                    <div style="float: right;">
                        <button type="link" class="btn btn-secondary" onclick="customerReportDownload('download')"><span class="fa fa-download"></span> Download</button>
                    </div>
                </div>
                <div class="clearfix" style="padding-bottom: 30px;"></div>
                <table class="table table-condensed table-striped" style="font-size: 0.8em;">
                    <thead>
                        <tr>
                            <th>S. No.</th>
                            <th>Customer ID</th>
                            <th>Customer Name</th>
                            <th>Customer Email</th>
                            <th>Company Name</th>
                            <th>Telephone</th>
                            <th>Town</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($_GET['fromDate'])) {
                            foreach ($query as $qry){
                                $sno++;
                                echo "<tr>
                                    <td>$sno</td>
                                    <td>$qry[customer_id]</td>
                                    <td>$qry[customer_name]</td>
                                    <td>$qry[email]</td>
                                    <td>$qry[company_name]</td>
                                    <td>$qry[telephone_no]</td>
                                    <td>$qry[town]</td>
                                </tr>";
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php include_once('../../footer.php'); ?>