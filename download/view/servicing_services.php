<?php
include_once('../../header.php');
include_once('../../dao/config/include.php');
include_once('../../dao/config/db.php'); 
include_once('../../dao/functions.php'); 

if (isset($_GET['from_date'])) {
	$fromDate = $_GET['from_date'];
}else{
	$fromDate = date('Y-m-d');
}

if (isset($_GET['to_date'])) {
	$toDate = $_GET['to_date'];
}else{
	$toDate = date('Y-m-d');
}

$services = services('2', 'all', $fromDate, $toDate);

?>
<div class="container-fluid primary-color">
    <div class="row p-1" >
        <div class="col-md-12">
            <div class="col-main">
                <div class="container-fluid bg-silver-fade">
                	<div class="row">
                      <div class="col-md-3">
                          
                      </div>
                      <div class="col-md-9">
                      	<div style="margin: 20px 0 20px 0; float: right;">
													<div class="row">									
														<div class="col-md-5">
														  <div class="input-group">
														    <div class="input-group-prepend">
														      <div class="input-group-text" id="btnGroupAddon">From</div>
														    </div>
														    <input type="text" class="form-control dateSelect" id="fromDate" placeholder="From Date" value="<?php echo $fromDate; ?>">
														  </div>								
														</div>
														<div class="col-md-5">
														  <div class="input-group">
														    <div class="input-group-prepend">
														      <div class="input-group-text" id="btnGroupAddon">To</div>
														    </div>
														    <input type="text" class="form-control dateSelect" id="toDate" placeholder="To Date" value="<?php echo $toDate; ?>">
															</div>								
														</div>
														<input type="hidden" id="service_id" value="2">
														<div class="col-md-2">
															<span class="btn btn-info" id="basic-addon2" onclick="serviceSearch('report')">Search</span>
														</div>
													</div>
                      	</div>
                      </div>					
		                </div>                
		                <ul class="nav nav-tabs">
		                  <li class="nav-item">
		                    <a class="nav-link" href="customers.php">Customers</a>
		                  </li>
		                  <li class="nav-item">
		                    <a class="nav-link" href="bookings.php">Bookings</a>
		                  </li>
		                  <li class="nav-item">
		                    <a class="nav-link" href="messages.php">Interactions</a>
		                  </li>
		                  <li class="nav-item">
		                    <a class="nav-link" href="escalations.php">Escalations</a>
		                  </li>
		                  <li class="nav-item">
		                    <a class="nav-link" href="sales_services.php">Sales Transactions</a>
		                  </li>
		                  <li class="nav-item">
		                    <a class="nav-link active-pop" href="servicing_services.php">Service Transactions</a>
		                  </li>
		                  <li class="nav-item">
		                    <a class="nav-link" href="sales_surveys.php">Sales Surveys</a>
		                  </li>
		                  <li class="nav-item">
		                    <a class="nav-link" href="service_surveys.php">Service Surveys</a>
		                  </li>
		                </ul>
		                <br>
		                <div>
                    <div style="float: left;">
                        <h3>Bookings</h3>
                    </div>
                    <div style="float: right;">
                        <button type="link" class="btn btn-secondary" onclick="serviceSearch('download')"><span class="fa fa-download"></span> Download</button>
                    </div>
                </div>
                <div class="clearfix" style="padding-bottom: 30px;"></div>
									<div style="overflow: auto; overflow-y: hidden; margin: 0 auto; white-space: nowrap">
										<table border="1" class="table table-bordered table-striped table-sm isuzu-datatable" style="font-size: 0.8em;border-collapse: collapse;">
											<thead>
												<th> Dealer </th>
												<th> Branch </th>
												<th> Entry by </th>
												<th> Transaction type </th>
												<th> Customer name </th>
												<th> Service advisor </th>
												<th> Received date </th>
												<th> Released date </th>
												<th> WRKORDNO </th>
												<th> Reg No. </th>
												<th> vin no </th>
												<th> Driver name </th>
												<th> Driver phone </th>
												<th> Work scope </th>
												<th> Survey status </th>
											</thead>
											<tbody>
												<?php foreach ($services as $service): ?>
												<?php
													$vehicle = vehicle($service['vehicle_id']);
													$customer = sel_customer($vehicle['customer_id']);
													$status = serviceSurveyStatus($service['id']);
													$reasons = array('1' => "Busy", '2' => "Rejecting", '3' => "Wrong number", '4' => "No number", '5' => "Unavailable", '6' => "Pending");
													$reason = '';
													foreach ($reasons as $key => $value) {
														if ($key == $status['reason']) {
															$reason = $value;
															break;
														}
													}
													if ($reason !== '') {
														$res = $status['successful']."( ".$reason." )";
													}else{
														$res = $status['successful'];
													}
												?>
												<tr>
													<td><?php echo $service['dealer_name'] ?></td>
													<td><?php echo $service['branch_name'] ?></td>
													<td><?php echo $service['user_name'] ?></td>
													<td><?php echo $service['transaction'] ?></td>
													<td><?php echo $customer['first_name'].' '.$customer['last_name'] ?></td>
													<td><?php echo $service['service_advisor'] ?></td>
													<td><?php echo $service['receieve_date'] ?></td>
													<td><?php echo $service['release_date'] ?></td>
													<td><?php echo $service['wrkord_no'] ?></td>
													<td><?php echo $vehicle['reg_no'] ?></td>
													<td><?php echo $vehicle['vin_no'] ?></td>
													<td><?php echo $service['driver_name'] ?></td>
													<td><?php echo $service['driver_phone'] ?></td>
													<td><?php echo $service['work_scope'] ?></td>
													<td><?php echo $res ?></td>
												</tr>
											<?php endforeach; ?>
											</tbody>
										</table>
									</div>
            </div>
        </div>
    </div>
</div>
<?php include_once('../../footer.php'); ?>