<?php
include_once('../../dao/config/include.php');
include_once('../../dao/config/db.php'); 
include_once('../../dao/functions.php'); 

if (isset($_GET['fromDate'])) {
	$fromDate = $_GET['fromDate'];
}else{
	$fromDate = date('Y-m-d');
}

if (isset($_GET['toDate'])) {
	$toDate = $_GET['toDate'];
}else{
	$toDate = date('Y-m-d');
}

if (isset($_GET['type'])) {
	$type = $_GET['type'];
	$service = $_GET['type'];
}else{
	$type = '';
	$service = '';
}
$surveys = surveys('2', $fromDate, $toDate);
?>

<?php
$report = fopen('../reports/ISUZU survey - '.date('Y-m-d').'.csv', 'w');
$serviceReport = array(
									"Dealer",
									"Branch",
									"Survey by",
									"Customer name",
									"Transaction type",
									"Reg No.",
									"vin no",
									"Successful survey?",
									"Reason",
									"Escalate?",
									"Q1",
									"Q2",
									"Q3",
									"Q4",
									"Q5",
									"Q6",
									"Q7",
									"Q8",
									"Q9",
									"Q10",
									"Q11",
									"Voice of ustomer",
									"Done on",
							);
fputcsv($report, $serviceReport);

foreach ($surveys as $survey) {
	$reasons = array('1' => "Busy", '2' => "Rejecting", '3' => "Wrong number", '4' => "No number", '5' => "Unavailable", '6' => "Pending");
	$reason = '';
	foreach ($reasons as $key => $value) {
		if ($key == $survey['reason']) {
			$reason = $value;
			break;
		}
	}

	$scores = surveyScore($survey['id']);
	$vehicle = vehicle($survey['vehicle_id']);

    $row = array(
					$survey['dealer_name'],
					$survey['branch_name'],
					$survey['user_name'],
					$survey['customer_name'],
					$survey['transaction'],
					$vehicle['reg_no'],
					$vehicle['vin_no'],
					$survey['successful'],
					$reason,
					$survey['escalate'],
		    );

				for ($i=1; $i <= 11 ; $i++) { 
					$state = 0;
					foreach ($scores as $score) {
						if ($i == $score['qn_id']) {
							$state += 1;
							break;
						}else{

						}
					}

					if ($state == '1') {
						$row[] = $score['score'];
						unset($state);
					}else{
						$row[] = '';
					}
				}
													
    $row[] = $survey['customer_voice'];
    $row[] = $survey['created_on'];
    print_r($row);
		fputcsv($report, $row);
}
 
fclose($report);

header('location: '.'../reports/ISUZU survey - '.date('Y-m-d').'.csv')

?>