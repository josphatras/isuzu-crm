<?php

include_once('../../dao/config/include.php');
include_once('../../dao/config/db.php'); 
session_start();
if (isset($_SESSION["isuzu_user_id"])) {
	$userId = $_SESSION["isuzu_user_id"];
}else{
	//header('location: /');
}

$report = fopen('../reports/ISUZU crm - '.date('Y-m-d').'.csv', 'w');
$agentsReport = array(
                "S. No.",
                "Customer ID",
                "Customer Name",
                "Customer Email",
                "Company Name",
                "Telephone",
                "Town",
);
fputcsv($report, $agentsReport);   
if (isset($_GET['searchPhrase'])) {
    $searchPhrase = $_GET['searchPhrase'];
    $fromDate = $_GET['fromDate'].' '.'00:00:18';
    $toDate = $_GET['toDate'].' '.'23:59:18';

    if ($searchPhrase == '') {
        $sql = "SELECT a.id AS customer_id,CONCAT(first_name,' ',last_name) AS customer_name,email,company_name,telephone_no,business_type,physical_address,town
        FROM users a
        LEFT JOIN customer_info b ON a.id = b.customer_id
        WHERE role = 3 AND a.created_on >= :fromDate AND a.created_on <= :toDate";
    }else{
        $sql = " SELECT a.id AS customer_id,CONCAT(first_name,' ',last_name) AS customer_name,email,company_name,telephone_no,business_type,physical_address,town
        FROM users a
        LEFT JOIN customer_info b ON a.id = b.customer_id
        WHERE role = 3 AND a.created_on >= :fromDate AND a.created_on <= :toDate AND


        /*FROM sma_messages msg WHERE (m_time >= '$fromDate' AND m_time <= '$toDate') AND (msg.customer_name LIKE '%$searchPhrase%' OR msg.phone_msisdn LIKE '%$searchPhrase%' OR msg.id_number LIKE '%$searchPhrase%' OR msg.sm_id LIKE '%$searchPhrase%' OR msg.m_platform = (SELECT p_id FROM sma_platforms WHERE p_text LIKE '%$searchPhrase%') OR  msg.category = (SELECT id FROM sma_cat WHERE cat_name LIKE '%$searchPhrase%') OR  msg.sub_category = (SELECT id FROM sma_sub_cat WHERE sub_cat_name LIKE '%$searchPhrase%'))*/";
        $words = explode(" ", $_GET['searchPhrase']);
        $conds = array();
        foreach ($words as $word) {
            $conds[] = "(a.first_name Like '%".$word."%' OR a.last_name LIKE '%".$word."%' OR b.company_name LIKE '%".$word."%' OR b.telephone_no LIKE '%".$word."%')";
        }
        $sql .= implode(' OR ', $conds);
    }


$stmt=$con->prepare($sql);
$stmt->bindParam(':fromDate', $fromDate, PDO::PARAM_INT);
$stmt->bindParam(':toDate', $toDate, PDO::PARAM_INT);
$stmt->execute();
$query = $stmt->fetchAll(PDO::FETCH_ASSOC);
$sno = 0;
$rowcount=$stmt->rowCount();
}

foreach ($query as $qry){
        $sno++;
    $row = array(
        $sno,
        $qry['customer_id'],
        $qry['customer_name'],
        $qry['email'],
        $qry['company_name'],
        $qry['telephone_no'],
        $qry['town'],
    );
		fputcsv($report, $row);
}
 
fclose($report);

header('location: '.'../reports/ISUZU crm - '.date('Y-m-d').'.csv')
?>