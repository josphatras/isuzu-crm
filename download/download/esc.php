<?php

include_once('../../dao/config/include.php');
include_once('../../dao/config/db.php'); 
session_start();
if (isset($_SESSION["isuzu_user_id"])) {
	$userId = $_SESSION["isuzu_user_id"];
}else{
	//header('location: /');
}
function age_in_days($day_a,$day_b){
    if(date('Y-m-d',strtotime($day_a))<=date('Y-m-d',strtotime($day_b))){
        $datetime1 = new DateTime($day_a);
        $datetime2 = new DateTime($day_b);
        $interval = $datetime1->diff($datetime2);
        return $interval->format('%a days');
    }
    else{
        $datetime1 = new DateTime($day_a);
        $datetime2 = new DateTime($day_b);
        $interval = $datetime1->diff($datetime2);
        return $interval->format('%R%a days');
    }
}
$report = fopen('../reports/ISUZU crm - '.date('Y-m-d').'.csv', 'w');
$agentsReport = array(
                    "S/No",
                    "Ticket ID",
                    "Customer Name",
                    "Telephone No",
                    "Email",
                    "Business Type",
                    "Physical Address",
                    "Town",
                    "Company Name",
                    "Category",
                    "Sub-category",
                    "Issue",
                    "Resolution",
                    "SLA",
                    "Escalated to",
                    "Status",
                    "Escalated on",
                    "Age",
                    "Resolution Date",
 
);
fputcsv($report, $agentsReport);   
if (isset($_GET['searchPhrase'])) {
    $searchPhrase = $_GET['searchPhrase'];
    $fromDate = $_GET['fromDate'].' '.'00:00:18';
    $toDate = $_GET['toDate'].' '.'23:59:18';

    if ($searchPhrase == '') {
        $sql = "SELECT  c.email,c.first_name customer_f_name, c.last_name customer_l_name, y.telephone_no, y.business_type, y.physical_address, y.town, y.company_name, a.id,a.msg_id,a.resolution,a.sla,a.escalated_to,a.status,
        a.resolution_date,a.created_on,
        b.text, (SELECT name FROM esc_contact WHERE id = a.escalated_to) escalated_usr,
        d.status_name,e.cat_name,f.sub_cat_name
        FROM tickets a
        LEFT JOIN messages b ON a.msg_id = b.id
        LEFT JOIN users c ON b.customer_id = c.id
        LEFT JOIN ticket_status d ON a.status = d.status_id
        LEFT JOIN categories e ON a.cat_id = e.id
        LEFT JOIN customer_info y ON y.customer_id = b.customer_id
        LEFT JOIN sub_categories f ON a.sub_cat_id = f.id WHERE a.created_on >= :fromDate AND a.created_on <= :toDate";
    }else{
        $sql = " SELECT a.id AS customer_id,CONCAT(first_name,' ',last_name) AS customer_name,email,company_name,telephone_no,business_type,physical_address,town
        FROM users a
        LEFT JOIN customer_info b ON a.id = b.customer_id
        WHERE role = 3 AND a.created_on >= :fromDate AND a.created_on <= :toDate AND";
        $words = explode(" ", $_GET['searchPhrase']);
        $conds = array();
        foreach ($words as $word) {
            $conds[] = "(a.first_name Like '%".$word."%' OR a.last_name LIKE '%".$word."%' OR b.company_name LIKE '%".$word."%' OR b.telephone_no LIKE '%".$word."%')";
        }
        $sql .= implode(' OR ', $conds);
    }


$stmt=$con->prepare($sql);
$stmt->bindParam(':fromDate', $fromDate, PDO::PARAM_INT);
$stmt->bindParam(':toDate', $toDate, PDO::PARAM_INT);
$stmt->execute();
$query = $stmt->fetchAll(PDO::FETCH_ASSOC);
$sno = 0;
$rowcount=$stmt->rowCount();
}

foreach($query as $rs){
    $sno++;
    $age = 0;
    if(isset($rs['resolution_date'])){
        if($rs['resolution_date']==''){
            $age = age_in_days($rs['created_on'],date('Y-m-d'));
        }
        else{
            $age = age_in_days($rs['created_on'],$rs['resolution_date']);
        }
    }
    else{
        $age = age_in_days($rs['created_on'],date('Y-m-d'));
    }
			$row = array(
                $sno,
                $rs['id'],
                $rs['customer_f_name'].' '.$rs['customer_l_name'],
                $rs['telephone_no'],
                $rs['email'],
                $rs['business_type'],
                $rs['physical_address'],
                $rs['town'],
                $rs['company_name'],
                $rs['cat_name'],
                $rs['sub_cat_name'],            
                $rs['text'],
                $rs['resolution'],
                $rs['sla'],
                $rs['escalated_usr'],
                $rs['status_name'],
                $rs['created_on'],
                $age,
                (isset($rs['resolution_date'])?date('Y-m-d G:i:s',strtotime($rs['resolution_date'])):"")
            );
		fputcsv($report, $row);
}
 
fclose($report);

header('location: '.'../reports/ISUZU crm - '.date('Y-m-d').'.csv')
?>