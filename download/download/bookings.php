<?php
include_once('../../dao/config/include.php');
include_once('../../dao/config/db.php'); 
include_once('../../dao/functions.php'); 

session_start();
if (isset($_SESSION["isuzu_user_id"])) {
    $userId = $_SESSION["isuzu_user_id"];
}else{
    header('location: /');
}

if (isset($_GET['from_date'])) {
    $fromDate = $_GET['from_date'];
}else{
    $fromDate = date('Y-m-d');
}

if (isset($_GET['to_date'])) {
    $toDate = $_GET['to_date'];
}else{
    $toDate = date('Y-m-d');
}

$bookings = bookings($fromDate, $toDate);

$report = fopen('../reports/ISUZU bookings - '.date('Y-m-d').'.csv', 'w');
$bookingReport = array(
                "No.",
                "Branch",
                "Booked By",
                "Customer",
                "Contact Person",
                "Telephone (Cell)",
                "Email Address",
                "Reg No.",
                "Model",
                "Section",
                "Service Type",
                "Appointment Date",
                "Appointment Time",
                "Repair Description",
                "Booked on",
);
fputcsv($report, $bookingReport);

foreach ($bookings as $booking) {
    $sno++;
    $custmer = sel_customer($booking['customer_id']);
    if ($booking['vehicle_id'] !== '') {
        $vehicle = vehicle($booking['vehicle_id']);
    }else{
        $vehicle = array('reg_no' => '', 'v_model' => '');
    }
    $row = array(
        $sno,
        $booking['branch'],
        $booking['agent'],
        $booking['customer_name'],
        $booking['contact_person'],
        $custmer['telephone_no'],
        $custmer['email'],
        $vehicle['reg_no'],
        $vehicle['v_model'],
        $booking['section'],
        $booking['service_type'],
        date('Y-m-d', strtotime($booking['appointment_date'])),
        date('G:i', strtotime($booking['appointment_date'])),
        $booking['repair_description'],
        $booking['created_on'],
    );
		fputcsv($report, $row);
}
 
fclose($report);

header('location: '.'../reports/ISUZU bookings - '.date('Y-m-d').'.csv')
?>