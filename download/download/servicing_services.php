<?php
include_once('../../dao/config/include.php');
include_once('../../dao/config/db.php'); 
include_once('../../dao/functions.php'); 

if (isset($_GET['fromDate'])) {
	$fromDate = $_GET['fromDate'];
}else{
	$fromDate = date('Y-m-d');
}

if (isset($_GET['toDate'])) {
	$toDate = $_GET['toDate'];
}else{
	$toDate = date('Y-m-d');
}

$services = services('2', 'all' $fromDate, $toDate);
?>

<?php
$report = fopen('../reports/ISUZU service - '.date('Y-m-d').'.csv', 'w');
$serviceReport = array(
												"Dealer",
												"Branch",
												"Entry by",
												"Transaction type",
												"Customer name",
												"Service advisor",
												"Received date",
												"Released date",
												"WRKORDNO",
												"Reg No.",
												"vin no",
												"Driver name",
												"Driver phone",
												"Work scope",
												"Survey status",								
										);
fputcsv($report, $serviceReport);

foreach ($services as $service) {

		$vehicle = vehicle($service['vehicle_id']);
		$customer = sel_customer($vehicle['customer_id']);
		$status = serviceSurveyStatus($service['id']);
		$reasons = array('1' => "Busy", '2' => "Rejecting", '3' => "Wrong number", '4' => "No number", '5' => "Unavailable", '6' => "Pending");
		$reason = '';
		foreach ($reasons as $key => $value) {
			if ($key == $status['reason']) {
				$reason = $value;
				break;
			}
		}
		if ($reason !== '') {
			$res = $status['successful']."( ".$reason." )";
		}else{
			$res = $status['successful'];
		}
    $row = array(
							$service['dealer_name'],
							$service['branch_name'],
							$service['user_name'],
							$service['transaction'],
							$customer['first_name'].' '.$customer['last_name'],
							$service['service_advisor'],
							$service['receieve_date'],
							$service['release_date'],
							$service['wrkord_no'],
							$vehicle['reg_no'],
							$vehicle['vin_no'],
							$service['driver_name'],
							$service['driver_phone'],
							$service['work_scope'],
							$res,
    );
		fputcsv($report, $row);
}
 
fclose($report);

header('location: '.'../reports/ISUZU service - '.date('Y-m-d').'.csv')

?>