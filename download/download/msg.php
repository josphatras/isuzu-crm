<?php

include_once('../../dao/config/include.php');
include_once('../../dao/config/db.php'); 
session_start();
if (isset($_SESSION["isuzu_user_id"])) {
	$userId = $_SESSION["qa_user_id"];
}else{
	//header('location: /');
}

$report = fopen('../reports/ISUZU crm - '.date('Y-m-d').'.csv', 'w');
$agentsReport = array(
                "S/No",
                "Interaction ID",
                "Date",
                "Customer",
                "Company",
                "Contact Type",
                "Telephone No",
                "Business Type",
                "Physical Address",
                "Town",
                "Category",
                "Sub-category",
                "Comments",
                "Agent",
                "Escalated",
);
fputcsv($report, $agentsReport);   

if (isset($_GET['searchPhrase'])) {
    $searchPhrase = $_GET['searchPhrase'];
    $fromDate = $_GET['fromDate'].' '.'00:00:18';
    $toDate = $_GET['toDate'].' '.'23:59:18';

    if ($searchPhrase == '') {
        $sql = "SELECT 
        a.id,a.customer_id,a.agent_id,a.contact_type, b.telephone_no, b.business_type, b.physical_address, b.town,a.cat_id,a.sub_cat_id,
        a.text,a.created_on,a.updated_on,b.company_name,g.first_name customer_f_name, g.last_name customer_l_name,
        CONCAT(c.first_name,' ',c.last_name) AS agent_name,
        d.name AS c_type,e.cat_name,
        f.sub_cat_name,
        (SELECT COUNT(id) FROM tickets h WHERE h.msg_id = a.id) AS in_escalations
        FROM messages a
        LEFT JOIN customer_info b ON a.customer_id = b.customer_id
        LEFT JOIN users c ON a.agent_id = c.id
        LEFT JOIN contact_type d ON a.contact_type = d.id
        LEFT JOIN categories e ON a.cat_id = e.id
        LEFT JOIN sub_categories f ON a.sub_cat_id = f.id
        LEFT JOIN users g ON a.customer_id = g.id
        WHERE a.created_on >= :fromDate AND a.created_on <= :toDate";
    }else{
        $sql = " SELECT a.id AS customer_id,CONCAT(first_name,' ',last_name) AS customer_name,email,company_name,telephone_no,business_type,physical_address,town
        FROM users a
        LEFT JOIN customer_info b ON a.id = b.customer_id
        WHERE role = 3 AND a.created_on >= :fromDate AND a.created_on <= :toDate AND


        /*FROM sma_messages msg WHERE (m_time >= '$fromDate' AND m_time <= '$toDate') AND (msg.customer_name LIKE '%$searchPhrase%' OR msg.phone_msisdn LIKE '%$searchPhrase%' OR msg.id_number LIKE '%$searchPhrase%' OR msg.sm_id LIKE '%$searchPhrase%' OR msg.m_platform = (SELECT p_id FROM sma_platforms WHERE p_text LIKE '%$searchPhrase%') OR  msg.category = (SELECT id FROM sma_cat WHERE cat_name LIKE '%$searchPhrase%') OR  msg.sub_category = (SELECT id FROM sma_sub_cat WHERE sub_cat_name LIKE '%$searchPhrase%'))*/";
        $words = explode(" ", $_GET['searchPhrase']);
        $conds = array();
        foreach ($words as $word) {
            $conds[] = "(a.first_name Like '%".$word."%' OR a.last_name LIKE '%".$word."%' OR b.company_name LIKE '%".$word."%' OR b.telephone_no LIKE '%".$word."%')";
        }
        $sql .= implode(' OR ', $conds);
    }


$stmt=$con->prepare($sql);
$stmt->bindParam(':fromDate', $fromDate, PDO::PARAM_INT);
$stmt->bindParam(':toDate', $toDate, PDO::PARAM_INT);
$stmt->execute();
$query = $stmt->fetchAll(PDO::FETCH_ASSOC);
$sno = 0;
$rowcount=$stmt->rowCount();
}

foreach($query as $rs){
    $sno++;
    $escalated = 'No';
    if($rs['in_escalations']>0){
        $escalated = 'Yes';
    }
			$row = array(
				$sno,
				$rs['id'],
				date('Y-M-d G:i:s',strtotime($rs['created_on'])),
				$rs['customer_f_name'].' '.$rs['customer_l_name'],
				$rs['company_name'],
				$rs['c_type'],
                $rs['telephone_no'],
                $rs['business_type'],
                $rs['physical_address'],
                $rs['town'],
				$rs['cat_name'],
				$rs['sub_cat_name'],
				$rs['text'],
				$rs['agent_name'],
				$escalated,
			);
		fputcsv($report, $row);
}
 
fclose($report);

header('location: '.'../reports/ISUZU crm - '.date('Y-m-d').'.csv')
?>