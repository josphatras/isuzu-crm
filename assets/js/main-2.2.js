$( document ).ready(function() {
    $('#responseSection').delay(1000).fadeOut(1500);
    $('#responseSection1').delay(1000).fadeOut(1500);
    
    $('#isuzu-datatable').dataTable();
});
window.onload = function () { 
  $('.isuzu-datatable').dataTable();
  $('.scrip-guide').dataTable({
    "pageLength": 3
  });
  $('#catTable').dataTable({
    "pageLength": 4
  });
  $('#subCatTable').dataTable({
    "pageLength": 4
  }); 
   $('#manItemResponse').fadeOut(5000);
};

$( function() {
  $( ".dateSelect" ).datepicker({ dateFormat: 'yy-mm-dd' });
});

function resetPassword(userId) {
  var request = $.ajax({
  url: 'dao/admin/reset_password.php?reset_password=' + userId,
  data: '',
  dataType: 'html',
  });
  request.done(function( data ) {
    $( "#resetResponse" ).html(data);
    $( ".resetResponse" ).delay(1000).fadeOut(1600);
  }); 
}


function forwardDisplay() {
  console.log('click display');
  $('#viewEscalation').hide();
  $('#forwardDisplay').show();
}

function mailLoading(){
  $('#mailLoading').show();
}

function searchEsc() {
  var searchPhrase = document.getElementById('searchPhrase').value;
  window.location.replace("dashboard.php?content=escalations&searchEsc=" + searchPhrase);
}

function searchReport() {
  var status = $('#esc_status').val();
  var fromDate = $('#fromDate').val(); 
  var toDate = $('#toDate').val();
  window.location.replace("admin.php?status=" + status + "&from_date=" + fromDate + "&to_date=" + toDate);
}

function reportDownload() {
  var status = $('#esc_status').val();
  var fromDate = $('#fromDate').val(); 
  var toDate = $('#toDate').val();
  window.location.replace("dao/download.php?status=" + status + "&from_date=" + fromDate + "&to_date=" + toDate);
}

//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
$(function() {
  $('#searchPhrase').keypress(function (e) {
   var key = e.which;
   if(key == 13)  // the enter key code
    {
      var searchPhrase = $('#searchPhrase').val();
      var request = $.ajax({
      url: 'dao/customer_search.php?searchPhrase=' + searchPhrase,
      data: '',
      dataType: 'html',
      });
      request.done(function( data ) {
        $( "#searchResults" ).html(data);
        $( ".dash-col-main" ).html(data);
      }); 
    }
  });
});

function searchCustomer() {
    var searchPhrase = $('#searchPhrase').val();
    var request = $.ajax({
    url: 'dao/customer_search.php?searchPhrase=' + searchPhrase,
    data: '',
    dataType: 'html',
    });
    request.done(function( data ) {
      $( "#searchResults" ).html(data);
      $( ".dash-col-main" ).html(data);
    }); 
}

function newCustomer() {
    var request = $.ajax({
    url: 'forms/customer_details.php',
    data: '',
    dataType: 'html',
    });
    request.done(function( data ) {
      $( "#formSection" ).html(data);
    });     
}


$( "#contactInfo" ).submit(function( event ) {
  event.preventDefault();
    var customerId = $('#customerId').val();
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: "dao/process.php",
        data: $('#contactInfo').serialize(),
        complete: function (data) {
          
        },
        success: function (data) {
        window.location.replace("dashboard.php?content=msg&msg_item=response&int=" + data.msgId + "&customerId=" + customerId);  
        },
    }); 
})


function searchSelect(customerId) {
  //var intId = $('#messageId').val();
  window.location.replace("dashboard.php?content=msg&msg_item=customer_profile&customerId=" + customerId);
}

function createCustomer(){
  //var intId = $('#messageId').val();
  window.location.replace("dashboard.php?content=msg&msg_item=customer");
}

function getSubCat(type) {
    if (type == 'msg') {
      var catId = $('#category').val();
    }else if(type == 'esc'){
      var catId = $('#escCategory').val();
    }
  var request = $.ajax({
  url: 'dao/get_items.php?type=' + type + '&subCategory=' + catId,
  data: '',
  dataType: 'html',
  });
  request.done(function( data ) {
    if (type == 'msg') {
      $( "#showSubCategory" ).html(data);
    }else if(type == 'esc'){
      $( "#escSubCat" ).html(data);
    }
    
  });   
}

/*function getContactSubCat() {
  var catId = $('#escCategory').val();  
  var request = $.ajax({
  url: 'dao/get_items.php?type=contactSubCat&escCatId=' + catId,
  data: '',
  dataType: 'html',
  });
  request.done(function( data ) {
    $( "#contactSubCatSec" ).html(data);
  });  
}*/

function getContacts() {
  var escSubCategory = $('#escSubCategory').val();
  var request = $.ajax({
  url: 'dao/get_items.php?contactSubCat=' + escSubCategory,
  data: '',
  dataType: 'html',
  });
  request.done(function( data ) {
    $( "#contacts" ).html(data);
  });  
}

function selectSubCat() {
  var catId = $('#category').val();
  var request = $.ajax({
  url: 'dao/get_items.php?catId=' + catId,
  data: '',
  dataType: 'html',
  });
  request.done(function( data ) {
      $( "#showSubCategory" ).html(data);
  });   
}

function dir_esc_sub_cat() {
  var catId = $('#category').val();
  var request = $.ajax({
  url: 'dao/get_items.php?dir_esc_sub_cats=' + catId,
  data: '',
  dataType: 'html',
  });
  request.done(function( data ) {
      $( "#esc_sub_cat" ).html(data);
  });   
}

function dir_int_sub_cat() {
  var catId = $('#intCategory').val();
  var request = $.ajax({
  url: 'dao/get_items.php?dir_int_sub_cats=' + catId,
  data: '',
  dataType: 'html',
  });
  request.done(function( data ) {
      $( "#int_sub_cat" ).html(data);
  });   
}

function showDetailForm() {
  var catId = $('#category').val();
  var request = $.ajax({
  url: 'load_forms.php?catId=' + catId,
  data: '',
  dataType: 'html',
  });
  request.done(function( data ) {
    $( "#showForm" ).html(data);
  });   
}

function customerDetailSec() {
  
}

function subCatChange() {
  var subCat = $("#subCategory :selected").text();
  if (subCat == 'Complaints') {
      $("#disComplain").show();
  }else{
    $("#disComplain").hide();
  }
}

$( "#customerDetails" ).submit(function( event ) {
  event.preventDefault();
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: "dao/process.php",
        data: $('#customerDetails').serialize(),
        complete: function (data) {
          
        },
        success: function (data) {
          if (data.status == 'success') {
            window.location.replace("dashboard.php?content=msg&msg_item=vehicle&customerId=" + data.customerId + "&next=yes");  
          }else{
            //console.log(data.status);
          }  
        },
    });   
});


$( "#vehicleDetails" ).submit(function( event ) {
  event.preventDefault();
  var customerId = $('#customerId').val();
  var next = $('#next').val();
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: "dao/process.php",
        data: $('#vehicleDetails').serialize(),
        complete: function (data) {
          
        },
        success: function (data) {
          if (data.status == 'success') {
            if (next == 'no') {
                window.location.replace("dashboard.php?content=customer&customer=" + customerId);
            }else if (next == 'yes') {
              if (data.dealer == '') {
                window.location.replace("dashboard.php?content=msg&msg_item=contact&customerId=" + customerId);
              }else{
                window.location.replace("dashboard.php?content=msg&msg_item=customer_profile&customerId=" + customerId);
              }
            }
          }else{
            //console.log(data.status);
          }  
        },
    }); 
});

function responseForm() {
    var intId = $('#messageId').val();
    var customerId = $('#customerId').val();
    window.location.replace("dashboard.php?content=msg&msg_item=message&int=" + intId +"&customerId=" + customerId);
}

function escalateMessage(escStatus) {
    var intId = $('#messageId').val();
    var customerId = $('#customerId').val();
    var escStatus = escStatus;
    var formElement = $('form')[0];
    var form_data = new FormData(formElement);
    form_data.append('escStatus', escStatus);
    form_data.append('customerId', customerId);
    form_data.append('msgId', intId);
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: "dao/process.php",
        data: form_data,
        processData: false,
        contentType: false,
        complete: function (data) {

        },
        success: function (data) {
          if (data.status == 'success') {
            console.log(customerId);
            window.location.replace("dashboard.php?content=msg&msg_item=closing&int=" + intId +"&customerId=" + customerId);  
          }else{
            $('#msgResponse').show();
            $('#msgResponse').fadeOut(5000);
          }  
        },
    }); 
}

function escOption(){
  var escalate = $('#escSelect').val();
  if (escalate == 'yes') {
    $('#escYes').show();
     $('#escNo').hide();
  }else if(escalate == 'no'){
    $('#escYes').hide();
     $('#escNo').show();
  }
}


function getVehModels(){
   var make = $("#make").val();
   if (make == '0') {
    $('#makeModelSection').show();
    $('#modelSection').hide();
   }else{
    $('#modelSection').show();
    $('#makeModelSection').hide();
     var link = 'dao/get_items.php?veh_make=' + make;
     var request = $.ajax({
      url: link,
      data: '',
      dataType: 'html',
     });
     request.done(function( data ){
      $('#vehModelDis').html(data);
     })
   }
}

function bodyTypes() {
  var bodyType = $('#bodyType').val();
  if (bodyType == '0') {
    $('#otherBodyTypeSec').show();
  }else{
    $('#otherBodyTypeSec').hide();
  }
}


function anthingElse() {
  var elseInfo = $('#elseInfo').val();
  if (elseInfo == 'Yes') {
    $('#otherSection').show();
    $('#thankYou').hide();
  }else if(elseInfo == 'No'){
    $('#otherSection').hide();
    $('#thankYou').show();
  }
}

function endContact(msg){
  if (msg == 'msgNo') {
    $('.dash-col-main').html('<div class="alert alert-success create-success">Successfully created!</div>');
    $('.create-success').fadeOut(5000);
  }else if(msg == 'msgYes'){
    $.ajax({
        type: "POST",
        dataType: '',
        url: "dao/process.php",
        data: $('#othermsgForm').serialize(),
        complete: function (data) {
        },
        success: function (data) {
        $('.dash-col-main').html('<div class="alert alert-success create-success">Successfully created!</div>');
        $('.create-success').fadeOut(5000);
        },
    });

  }
}

function submitEscEdit() {
    $.ajax({
        type: "POST",
        dataType: '',
        url: "dao/process.php",
        data: $('#editesc').serialize(),
        complete: function (data) {
        },
        success: function (data) {
        $('#showEscEditForm').html('<div class="alert alert-success create-success"> Successfully edited</div>');
          setTimeout(function() {
            $('#getescEdit').modal('hide');
            window.location.reload();
          }, 3000);
        },
    });
}

function saveComment() {
   $.ajax({
        type: "POST",
        dataType: '',
        url: "dao/process.php",
        data: $('#commentForm').serialize(),
        complete: function (data) {
        },
        success: function (data) {
        $('#showCommentForm').html('<div class="text-success"> Comment successfully saved!</div>');
          setTimeout(function() {
            $('#getescEdit').modal('hide');
            window.location.reload();
          }, 3000);
        },
    });
}

//::::::::::::::::::::::::::::::::::: SETTINGS  ::::::::::::::::::::::::::::::::::::
function showAddCat() {
  $('#catButton').hide();
  $('#addCat').show();
}

function showAddSubCat() {
  $('#subCatButton').hide();
  $('#addSubCat').show();
} 

function addCat() {
  var categoryType = $('#categoryType').val();
  var category = $('#category').val();
  var reqlink = 'dao/process.php?categoryType=' + categoryType + '&category=' + category;
  var request = $.ajax({
    url: reqlink,
    data: '',
    dataType: 'html',
  });
  request.done(function( data ) {
    $('#addCat').html('<div class="alert alert-success">Category successfully created!</div>');
    $('#addCat').fadeOut(3000);
          setTimeout(function() {
            $('#getescEdit').modal('hide');
              window.location.reload();
          }, 3000);  
  });   
}

function addCat() {
  var categoryType = $('#categoryType').val();
  var category = $('#category').val();
  var reqlink = 'dao/process.php?categoryType=' + categoryType + '&category=' + category;
  var request = $.ajax({
    url: reqlink,
    data: '',
    dataType: 'html',
  });
  request.done(function( data ) {
    $('#addCat').html('<div class="alert alert-success">Category successfully created!</div>');
    $('#addCat').fadeOut(3000);
          setTimeout(function() {
              window.location.reload();
          }, 3000);  
  });   
}

function getCategories() {
    var type = $('#typeSelect').val();
    var request = $.ajax({
    url: 'dao/get_items.php?catType=' + type,
    data: '',
    dataType: 'html',
    });
    request.done(function( data ) {
      $('#subCat_catSec').html(data);
    });    
}

function addSubCat() {
  var catId = $('#category').val();
  var subCat =  $('#subCategory').val();
  var request = $.ajax({
  url: 'dao/process.php?catId=' + catId + '&sub_category=' + subCat,
  data: '',
  dataType: 'html',
  });
  request.done(function( data ) {
    $('#addSubCat').html('<div class="alert alert-success">Category successfully created!</div>');
    $('#addSubCat').fadeOut(3000);
          setTimeout(function() {
              window.location.reload();
          }, 3000);
  });
}

function showAddScript() {
  $('#addScriptSec').show();
  $('#viewScriptSec').hide();
}

function saveScriptContent() {
 $.ajax({
      type: "POST",
      dataType: '',
      url: "dao/process.php",
      data: $('#scriptContent').serialize(),
      complete: function (data) {
      },
      success: function (data) {
      $('#addScriptSec').html('<div class="alert alert-success">Successfully created!</div>');
         $('#addScriptSec').fadeOut(5000);
        setTimeout(function() {
          window.location.reload();
        }, 3000);
      },
  }); 
}


function catVisibility(visible,catId) {
    var request = $.ajax({
    url: 'dao/process.php?visible=' + visible +'&catId=' + catId,
    data: '',
    dataType: '',
    });
    request.done(function( data ) {
      $( "#cat"+catId ).replaceWith(data);
    }); 
}

function subCatVisibility(visible,subCatId) {
    var request = $.ajax({
    url: 'dao/process.php?visible=' + visible +'&subCatId=' + subCatId,
    data: '',
    dataType: '',
    });
    request.done(function( data ) {
      $( "#subCat"+subCatId ).replaceWith(data);
    }); 
}

function showAddContact() {
  $('#addContactSec').show();
  $('#contactView').hide();
}



$( "#esc_contact_form" ).submit(function( event ) {
  event.preventDefault();
    $.ajax({
        type: "POST",
        dataType: '',
        url: "dao/process.php",
        data: $('#esc_contact_form').serialize(),
        complete: function (data) {
        },
        success: function (data) {
        $('#addContactSec').html('<div class="alert alert-success create-success">Contact successfully created!</div>');
        $('#contactView').show();
        $('#addContactSec').fadeOut(5000);
          setTimeout(function() {
            window.location.reload();
          }, 5050);
        },
    });
});
//:::::::::::::::::::::::::::: Direct contact capture ::::::::::::::::::::::::::::
function showDirInt(){
  $('#add-int').show();
}

function showDirEsc(){
  $('#add_esc').show();
}

$( "#dir_int" ).submit(function( event ) {
  event.preventDefault();
    $.ajax({
        type: "POST",
        dataType: '',
        url: "dao/dir_process.php",
        data: $('#dir_int').serialize(),
        complete: function (data) {
        },
        success: function (data) {
        $('#add-int').html('<div class="alert alert-success create-success">Successfully created!</div>');
        $('#add-int').fadeOut(5000);
          setTimeout(function() {
            window.location.reload();
          }, 5050);
        },
    });
});

$( "#dir_esc" ).submit(function( event ) {
  event.preventDefault();
    $.ajax({
        type: "POST",
        dataType: '',
        url: "dao/dir_process.php",
        data: $('#dir_esc').serialize(),
        complete: function (data) {
        },
        success: function (data) {
        $('#add_esc').html('<div class="alert alert-success create-success">Successfully created!</div>');
        $('#add_esc').fadeOut(5000);
          setTimeout(function() {
            window.location.reload();
          }, 5050);
        },
    });
});


function customerReportDownload(type) {
  var searchPhrase = $('#searchPhrase').val();
  var fromDate =  $('#fromDate').val();
  var toDate =  $('#toDate').val();   
  if (type == 'view') {  
  window.location.replace("customers.php?searchPhrase=" + searchPhrase + "&fromDate=" + fromDate + "&toDate=" + toDate);
  }else if(type == 'download'){
  window.location.replace("../download/customer.php?searchPhrase=" + searchPhrase + "&fromDate=" + fromDate + "&toDate=" + toDate);
  }
  
}

function interactionReportDownload(type) {
  var searchPhrase = $('#searchPhrase').val();
  var fromDate =  $('#fromDate').val();
  var toDate =  $('#toDate').val();    
  if (type == 'view') {
  window.location.replace("messages.php?searchPhrase=" + searchPhrase + "&fromDate=" + fromDate + "&toDate=" + toDate);
  }else if(type == 'download'){
  window.location.replace("../download/msg.php?searchPhrase=" + searchPhrase + "&fromDate=" + fromDate + "&toDate=" + toDate);
  }
  
}

function escalationsReportDownload(type) {
  var searchPhrase = $('#searchPhrase').val();
  var fromDate =  $('#fromDate').val();
  var toDate =  $('#toDate').val();    
  if (type == 'view') { 
  window.location.replace("escalations.php?searchPhrase=" + searchPhrase + "&fromDate=" + fromDate + "&toDate=" + toDate);
  }else if(type == 'download'){
  window.location.replace("../download/esc.php?searchPhrase=" + searchPhrase + "&fromDate=" + fromDate + "&toDate=" + toDate);
  }
  
}


function dummyInt() {
    var customerName = $('#customerName').val();
    var companyName = $('#companyName').val();
    var email = $('#email').val();
    var telNo = $('#telNo').val();
    var busType = $('#busType').val();
    var phyAddress = $('#phyAddress').val();
    var town = $('#town').val();

    var interaction = $('form')[1];
    var form_data = new FormData(interaction);
    form_data.append('customerName', customerName);
    form_data.append('companyName', companyName);
    form_data.append('email', email);
    form_data.append('telNo', telNo);
    form_data.append('busType', busType);
    form_data.append('phyAddress', phyAddress);
    form_data.append('town', town);
    console.log(form_data);
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: "dao/dummy_process.php",
        data: form_data,
        processData: false,
        contentType: false,
        complete: function (data) {

        },
        success: function (data) {
          if (data.status == 'success') {
            $('form')[0].reset();
            $('form')[1].reset();
            $('#dummyResponse').html('<div id="dummyDummy"><div class="alert alert-success create-success">Interaction created Successfully!</div></div>');
            $('#dummyDummy').fadeOut(5000);
          }else if(data.status == 'fail'){
            $('#dummyResponse').html('<div id="dummyDummy"><div class="alert alert-danger create-danger">One or more empty fields</div></div>');
            $('#dummyDummy').fadeOut(5000);
          }  
        },
    }); 
}

function dummyEsc() {
    var customerName = $('#customerName').val();
    var companyName = $('#companyName').val();
    var email = $('#email').val();
    var telNo = $('#telNo').val();
    var busType = $('#busType').val();
    var phyAddress = $('#phyAddress').val();
    var town = $('#town').val();

    var interaction = $('form')[2];
    var form_data = new FormData(interaction);
    form_data.append('customerName', customerName);
    form_data.append('companyName', companyName);
    form_data.append('email', email);
    form_data.append('telNo', telNo);
    form_data.append('busType', busType);
    form_data.append('phyAddress', phyAddress);
    form_data.append('town', town);
    console.log(form_data);
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: "dao/dummy_process.php",
        data: form_data,
        processData: false,
        contentType: false,
        complete: function (data) {

        },
        success: function (data) {
          if (data.status == 'success') {
            $('form')[0].reset();
            $('form')[2].reset();
            $('#dummyResponse').html('<div id="dummyDummy"><div class="alert alert-success create-success">Escalated Successfully!</div></div>');
            $('#dummyDummy').fadeOut(5000);          
          }else{
            $('#dummyResponse').html('<div id="dummyDummy"><div class="alert alert-danger create-danger">One or more empty fields</div></div>');
            $('#dummyDummy').fadeOut(5000);
          }  
        },
    }); 
}

function showSendSMS(){
  if ( $(smsSection).css('display') == 'none' ){
    $('#smsSection').show();
  }else{
    $('#smsSection').hide();
  }
}

function sendSMS() {
    $('#submit_btn').prop('disabled', true);
    $('#loader').addClass('small-loader');
    var smsNumber = $('#smsNumber').val();
    var smsText = $('#smsText').val();
    var form_data = new FormData();
    form_data.append('smsNumber', smsNumber);
    form_data.append('smsText', smsText);
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: "dao/sms_send.php",
        data: form_data,
        processData: false,
        contentType: false,
        complete: function (data) {

        },
        success: function (data) {
          if (data.status == 'success') {
            $('#smsText').val('');
            $('#loader').removeClass('small-loader');
            $('#smsResponse').html('<div id="smsRes"><div class="alert alert-success create-success">SMS Sent!</div></div>');
            $('#smsRes').fadeOut(5000);
          }else{
            $('#submit_btn').prop('disabled', false);
            $('#loader').removeClass('small-loader');
            $('#smsResponse').html('<div id="smsRes"><div class="alert alert-warning create-danger">Problem occurred while sending SMS</div></div>');
            $('#smsRes').fadeOut(5000);
          }  
        },
        error: function () {
            $('#submit_btn').prop('disabled', false);
            $('#loader').removeClass('small-loader');
            $('#smsResponse').html('<div id="smsRes"><div class="alert alert-danger create-danger">Problem occurred while sending SMS. Check your network connection.</div></div>');
            $('#smsRes').fadeOut(5000);
        }

    }); 
}

function showResolution(){
  var status = $('#escStatus').val();
  if (status == '3') {
    $('#resolutionSec').show();
  }else{
    $('#resolutionSec').hide();
  }
}

function showCustomerEdit() {
  if ( $(smsSection).css('display') == 'none' ){
    $('#editCustomerSec').show();
    $('#customerInfoSec').hide();
  }else{
    $('#editCustomerSec').hide();
    $('#customerInfoSec').show();
  }
}

$( "#customer_edit" ).submit(function( event ) {
  event.preventDefault();
    $.ajax({
        type: "POST",
        dataType: '',
        url: "dao/save_edit.php",
        data: $('#customer_edit').serialize(),
        complete: function (data) {
        },
        success: function (data) {
        $('#editCustomerSec').html('<div class="alert alert-success create-success">Successfully Edited!</div>');
        $('#editCustomerSec').fadeOut(5000);
          setTimeout(function() {
            window.location.reload();
          }, 5050);
        },
    });
});

function delUser(userId, userName){
  $('#delUserSec').modal('show');
  $('#userName').html(userName);
  $('#delBtn').html('<button type="button" class="btn btn-warning" onclick="deleteUser('+userId+')">Yes</button>');
}

function deleteUser(userId){
    var request = $.ajax({
    url: 'dao/process.php?user_delete=' + userId,
    data: '',
    dataType: 'html',
    });
    request.done(function( data ) {
        $('.modal-body').html('<div id="successMsg" class="alert alert-success create-success">Successfully Deleted!</div>');
        $('#successMsg').fadeOut(5000);
          setTimeout(function() {
            window.location.reload();
          }, 5050);
    });
}



function delScriptModel(scriptId, scriptTitle){
  $('#delScript').modal('show');
  $('#delScriptSec').modal('show');
  $('#scriptTitle').html(scriptTitle);
  $('#delBtn').html('<button type="button" class="btn btn-warning" onclick="deleteScript('+scriptId+')">Yes</button>');

}

function deleteScript(scriptId){
    var request = $.ajax({
    url: 'dao/process.php?delete_script=' + scriptId,
    data: '',
    dataType: 'html',
    });
    request.done(function( data ) {
        $('.modal-body').html('<div id="successMsg" class="alert alert-success create-success">Successfully Deleted!</div>');
        $('#successMsg').fadeOut(5000);
          setTimeout(function() {
            window.location.reload();
          }, 5050);
    });
}


function delContactModel(contactId, conName){
  $('#delContact').modal('show');
  $('#delContactSec').modal('show');
  $('#contactName').html(contactId);
  $('#delBtn').html('<button type="button" class="btn btn-warning" onclick="deleteContact('+contactId+')">Yes</button>');

}

function deleteContact(contactId){
    var request = $.ajax({
    url: 'dao/process.php?delete_contact=' + contactId,
    data: '',
    dataType: 'html',
    });
    request.done(function( data ) {
        $('.modal-body').html('<div id="successMsg" class="alert alert-success create-success">Successfully Deleted!</div>');
        $('#successMsg').fadeOut(5000);
          setTimeout(function() {
            window.location.reload();
          }, 5050);
    });
}

$('#new_booking').submit(function(event){
event.preventDefault();
  $('#btn_new_book').prop('disabled', true);
  $('#loading').addClass('small-loader');
  var vehicle = $('#vehicle').val();
  console.log(vehicle)
  if (vehicle == '') {
    $('#loading').removeClass('small-loader');
    $('#btn_new_book').prop('disabled', false);
    $('#response').html('<div class="alert alert-danger create-danger response">Vehicle not selected</div>');
    $('.response').fadeOut(4000);
  }else{
    var formElement = document.getElementById('new_booking');
    var form_data = new FormData(formElement);
    form_data.append('vehicle', vehicle);
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: "dao/process.php",
        data: form_data,
        processData: false,
        contentType: false,
        complete: function (data) {

        },
        success: function (data) {
            if (data.status == 'success') {
              setTimeout(function() {
                $('#loading').removeClass('small-loader');
                $('#response').html('<div class="alert alert-success create-success response">Successfully booked!</div>');
                $('.response').fadeOut(3000);
                setTimeout(function() {
                  window.location.replace('dashboard.php?content=bookings&item=view_bookings')
                }, 3000);              
            }, 2000); 
            }else{
              $('#loading').removeClass('small-loader');
              $('#btn_new_book').prop('disabled', false);
              $('#response').html('<div class="alert alert-danger create-danger response">Problem occurred while saving booking</div>');
              $('.response').fadeOut(4000);
            }
          },
        error: function(data){
          $('#btn_new_book').prop('disabled', false);
          $('#loading').removeClass('small-loader');
          $('#response').html('<div class="alert alert-danger create-danger">Problem occurred while saving booking</div>');
          $('.create-danger').fadeOut(4000);
        }      
    });
  }
});

function appointment() {
  var branch = $('#branch').val();
  var section = $('#section').val();
  var date = $('#app_date').val();
  if (section !== '' && date !== '') {
    $('#app_time').prop('disabled', true);
    $('#check_response').html('<p style="color:green">Checking...</p>');
    var request = $.ajax({
    url: 'dao/get_items.php?av_d=' + date + '&av_b=' + branch + '&av_s=' + section,
    data: '',
    dataType: 'html',
    });
    request.done(function( data ) {
      setTimeout(function() {
        $('#app_time').prop('disabled', false);
        $("#app_time").html(data);
        $('#check_response').html('');
      }, 1000);
    });    
  }else{
    $('#app_time').prop('disabled', true);
  }
}

function bookingsSearch(type){
  var fromDate = $('#fromDate').val(); 
  var toDate = $('#toDate').val();

  if (type == 'report') {
    window.location.replace("?from_date=" + fromDate + "&to_date=" + toDate);
  }else if (type == 'view') {
    window.location.replace("dashboard.php?content=bookings&item=view_bookings&from_date=" + fromDate + "&to_date=" + toDate);
  }else if (type == 'download') {
    window.location.replace("../download/bookings.php?fromDate=" + fromDate + "&toDate=" + toDate);
  }
}

function bookingCl(){
  var branch = $('#branch').val();
  var date = $('#date').val();
  window.location.replace("?content=bookings&item=view_calendar&branch=" + branch + "&date=" + date);
}

$( "#advisor_create_customer" ).submit(function( event ) {
  event.preventDefault();
  $('#submit_btn').prop('disabled', true);
    $.ajax({
        type: "POST",
        dataType: 'JSON',
        url: "dao/dummy_process.php",
        data: $('#advisor_create_customer').serialize(),
        complete: function (data) {

        },
        success: function (data) {
          if (data.status == 'success') {
            $('#response').html('<div class="alert alert-success c_success">Customer successfully created!</div>');
            $('.c_success').fadeOut(5000);
            setTimeout(function() {
              window.location.replace('dashboard.php?content=customer&customer=' + data.user);
            }, 5050);
          }else if (data.status == 'fail') {
            $('#submit_btn').prop('disabled', false);
            $('#response').html('<div class="alert alert-warning c_warning">Something went wrong</div>');
            $('.c_warning').fadeOut(5000);
          }
        },
        error: function () {
          $('#submit_btn').prop('disabled', false);
          $('#response').html('<div class="alert alert-danger c_fail">Something went wrong. Check your internet connection.</div>');
          $('.c_fail').fadeOut(5000);
        },
    });
});


//::::::::::::::::::::::::::::::::: Jan 2019 Service & Survey  ::::::::::::::::::::::::::::::::::::::::

$( "#survey" ).submit(function( event ) {
  event.preventDefault();
  $('#submit_btn').prop('disabled', true);
  $('.loading').addClass('fa fa-spinner fa-spin');
    $.ajax({
        type: "POST",
        dataType: 'JSON',
        url: "dao/survey.php",
        data: $('#survey').serialize(),
        complete: function (data) {

        },
        success: function (data) {
          if (data.status == 'success') {
            setTimeout(function() {
              $('.loading').removeClass('fa fa-spinner fa-spin');
              $('#response').html('<div class="alert alert-success c_success">Customer successfully created!</div>');
              $('.c_success').fadeOut(5000);              
              window.location.replace('?content=surveys&type=view_services');
            }, 1000);
          }else if (data.status == 'fail') {
            setTimeout(function() {
              $('#submit_btn').prop('disabled', false);
              $('.loading').removeClass('fa fa-spinner fa-spin');
              $('#response').html('<div class="alert alert-danger c_warning">' + data.msg + '</div>');
              $('.c_warning').fadeOut(5000);
            }, 1000);            
          }
        },
        error: function () {
          setTimeout(function() {
            $('#submit_btn').prop('disabled', false);
            $('.loading').removeClass('fa fa-spinner fa-spin');
            $('#response').html('<div class="alert alert-danger c_fail">Something went wrong. Check your internet connection.</div>');
            $('.c_fail').fadeOut(5000);
          }, 1000);
        },
    });
});



$( "#service" ).submit(function( event ) {
  event.preventDefault();
  $('#submit_btn').prop('disabled', true);
  $('.loading').addClass('fa fa-spinner fa-spin');
  var vehicle = $('#vehicle').val();
  var formInfo = document.getElementById('service');
  var form_data = new FormData(formInfo);
  form_data.append('vehicle', vehicle);
    $.ajax({
        type: "POST",
        dataType: 'JSON',
        url: "dao/service.php",
        data: form_data,
        processData: false,
        contentType: false,
        complete: function (data) {

        },
        success: function (data) {
          if (data.status == 'success') {
            setTimeout(function() {
              $('.loading').removeClass('fa fa-spinner fa-spin');
              $('#response').html('<div class="alert alert-success c_success">Customer successfully created!</div>');
              $('.c_success').fadeOut(5000);              
            }, 1000);
            setTimeout(function() {
              window.location.replace('?content=services&type=history&service=' + $('#transaction_type').val());
            }, 1000);
          }else if (data.status == 'fail') {
            setTimeout(function() {
              $('#submit_btn').prop('disabled', false);
              $('.loading').removeClass('fa fa-spinner fa-spin');
              $('#response').html('<div class="alert alert-danger c_warning">' + data.msg + '</div>');
              $('.c_warning').fadeOut(5000);
            }, 1000);            
          }
        },
        error: function () {
          setTimeout(function() {
            $('#submit_btn').prop('disabled', false);
            $('.loading').removeClass('fa fa-spinner fa-spin');
            $('#response').html('<div class="alert alert-danger c_fail">Something went wrong. Check your internet connection.</div>');
            $('.c_fail').fadeOut(5000);
          }, 1000);
        },
    });
});



function dealerBranches(){
  var dealer = $('#dealer').val();
  var request = $.ajax({
  url: 'dao/get_items.php?dealer_braches=' + dealer,
  data: '',
  dataType: 'html',
  });
  request.done(function( data ) {
      $( "#branch" ).html(data);
  });
}

function serviceSection() {
  $('#serviceSec').hide();
  $('#salesSec').hide();
  $('#service_loading').addClass('fa fa-spinner fa-spin');
  var type = $('#transaction_type').val();
  setTimeout(function() {
    $('#service_loading').removeClass('fa fa-spinner fa-spin');
    if (type == '1') {
      $('#serviceSec').hide();
      $('#salesSec').show();
    }else if (type == '2') {
      $('#serviceSec').show();
      $('#salesSec').hide();
    }
  }, 800);
}

function dealerRole(){
  var checkDealer = $('input[name=dealer]:checked').val();
  if (checkDealer !== undefined) {
    $('#dealerSec').show();
  }else{
    $('#dealerSec').hide();
  }
}

function successSurvey(){
  $('#voice_sec').show();
  var success = $('input[name=success]:checked').val();
  if (success == 'unsuccess') {
    $('#dispositionSec').show();
    $('#survey_esc_sec').hide();
  }else{
    $('#dispositionSec').hide();
  }
  
}

function escalateSection(){
  var escalate = $('input[name=escalate_survey]:checked').val();
  if (escalate == 'yes') {
    $('#survey_esc_sec').show();
    $('#saveWithEscalate').show();
    $('#saveWithoutEscalate').hide();
  }else{
    $('#survey_esc_sec').hide();
    $('#saveWithEscalate').hide();
    $('#saveWithoutEscalate').show();
  }
}

function delServicePop(ticket_id){
  $('#transaction_id').val(ticket_id);
  $('#delete_record').modal('show');
}

function deleteService(){
  $('.loading').addClass('fa fa-spinner fa-spin');
   var service_id = $('#transaction_id').val();
  var form_data = new FormData();
  form_data.append('form_type', 'delete_service');
  form_data.append('service_id', service_id);
    $.ajax({
        type: "POST",
        dataType: 'JSON',
        url: "dao/service.php",
        data: form_data,
        processData: false,
        contentType: false,
        complete: function (data) {

        },
        success: function (data) {
            setTimeout(function() {
              $('.loading').removeClass('fa fa-spinner fa-spin');
              $('#response').html('<div class="alert alert-success c_success">Customer successfully created!</div>');
              $('.c_success').fadeOut(5000);              
            }, 1000);
            setTimeout(function() {
              window.location.reload();
            }, 1000);
        },
        error: function () {
          setTimeout(function() {
            $('#submit_btn').prop('disabled', false);
            $('.loading').removeClass('fa fa-spinner fa-spin');
            $('#response').html('<div class="alert alert-danger c_fail">Something went wrong. Check your internet connection.</div>');
            $('.c_fail').fadeOut(5000);
          }, 1000);
        },
    });
}


function delSurveyPop(survey_id){
  $('#survey_id').val(survey_id);
  $('#delete_record').modal('show');
}

function deleteSurvey(){
  $('.loading').addClass('fa fa-spinner fa-spin');
   var survey_id = $('#survey_id').val();
  var form_data = new FormData();
  form_data.append('form_type', 'delete_survey');
  form_data.append('survey_id', survey_id);
    $.ajax({
        type: "POST",
        dataType: 'JSON',
        url: "dao/survey.php",
        data: form_data,
        processData: false,
        contentType: false,
        complete: function (data) {

        },
        success: function (data) {
            setTimeout(function() {
              $('.loading').removeClass('fa fa-spinner fa-spin');
              $('#response').html('<div class="alert alert-success c_success">Customer successfully created!</div>');
              $('.c_success').fadeOut(5000);              
            }, 1000);
            setTimeout(function() {
              window.location.reload();
            }, 1000);
        },
        error: function () {
          setTimeout(function() {
            $('#submit_btn').prop('disabled', false);
            $('.loading').removeClass('fa fa-spinner fa-spin');
            $('#response').html('<div class="alert alert-danger c_fail">Something went wrong. Check your internet connection.</div>');
            $('.c_fail').fadeOut(5000);
          }, 1000);
        },
    });
}

function delCustomerPop(customerName, customerId){
  $('#customer_id').val(customerId);
  $('#customer_name').html(customerName);
  $('#delete_record').modal('show');
}

function deleteCustomer(){
  $('.loading').addClass('fa fa-spinner fa-spin');
  var customerId = $('#customer_id').val();
  var form_data = new FormData();
  form_data.append('form_type', 'delete_customer');
  form_data.append('customer_id', customerId);
  $.ajax({
      type: "POST",
      dataType: 'JSON',
      url: "dao/account.php",
      data: form_data,
      processData: false,
      contentType: false,
      complete: function (data) {

      },
      success: function (data) {
          setTimeout(function() {
            $('.loading').removeClass('fa fa-spinner fa-spin');
            $('#response').html('<div class="alert alert-success c_success">Customer successfully created!</div>');
            $('.c_success').fadeOut(5000);              
          }, 1000);
          setTimeout(function() {
            window.location.reload();
          }, 1000);
      },
      error: function () {
        setTimeout(function() {
          $('#submit_btn').prop('disabled', false);
          $('.loading').removeClass('fa fa-spinner fa-spin');
          $('#response').html('<div class="alert alert-danger c_fail">Something went wrong. Check your internet connection.</div>');
          $('.c_fail').fadeOut(5000);
        }, 1000);
      },
  });
}

function otherModels() {
  var otherModel = $('input[name=other_model]:checked').val();
  if (otherModel == 'Yes') {
    $('#otherModelsSec').show();
  }else if(otherModel == 'No'){
    $('#otherModelsSec').hide();
  }else{
    $('#otherModelsSec').hide();
  }
}

function vinValidation(){
  var vinNo = $('#vinNo').val();
  if (vinNo.length > 17) {
    $('#vinVerResponse').html('<p style="color:#FF0202">Please enter less than 17 charcaters VIN NO.</p>');
  }else{
    $('#vinVerResponse').html('<p style="color:#87C540">' + (17 - vinNo.length) +' out of 17 characters remaining</p>');
  }
}

function serviceSearch(type){
  var fromDate = $('#fromDate').val(); 
  var toDate = $('#toDate').val();
  var service = $('#service_id').val();
  console.log(service);
  if (type == 'report') {
    window.location.replace("?service=" + service + "&from_date=" + fromDate + "&to_date=" + toDate);
  }else if (type == 'view') {
    window.location.replace("dashboard.php?content=services&type=history&service=" + service + "&from_date=" + fromDate + "&to_date=" + toDate);
  }else if (type == 'download') {
    if (service == '1') {
      window.location.replace("../download/sales_services.php?service=" + service + "&fromDate=" + fromDate + "&toDate=" + toDate);
    }else if (service == '2') {
      window.location.replace("../download/servicing_services.php?service=" + service + "&fromDate=" + fromDate + "&toDate=" + toDate);
    }
  }
}

function surveySearch(type){
  var fromDate = $('#fromDate').val(); 
  var toDate = $('#toDate').val();
  var service = $('#service_id').val();
  if (type == 'report') {
    window.location.replace("?type=" + service + "&from_date=" + fromDate + "&to_date=" + toDate);
  }else if (type == 'view') {
    window.location.replace("dashboard.php?content=surveys&type=" + service + "&from_date=" + fromDate + "&to_date=" + toDate);
  }else if (type == 'download') {
    if (service == '1') {
      window.location.replace("../download/sales_surveys.php?type=1&fromDate=" + fromDate + "&toDate=" + toDate);
    }else if (service == '2') {
      window.location.replace("../download/service_surveys.php?type=2&fromDate=" + fromDate + "&toDate=" + toDate);
    }
  }
}

